<?php 
  session_start();
  session_write_close();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
      <?php 
        $oldal = 'hasznalati-utmutato';
        include '../config.php';
        include $gyoker.'/module/mod_head.php';

      ?>

      <title>Kereső | <?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">      
  </head>
  <body>
    <?php include $gyoker.'/module/mod_body_start.php'; ?>
    <div class="page">
      <?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(../images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                <h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5 fejresz-title">Magyar nyelvű használati útmutató letöltés</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">
            <ul class="breadcrumbs-custom-path">
              <li><a href="<?=$domain?>">Főoldal</a></li>
              <li class="active">Magyar nyelvű használati útmutató letöltés</li>
            </ul>
          </div>
        </div>
      </section>

      <section class="section section-lg bg-default">
        <div class="container">
          <div class="row row-30 justify-content-center">
            <div class="col-md-12">

                <p class="text-center"><b>Használati útmutatót keresel?</b></p>
                <p class="text-center">Keress cikkszámra, típusra vagy megnevezésre, és töltsd le a használati útmutatót.</p>

                <form class="search_form pt-4" action="<?php print $domain; ?>/hasznalati-utmutato/" method="GET" id="hasznalati-utmutato">
                  <div class="input-group">
                    <input style="height: auto;" type="search" class="form-control" aria-label="Nem találod? Keresd itt." aria-describedby="button-addon2" name="keres_utmutato" placeholder="Nem találod? Keresd itt." autocomplete="off" value="<?php if(!empty($_GET['keres_utmutato'])) { echo $_GET['keres_utmutato']; } ?>">
                    <div class="input-group-append">
                      <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><span class="fa fa-search"></span></button>
                    </div>
                  </div> 			
                </form>        
                
                
                <?php

                    if (!empty($_GET['keres_utmutato']))
                    {                      
                      $keres_szoveg_tomb = preg_split('/[\s]+/', $_GET['keres_utmutato']);
                      $feltetel = 'AND (';
                      foreach($keres_szoveg_tomb as $key => $row_tomb)
                      {
                        if ($feltetel != 'AND (')
                        {
                          $feltetel .= ' AND ';
                        }				
                        $feltetel .= "(t.nev LIKE :feltetel".$key." OR t.cikkszam LIKE :feltetel".$key." OR t.rovid_leiras LIKE :feltetel".$key." OR rovid_leiras LIKE :feltetelh".$key.")";
                      }
                      $feltetel .= ')';
  
                      echo '<h6 class="pt-5 pb-2 text-center">Találatok</h6>';
                      
                      echo '<div class="table-responsive"><table class="table table-bordered">
                          <thead>
                            <tr>
                              <th width="50%">Név</th>
                              <th>Cikkszám</th>
                              <th class="text-center">Letöltés</th>
                            </tr>
                          </thead>
                      <tbody>';
  
                      $query = "SELECT 
                                  tk.*
                                  , t.cikkszam
                                  , t.nev
                                  , CONCAT(tcs.nev_url,'/',t.nev_url) as termek_url
                                  , t.lathato
                                FROM ".$webjel."termek_kepek tk 
                                INNER JOIN ".$webjel."termekek t
                                ON t.id = tk.termek_id
                                INNER JOIN ".$webjel."term_csoportok tcs
                                ON tcs.id = t.csop_id
                                WHERE tk.spec=1 $feltetel
                                ORDER BY tk.kep ASC";
                      $result = $pdo->prepare($query);
                      foreach ($keres_szoveg_tomb as $key => $value)
                      {
                        $result->bindValue(':feltetel'.$key, '%'.$value.'%');
                        $result->bindValue(':feltetelh'.$key, '%'.htmlentities($value).'%');
                      }
                      $result->execute();                      
                      foreach ($result as $key => $value)
                      {
                        echo '<tr>
                                <td>';

                        if ($value['lathato'] == 1) {
                          echo '<a href="'.$domain.'/termekek/'.$value['termek_url'].'" target="_blank">'.$value['nev'].'</a>';
                        } else {
                          echo $value['nev'];
                        }
                        
                        echo ' </td>                        
                                <td>
                                  '.$value['cikkszam'].'
                                </td>
                                <td class="text-center">
                                  <a href="'.$domain.'/dokumentumok/termekek/'.$value['kep'].'" target="_blank" title="'.$value['kep'].'">
                                    <img src="'.$domain.'/images/'.strtolower($value['extension']).'.svg" style="max-width: 50px;">
                                  </a>
                                </td>
                            </tr>';
                      }

                      if (!isset($value['cikkszam'])) {
                          echo '<tr><td colspan="3" align="center">Sajnáljuk, ennek a terméknek a használati útmutatója nem érhető el digitális formátumban, kérjük keresse a termék csomagolásában! </td></tr>';


                      }

                      echo '</tbody></table></div>';

                    }

                    print '<p class="text-center pt-5"><b>A kereső segítségével a Metroman Webáruház által jelenleg forgalmazott és régebbi, akár kifutott termékek használati útmutatója tölthető le. Egyes termékek útmutatója nem érhető el digitális formátumban, ilyen esetben kérjük keresse a termék csomagolásában!</b></p>';
                    print '<p class="text-center"><b>Figyelem! A használati útmutatók letöltése csak saját használatra megengedett. Kereskedelmi célú felhasználása, nyilvános közzététele interneten vagy nyomtatott formában tilos!</b></p>';                    
                ?>
            </div>
          </div>
        </div>
      </section>      

      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>
    
  </body>
</html>