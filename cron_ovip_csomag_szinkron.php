<?php
	// folyamatában látszódjon a kiírása
	ob_implicit_flush(true);
	ob_end_flush();

	ini_set('xdebug.var_display_max_depth', -1);
	ini_set('xdebug.var_display_max_children', -1);
	ini_set('xdebug.var_display_max_data', -1);

	include 'config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	$options = array(
	  	'location' => $ovip_soap_link,
	 	'uri' => $ovip_soap_link,
	  	'encoding' => 'UTF-8',
	  	'trace' => 1 //csak hibakereséshez kell
	);	

	$request = 'getManufacture';

	$signature = hash('sha256', trim($ovip_ugyfel_id . $ovip_webshop_id . $ovip_authCode . $request . $ovip_ip_cim)); //hash generálása 

	$request = array(
		'request' => $request,
		'user_id' => $ovip_ugyfel_id,
		'signature' => $signature,
		'webshop_id' => $ovip_webshop_id
	);

	try {		

		$client = new SoapClient(NULL,$options);
		
		$tetelek = $client->getRequest($request);

		if (is_array($tetelek))
		{
			foreach ($tetelek as $csomag)
			{
				$query = "SELECT id FROM ".$webjel."termekek WHERE ovip_id=".$csomag['ovip_product_id'];
				$res = $pdo->prepare($query);
				$res->execute();
				$row_termek = $res -> fetch();	

				$deletecommand = "DELETE FROM ".$webjel."termekek_csomagok WHERE csomag_termek_id =".$row_termek['id'];
				$result = $pdo->prepare($deletecommand);
				$result->execute();							

				//var_dump($csomag);

				foreach ($csomag['parts'] as $key => $csomag_tetelek)
				{
					$termek_id = $pdo->query("SELECT id FROM ".$webjel."termekek WHERE ovip_id=".$csomag_tetelek['ovip_product_id'])->fetchColumn();

					//insert
					$query = "INSERT INTO ".$webjel."termekek_csomagok (csomag_termek_id,termek_id,mennyiseg) VALUES (:csomag_termek_id,:termek_id,:mennyiseg)";
					$result = $pdo->prepare($query);
					$result->execute(array(
									  ':csomag_termek_id'=>$row_termek['id'],				
									  ':termek_id'=>$termek_id,				
									  ':mennyiseg'=>$csomag_tetelek['quantity']));	

				}
			}


		    echo "Csomag kész<br/>";

		}
		else
		{
			echo "Csomag üzenet:".$tetelek."<br/>";
		}

	} catch (Exception $e) {

		echo "Csomag Szerver üzenet: ".$e->getMessage()."<br/>";

	}	



?>

							  