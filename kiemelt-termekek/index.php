<?php 
  session_start();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
  
  <?php
    $oldal = 'kiemelt-termekek';
    include '../config.php';
    $title = 'Extra jó ajánlatok a Metroman webáruházban';
    $description = 'Különleges válogatott ajánlatok egy helyen! Autófelszerelés Autóextra  Biztonságtechnikai és sok egyéb Elektronikai termékek nagy választékban házhoz szállítással is!';
    include $gyoker.'/module/mod_head.php';
  ?>

      <title><?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>"> 
  </head>
  <body> 
    <?php include $gyoker.'/module/mod_body_start.php'; ?>  
    <div class="page">
      <!-- Page Header-->
      <?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(<?=$domain?>/images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                  <h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5 fejresz-title">Kiemelt termékek</h2>                
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">
               
          </div>
        </div>
      </section>

      <section class="section section-sm">
        <div class="container">
          <div class="row row-40 row-lg-60">
            <div class="col-lg-3" style="text-align: initial;">
              <?php include $gyoker.'/module/mod_sidebar.php'; ?>
            </div>
            <div class="col-lg-9">
              <ul class="breadcrumbs-custom-path">
                <li><a href="<?=$domain?>">Főoldal</a></li>
                <li class="active">Kiemelt termékek</li>
              </ul>              
              <?php include $gyoker.'/webshop/kiemelt_termekek.php'; ?>
            </div>              
          </div>
        </div>
      </section>         

      <!-- Page Footer-->
      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>  
    <script src="<?php print $domain; ?>/webshop/scripts/termekek.js?v=01" type="text/javascript"></script>
    <script type="text/javascript" src="<?php print $domain; ?>/scripts/sajat.js"></script>
    <script src="<?=$domain?>/js/jquery.waitforimages.min.js"></script>
    <script>
      $(document).ready(function(){
        
        $(".kat_menu ul:has(.aktiv_kat)").css("display", "block"); // Kinyitjuk az aktív menüt.
        
        // multilevel megjelölése
        $(document).find(".kat_menu li").each(function() {
          if($(this).find("ul").size() != 0){
            $(this).find("a:first").after('<span class="kat_menu_lenyito_jel"></span>');
          }
        }); 
        // aktív kategória lenyitójának megjelölése
        $(document).find(".kat_menu li.aktiv_kat").each(function() {
          $(this).parents("ul").parent("li").find("span:first").addClass('kinyitva');
        });
      
        $( ".kat_menu .kat_menu_lenyito_jel" ).click(function() {
            if($(this).hasClass('kinyitva')){
              $(this).removeClass('kinyitva');
            }
            else{
              $(this).addClass('kinyitva');
            }
            
            $(this).next('ul').animate({
              height: [ "toggle", "swing" ]
            }, 300);
        });
      });

    </script>     
    
  </body>
</html>