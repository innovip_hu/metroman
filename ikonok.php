<?php 
  session_start();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
      <?php 
        $oldal = 'ikonok';
        include 'config.php';
        include $gyoker.'/module/mod_head.php';
      ?>

      <title><?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">      
  </head>
  <body>
    <?php include $gyoker.'/module/mod_body_start.php'; ?>
    <div class="page">
      <?php include $gyoker.'/module/mod_header.php'; ?>
 
      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(../images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                <h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5">Ikonok</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">
            <ul class="breadcrumbs-custom-path">
              <li><a href="<?=$domain?>">Főoldal</a></li>
              <li class="active">Ikonok</li>
            </ul>
          </div>
        </div>
      </section>

      <section class="section section-lg bg-default text-center">
        <div class="container">

          <?php
          /**
           * Remove items from an array
           * @param  array                $array                  The array to manage
           * @param  void                 $element                An array or a string of the item to remove
           * @return array                                        The cleaned array with resetted keys
           */
          function array_delete($array, $element) {
                  return (is_array($element)) ? array_values(array_diff($array, $element)) : array_values(array_diff($array, array($element)));
          }

          $icons_file = "css/fonts.css";
          $parsed_file = file_get_contents($icons_file);
          preg_match_all("/mdi\-([a-zA-z0-9\-]+[^\:\.\,\s])/", $parsed_file, $matches);
          $exclude_icons = array("fa-lg", "fa-2x", "fa-3x", "fa-4x", "fa-5x", "fa-ul", "fa-li", "fa-fw", "fa-border", "fa-pulse", "fa-rotate-90", "fa-rotate-180", "fa-rotate-270", "fa-spin", "fa-flip-horizontal", "fa-flip-vertical", "fa-stack", "fa-stack-1x", "fa-stack-2x", "fa-inverse");
          $icons = (object) array("icons" => array_delete($matches[0], $exclude_icons));

          //var_dump($icons);
          ?>
  
                  <ul class="list-social list-social-3 list-inline list-inline-xl">
                    <?php foreach ($icons->icons as $key => $value): ?>
                      <li><a class="icon mdi <?=$value?>" href="#" title="<?=$value?>"></a></li>  
                    <?php endforeach ?>
                  </ul>
        </div>
      </section>


      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>
  </body>
</html>