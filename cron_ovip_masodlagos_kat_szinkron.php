<?php
	// folyamatában látszódjon a kiírása
	ob_implicit_flush(true);
	ob_end_flush();

	ini_set('xdebug.var_display_max_depth', -1);
	ini_set('xdebug.var_display_max_children', -1);
	ini_set('xdebug.var_display_max_data', -1);

	include 'config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	$options = array(
	  	'location' => $ovip_soap_link,
	 	'uri' => $ovip_soap_link,
	  	'encoding' => 'UTF-8',
	  	'trace' => 1 //csak hibakereséshez kell
	);	

	$request = 'getCategoriesPlus';

	$signature = hash('sha256', trim($ovip_ugyfel_id . $ovip_webshop_id . $ovip_authCode . $request . $ovip_ip_cim)); //hash generálása 

	$request = array(
		'request' => $request,
		'user_id' => $ovip_ugyfel_id,
		'signature' => $signature,
		'webshop_id' => $ovip_webshop_id
	);

	try {		

		$client = new SoapClient(NULL,$options);
		
		$tetelek = $client->getRequest($request);

		if (is_array($tetelek))
		{

			$pdo->exec("TRUNCATE TABLE ".$webjel."termek_termek_csoportok");

			foreach ($tetelek as $elem)
			{
					$insertcommand = "INSERT INTO ".$webjel."termek_termek_csoportok (ovip_product_id,ovip_category_id) VALUES (:ovip_product_id,:ovip_category_id)";
					$result = $pdo->prepare($insertcommand);
					$result->execute(array(':ovip_product_id'=>$elem['ovip_product_id'],
									  ':ovip_category_id'=>$elem['ovip_category_id']));					
			}

			$query = "SELECT * FROM ".$webjel."termek_termek_csoportok";
			foreach ($pdo->query($query) as $value)
			{
					$query = "SELECT id FROM ".$webjel."termekek WHERE ovip_id=".$value['ovip_product_id'];
					$res = $pdo->prepare($query);
					$res->execute();
					$row = $res -> fetch();

					$updatecommand = "UPDATE ".$webjel."termek_termek_csoportok SET termek_id=? WHERE ovip_product_id=?";
					$result = $pdo->prepare($updatecommand);
					$result->execute(array($row['id'],$value['ovip_product_id']));

					$query = "SELECT id FROM ".$webjel."term_csoportok WHERE ovip_id=".$value['ovip_category_id'];
					$res = $pdo->prepare($query);
					$res->execute();
					$row = $res -> fetch();

					$updatecommand = "UPDATE ".$webjel."termek_termek_csoportok SET termek_csoport_id=? WHERE ovip_category_id=?";
					$result = $pdo->prepare($updatecommand);
					$result->execute(array($row['id'],$value['ovip_category_id']));			

			}

			
		    echo "Kategoriak kesz<br/>";

		}
		else
		{
			echo "Kategoriak HIBA:".$tetelek."<br/>";
		}

	} catch (Exception $e) {

		echo "Kategoriak HIBA: ".$e->getMessage()."<br/>";

	}	




?>

							  