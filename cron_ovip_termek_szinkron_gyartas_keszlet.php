<?php
	// folyamatában látszódjon a kiírása
	ob_implicit_flush(true);
	ob_end_flush();

	ini_set('xdebug.var_display_max_depth', -1);
	ini_set('xdebug.var_display_max_children', -1);
	ini_set('xdebug.var_display_max_data', -1);
	ini_set("default_socket_timeout", 600);


	include 'config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	$options = array(
	  	'location' => $ovip_soap_link,
	 	'uri' => $ovip_soap_link,
	  	'encoding' => 'UTF-8',
	  	'trace' => 1 //csak hibakereséshez kell
	);	

	$request = 'getStock';

	$signature = hash('sha256', trim($ovip_ugyfel_id . $ovip_webshop_id . $ovip_authCode . $request . $ovip_ip_cim)); //hash generálása 

	$request = array(
		'request' => $request,
		'user_id' => $ovip_ugyfel_id,
		'signature' => $signature,
		'webshop_id' => $ovip_webshop_id
	);

	try {		

		$client = new SoapClient(NULL,$options);
		
		$tetelek = $client->getRequest($request);

		$file = '/webshop/log_keszlet/'.date('Ymd-His').'.log';
		$current =  print_r($tetelek, true);
		file_put_contents($gyoker.$file, $current);

		if (is_array($tetelek))
		{
			foreach ($tetelek as $elem) {
				$pdo->exec("UPDATE ".$webjel."termekek SET raktaron=".$elem['free_stock']." WHERE ovip_id=".$elem['ovip_product_id']);

				$insertcommand = "INSERT INTO ".$webjel."termekek_keszletek (stock,free_stock,ovip_product_id) VALUES (:stock,:free_stock,:ovip_product_id)";
				$result = $pdo->prepare($insertcommand);
				$result->execute(array(':stock'=>$elem['stock'],
								  ':free_stock'=>$elem['free_stock'],
								  ':ovip_product_id'=>$elem['ovip_product_id']));				
			}
		    echo "Raktarkelszlet kesz<br/>";

		}
		else
		{
			echo "Raktarkelszlet HIBA:".$tetelek."<br/>";
		}

	} catch (Exception $e) {

		$file = '/webshop/log_keszlet/'.date('Ymd-His').'_hiba.log';
		$current =  print_r($e->getMessage(), true);
		file_put_contents($gyoker.$file, $current);		

		echo "Raktarkelszlet HIBA: ".$e->getMessage()."<br/>";

	}	

	$request = 'getStockManufacture';

	$signature = hash('sha256', trim($ovip_ugyfel_id . $ovip_webshop_id . $ovip_authCode . $request . $ovip_ip_cim)); //hash generálása 

	$request = array(
		'request' => $request,
		'user_id' => $ovip_ugyfel_id,
		'signature' => $signature,
		'webshop_id' => $ovip_webshop_id
	);

	try {		

		$client = new SoapClient(NULL,$options);
		
		$tetelek = $client->getRequest($request);

		if (is_array($tetelek))
		{
			foreach ($tetelek as $elem) {
				//$keszlet_gyartas = $elem['free_stock'] + $elem['stock'];
				$keszlet_gyartas = $elem['free_stock'];
				$pdo->exec("UPDATE ".$webjel."termekek SET raktaron=".$keszlet_gyartas." WHERE ovip_id=".$elem['ovip_product_id']);

				$insertcommand = "INSERT INTO ".$webjel."termekek_csomag_keszletek (free_stock,ovip_product_id) VALUES (:free_stock,:ovip_product_id)";
				$result = $pdo->prepare($insertcommand);
				$result->execute(array(
								  ':free_stock'=>$elem['free_stock'],
								  ':ovip_product_id'=>$elem['ovip_product_id']));				
			}
		    echo "Raktarkelszlet kesz<br/>";

		}
		else
		{
			echo "Raktarkelszlet HIBA:".$tetelek."<br/>";
		}

	} catch (Exception $e) {

		echo "Raktarkelszlet HIBA: ".$e->getMessage()."<br/>";

	}							  

	$deletecommand = "DELETE FROM ".$webjel."termekek_keszletek WHERE datum <= DATE_SUB(NOW(),INTERVAL 14 DAY)";
	$result = $pdo->prepare($deletecommand);
	$result->execute();	

	$deletecommand = "DELETE FROM ".$webjel."termekek_csomag_keszletek WHERE datum <= DATE_SUB(NOW(),INTERVAL 14 DAY)";
	$result = $pdo->prepare($deletecommand);
	$result->execute();		