<?php
	// folyamatában látszódjon a kiírása
	ob_implicit_flush(true);
	ob_end_flush();

	ini_set('xdebug.var_display_max_depth', -1);
	ini_set('xdebug.var_display_max_children', -1);
	ini_set('xdebug.var_display_max_data', -1);

	include 'config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	$options = array(
	  	'location' => $ovip_soap_link,
	 	'uri' => $ovip_soap_link,
	  	'encoding' => 'UTF-8',
	  	'trace' => 1 //csak hibakereséshez kell
	);	

	$request = 'getPricelist';

	$signature = hash('sha256', trim($ovip_ugyfel_id . $ovip_webshop_id . $ovip_authCode . $request . $ovip_ip_cim)); //hash generálása 

	$request = array(
		'request' => $request,
		'user_id' => $ovip_ugyfel_id,
		'signature' => $signature,
		'webshop_id' => $ovip_webshop_id
	);

	try {		

		$client = new SoapClient(NULL,$options);
		
		$tetelek = $client->getRequest($request);

		if (is_array($tetelek))
		{
			$pdo->exec("UPDATE ".$webjel."arlista SET ervenyes=0"); //láthatatlanná tesz mindent szinkron előtt

			foreach ($tetelek as $arlista)
			{
				$query = "SELECT * FROM ".$webjel."arlista WHERE ovip_id=".$arlista['price_list_ovip_id'];
				$res = $pdo->prepare($query);
				$res->execute();
				$row_arlista = $res -> fetch();		

				//var_dump($arlista);

				if (isset($row_arlista['id']))
				{
					//update
					$updatecommand = "UPDATE ".$webjel."arlista SET nev=?, szinkron=NOW(), ervenyes=1 WHERE id=?";
					$result = $pdo->prepare($updatecommand);
					$result->execute(array($arlista['price_list_name'],$row_arlista['id']));	

					$arlista_azonosito = $row_arlista['id'];
				}
				else
				{
					//insert
					$insertcommand = "INSERT INTO ".$webjel."arlista (nev,ovip_id,szinkron,ervenyes) VALUES (:nev,:ovip_id,NOW(),1)";
					$result = $pdo->prepare($insertcommand);
					$result->execute(array(':nev'=>$arlista['price_list_name'],
									  ':ovip_id'=>$arlista['price_list_ovip_id']));

					$arlista_azonosito = $pdo->lastInsertId();

				}

				$deletecommand = "DELETE FROM ".$webjel."arlista_arak WHERE arlista_id = ?";
				$result = $pdo->prepare($deletecommand);
				$result->execute(array($arlista_azonosito));				

				foreach ($arlista['price_list_prices'] as $arlista_arak)
				{

					$query = "INSERT INTO ".$webjel."arlista_arak (ovip_termek_id,arlista_id,ar,ar_akcios,akcio_tol,akcio_ig,afa) VALUES (:ovip_termek_id, :arlista_id, :ar, :ar_akcios, :akcio_tol, :akcio_ig, :afa)";
					$result = $pdo->prepare($query);
					$result->execute(array(
									  ':ovip_termek_id'=>$arlista_arak['ovip_product_id'],				
									  ':arlista_id'=>$arlista_azonosito,				
									  ':ar'=>$arlista_arak['gross_price'],				
									  ':ar_akcios'=>$arlista_arak['gross_sale_price'],				
									  ':akcio_tol'=>$arlista_arak['sale_start'],				
									  ':akcio_ig'=>$arlista_arak['sale_end'],				
									  ':afa'=>$arlista_arak['tax']));						

					/*
					$query = "SELECT id FROM ".$webjel."arlista_arak WHERE arlista_id=".$arlista_azonosito." AND ovip_termek_id=".$arlista_arak['ovip_product_id'];
					$res = $pdo->prepare($query);
					$res->execute();
					$row_arlista_tetelek = $res -> fetch();	

					if (isset($row_arlista_tetelek['id']))
					{
						//update
						$updatecommand = "UPDATE ".$webjel."arlista_arak SET ar=?, ar_akcios=?, akcio_tol=?, akcio_ig=?, afa=? WHERE id=?";
						$result = $pdo->prepare($updatecommand);
						$result->execute(array($arlista_arak['gross_price'],$arlista_arak['gross_sale_price'],$arlista_arak['sale_start'],$arlista_arak['sale_end'],$arlista_arak['tax'],$row_arlista_tetelek['id']));
					}
					else
					{
						//$termek_id = $pdo->query("SELECT id FROM ".$webjel."termekek WHERE ovip_id=".$arlista_arak['ovip_product_id'])->fetchColumn();
						//insert
						$query = "INSERT INTO ".$webjel."arlista_arak (termek_id,ovip_termek_id,arlista_id,ar,ar_akcios,akcio_tol,akcio_ig,afa) VALUES (:termek_id,:ovip_termek_id, :arlista_id, :ar, :ar_akcios, :akcio_tol, :akcio_ig, :afa)";
						$result = $pdo->prepare($query);
						$result->execute(array(
										  //':termek_id'=>$termek_id,				
										  ':ovip_termek_id'=>$arlista_arak['ovip_product_id'],				
										  ':arlista_id'=>$arlista_azonosito,				
										  ':ar'=>$arlista_arak['gross_price'],				
										  ':ar_akcios'=>$arlista_arak['gross_sale_price'],				
										  ':akcio_tol'=>$arlista_arak['sale_start'],				
										  ':akcio_ig'=>$arlista_arak['sale_end'],				
										  ':afa'=>$arlista_arak['tax']));	
					}
					*/				

				}
			}


		    echo "Árlista kész<br/>";

		}
		else
		{
			echo "Árlista üzenet:".$tetelek."<br/>";
		}

	} catch (Exception $e) {

		echo "Árlista Szerver üzenet: ".$e->getMessage()."<br/>";

	}	


	$query = "SELECT id,ovip_termek_id FROM ".$webjel."arlista_arak WHERE termek_id=0";
	foreach ($pdo->query($query) as $key => $value) {

		$query_term = "SELECT id FROM ".$webjel."termekek WHERE ovip_id=".$value['ovip_termek_id'];
		$res = $pdo->prepare($query_term);
		$res->execute();
		$row = $res -> fetch();

		if (isset($row['id'])) {
			$updatecommand = "UPDATE ".$webjel."arlista_arak SET termek_id=".$row['id']." WHERE id=".$value['id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();			
		}		
	}