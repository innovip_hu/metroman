<?php
	ini_set('max_execution_time', 0);

	include 'config.php';

	$metrogyoker = $gyoker."/webshop/log_rendelesek";

	$scanned_directory = array_diff(scandir($metrogyoker), array('..', '.', '.gitignore'));

	$i = 0;

	$eddig = date('Y-m-d', strtotime('-4 weeks'));

	foreach ($scanned_directory as $value)
	{
		$i++;

		if ( date ("Y-m-d", filemtime($metrogyoker.'/'.$value)) < $eddig) {
			//var_dump($value);

			unlink($metrogyoker.'/'.$value);
		}

	}