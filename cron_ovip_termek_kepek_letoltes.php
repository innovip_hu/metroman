<?php
	// folyamatában látszódjon a kiírása
	ob_implicit_flush(true);
	ob_end_flush();

	ini_set('xdebug.var_display_max_depth', -1);
	ini_set('xdebug.var_display_max_children', -1);
	ini_set('xdebug.var_display_max_data', -1);

	include 'config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}	

	$dir = $gyoker."/images/termekek/";

	function fileLetolesOvipbol($link) {
		$curlCh = curl_init();
		curl_setopt($curlCh, CURLOPT_URL, $link);
		curl_setopt($curlCh, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlCh, CURLOPT_TIMEOUT, 20);
		$curlData = curl_exec ($curlCh);
		$responseCode = curl_getinfo($curlCh, CURLINFO_HTTP_CODE);
		curl_close ($curlCh);

		// Check the response code
		if($responseCode == 200){
		    return $curlData;
		}else{
		    return false;
		}		
	}

	$query = "SELECT * FROM ".$webjel."termek_kepek_letolteni WHERE `probalkozas` < 3 LIMIT 100";
	foreach ($pdo->query($query) as $value) {

		if ($value['ovip_csoport_id'] > 0) {
			$query = "SELECT id,kep FROM ".$webjel."term_csoportok WHERE ovip_id=?";
			$res = $pdo->prepare($query);
			$res->execute(array($value['ovip_csoport_id']));
			$row_csoport = $res -> fetch();				

			if (isset($row_csoport['id'])) {
				$csop_id = $row_csoport['id'];

				$kep = pathinfo($value['kep'],PATHINFO_BASENAME);
				$ovipdir = pathinfo($value['kep'],PATHINFO_DIRNAME);

				$helyes_link = $ovipdir.'/'.rawurlencode($kep);

				$letoltes_csop = fileLetolesOvipbol($helyes_link);

				if ($letoltes_csop) {
					$downloadPath = $dir.$kep;
					$file = fopen($downloadPath, "w+");
					fputs($file, $letoltes_csop);
					fclose($file);	

					if ($row_csoport['kep'] != '') {
						unlink($dir.$row_csoport['kep']);
					}

					$updatecommand = "UPDATE ".$webjel."term_csoportok SET kep=? WHERE id=?";
					$result = $pdo->prepare($updatecommand);
					$result->execute(array($kep,$csop_id));					

					$deletecommand = "DELETE FROM ".$webjel."termek_kepek_letolteni WHERE id =".$value['id'];
					$result = $pdo->prepare($deletecommand);
					$result->execute();	

				} else {
					$updatecommand = "UPDATE ".$webjel."termek_kepek_letolteni SET `probalkozas` = `probalkozas` + 1 WHERE id=?";
					$result = $pdo->prepare($updatecommand);
					$result->execute(array($value['id']));
				}
			}
		}
		
	}

?>							  