<?php
	include 'config.php';

	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	require_once($gyoker.'/adm/PHPExcel.php');

	$inputFileName = $gyoker.'/Metroman_termek_import_ures.xlsx';

	$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);

	$tmp_handle = 'export-1.csv';

	$tmp_handle_file = fopen($tmp_handle, 'r');

	$i = 0;
	$j = 3;
	while (($data = fgetcsv($tmp_handle_file, 0, ";")) !== FALSE)
	{

		if ($i == 0)
		{
			var_dump($data);
			//fejléc
		}
		else
		{


/*
C:\wamp64\www\metroman\test.php:28:
array (size=12)
  0 => string 'shop_name' (length=9)
  1 => string 'invoicing_name' (length=14)
  2 => string 'url' (length=3)
  3 => string 'sku' (length=3)
  4 => string 'short_description' (length=17)
  5 => string 'description' (length=11)
  6 => string 'main_category' (length=13)
  7 => string 'categories' (length=10)
  8 => string 'price' (length=5)
  9 => string 'sale_price' (length=10)
  10 => string 'cost_price' (length=10)
  11 => string 'status' (length=6)
*/
			$prefix = 'M000';
			$str = $data[3];

			if (substr($str, 0, strlen($prefix)) == $prefix) {
			    $str = substr($str, strlen($prefix));
			    $ujszoveg = "MH";
			    $str = $ujszoveg.$str;
			} 

			//$kategoriak = unserialize($data[1]);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(0, $j, $data[1]);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(1, $j, $data[6]);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(2, $j, $str);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, $j, number_format(($data[8] / 100), 0, '',''));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(8, $j, number_format(($data[9] / 100), 0, '',''));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(12, $j, $data[11] == 'Inaktív' ? 0 : 1);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(13, $j, $data[1]);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(14, $j, html_entity_decode($data[4]));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(15, $j, html_entity_decode($data[5]));

			$j++;
		}

		$i++;
		
	}


			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save($gyoker.'/Metroman_termek_import.xlsx');	

/*




			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C7', $value['nev']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C8', $value['szuletesi_nev']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C9', $value['szuletesi_hely'].', '.$value['szuletesi_ido']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C10', $value['anyja_neve']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F11', $value['nyilvantartasi_szam']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C13', $value['szakmacsoport']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C14', $value['szakkepesites']);

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B23', $row_tanf['intezmeny'].' '.$row_tanf['intezmeny_helyszin']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B24', $row_tanf['vegleges_idopont']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B25', $row_tanf['oktatas_nev']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B26', $row_tanf['nyilvantartasi_szam']);
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F30', $row_tanf['pontertek']);
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A33', 'Dátum: '.$row_tanf['intezmeny_varos'].', '.$row_tanf['vegleges_idopont']);

			$objPHPExcel->setActiveSheetIndex(0);

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save($gyoker.'/dokumentumok/sikeres_vizsga/'.$fn2);			


			$pdo->exec("UPDATE ".$webjel."tanfolyamok_resztvevok SET igazolas='".$fn2."' WHERE id=".$value['id']);		

*/

?>

