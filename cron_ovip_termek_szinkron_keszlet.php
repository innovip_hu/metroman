<?php
	// folyamatában látszódjon a kiírása
	ob_implicit_flush(true);
	ob_end_flush();

	ini_set('xdebug.var_display_max_depth', -1);
	ini_set('xdebug.var_display_max_children', -1);
	ini_set('xdebug.var_display_max_data', -1);

	include 'config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	if (date('H:i:s') < '22:00:00' && date('H:i:s') > '04:00:00')
	{
		$options = array(
		  	'location' => $ovip_soap_link,
		 	'uri' => $ovip_soap_link,
		  	'encoding' => 'UTF-8',
		  	'trace' => 1 //csak hibakereséshez kell
		);	

		$request = 'getStock';

		$signature = hash('sha256', trim($ovip_ugyfel_id . $ovip_webshop_id . $ovip_authCode . $request . $ovip_ip_cim)); //hash generálása 

		$request = array(
			'extra_data' => date('Y-m-d',strtotime('-1 day')),
			'request' => $request,
			'user_id' => $ovip_ugyfel_id,
			'signature' => $signature,
			'webshop_id' => $ovip_webshop_id
		);

		try {		

			$client = new SoapClient(NULL,$options);
			
			$tetelek = $client->getRequest($request);

			if (is_array($tetelek))
			{
				foreach ($tetelek as $elem) {
					$pdo->exec("UPDATE ".$webjel."termekek SET raktaron=".$elem['free_stock']." WHERE ovip_id=".$elem['ovip_product_id']);
				}
			    echo "Raktarkelszlet kesz<br/>";

			}
			else
			{
				echo "Raktarkelszlet HIBA:".$tetelek."<br/>";
			}

		} catch (Exception $e) {

			echo "Raktarkelszlet HIBA: ".$e->getMessage()."<br/>";

		}


		$request = 'getStockManufacture';

		$signature = hash('sha256', trim($ovip_ugyfel_id . $ovip_webshop_id . $ovip_authCode . $request . $ovip_ip_cim)); //hash generálása 

		$request = array(
			'extra_data' => date('Y-m-d',strtotime('-1 day')),
			'request' => $request,
			'user_id' => $ovip_ugyfel_id,
			'signature' => $signature,
			'webshop_id' => $ovip_webshop_id
		);

		try {		

			$client = new SoapClient(NULL,$options);
			
			$tetelek = $client->getRequest($request);

			if (is_array($tetelek))
			{
				foreach ($tetelek as $elem) {
					//$keszlet_gyartas = $elem['free_stock'] + $elem['stock'];
					$keszlet_gyartas = $elem['free_stock'];
					$pdo->exec("UPDATE ".$webjel."termekek SET raktaron=".$keszlet_gyartas." WHERE ovip_id=".$elem['ovip_product_id']);

					$insertcommand = "INSERT INTO ".$webjel."termekek_csomag_keszletek (free_stock,ovip_product_id) VALUES (:free_stock,:ovip_product_id)";
					$result = $pdo->prepare($insertcommand);
					$result->execute(array(
									  ':free_stock'=>$elem['free_stock'],
									  ':ovip_product_id'=>$elem['ovip_product_id']));					
				}
			    echo "Raktarkelszlet kesz<br/>";

			}
			else
			{
				echo "Raktarkelszlet HIBA:".$tetelek."<br/>";
			}

		} catch (Exception $e) {

			echo "Raktarkelszlet HIBA: ".$e->getMessage()."<br/>";

		}	
	}								  