<?php 
  session_start();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
  
  <?php
    $oldal = 'termekek';
    include '../config.php';
    include $gyoker.'/module/mod_head.php';
  ?>
    <link rel="stylesheet" href="<?=$domain?>/termekek/szures.css">
  </head>
  <body> 
    <?php include $gyoker.'/module/mod_body_start.php'; ?>  
    <div class="page">
      <!-- Page Header-->
      <?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(<?=$domain?>/images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                <?php 
                  if (!empty($_GET['term_urlnev']))
                  {
                    $res = $pdo->prepare("SELECT nev FROM ".$webjel."termekek where nev_url=?");
                    $res->execute(array($_GET['term_urlnev']));
                    $row  = $res -> fetch(); 
                    print '<h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5 fejresz-title">'.$row['nev'].'</h2>';
                  }
                  elseif (!empty($_GET['kat_urlnev']))
                  {
                    $res = $pdo->prepare("SELECT nev, leiras FROM ".$webjel."term_csoportok where nev_url=?");
                    $res->execute(array($_GET['kat_urlnev']));
                    $row  = $res -> fetch(); 
                    print '<h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5 fejresz-title">'.$row['nev'].'</h2>';
                    if ($row['leiras'] != '') {
                      $csop_leiras_uj = $row['leiras'];
                    }
                  }
                  else
                  {
                    print '<h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5 fejresz-title">Termékek</h2>'; 
                  }
                ?>                
                
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">
            <?php if (isset($csop_leiras_uj)): ?>
              <p class="fejresz-leiras"><?=$csop_leiras_uj?></p>
            <?php endif ?>
          </div>
        </div>
      </section>

      <section class="section section-sm">
        <div class="container">
          <div class="row row-10">
            <div class="col-12 d-md-none">
              <?php 
                $szulkat = '';
                $szul_szulkat = '';
                $szul_szul_szulkat = '';
                $szulkat_nev = '';
                $szul_szulkat_nev = '';
                $szul_szul_szulkat_nev = '';
                if (isset($_GET['kat_urlnev']))
                {
                  $query = "SELECT * FROM ".$webjel."term_csoportok where nev_url=?";
                  $res = $pdo->prepare($query);
                  $res->execute(array($_GET['kat_urlnev']));
                  $row_1  = $res -> fetch();
                  $szulkat = $row_1['csop_id'];
                  $szulkat_nev = $row_1['nev'];
                  $szulkat_url = $row_1['nev_url'];
                  if ($szulkat > 0)
                  {
                    $query = "SELECT * FROM ".$webjel."term_csoportok where id=".$szulkat;
                    $res = $pdo->prepare($query);
                    $res->execute();
                    $row_2  = $res -> fetch();
                    $szul_szulkat = $row_2['csop_id'];
                    $szul_szulkat_nev = $row_2['nev'];
                    $szul_szulkat_url = $row_2['nev_url'];
                    if ($szul_szulkat != 0 || $szul_szulkat != '')
                    {
                      $query = "SELECT * FROM ".$webjel."term_csoportok where id=".$szul_szulkat;
                      $res = $pdo->prepare($query);
                      $res->execute();
                      $row_3  = $res -> fetch();
                      $szul_szul_szulkat = $row_3['csop_id'];
                      $szul_szul_szulkat_nev = $row_3['nev'];
                      $szul_szul_szulkat_url = $row_3['nev_url'];
                    }
                  }
                }
                
                // if(!isset($_GET['term_urlnev']) || $_GET['term_urlnev'] == '')
                // {
                  //print '<h2 class="pull-left">'.$row['nev'].'</h2>';
                // }
                print '<ul class="breadcrumbs-custom-path pb-4">';
                    print '<li><a href="'.$domain.'">Főoldal</a></li>'; 
                    print '<li><a href="'.$domain.'/termekek/">Termékek</a></li>'; 
                  if($szul_szul_szulkat_nev != '' && $_GET['kat_urlnev'] != $szul_szul_szulkat_nev)
                  {
                    print '<li><a href="'.$domain.'/termekek/'.$szul_szul_szulkat_url.'/">'.$szul_szul_szulkat_nev.'</a></li>';
                  }
                  if($szul_szulkat_nev != '' && $_GET['kat_urlnev'] != $szul_szul_szulkat_nev)
                  {
                    print '<li><a href="'.$domain.'/termekek/'.$szul_szulkat_url.'/">'.$szul_szulkat_nev.'</a></li>';
                  }
                  if($szulkat_nev != '' && $_GET['kat_urlnev'] != $szul_szul_szulkat_nev)
                  {
                    print '<li><a href="'.$domain.'/termekek/'.$szulkat_url.'/">'.$szulkat_nev.'</a></li>';
                  }
                  if (!empty($_GET['term_urlnev']))
                  {
                    print '<li class="active">'.$row['nev'].'</li>';
                  }                 
                print '</ul>';
              ?>              
            </div>
            <?php if (!empty($_GET['term_urlnev']) || (!isset($_GET['term_urlnev']) && !isset($_GET['kat_urlnev']))): ?>
              <div class="col-lg-12">
                <?php include $gyoker.'/webshop/termekek.php'; ?>
              </div>               
            <?php else: ?>
              <div class="col-lg-3" style="text-align: initial;">
                <?php include $gyoker.'/module/mod_sidebar.php'; ?>
              </div>
              <div class="col-lg-9">
                <?php include $gyoker.'/webshop/termekek.php'; ?>
              </div>              
            <?php endif ?>
          </div>
        </div>
      </section>         

      <!-- Page Footer-->
      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>  
    <script src="<?php print $domain; ?>/webshop/scripts/termekek.js?v=06" type="text/javascript"></script>
    <script type="text/javascript" src="<?php print $domain; ?>/scripts/sajat.js"></script>
    <script src="<?=$domain?>/js/jquery.waitforimages.min.js"></script>
    <script>
      $(document).ready(function(){
        
        $(".kat_menu ul:has(.aktiv_kat)").css("display", "block"); // Kinyitjuk az aktív menüt.
        
        // multilevel megjelölése
        $(document).find(".kat_menu li").each(function() {
          if($(this).find("ul").size() != 0){
            $(this).find("a:first").after('<span class="kat_menu_lenyito_jel"></span>');
          }
        }); 
        // aktív kategória lenyitójának megjelölése
        $(document).find(".kat_menu li.aktiv_kat").each(function() {
          $(this).parents("ul").parent("li").find("span:first").addClass('kinyitva');
        });
      
        $( ".kat_menu .kat_menu_lenyito_jel" ).click(function() {
            if($(this).hasClass('kinyitva')){
              $(this).removeClass('kinyitva');
            }
            else{
              $(this).addClass('kinyitva');
            }
            
            $(this).next('ul').animate({
              height: [ "toggle", "swing" ]
            }, 300);
        });
      });

      $(document).on("change", "#szuro_form input[type=checkbox]", function() {
        szures($(this).data('id'));
      });

      $(document).ready(function(){
        $('.filter_box .headerFilter').click(function(){
          var div = $(this);
          var id = div.nextAll('div.belso-szuro').attr('id');
          div.nextAll('div.belso-szuro').animate({
            opacity: [ "toggle" ]
          }, 200);

          if ($('span',this).hasClass('fa-chevron-up'))
          {
            $('span',this).removeClass('fa-chevron-up').addClass('fa-chevron-down');
          }
          else
          {
            $('span',this).removeClass('fa-chevron-down').addClass('fa-chevron-up');
          }  

          if (!$('.szuro_mobil_title').is(':visible')) {
            csukasZaras(id);  
          }   
        }); 

        function csukasZaras(id) {
          $('.filter_box .headerFilter').each(function(){
              var h3tag = $(this);
              var divtag = h3tag.nextAll('div.belso-szuro');
              var checkID = divtag.attr('id');
              if (divtag.is(':visible') && checkID != id) {
                  divtag.animate({
                    opacity: [ "toggle" ]
                  }, 200); 

                  if ($('span',this).hasClass('fa-chevron-up'))
                  {
                    $('span',this).removeClass('fa-chevron-up').addClass('fa-chevron-down');
                  }
                  else
                  {
                    $('span',this).removeClass('fa-chevron-down').addClass('fa-chevron-up');
                  }                   
              }             
          });
        }    
      });

    </script>     

    <div class="modal fade text-center" id="kosarba_felugro_ablak" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>A terméket sikeresen a kosárba helyezted!</p>
          </div>
          <div class="modal-footer align-self-center">
            <div class="group">
              <a class="button button-xs button-default-outline-2" data-dismiss="modal" style="cursor: pointer;">Tovább nézelődöm</a>
              <a class="button button-xs button-primary" href="<?=$domain?>/kosar">Kosárba lépek</a>
            </div>
          </div>
        </div>
      </div>
    </div>  
<script type="text/javascript">
  $(document).ready(function() {
    $('.lightGallery').lightGallery({
        showThumbByDefault:true,
        selector: '.item .slick-product-figure a',
        share: false,
        download: false,
        autoplay: false
    });

    $('.kapcs_div_kapcsolo').click(function(){
        $('#kapcs_div').hide();
    });

    $('.kapcs_div_kapcsolo_be').click(function(){
        $('#kapcs_div').show();
    });

  });
</script>     
  </body>
</html>