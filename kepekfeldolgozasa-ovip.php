<?php
	ini_set('max_execution_time', 0);

	include 'config.php';

  	// PDO
	  $dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	  try
	  {
		  $pdo = new PDO(
		  $dsn, $dbuser, $dbpass,
		  Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		  );
		  $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	  }
	  catch (PDOException $e)
	  {
		  $protocol = 'HTTP/1.0';
  
		  if ( $_SERVER['SERVER_PROTOCOL'] === 'HTTP/1.1' ) {
			  $protocol = 'HTTP/1.1';
		  }
  
		  header( $protocol . ' 503 Service Unavailable', true, 503 );
		  header( 'Retry-After: 3600' );
  
		  die("Nem lehet kapcsolódni az adatbázishoz!");
	  }	

	$cikkszamok = [];

	$handle = fopen('termektorzs.csv','r');
	ini_set('auto_detect_line_endings',TRUE);
	while ( ($data = fgetcsv($handle, 0, ";") ) !== FALSE ) {
		$cikkszamok[] = $data[2];
	}	

	fclose($handle);

	$kepgyoker = $gyoker."/dokumentumok/termekek";

	$zip_seged = [];
	$sorrend_utolso = "";

	$query = "SELECT 
		tk.kep, t.cikkszam 
	FROM ".$webjel."termek_kepek tk
	INNER JOIN ".$webjel."termekek t
	ON t.id = tk.termek_id
	WHERE tk.spec = 1
	ORDER BY t.cikkszam ASC, tk.alap DESC, tk.id ASC";
	foreach ($pdo->query($query) as $key => $kepek) {

		// if (!in_array($kepek['cikkszam'],$cikkszamok)) {
		// 	continue;
		// }		

		if ($sorrend_utolso != $kepek['cikkszam']) {
			$sorrend = 0;
			$sorrend_utolso = $kepek['cikkszam'];
		}

		$filesize = filesize($kepgyoker . '/' . $kepek['kep']);

		$zip_seged[] = [
			'cikkszam' => $kepek['cikkszam'],
			'kep' => $kepek['kep'],
			'size' => $filesize,
			'sorrend' => $sorrend
		];
		
		$sorrend++;
	}	

	$zip_allomanyok = [];
	$zip_temp = [];
	$darabszam = 0;
	$size = 0;
	$i = 1;

	foreach ($zip_seged as $zip) {
		
		if ($size == 0 && $i == 1)
		{
			$zip_temp[] = $zip;
			$size += $zip['size'];			
			$darabszam++;
		}
		// elseif ($darabszam < 100 && ($zip['size'] + $size) < 48000000)
		elseif ($darabszam < 50)
		{
			$zip_temp[] = $zip;
			$size += $zip['size'];
			$darabszam++;
		}
		else
		{
			$size = 0;
			$darabszam = 0;
			
			$zip_allomanyok[$i] = $zip_temp;
			$zip_temp = [];

			$i++;

			$zip_temp[] = $zip;

			$size += $zip['size'];	
			$darabszam++;			
		}
	}

	if (!empty($zip_temp)) {
		$zip_allomanyok[$i] = $zip_temp;
	}


	if (isset($_GET['id'])) {
		// letöltés feldolgozása

		$temp_dir = sys_get_temp_dir() . "/download_temp";
		if (!file_exists($temp_dir)) {
			mkdir($temp_dir);
		}		

		$zip = new ZipArchive();
		$zip_file = $temp_dir . "/download_tempzip.zip";
		$zip->open($zip_file, ZipArchive::CREATE);

		foreach ($zip_allomanyok[$_GET['id']] as $tomoriteni) {
			$ext = pathinfo($tomoriteni['kep'], PATHINFO_EXTENSION);
			$file_path = $kepgyoker .'/' . $tomoriteni['kep'];

			// $menteni_fajlnev = $tomoriteni['id'];
			$menteni_fajlnev = $tomoriteni['cikkszam'];
			if ($tomoriteni['sorrend'] > 0) {
				$menteni_fajlnev .= '_altpic_' . $tomoriteni['sorrend'];	
			}
			$menteni_fajlnev .= '.' . $ext;

			// $zip->addFile($file_path, $menteni_fajlnev);
			$zip->addFile($file_path, $tomoriteni['kep']);
		}

		$zip->close();
		
	
		// Zip fájlok letöltése
		header("Content-Description: File Transfer"); 
		header('Content-Type: application/zip');
		header('Content-Length: '.filesize($zip_file));
		header('Content-Disposition: attachment; filename=metroman_betoltes_'.str_pad($_GET['id'], 3, '0', STR_PAD_LEFT).'.zip');
		readfile($zip_file); // Csak az első Zip fájlt küldjük vissza
		
		// sleep(10);

		// Ideiglenes munkakönyvtár törlése
		array_map('unlink', glob("$temp_dir/*.*"));
		rmdir($temp_dir);		
		

	} else {
		// zipek listázása
		foreach ($zip_allomanyok as $key => $value) {
			echo '<a href="'.$domain.'/kepekfeldolgozasa-ovip.php?id='.$key.'">Letöltés: metroman_betoltes_'.str_pad($key, 3, '0', STR_PAD_LEFT).'.zip</a><br/>';
		}
	}