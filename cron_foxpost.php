<?php
	include 'config.php';

	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsol�dni az adatb�zishoz!");
	}

	$json = file_get_contents('http://cdn.foxpost.hu/foxpost_terminals_extended_v3.json');
	$json_string = $json;
	if($json_string != '')
	{
		$pdo->exec("TRUNCATE TABLE ".$webjel."foxpost_csomagpontok");
		$my_array_data = json_decode($json_string, TRUE);
		foreach ($my_array_data as $elem)
		{
			$nyitvatartas = 'Hétfő: '.$elem['open']['hetfo'].',
Kedd: '.$elem['open']['kedd'].',
Szerda: '.$elem['open']['szerda'].',
Csütörtök: '.$elem['open']['csutortok'].',
Péntek: '.$elem['open']['pentek'].',
Szombat: '.$elem['open']['szombat'].',
Vasárnap: '.$elem['open']['vasarnap'];

			$insertcommand = "INSERT INTO ".$webjel."foxpost_csomagpontok (foxpost_id, nev, varos, cim, megkozelithetoseg, geolat, geolng, nyitvatartas, szinkron) VALUES (:foxpost_id, :nev, :varos, :cim, :megkozelithetoseg, :geolat, :geolng, :nyitvatartas, NOW())";
			$result = $pdo->prepare($insertcommand);
			$result->execute(array(':foxpost_id'=>$elem['place_id'],
								':nev'=>$elem['name'], 
								':varos'=>$elem['group'], 
								':cim'=>$elem['address'], 
								':megkozelithetoseg'=>$elem['findme'], 
								':geolat'=>$elem['geolat'], 
								':geolng'=>$elem['geolng'], 
								':nyitvatartas'=>$nyitvatartas));
		}
	}
	// var_dump( $my_array_data);
?>
