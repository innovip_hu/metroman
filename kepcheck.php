<?php
	include 'config.php';

	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsol�dni az adatb�zishoz!");
	}

	function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
	    header('Content-Type: application/csv');
	    header('Content-Disposition: attachment; filename="'.$filename.'";');

	    // open the "output" stream
	    // see http://www.php.net/manual/en/wrappers.php.php#refsect2-wrappers.php-unknown-unknown-unknown-descriptioq
	    $f = fopen('php://output', 'w');

	    foreach ($array as $line) {
	        fputcsv($f, $line, $delimiter);
	    }
	}   	

	$eredmeny_tomb = array();


	$query = "SELECT tk.*,t.cikkszam FROM ".$webjel."termek_kepek tk
	INNER JOIN ".$webjel."termekek t
	ON t.id = tk.termek_id
	WHERE spec = 0";

	foreach ($pdo->query($query) as $key => $value) {
		if (!file_exists($gyoker.'/images/termekek/'.$value['kep'])) {
			$eredmeny_tomb[] = array($value['cikkszam'],$value['kep']);
		}
	}

	if (!empty($eredmeny_tomb)) {
		array_to_csv_download($eredmeny_tomb);
	}
?>
