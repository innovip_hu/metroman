<?php 
	include 'config.php';

	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	}
	catch (PDOException $e)
	{
	    $protocol = 'HTTP/1.0';

	    if ( $_SERVER['SERVER_PROTOCOL'] === 'HTTP/1.1' ) {
	        $protocol = 'HTTP/1.1';
	    }

	    header( $protocol . ' 503 Service Unavailable', true, 503 );
	    header( 'Retry-After: 3600' );

		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	$query_kosarak = "SELECT k.kosar_id, u.email, u.vezeteknev
			FROM ".$webjel."kosar k
			INNER JOIN ".$webjel."users u
			ON u.id = k.user_id
			WHERE k.user_id != 0 AND k.datum <= DATE_SUB(NOW(),INTERVAL 2 DAY)
			AND NOT EXISTS (
				SELECT 1 FROM ".$webjel."kosar_ertesitesek WHERE kosar_id = k.kosar_id
			)
			GROUP BY k.kosar_id";
	//echo  $query_kosarak;
	foreach ($pdo->query($query_kosarak) as $row_kosar)
	{

		// $tabla_tetelek = '<table align="left" cellpadding="0" cellspacing="0" style="background-color: #FFFFFF; font-family: calibri, sans-serif;" width="100%">
		// 			<tr><td colspan="5">';


		//Kosár tartalma
		$query = "SELECT * FROM ".$webjel."kosar WHERE kosar_id=".$row_kosar['kosar_id'];
		$ar = 0;
		$id = 0;
		$tabla_tetelek_uj = "";
		// $tabla_tetelek.= '<table align="left" cellpadding="6" cellspacing="0" style="font-family: calibri, sans-serif;" width="100%"">';
		// $tabla_tetelek.= '<tr align="left" style="font-weight:bold;" style="background-color: #3E5878; color: #FFFFFF;">
		// 			<td width="175" colspan="2" style="background-color: #3E5878; color: #FFFFFF;">Termék neve</td>
		// 			<td style="background-color: #3E5878; color: #FFFFFF;">Mennyiség</td>
		// 			<td style="background-color: #3E5878; color: #FFFFFF;">Egységár</td>
		// 			<td align="right" style="background-color: #3E5878; color: #FFFFFF;">Összesen</td>
		// 		</tr>'; //colspan ki, ha van kép
		foreach ($pdo->query($query) as $row)
		{
			if($row['term_akcios_ar'] == 0) //ha nem akciós
			{
				// $term_ar = $row['term_ar'] - (round($row['term_ar'] * ($kupon_kedv / 100)));
				$term_ar = $row['term_ar'];
			}
			else //ha akciós
			{
				// $term_ar = $row['term_akcios_ar'] - (round($row['term_akcios_ar'] * ($kupon_kedv / 100)));
				$term_ar = $row['term_akcios_ar'];
			}
			//termék adatai
			$query_term = "SELECT * FROM ".$webjel."termekek where id=".$row['term_id'];
			$res = $pdo->prepare($query_term);
			$res->execute();
			$row_term = $res -> fetch();

			if ($row_term['kep'] != '')
			{
				$kep = '<img src="'.$domain.'/images/termekek/'.$row_term['kep'].'" width="30" border="0">';
			}
			else
			{
				$kep = '';
			}
			$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_term['csop_id'];
			$res = $pdo->prepare($query_csop);
			$res->execute();
			$row_csop = $res -> fetch();
		
			/*
			$tabla_tetelek.= '<tr>
						<td><a href="'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row_term['nev_url'].'">
							<b>'.$row['term_nev'].'</b>';
						// Jellemzők
						$query_jell = "SELECT * FROM ".$webjel."kosar_tetel_termek_parameter_ertek WHERE kosar_tetel_id = ".$row['id']." ORDER BY termek_parameter_nev ASC";
						$elso = 1;
						foreach ($pdo->query($query_jell) as $row_jell)
						{
							if($elso == 1) { $tabla_tetelek.= ''; $elso = 0; } else { $tabla_tetelek.= '<br/>'; }
							$tabla_tetelek.= $row_jell['termek_parameter_nev'].': '.$row_jell['termek_parameter_ertek_nev'];
						}						

						
						$tabla_tetelek.= '</a></td>
						<td></td>
						<td>'.$row['term_db'].'</td>
						<td>'.number_format($term_ar, 0, ',', ' ').' Ft</td>
						<td align="right">'.number_format($term_ar * $row['term_db'], 0, ',', ' ').' Ft</td>
					</tr>';
			*/

			$tabla_tetelek_uj .= '<tr>
				<td style="border-bottom: 1px solid #d2d2d2;"><a  style="text-decoration: none; color: #11408F" 
					href="'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row_term['nev_url'].'"
					moz-do-not-send="true">
					<h4 style="margin-bottom: 0;" class="margbot0">'.$row['term_nev'].'</h4></a>';

			if ($row_term['cikkszam'] != '')
			{
				$tabla_tetelek_uj .= '<span style="font-size: 85%;">Cikkszám:'.$row_term['cikkszam'].'</span>';
			}			
			
			$tabla_tetelek_uj .= '</td><td style="border-bottom: 1px solid #d2d2d2;"><br>
				</td>
				<td style="border-bottom: 1px solid #d2d2d2; text-align: center;">'.$row['term_db'].' db</td>
				<td style="border-bottom: 1px solid #d2d2d2;" align="right">'.number_format($term_ar, 0, ',', ' ').' Ft</td>
				<td style="border-bottom: 1px solid #d2d2d2;" align="right">'.number_format($term_ar * $row['term_db'], 0, ',', ' ').' Ft</td>
			</tr>';
			$ar = $ar + ($term_ar * $row['term_db']);
			$kupon_id = $row['kupon_id'];
		}

		
		// $tabla_tetelek.= '<tr>
		// 			<td colspan="3"></td>
		// 			<td>Összesen</td>
		// 			<td align="right">'.number_format($ar, 0, ',', ' ').' Ft</td>
		// 		</tr>';
				
	
		// $tabla_tetelek.= '</td></tr></table>';
		
		//E-MAIL
		//üzenet
		/*
		$mess =  '<table align="center" cellpadding="12" cellspacing="0" border="0" style="background-color: #fff;">
					<tr>
						<td>
							<a href="'.$domain.'" style="color: blue;" target="_blank"><img src="'.$domain.'/images/logo.png" width="200" title="'.$webnev.'" /></a>
						</td>
					</tr>
					<tr>
						<td style="text-align:left;">
							<p><b>Kedves '.$row_kosar['vezeteknev'].'!</b></p>
							<p>A kosaradban az alábbi termékek várnak rád! Amennyiben szeretnéd őket megvásárolni, <a href="'.$domain.'/belepes">csak jelentkezz be</a>.</p>
						</td>
					</tr>
						<td>
							'.$tabla_tetelek.'
						</td>
					<tr>
						<td style="text-align:left;">
							<p>Reméljük hamarosan viszont látunk!</p>
							<p>
								Üdvözlettel:
								<br>
								'.$webnev.'
							</p>
							<p>Ha még nem tetted, kövess minket a Facebookon is, hogy ne maradj le akcióinkról és újdonságainkról!
							<br><a href="https://www.facebook.com/MetromanHungariaKft/" target="_blank"><img src="'.$domain.'/images/facebook.png" style="width: 40px; margin-top: 10px;" alt="Facebook"></a></p>
						</td>
					</tr>
				</table>';

				*/

		//phpMailer
		$level_tartalma = file_get_contents($gyoker.'/webshop/email_sablon/metroman_01_UJ_kosar_elhagyas.html');

		$level_tartalma = str_replace("{nev}", $row_kosar['vezeteknev'], $level_tartalma);  
		$level_tartalma = str_replace("{kosar_tetel_uj}", $tabla_tetelek_uj, $level_tartalma);  
		$level_tartalma = str_replace("{ar}", number_format($ar,0,'',' '), $level_tartalma);  

		require_once($gyoker.'/webshop/PHPMailer-master/PHPMailerAutoload.php');
		$mail = new PHPMailer();
		$mail->isHTML(true);
		$mail->CharSet = 'UTF-8';
		// $mail->addCustomHeader('MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n");
		if($conf_smtp == 1)
		{
			// SMTP
			$mail->IsSMTP(); // telling the class to use SMTP
			$mail->Host       = $smtp_host; // SMTP server
			$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
													   // 1 = errors and messages
													   // 2 = messages only
			$mail->SMTPAuth   = true;                  // enable SMTP authentication
			$mail->Host       = $smtp_host; // sets the SMTP server
			$mail->SMTPSecure = $smtp_protokol;
			$mail->Port       = $smtp_port;                    // set the SMTP port for the GMAIL server
			$mail->Username   = $smtp_user; // SMTP account username
			$mail->Password   = $smtp_pass;        // SMTP account password
			$mail->SetFrom($smtp_email, $smtp_name);
			$mail->AddReplyTo($smtp_email, $smtp_name);
		}
		else
		{
			$mail->SetFrom($email, $webnev);
		}
		$mail->AddAddress($row_kosar['email'], $row_kosar['vezeteknev']);
		$mail->Subject = "Rendelésed függőben maradt";
		// $htmlmsg = '<html><body style="background-color: #d2d2d2">'.$mess.'</body></html>';

		$mail->Body = $level_tartalma;

		//echo $htmlmsg;
		if(!$mail->Send()) {
		  echo "Mailer Error: " . $mail->ErrorInfo;
		}
		else
		{
			$insertcommand = "INSERT INTO ".$webjel."kosar_ertesitesek (kosar_id,email) VALUES (:kosar_id,:email)";
			$result = $pdo->prepare($insertcommand);
			$result->execute(array(':kosar_id'=>$row_kosar['kosar_id'],
							  ':email'=>$row_kosar['email']));			
		}

	}