<?php 
  session_start();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
      <?php 
        $oldal = 'beszallitoknak';
        include '../config.php';
        include $gyoker.'/module/mod_head.php';
      ?>

      <title>Beszállítóknak | <?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">      

  </head>
  <body>
    <?php include $gyoker.'/module/mod_body_start.php'; ?>
    <div class="page">
      <?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(../images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                <h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5 fejresz-title">Beszállítóknak</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">
            <ul class="breadcrumbs-custom-path">
              <li><a href="<?=$domain?>">Főoldal</a></li>
              <li class="active">Beszállítóknak</li>
            </ul> 
          </div>
        </div>
      </section>

      <section class="section section-lg bg-default">
        <div class="container">         
          <div class="row row-30 justify-content-center">
            <div class="col-md-12 text-center">
                <p>Üzletünk folyamatosan keres új beszállítókat.  Amennyiben eladni szeretne nekünk, ajánlatát az alábbi elérhetőségen teheti meg:</p>

                <img src="../images/elerhetoseg-email.jpg" alt="Beszállítóknak">
            </div>
          </div>
        </div>
      </section>      

      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>

  </body>
</html>