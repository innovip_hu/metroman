<?php
	include 'config.php';

  	// PDO
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	}
	catch (PDOException $e)
	{
	    $protocol = 'HTTP/1.0';

	    if ( $_SERVER['SERVER_PROTOCOL'] === 'HTTP/1.1' ) {
	        $protocol = 'HTTP/1.1';
	    }

	    header( $protocol . ' 503 Service Unavailable', true, 503 );
	    header( 'Retry-After: 3600' );

		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
		

	$osszes_csoport_check = array();

	$query_cs = "SELECT id FROM ".$webjel."term_csoportok WHERE csop_id=0";
	foreach ($pdo->query($query_cs) as $key => $value_cs)
	{
		$osszes_csoport_check[] = $value_cs['id'];

		$query_cs_1 = "SELECT id FROM ".$webjel."term_csoportok WHERE csop_id=".$value_cs['id'];
		foreach ($pdo->query($query_cs_1) as $key => $value_cs_1)
		{
			$osszes_csoport_check[] = $value_cs_1['id'];

			$query_cs_2 = "SELECT id FROM ".$webjel."term_csoportok WHERE csop_id=".$value_cs_1['id'];
			foreach ($pdo->query($query_cs_2) as $key => $value_cs_2)
			{
				$osszes_csoport_check[] = $value_cs_2['id'];

			}				
		}			
	}

	var_dump($osszes_csoport_check);


	function kategoria($kategoria,$csop_id) {

		global $webjel;
		global $pdo;

		$query = "SELECT id FROM ".$webjel."term_csoportok WHERE csop_id=".$csop_id;
		foreach ($pdo->query($query) as $key => $row)
		{
			$kategoria[] = $row['id'];
			$kategoria = kategoria($kategoria,$row['id']);
		}

		return $kategoria;

	}

	$rekurziv_kategoria = [];

	var_dump(kategoria($rekurziv_kategoria,0));