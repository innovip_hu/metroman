<?php
	include 'config.php';
	set_time_limit(0);

// PDO
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}	

	$query = "SELECT * FROM ".$webjel."term_csoportok_parameter_ertek";
	foreach ($pdo->query($query) as $key => $value) {
		$pdo->exec("UPDATE ".$webjel."term_csoportok_parameter_ertek SET ertek2='".htmlentities($value['ertek'])."' WHERE id=".$value['id']);
	}

?>