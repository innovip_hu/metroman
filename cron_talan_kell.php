<?php

					$query_rend = "SELECT 
							".$webjel."rendeles.vasarlo_nev,
							".$webjel."rendeles.vasarlo_email,
							".$webjel."rendeles.szallitasi_dij,
							SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar > 0, ".$webjel."rendeles_tetelek.term_akcios_ar * ".$webjel."rendeles_tetelek.term_db, ".$webjel."rendeles_tetelek.term_ar * ".$webjel."rendeles_tetelek.term_db)) as rendelt_osszeg
							FROM ".$webjel."rendeles
							INNER JOIN ".$webjel."rendeles_tetelek 
							ON ".$webjel."rendeles.id = ".$webjel."rendeles_tetelek.rendeles_id 							
							WHERE ".$webjel."rendeles.id=".$_GET['txid']."
							GROUP BY ".$webjel."rendeles.id";
					$res = $pdo->prepare($query_rend);
					$res->execute();
					$row_rend = $res -> fetch();

					// Email küldés
					$uzenet = '<p>A rendelés azonosítója: '.$_GET['txid'].'</p>';
					$uzenet .= '<p>Pénznem: HUF, Összeg: '.number_format($row_rend['rendelt_osszeg'] + $row_rend['szallitasi_dij'],0,'',' ').' Ft</p>';

					$uzenet .= '<p>Banki engedélyszám: '.$response.'</p>';
					
					$uzenet .= '<p>Kereskedő neve: Metroman Hungária Kft., Weboldal: '.$domain.'</p>';
					
					include $gyoker.'/module/mod_email_bank.php';
					require_once($gyoker.'/webshop/PHPMailer-master/PHPMailerAutoload.php');
					$mail = new PHPMailer();
					$mail->isHTML(true);
					$mail->CharSet = 'UTF-8';
					// $mail->addCustomHeader('MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n");
					if($conf_smtp == 1)
					{
						// SMTP
						$mail->IsSMTP(); // telling the class to use SMTP
						$mail->Host       = $smtp_host; // SMTP server
						$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
																   // 1 = errors and messages
																   // 2 = messages only
						$mail->SMTPAuth   = true;                  // enable SMTP authentication
						$mail->Host       = $smtp_host; // sets the SMTP server
						$mail->Port       = $smtp_port;                    // set the SMTP port for the GMAIL server
						$mail->Username   = $smtp_user; // SMTP account username
						$mail->Password   = $smtp_pass;        // SMTP account password
						$mail->SetFrom($smtp_email, $smtp_name);
						$mail->AddReplyTo($smtp_email, $smtp_name);
					}
					else
					{
						$mail->SetFrom($email, $webnev);
					}

					$mail->AddAddress($row_rend['vasarlo_email'], $row_rend['vasarlo_nev']);
					$mail->Subject = "Sikeres bankkártyás fizetés";
					$htmlmsg = $mess;
					//file_put_contents('filename.php', $htmlmsg);
					$mail->Body = $htmlmsg;
					if(!$mail->Send()) {
						echo "Mailer Error: " . $mail->ErrorInfo;
					}