<?php 
  session_start();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
      <?php 
        $oldal = 'fogyaszto-barat';
        include '../config.php';
        include $gyoker.'/module/mod_head.php';
      ?>

      <title>Fogyasztó Barát tájékoztató | <?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">      

      <style>
        #gendoc_at * {
          font-size: unset;
        }

        #gendoc_at h1, #gendoc_at h1 {
            padding: unset;
            margin: unset;
        }        
      </style>
  </head>
  <body>
    <?php include $gyoker.'/module/mod_body_start.php'; ?>
    <div class="page">
      <?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(../images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                <h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5 fejresz-title">Fogyasztó Barát tájékoztató</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">
            <ul class="breadcrumbs-custom-path">
              <li><a href="<?=$domain?>">Főoldal</a></li>
              <li class="active">Fogyasztó Barát tájékoztató</li>
            </ul> 
          </div>
        </div>
      </section>

      <section class="section section-lg bg-default">
        <div class="container">         
          <div class="row row-30 justify-content-center">
            <div class="col-md-12">
                <script id="barat_script">var st = document.createElement("script");st.src = "//admin.fogyasztobarat.hu/e-api.js";st.type = "text/javascript";st.setAttribute("data-id", "ZRFI4WU7");st.setAttribute("id", "fbarat-embed");st.setAttribute("data-type", "def");var s = document.getElementById("barat_script");s.parentNode.insertBefore(st, s);</script>
            </div>
          </div>
        </div>
      </section>      

      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>

  </body>
</html>