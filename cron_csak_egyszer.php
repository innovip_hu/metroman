<?php
	// folyamatában látszódjon a kiírása
	ob_implicit_flush(true);
	ob_end_flush();

	ini_set('xdebug.var_display_max_depth', -1);
	ini_set('xdebug.var_display_max_children', -1);
	ini_set('xdebug.var_display_max_data', -1);

	include 'config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}


	$query = "SELECT id FROM ".$webjel."termekek";
	foreach ($pdo->query($query) as $key => $value)
	{
		$kiemelt = 0;
		$kifutott = 0;
		$ppp_tiltas = 0;

		$query = "SELECT ertek FROM ".$webjel."termek_termek_parameter_ertekek WHERE termek_parameter_id=1 AND termek_id=?"; //kiemelt
		$res = $pdo->prepare($query);
		$res->execute(array($value['id']));
		$row_param = $res -> fetch();		

		if (isset($row_param['ertek']))
		{
			$kiemelt = $row_param['ertek'];
		}

		$query = "SELECT ertek FROM ".$webjel."termek_termek_parameter_ertekek WHERE termek_parameter_id=2 AND termek_id=?"; //ppp_tiltas
		$res = $pdo->prepare($query);
		$res->execute(array($value['id']));
		$row_param = $res -> fetch();		

		if (isset($row_param['ertek']))
		{
			$ppp_tiltas = $row_param['ertek'];
		}


		$query = "SELECT ertek FROM ".$webjel."termek_termek_parameter_ertekek WHERE termek_parameter_id=4 AND termek_id=?"; //kfutott
		$res = $pdo->prepare($query);
		$res->execute(array($value['id']));
		$row_param = $res -> fetch();		

		if (isset($row_param['ertek']))
		{
			$kifutott = $row_param['ertek'];
		}
		/*

		$updatecommand = "UPDATE ".$webjel."termekek SET kiemelt=?, kifutott=?, ppp_tiltas=? WHERE id=?";
		$result = $pdo->prepare($updatecommand);
		$result->execute(array($kiemelt,$kifutott,$ppp_tiltas,$value['id']));		

		*/

		$updatecommand = "UPDATE ".$webjel."termekek SET kifutott=? WHERE id=?";
		$result = $pdo->prepare($updatecommand);
		$result->execute(array($kifutott,$value['id']));		

	}


?>

							  