<?php
	// folyamatában látszódjon a kiírása
	ob_implicit_flush(true);
	ob_end_flush();

	ini_set('xdebug.var_display_max_depth', -1);
	ini_set('xdebug.var_display_max_children', -1);
	ini_set('xdebug.var_display_max_data', -1);

	include 'config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	if (date('H:i:s') < '22:00:00' && date('H:i:s') > '04:00:00')
	{

		$options = array(
		  	'location' => $ovip_soap_link,
		 	'uri' => $ovip_soap_link,
		  	'encoding' => 'UTF-8',
		  	'trace' => 1 //csak hibakereséshez kell
		);	

		$request = 'getParams';

		$signature = hash('sha256', trim($ovip_ugyfel_id . $ovip_webshop_id . $ovip_authCode . $request . $ovip_ip_cim)); //hash generálása 

		$request = array(
			'request' => $request,
			'user_id' => $ovip_ugyfel_id,
			'signature' => $signature,
			'webshop_id' => $ovip_webshop_id
		);

		try {		

			$client = new SoapClient(NULL,$options);
			
			$tetelek = $client->getRequest($request);

			if (is_array($tetelek))
			{
				foreach ($tetelek as $elem) 
				{
					$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termek_uj_parameterek WHERE ovip_id = ".$elem['ovip_parameter_id']);
					$res->execute();
					$rownum = $res->fetchColumn();

					if ($rownum == 0) {
						$insertcommand = "INSERT INTO ".$webjel."termek_uj_parameterek (nev, ovip_id) VALUES (:nev, ".$elem['ovip_parameter_id'].")";
						$result = $pdo->prepare($insertcommand);
						$result->execute(array(':nev'=>$elem['parameter_name']));				
					}
					else
					{
						$updatecommand = "UPDATE ".$webjel."termek_uj_parameterek SET nev=? WHERE ovip_id=".$elem['ovip_parameter_id'];
						$result = $pdo->prepare($updatecommand);
						$result->execute(array($elem['parameter_name']));
					}

				}

			    echo "Parameterek kesz<br/>";

			}
			else
			{
				echo "Parameterek HIBA:".$tetelek."<br/>";
			}

		} catch (Exception $e) {

			echo "Parameterek HIBA: ".$e->getMessage()."<br/>";

		}

		$request = 'getParamValues';

		$signature = hash('sha256', trim($ovip_ugyfel_id . $ovip_webshop_id . $ovip_authCode . $request . $ovip_ip_cim)); //hash generálása 

		$request = array(
			'extra_data' => date('Y-m-d',strtotime('-1 day')),
			'request' => $request,
			'user_id' => $ovip_ugyfel_id,
			'signature' => $signature,
			'webshop_id' => $ovip_webshop_id
		);

		try {		

			$client = new SoapClient(NULL,$options);
			
			$tetelek = $client->getRequest($request);

			if (is_array($tetelek))
			{
				foreach ($tetelek as $elem)
				{
					$query = "SELECT id FROM ".$webjel."termek_uj_parameterek WHERE ovip_id=".$elem['ovip_parameter_id'];
					$res = $pdo->prepare($query);
					$res->execute();
					$row = $res -> fetch();

					$query_term = "SELECT id FROM ".$webjel."termekek WHERE ovip_id=".$elem['ovip_product_id'];
					$res = $pdo->prepare($query_term);
					$res->execute();
					$row_term = $res -> fetch();

					$query_param = "SELECT * FROM ".$webjel."termek_uj_parameter_ertekek WHERE ovip_id='".$elem['ovip_parameter_value_id']."' AND ovip_termek_id=".$elem['ovip_product_id'];
					$res = $pdo->prepare($query_param);
					$res->execute();
					$row_param = $res -> fetch();

					if (isset($row_param['id']) && $row_param['id'] > 0) { // van már ilyen paraméter

						$updatecommand = "UPDATE ".$webjel."termek_uj_parameter_ertekek SET ertek=? WHERE id=?";
						$result = $pdo->prepare($updatecommand);
						$result->execute(array($elem['parameter_value'],$row_param['id']));	
					}
					else { // Nincs még, létre kell hozni (az ovip_id a paraméter id-je lesz, mert mincsegy mi van ott csak ne 0 legyen)

						$insertcommand = "INSERT INTO ".$webjel."termek_uj_parameter_ertekek (parameter_id, termek_id, ertek, ovip_id, ovip_termek_id) VALUES (:parameter_id, :termek_id, :ertek, :ovip_id, :ovip_termek_id)";
						$result = $pdo->prepare($insertcommand);
						$result->execute(array(':parameter_id'=>$row['id'],
											':termek_id'=>$row_term['id'],
											':ertek'=>$elem['parameter_value'],
											':ovip_id'=>$elem['ovip_parameter_value_id'],
											':ovip_termek_id'=>$elem['ovip_product_id']));
					}
				}

			    echo "Parameterek ertekek kesz<br/>";

			}
			else
			{
				echo "Parameterek ertekek HIBA:".$tetelek."<br/>";
			}

		} catch (Exception $e) {

			echo "Parameterek ertekek HIBA: ".$e->getMessage()."<br/>";

		}	


		$request = 'getCategories';

		$signature = hash('sha256', trim($ovip_ugyfel_id . $ovip_webshop_id . $ovip_authCode . $request . $ovip_ip_cim)); //hash generálása 

		$request = array(
			'request' => $request,
			'user_id' => $ovip_ugyfel_id,
			'signature' => $signature,
			'webshop_id' => $ovip_webshop_id
		);

		try {		

			$client = new SoapClient(NULL,$options);
			
			$tetelek = $client->getRequest($request);

			if (is_array($tetelek))
			{

				$pdo->exec("UPDATE ".$webjel."term_csoportok SET lathato=0 WHERE ovip_id != 0"); //láthatatlanná tesz mindent szinkron előtt

				foreach ($tetelek as $elem)
				{
					$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE ovip_id = ".$elem['ovip_category_id']);
					$res->execute();
					$rownum = $res->fetchColumn();
					if ($rownum == 0)
					{
						$insertcommand = "INSERT INTO ".$webjel."term_csoportok (nev, seo_title, seo_description, sorrend, lathato, leiras, ovip_id, ovip_csoport_id) VALUES (:nev, :seo_title, :seo_description, :sorrend, 1, :leiras, :ovip_id, :ovip_csoport_id)";
						$result = $pdo->prepare($insertcommand);
						$result->execute(array(':nev'=> $elem['name'],
											   ':seo_title' => $elem['seo_title'],
											   ':seo_description' => $elem['seo_description'],
											   ':sorrend' => $elem['order'],
											   ':leiras' => $elem['description'],
											   ':ovip_id' => $elem['ovip_category_id'],
											   ':ovip_csoport_id' => $elem['parent_category_id']));

						if ($elem['image'] != '') {
							$insertcommand = "INSERT INTO ".$webjel."termek_kepek_letolteni (kep, ovip_csoport_id) VALUES (:kep, :ovip_csoport_id)";
							$result = $pdo->prepare($insertcommand);
							$result->execute(array(':kep'=>$elem['image'],
									  			   ':ovip_csoport_id'=>$elem['ovip_category_id']));
						}						


					} else //update
					{
						$updatecommand = "UPDATE ".$webjel."term_csoportok SET nev=?, seo_title=?, seo_description=?, sorrend=?, lathato=1, leiras=?, ovip_csoport_id=? WHERE ovip_id =".$elem['ovip_category_id'];
						$result = $pdo->prepare($updatecommand);
						$result->execute(array($elem['name'], $elem['seo_title'], $elem['seo_description'], $elem['order'], $elem['description'], $elem['parent_category_id']));

						if ($elem['image'] != '') {
							$kepcheck = pathinfo($elem['image'],PATHINFO_BASENAME);

							$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE ovip_id=? AND kep=?");
							$res->execute(array($elem['ovip_category_id'], $kepcheck));
							$rownum_kep = $res->fetchColumn();					

							$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termek_kepek_letolteni WHERE kep=?");
							$res->execute(array($elem['image']));
							$rownum_kep_letoltes = $res->fetchColumn();

							if ($rownum_kep == 0 && $rownum_kep_letoltes == 0) {

								$insertcommand = "INSERT INTO ".$webjel."termek_kepek_letolteni (kep, ovip_csoport_id) VALUES (:kep, :ovip_csoport_id)";
								$result = $pdo->prepare($insertcommand);
								$result->execute(array(':kep'=>$elem['image'],
										  			   ':ovip_csoport_id'=>$elem['ovip_category_id']));

							}						
						}						
					}			
				}
				
				$query = "SELECT * FROM ".$webjel."term_csoportok WHERE ovip_id != 0 AND ovip_csoport_id != 0";
				foreach ($pdo->query($query) as $value)
				{
					$csopque = "SELECT * FROM ".$webjel."term_csoportok WHERE ovip_id=".$value['ovip_csoport_id'];
					$res = $pdo->prepare($csopque);
					$res->execute();
					$row = $res -> fetch();

					$pdo->exec("UPDATE ".$webjel."term_csoportok SET csop_id = ".$row['id']." WHERE id = ".$value['id']."");				
				}

				$query = 'SELECT * FROM '.$webjel.'term_csoportok WHERE nev_url=""';
				foreach ($pdo->query($query) as $row)
				{
					$nev_url = $row['nev'];
					$patterns = array();
					$patterns[0] = '/á/';
					$patterns[1] = '/é/';
					$patterns[2] = '/í/';
					$patterns[3] = '/ó/';
					$patterns[4] = '/ö/';
					$patterns[5] = '/ő/';
					$patterns[6] = '/ú/';
					$patterns[7] = '/ü/';
					$patterns[8] = '/ű/';
					$patterns[9] = '/Á/';
					$patterns[10] = '/É/';
					$patterns[11] = '/Í/';
					$patterns[12] = '/Ó/';
					$patterns[13] = '/Ö/';
					$patterns[14] = '/Ő/';
					$patterns[15] = '/Ú/';
					$patterns[16] = '/Ü/';
					$patterns[17] = '/Ű/';
					$patterns[18] = '/ /';
					$replacements = array();
					$replacements[0] = 'a';
					$replacements[1] = 'e';
					$replacements[2] = 'i';
					$replacements[3] = 'o';
					$replacements[4] = 'o';
					$replacements[5] = 'o';
					$replacements[6] = 'u';
					$replacements[7] = 'u';
					$replacements[8] = 'u';
					$replacements[9] = 'A';
					$replacements[10] = 'E';
					$replacements[11] = 'I';
					$replacements[12] = 'O';
					$replacements[13] = 'O';
					$replacements[14] = 'O';
					$replacements[15] = 'U';
					$replacements[16] = 'U';
					$replacements[17] = 'U';
					$replacements[18] = '-';
					$nev_url = preg_replace($patterns, $replacements, $nev_url);
					$nev_url = preg_replace('/[^a-zA-Z0-9_-]/', '', $nev_url);
					$nev_url = preg_replace('/-+/', '-', $nev_url);
					if ($nev_url == '')
					{
						$nev_url = $row['id'];
					}
					// Egyezőség vizsgálata
					$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE nev_url = '".$nev_url."'");
					$res->execute();
					$rownum = $res->fetchColumn();
					if ($rownum > 0) // Ha van már ilyen nevű
					{
						$nev_url = $nev_url.'-'.$row['id'];
					}
					$updatecommand = "UPDATE ".$webjel."term_csoportok SET nev_url = '".$nev_url."' WHERE id = ".$row["id"] ;
					$result = $pdo->prepare($updatecommand);
					$result->execute();
				}


			    echo "Kategoriak kesz<br/>";

			}
			else
			{
				echo "Kategoriak HIBA:".$tetelek."<br/>";
			}

		} catch (Exception $e) {

			echo "Kategoriak HIBA: ".$e->getMessage()."<br/>";

		}	


		$request = 'getProducts';

		$signature = hash('sha256', trim($ovip_ugyfel_id . $ovip_webshop_id . $ovip_authCode . $request . $ovip_ip_cim)); //hash generálása 

		$request = array(
			'extra_data' => date('Y-m-d',strtotime('-1 day')),
			'request' => $request,
			'user_id' => $ovip_ugyfel_id,
			'signature' => $signature,
			'webshop_id' => $ovip_webshop_id
		);

		try {		

			$client = new SoapClient(NULL,$options);
			
			$tetelek = $client->getRequest($request);

			if (is_array($tetelek))
			{
				foreach ($tetelek as $elem)
				{
					$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE ovip_id = ".$elem['ovip_product_id']);
					$res->execute();
					$rownum = $res->fetchColumn();

					$lathatosag = $elem['webshop_visible'];	

					$beszallito_id = 0;

					$orderable = $elem['orderable'] == 1 ? 1 : 0;

					if (!empty($elem['manufacturer']))
					{
						$query = "SELECT id FROM ".$webjel."beszallito WHERE nev=?";
						$res = $pdo->prepare($query);
						$res->execute(array($elem['manufacturer']));
						$row_gyarto = $res -> fetch();					

						if (isset($row_gyarto['id']))
						{
							$beszallito_id = $row_gyarto['id'];
						}
						else
						{
							$insertcommand = "INSERT INTO ".$webjel."beszallito (nev) VALUES (:nev)";
							$result = $pdo->prepare($insertcommand);
							$result->execute(array(':nev'=>$elem['manufacturer']));	

							$beszallito_id = $pdo->lastInsertId();		
						}
					}

					if ($rownum == 0 && $elem['webshop_visible'] == 1) {

						$query = "SELECT * FROM ".$webjel."afa WHERE afa=".$elem['tax'];
						$res = $pdo->prepare($query);
						$res->execute();
						$row = $res -> fetch();

						$katquery = "SELECT * FROM ".$webjel."term_csoportok WHERE ovip_id=".$elem['ovip_category_id']."";
						$res = $pdo->prepare($katquery);
						$res->execute();
						$row_kat = $res -> fetch();

						if ($elem['ovip_category_id'] == 0) {
							$row_kat['id'] = 0;
						}

						$insertcommand = "INSERT INTO ".$webjel."termekek (beszallito_id,csop_id, nev, seo_title, seo_description, vonalkod, cikkszam, ar, afa, lathato, akciosar, akcio_tol, akcio_ig, ovip_id, ovip_csoport_id, rovid_leiras, leiras, ovip_torolt, rendelheto, ovip_szinkron) 
							VALUES (:beszallito_id, ".$row_kat['id'].", :nev, :seo_title, :seo_description, :vonalkod, :cikkszam, :ar, :afa, :lathato, :akciosar, :akcio_tol, :akcio_ig, :ovip_id, :ovip_csoport_id, :rovid_leiras, :hosszu_leiras, :ovip_torolt, :rendelheto, NOW())";
						$result = $pdo->prepare($insertcommand);
						$result->execute(array(':nev'=> $elem['name'],
											   ':seo_title' => $elem['seo_title'],
											   ':seo_description' => $elem['seo_description'],
											   ':beszallito_id' => $beszallito_id,
											   ':vonalkod' => $elem['bar_code'],
											   ':cikkszam' => $elem['sku'],
											   ':ar' => $elem['gross_price'],
											   ':afa' => $row['id'],
											   ':lathato' => $lathatosag,
											   ':akciosar' => $elem['gross_sale_price'],
											   ':akcio_tol' => $elem['sale_start'],
											   ':akcio_ig' => $elem['sale_end'],
											   ':ovip_id' => $elem['ovip_product_id'],
											   ':ovip_csoport_id' => $elem['ovip_category_id'],
											   ':rovid_leiras' => $elem['short_description'],
											   ':hosszu_leiras' => $elem['long_description'],
											   ':ovip_torolt' => $elem['deleted'],
											   ':rendelheto' => $orderable
											   ));

						$last_id = $pdo->lastInsertId();

						$paramquery = "SELECT * FROM ".$webjel."termek_uj_parameter_ertekek WHERE ovip_termek_id=".$elem['ovip_product_id'];
						foreach ($pdo->query($paramquery) as $value) {
							$pdo->exec("UPDATE ".$webjel."termek_uj_parameter_ertekek SET termek_id = ".$last_id." WHERE ovip_termek_id = ".$elem['ovip_product_id']."");	
						}

					} 
					else //update
					{
						$query = "SELECT * FROM ".$webjel."afa WHERE afa=".$elem['tax'];
						$res = $pdo->prepare($query);
						$res->execute();
						$row = $res -> fetch();

						$katquery = "SELECT * FROM ".$webjel."term_csoportok WHERE ovip_id=".$elem['ovip_category_id']."";
						$res = $pdo->prepare($katquery);
						$res->execute();
						$row_kat = $res -> fetch();

						if ($elem['ovip_category_id'] == 0) {
							$row_kat['id'] = 0;
						}

						if ($elem['deleted'] == 1)
						{
							$updatecommand = "UPDATE ".$webjel."termekek SET csop_id=?, nev=?, seo_title=?, seo_description=?, vonalkod=?, cikkszam=?, ar=?, afa=?, akciosar=?, akcio_tol=?, akcio_ig=?, ovip_csoport_id=?, rovid_leiras=?, leiras=?, ovip_szinkron=now(), beszallito_id=?, ovip_torolt=?, rendelheto=? WHERE ovip_id=".$elem['ovip_product_id'];
							$result = $pdo->prepare($updatecommand);
							$result->execute(array($row_kat['id'], $elem['name'], $elem['seo_title'], $elem['seo_description'], $elem['bar_code'], $elem['sku'], $elem['gross_price'], $row['id'], $elem['gross_sale_price'], $elem['sale_start'], $elem['sale_end'], $elem['ovip_category_id'], $elem['short_description'], $elem['long_description'], $beszallito_id, $elem['deleted'], $orderable));
						}
						else
						{
							$updatecommand = "UPDATE ".$webjel."termekek SET csop_id=?, nev=?, seo_title=?, seo_description=?, vonalkod=?, cikkszam=?, ar=?, afa=?, lathato=?, akciosar=?, akcio_tol=?, akcio_ig=?, ovip_csoport_id=?, rovid_leiras=?, leiras=?, ovip_szinkron=now(), beszallito_id=?, ovip_torolt=?, rendelheto=? WHERE ovip_id=".$elem['ovip_product_id'];
							$result = $pdo->prepare($updatecommand);
							$result->execute(array($row_kat['id'], $elem['name'], $elem['seo_title'], $elem['seo_description'], $elem['bar_code'], $elem['sku'], $elem['gross_price'], $row['id'], $lathatosag, $elem['gross_sale_price'], $elem['sale_start'], $elem['sale_end'], $elem['ovip_category_id'], $elem['short_description'], $elem['long_description'], $beszallito_id, $elem['deleted'], $orderable));
						}

						$paramquery = "SELECT *, ".$webjel."termekek.id as id
							FROM ".$webjel."termek_uj_parameter_ertekek
							INNER JOIN ".$webjel."termekek
							ON ".$webjel."termekek.ovip_id = ".$webjel."termek_uj_parameter_ertekek.ovip_termek_id
							WHERE ".$webjel."termek_uj_parameter_ertekek.ovip_termek_id=".$elem['ovip_product_id'];
						foreach ($pdo->query($paramquery) as $value) {
							$pdo->exec("UPDATE ".$webjel."termek_uj_parameter_ertekek SET termek_id = ".$value['id']." WHERE ovip_termek_id = ".$elem['ovip_product_id']."");	
						}				

					}
				}

				$query = 'SELECT * FROM '.$webjel.'termekek WHERE nev_url=""';
				foreach ($pdo->query($query) as $row)
				{
					$nev_url = $row['nev'];
					$patterns = array();
					$patterns[0] = '/á/';
					$patterns[1] = '/é/';
					$patterns[2] = '/í/';
					$patterns[3] = '/ó/';
					$patterns[4] = '/ö/';
					$patterns[5] = '/ő/';
					$patterns[6] = '/ú/';
					$patterns[7] = '/ü/';
					$patterns[8] = '/ű/';
					$patterns[9] = '/Á/';
					$patterns[10] = '/É/';
					$patterns[11] = '/Í/';
					$patterns[12] = '/Ó/';
					$patterns[13] = '/Ö/';
					$patterns[14] = '/Ő/';
					$patterns[15] = '/Ú/';
					$patterns[16] = '/Ü/';
					$patterns[17] = '/Ű/';
					$patterns[18] = '/ /';
					$patterns[19] = '/\//';
					$patterns[20] = '/"/';
					$replacements = array();
					$replacements[0] = 'a';
					$replacements[1] = 'e';
					$replacements[2] = 'i';
					$replacements[3] = 'o';
					$replacements[4] = 'o';
					$replacements[5] = 'o';
					$replacements[6] = 'u';
					$replacements[7] = 'u';
					$replacements[8] = 'u';
					$replacements[9] = 'A';
					$replacements[10] = 'E';
					$replacements[11] = 'I';
					$replacements[12] = 'O';
					$replacements[13] = 'O';
					$replacements[14] = 'O';
					$replacements[15] = 'U';
					$replacements[16] = 'U';
					$replacements[17] = 'U';
					$replacements[18] = '-';
					$replacements[19] = '-';
					$replacements[20] = 'col';
					$nev_url = preg_replace($patterns, $replacements, $nev_url);
					$nev_url = preg_replace('/[^a-zA-Z0-9_-]/', '', $nev_url);
					$nev_url = preg_replace('/-+/', '-', $nev_url);
					if ($nev_url == '')
					{
						$nev_url = $row['id'];
					}
					// Egyezőség vizsgálata
					$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE nev_url = '".$nev_url."'");
					$res->execute();
					$rownum = $res->fetchColumn();
					if ($rownum > 0) // Ha van már ilyen nevű
					{
						$nev_url = $nev_url.'-'.$row['id'];
					}
					$updatecommand = "UPDATE ".$webjel."termekek SET nev_url = '".$nev_url."' WHERE id = ".$row["id"] ;
					$result = $pdo->prepare($updatecommand);
					$result->execute();	
				}	
			    echo "Termekek kesz<br/>";

			}
			else
			{
				echo "Termekek HIBA:".$tetelek."<br/>";
			}

		} catch (Exception $e) {

			echo "Termekek HIBA: ".$e->getMessage()."<br/>";

		}


	}


?>

							  