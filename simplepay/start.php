<?php

/**
 *  Copyright (C) 2019 OTP Mobil Kft.
 *
 *  PHP version 7
 *
 *  This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses
 *
 * @category  SDK
 * @package   SimplePayV2_SDK
 * @author    SimplePay IT Support <itsupport@otpmobil.com>
 * @copyright 2019 OTP Mobil Kft.
 * @license   http://www.gnu.org/licenses/gpl-3.0.html  GNU GENERAL PUBLIC LICENSE (GPL V3.0)
 * @link      http://simplepartner.hu/online_fizetesi_szolgaltatas.html
 */
 
//Optional error riporting
error_reporting(E_ALL);
ini_set('display_errors', '1');

//Import config data    
require_once 'src/config.php';

//Import SimplePayment class
require_once 'src/SimplePayV21.php';

$trx = new SimplePayStart;

$currency = 'HUF';
$trx->addData('currency', $currency);

$trx->addConfig($config);

//ORDER PRICE/TOTAL
//-----------------------------------------------------------------------------------------
$trx->addData('total', $fizetendo);    

//ORDER ITEMS
//-----------------------------------------------------------------------------------------  
    
$query = "SELECT * FROM ".$webjel."rendeles_tetelek WHERE rendeles_id=".$last_rendeles_id;
foreach ($pdo->query($query) as $key => $value)
{
    $ar = $value['term_akcios_ar'] != 0 ? $value['term_akcios_ar'] : $value['term_ar'];
    $trx->addItems(
        array(
            'ref' => $value['term_id'],
            'title' => $value['term_nev'],
            'description' => '',
            'amount' => $value['term_db'],
            'price' => $ar,
            'tax' => 0
            )
    );       
}


// SHIPPING COST
//-----------------------------------------------------------------------------------------
$trx->addData('shippingCost', $szall_kolts);


// DISCOUNT
//-----------------------------------------------------------------------------------------
//$trx->addData('discount', 10);


// ORDER REFERENCE NUMBER 
// uniq oreder reference number in the merchant system
//-----------------------------------------------------------------------------------------
$trx->addData('orderRef', $last_rendeles_id);


// CUSTOMER
// customer's name
//-----------------------------------------------------------------------------------------
$trx->addData('customer', $szla_vasarlo_nev);


// EMAIL
// customer's email
//-----------------------------------------------------------------------------------------
$trx->addData('customerEmail', $szla_vasarlo_email);


// LANGUAGE
// HU, EN, DE, etc.
//----------------------------------------------------------------------------------------- 
$trx->addData('language', 'HU');


// TWO STEP
// true, or false
// The this field does not exist is equal false value 
// possibility of two step needs IT support setting
//-----------------------------------------------------------------------------------------
/*
$twoStep = false;
if (isset($_REQUEST['twoStep'])) {
    $twoStep = true;
}
$trx->addData('twoStep', $twoStep);
*/


// TIMEOUT
// 2018-09-15T11:25:37+02:00
//-----------------------------------------------------------------------------------------
$timeoutInSec = 600;
$timeout = @date("c", time() + $timeoutInSec);
$trx->addData('timeout', $timeout);

                        
// METHODS
// CARD or WIRE
//-----------------------------------------------------------------------------------------
$trx->addData('methods', array('CARD'));


// REDIRECT URLs
//-----------------------------------------------------------------------------------------

// common URL for all result
$trx->addData('url', $config['URL']);

// uniq URL for every result type
/*
    $trx->addGroupData('urls', 'success', $config['URLS_SUCCESS']);
    $trx->addGroupData('urls', 'fail', $config['URLS_FAIL']);
    $trx->addGroupData('urls', 'cancel', $config['URLS_CANCEL']);
    $trx->addGroupData('urls', 'timeout', $config['URLS_TIMEOUT']);
*/


// INVOICE DATA
//-----------------------------------------------------------------------------------------
$trx->addGroupData('invoice', 'name', $szla_vasarlo_nev);
$trx->addGroupData('invoice', 'company', '');
$trx->addGroupData('invoice', 'country', 'hu');
//$trx->addGroupData('invoice', 'state', 'Budapest');
$trx->addGroupData('invoice', 'city', $szla_vasarlo_varos);
$trx->addGroupData('invoice', 'zip', $szla_vasarlo_irszam);
$trx->addGroupData('invoice', 'address', $szla_vasarlo_cim);
$trx->addGroupData('invoice', 'address2', '');
$trx->addGroupData('invoice', 'phone', $telefon);

// DELIVERY DATA
//-----------------------------------------------------------------------------------------
/*
$trx->addGroupData('delivery', 'name', 'SimplePay V2 Tester');
$trx->addGroupData('delivery', 'company', '');
$trx->addGroupData('delivery', 'country', 'hu');
$trx->addGroupData('delivery', 'state', 'Budapest');
$trx->addGroupData('delivery', 'city', 'Budapest');
$trx->addGroupData('delivery', 'zip', '1111');
$trx->addGroupData('delivery', 'address', 'Address 1');
$trx->addGroupData('delivery', 'address2', '');
$trx->addGroupData('delivery', 'phone', '06203164978');
*/


//payment starter element
// auto: (immediate redirect)
// button: (default setting)
// link: link to payment page
//-----------------------------------------------------------------------------------------
$trx->formDetails['element'] = 'auto';


//create transaction in SimplePay system
//-----------------------------------------------------------------------------------------
$trx->runStart();


//create html form for payment using by the created transaction
//-----------------------------------------------------------------------------------------
$trx->getHtmlForm();


//print form
//-----------------------------------------------------------------------------------------
print $trx->returnData['form'];


// test data
//-----------------------------------------------------------------------------------------

/*
print "API CALL";
print "<pre>";
print_r($trx->getTransactionBase());
print "</pre>";

print "API RESULT";
print "<pre>";
print_r($trx->getReturnData());
print "</pre>";
*/