<?php

/**
 *  Copyright (C) 2020 OTP Mobil Kft.
 *
 *  PHP version 7
 *
 *  This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  SDK
 * @package   SimplePayV2_SDK
 * @author    SimplePay IT Support <itsupport@otpmobil.com>
 * @copyright 2020 OTP Mobil Kft.
 * @license   http://www.gnu.org/licenses/gpl-3.0.html  GNU GENERAL PUBLIC LICENSE (GPL V3.0)
 * @link      http://simplepartner.hu/online_fizetesi_szolgaltatas.html
 */

//Optional error riporting
error_reporting(E_ALL);
ini_set('display_errors', '1');

 //Import config data
require_once 'src/config.php';

//Import SimplePayment class
require_once 'src/SimplePayV21.php';

$trx = new SimplePayBack;

$trx->addConfig($config);


//result
//-----------------------------------------------------------------------------------------
$result = array();
if (isset($_REQUEST['r']) && isset($_REQUEST['s'])) {
    if ($trx->isBackSignatureCheck($_REQUEST['r'], $_REQUEST['s'])) {
        $result = $trx->getRawNotification();
    }
}


// test data
//-----------------------------------------------------------------------------------------
/*
print "<pre>";
print '<a href="index.html">INDEX</a>';
print "<br/><br/>";
print_r($result);
print "</pre>";
*/

if (isset($result['e']) && $result['e'] == 'CANCEL')
{
    print '<h4 style="text-align:center;"">Sikertelen tranzakció!</h4>';
    print '<p style="text-align:center; color:#D27290;">Ön megszakította a fizetést</p>';
}
else if (isset($result['e']) && $result['e'] == 'TIMEOUT')
{
    print '<h4 style="text-align:center;"">Sikertelen tranzakció!</h4>';
    print '<p style="text-align:center; color:#D27290;">Ön túllépte a tranzakció elindításának lehetséges maximális idejét.</p>';
}
else if (isset($result['e']) && $result['e'] == 'FAIL')
{
    print '<h4 style="text-align:center;"">Sikertelen tranzakció!</h4>';
    print '<p style="text-align:center;">SimplePay tranzakció azonosító: '.$result['t'].'</p>';
    print '<p style="text-align:center;">Kérjük, ellenőrizze a tranzakció során megadott adatok helyességét. Amennyiben minden adatot helyesen adott meg, a visszautasítás okának kivizsgálása kapcsán kérjük, szíveskedjen kapcsolatba lépni kártyakibocsátó bankjával.</p>';
}
else if (isset($result['e']) && $result['e'] == 'SUCCESS')
{
    header('Location: '.$domain.'/koszonjuk/?rendeles_id='.$result['o'].'&tranzakcio_azonosito='.$result['t']);
    exit();
    
    // print '<h4 style="text-align:center;"">Sikeres tranzakció!</h4>';
    // print '<p style="text-align:center;">SimplePay tranzakció azonosító: '.$result['t'].'</p>';

    // print '<h3 class="mt-5" style="text-align: center;"><i class="mdi mdi-checkbox-marked-circle text-green"></i> Rendelésedet sikeresen rögzítettük!</h3>
    //                 <h4 style="text-align: center;">Hamarosan e-mail-ben is megküldjük a vásárlásod adatait.</h4>
    //                 <h4 style="text-align: center;">További szép napot kívánunk!</h4>';

    // echo '<div class="mt-5">';
    // echo '<h4 class="text-center mb-3 h5">Ezeket a termékeket vásároltad meg</h4>';


    // $query = "SELECT
    //         t.id,
    //         t.nev,
    //         t.csop_id,
    //         t.cikkszam,
    //         t.nev_url,
    //         csop.nev_url as csopnev_url
    // FROM ".$webjel."termekek t
    // INNER JOIN ".$webjel."term_csoportok csop
    // ON csop.id = t.csop_id
    // INNER JOIN ".$webjel."rendeles_tetelek rt
    // ON rt.term_id = t.id
    // WHERE rt.rendeles_id = ".$result['o']."
    // GROUP BY rt.id";
    // foreach ($pdo->query($query) as $key => $value) {

    //     $link = $domain.'/termekek/'.$value['csopnev_url'].'/'.$value['nev_url'];

    //     $query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$value['id']." AND spec=0 ORDER BY alap DESC LIMIT 1";
    //     $res = $pdo->prepare($query_kep);
    //     $res->execute();
    //     $row_kep = $res -> fetch();
    //     $alap_kep = $row_kep['kep'];
    //     if ($alap_kep == '') 
    //     {
    //         $kep_link = $domain.'/webshop/images/noimage.png';
    //     }
    //     elseif ($row_kep['ovip_termek_id'] !=0)
    //     {
    //         if($row_kep['thumb'] != '')
    //         {
    //             $kep = $row_kep['thumb'];
    //         }
    //         else
    //         {
    //             $kep = $row_kep['kep'];
    //         }
    //         $kep_link = $kep;
    //     }
    //     else
    //     {
    //         if($row_kep['thumb'] != '')
    //         {
    //             $kep = $row_kep['thumb'];
    //         }
    //         else
    //         {
    //             $kep = $row_kep['kep'];
    //         }
    //         $kep_link = $domain.'/images/termekek/'.$kep;
    //     }   

    //     if ($key != 0)
    //     {
    //           echo '<hr class="my-3">';
    //     }                       

    //     include $gyoker.'/webshop/termek_csik_rendeles.php';
    // }       

    // echo '</div>';   

    // $rendeles_id_etracking = $result['o'];
    // include $gyoker.'/webshop/ecommerce_tracking.php';         

                        
}