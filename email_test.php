<?php
	include 'config.php';

// PDO
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}	

	// Email küldés

	$jelszo = 'asdf';


	$mess =  "Kedves Látogató!". "<br><br>" ;
	$mess .=  "Az új jelszavad: ".$jelszo."<br>Jelszavad megváltoztathatod a Profil menüpontban.<br><br>" ;
	$mess .=  "<br><br>Üdvözlettel: <a href='".$domain."' style='color: blue;' target='_blank'>".$webnev."</a>" ;

	$htmlmsg = '<html>
		<body>
			'.$mess.'
		</body>
	</html>';	

	require_once($gyoker.'/webshop/PHPMailer-master/PHPMailerAutoload.php');
	$mail = new PHPMailer();
	$mail->isHTML(true);
	$mail->CharSet = 'UTF-8';
	// $mail->addCustomHeader('MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n");
	if($conf_smtp == 1)
	{
		// SMTP
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host       = $smtp_host; // SMTP server
		$mail->SMTPDebug  = true;                     // enables SMTP debug information (for testing)
												   // 1 = errors and messages
												   // 2 = messages only
		$mail->SMTPAuth   = true;  
		$mail->SMTPSecure = $smtp_protokol;                 // enable SMTP authentication
		$mail->Host       = $smtp_host; // sets the SMTP server
		$mail->Port       = $smtp_port;                    // set the SMTP port for the GMAIL server
		$mail->Username   = $smtp_user; // SMTP account username
		$mail->Password   = $smtp_pass;        // SMTP account password
		$mail->SetFrom($smtp_email, $smtp_name);
		$mail->AddReplyTo($smtp_email, $smtp_name);
	}
	else
	{
		$mail->SetFrom($email, $webnev);
	}

	$mail->AddAddress('bartus.daniel@innovip.hu', 'teszt elek');
	$mail->Subject = "Új jelszó érkezett";
	$htmlmsg = $mess;
	//file_put_contents('filename.php', $htmlmsg);
	$mail->Body = $htmlmsg;
	if(!$mail->Send()) {
		echo "Mailer Error: " . $mail->ErrorInfo;
	}
?>