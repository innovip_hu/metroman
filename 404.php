<?php 
  header('HTTP/1.0 404 Not Found', true, 404);
  session_start();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
      <?php 
        $oldal = '404';
        include 'config.php';
        include $gyoker.'/module/mod_head.php';
      ?>

      <title><?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">      
  </head>
  <body>
    <?php include $gyoker.'/module/mod_body_start.php'; ?>
    <div class="page">
      <?php include $gyoker.'/module/mod_header.php'; ?>
 
      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(../images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                <h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5 fejresz-title">404</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">
            <ul class="breadcrumbs-custom-path">
              <li><a href="<?=$domain?>">Főoldal</a></li>
              <li class="active">404</li>
            </ul>
          </div>
        </div>
      </section>

      <section class="section section-lg bg-default text-center">
        <div class="container">

          <h5 class="title-4">Ez az oldal nem létezik vagy korábban megszűnt. Válasszon hasonlót terméket a kategóriákból vagy az alábbi termékekből.</h5>

          <a class="button button-gradient button-lg" href="<?=$domain?>">Tovább a főoldalra</a>  

          <h4 class="heading-4 pt-5">Ajánlott termékek</h4>
          <?php if (isset($_SERVER['REDIRECT_URL'])): ?>
            <?php include $gyoker.'/webshop/404_termekek.php'; ?>        
          <?php endif ?>

          <?php if (!isset($volt_404)): ?>
               <?php include $gyoker.'/webshop/akcios_termekek.php'; ?>
          <?php endif ?>   

        </div>
      </section>


      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>
    <script type="text/javascript" src="<?php print $domain; ?>/scripts/sajat.js"></script>
  </body>
</html>