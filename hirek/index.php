<?php 
  session_start();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
      <?php 
        $oldal = 'hirek';
        include '../config.php';
        include $gyoker.'/module/mod_head.php';

        if (file_exists($gyoker.'/hirek/css/hirek.css')) {
          echo '<link rel="stylesheet" href="'.$domain.'/hirek/css/hirek.css?v='.filemtime($gyoker.'/hirek/css/hirek.css').'">';
        }        
      ?>

  </head>
  <body>
    <?php include $gyoker.'/module/mod_body_start.php'; ?>
    <div class="page">
      <?php include $gyoker.'/module/mod_header.php'; ?>

      <?php 
        if (isset($_GET['nev_url']) && $_GET['nev_url'] != '')
        {
          $res = $pdo->prepare("SELECT * FROM ".$webjel."hirek WHERE nev_url=?");
          $res->execute(array($_GET['nev_url']));
          $row  = $res -> fetch();

          $oldal_cim = $row['cim'];
          $breacrumb = '<li><a href='.$domain.'/hirek/>Hírek</a></li>';
          $breacrumb .= '<li class="active">'.$row['cim'].'</li>';
        } else {
          $oldal_cim = 'Hírek';
          $breacrumb = '<li class="active">Hírek</li>';
        }
      ?>

      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(../images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                <h1 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5 heading-2 fejresz-title"><?=$oldal_cim?></h1>
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">
            <ul class="breadcrumbs-custom-path">
              <li><a href="<?=$domain?>">Főoldal</a></li>
              <?=$breacrumb?>
            </ul>
          </div>
        </div>
      </section>

      <section class="section section-lg bg-gray-100">
        <?php if (isset($_GET['nev_url'])): ?>
          <div class="container">
        <?php else: ?>  
          <div class="container-fluid">
        <?php endif ?>
        
          <div class="row row-xl row-30">

            <?php include $gyoker.'/webshop/hirek.php'; ?>

          </div>
        </div>
      </section>       

      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>    

    <script type="text/javascript" src="<?php print $domain; ?>/scripts/sajat.js"></script>
  </body>
</html>