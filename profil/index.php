<?php 
  session_start();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
      <?php 
        $oldal = 'profil';
        include '../config.php';
        include $gyoker.'/module/mod_head.php';

        if (!isset($_SESSION['login_id'])) {
        	header('Location: '.$domain);
        }
        if (isset($_SESSION['reg_nelkul'])) {
        	header('Location: '.$domain);
        }   


      ?>

      <title>Profil | <?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">      

      <style type="text/css">
      	.sajatdiv ul li {
      		position: relative !important;
      		padding-left: 31px !important;
      	}
      	.sajatdiv ul li:before {
      		content: '\e02d' !important;
      		font-family: "fl-budicons-free" !important;
      		font-size: 12px !important;
      		display: inline-block !important;
      		color: #3470af !important;
      		position: absolute !important;
      		left: 0 !important;
      		top: 6px !important;
      	}

      </style>        
  </head>
  <body>
    <?php include $gyoker.'/module/mod_body_start.php'; ?>
    <div class="page">
      <?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(../images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                <h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5 fejresz-title">Profil</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">
            <ul class="breadcrumbs-custom-path">
              <li><a href="<?=$domain?>">Főoldal</a></li>
              <li class="active">Profil</li>
            </ul>
          </div>
        </div>
      </section>

      <section class="section section-lg bg-default">
        <div class="container">
          <div class="row row-30">
              <div class="col-lg-3 col-md-3 padbot30">
                <?php
                  include $gyoker.'/module/mod_sidebar_profil.php';
                ?>
              </div>
              <div class="col-lg-9 col-md-9 margbot30 text-left sajatdiv">
                <?php
                if (isset($_GET['command']) && $_GET['command'] == 'profil')
                {
                  include $gyoker.'/webshop/profil.php';
                }                 
                elseif (isset($_GET['command']) && $_GET['command'] == 'rendelesek')
                {
                  include $gyoker.'/webshop/rendelesek.php';
                }   
                elseif (isset($_GET['command']) && $_GET['command'] == 'kivansaglista')
                {
                  include $gyoker.'/webshop/kivansaglista.php';
                }                                                                                                  
                else
                {
                  include $gyoker.'/webshop/profil.php';
                }  
                ?>
              </div>
          </div>
        </div>
      </section>      

      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>

	  <script>
	    $(document).on('click', '.lenyito_sor', function(){
	      var rendeles_id = $(this).attr('attr_rendeles_id');
	      $('.lenyilo_sor').each(function() {
	        if($(this).attr('attr_rendeles_id') == rendeles_id) {
	          $(this).animate({
	            height: [ "toggle", "swing" ]
	          }, 300);
	        }
	      }); 
	    });
	  </script>       
  </body>
</html>