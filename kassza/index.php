<?php 
  session_start();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
      <?php 
        $oldal = 'kassza';
        include '../config.php';
        include $gyoker.'/module/mod_head.php';
      ?>

      <title><?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">      
  </head>
  <body>
    <?php include $gyoker.'/module/mod_body_start.php'; ?>
    <div class="page">
      <?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(../images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                <h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5 fejresz-title">Kassza</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">
            <ul class="breadcrumbs-custom-path">
              <li><a href="<?=$domain?>">Főoldal</a></li>
              <li class="active">Kassza</li>
            </ul> 
          </div>
        </div>
      </section>

      <section class="section section-lg bg-default">
        <div class="container">         
          <div class="row row-30 justify-content-center">
            <div class="col-md-12">      
              <?php include $gyoker.'/webshop/kassza_4.php'; ?>
            </div>
          </div>
        </div>
      </section>         
    

      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>

    <script src="<?php print $domain; ?>/webshop/scripts/kassza_4.js?v=27" type="text/javascript"></script>
    <script src="<?php print $domain; ?>/webshop/scripts/postapont-valaszto.js" type="text/javascript"></script>
    <script src="<?php print $domain; ?>/webshop/scripts/gls_cspont-valaszto.js?v=4" type="text/javascript"></script>
  </body>
</html>