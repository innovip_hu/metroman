<?php 
	include 'config.php';

	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	$query = "SELECT id FROM ".$webjel."rendeles WHERE fizetve_otp = 2";
	foreach ($pdo->query($query) as $value)
	{
			$url = 'https://ebank.khb.hu/PaymentGatewayTest/PGResult?mid=1210&txid='.$value['id'];
		    $ch = curl_init($url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    $response = curl_exec($ch);
		    curl_close($ch);
			
			$eredmeny_tomb = explode("\n", $response);

	      	if (trim($eredmeny_tomb[0]) == 'ACK')
	      	{				
				// Fizetés visszaigazolása
				$updatecommand = "UPDATE ".$webjel."rendeles SET fizetve_otp=1 WHERE id='".$value['id']."'";
				$result = $pdo->prepare($updatecommand);
				$result->execute();					
			}	
	      	elseif (trim($eredmeny_tomb[0]) !== 'PEN')
	      	{				
				// Fizetés visszaigazolása
				$updatecommand = "UPDATE ".$webjel."rendeles SET fizetve_otp=0 WHERE id='".$value['id']."'";
				$result = $pdo->prepare($updatecommand);
				$result->execute();					
			}					
	}




?>