<?php 
  session_start();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
      <?php 
        $oldal = 'kapcsolat';
        include '../config.php';
        include $gyoker.'/module/mod_head.php';

      ?>

      <title>Cégadatok és kapcsolatfelvétel | <?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">      
  </head>
  <body>
    <?php include $gyoker.'/module/mod_body_start.php'; ?>
    <div class="page">
      <?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(../images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                <h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5">Cégadatok és kapcsolatfelvétel</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">
            <ul class="breadcrumbs-custom-path">
              <li><a href="<?=$domain?>">Főoldal</a></li>
              <li class="active">Cégadatok és kapcsolatfelvétel</li>
            </ul>  
          </div>
        </div>
      </section>

      <section class="section section-lg bg-default">
        <div class="container">        
          <div class="row row-30">
            <div class="col-md-6">
                <h5>Cégadatok és kapcsolatfelvétel</h5>

                <p class="mt-2">Metroman Hungária KFT<br>
                8800 Nagykanizsa Hevesi u. 8.</p>

                <p>Telefon: <a href="tel:+36 30 348 7254">+36 30 348 7254</a>, <a href="tel:+36 93 900 837">+36 93 900 837</a>
                <br>E-mail: <a href="mailto:info@metroman.hu">info@metroman.hu</a></p>

                <p><b>Nyitvatartás:</b>
                <br>Hétfőtől péntekig: de. 9-12h; 13-17h
                <br>Szombat: 9-12h</p>

                <?php   $query = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
                  $res = $pdo->prepare($query);
                  $res->execute();
                  $row = $res -> fetch();  ?>

                <?php if ($row['kapcsolat'] != ''): ?>
                    <p><?php echo nl2br($row['kapcsolat']); ?></p>
                <?php endif ?>

                <h5 class="mt-5">Cégadatok</h5>

                <p class="mt-2">Metroman Hungária Kft.
                <br>8800 Nagykanizsa Hevesi u. 8.
                <br><b>Adószám:</b> 12476602-2-20
                <br><b>EU adószám:</b> HU12476602
                <br><b>Cégjegyzék szám:</b> 20-09-064568
                <br><b>A cégbírósági végzés kiállítója:</b> Zala Megyei Bíróság Cégbíróság Zalaegerszeg
                <br><b>GKM nyilvántartási szám:</b> C/002524/2004
                <br><b>Adatkezelési nyilvántartási szám:</b> NAIH-58987/2012</p>

                <h5 class="mt-5">Ügyfélszolgálat és panaszkezelés helye</h5>

                <p class="mt-2">Metroman Hungária Kft.
                <br>8800 Nagykanizsa Hevesi u. 8.</p>
            </div>
            <div class="col-md-6">
              <article class="video-modern">
                <img class="img-responsive" src="<?=$domain?>/images/Metroman_uzlet.jpg" alt="Cégadatok és kapcsolatfelvétel"/>
              </article>              
            </div>
          </div>
        </div>
      </section>      

      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>
  </body>
</html>