<?php 
	include 'config.php';

	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	}
	catch (PDOException $e)
	{
	    $protocol = 'HTTP/1.0';

	    if ( $_SERVER['SERVER_PROTOCOL'] === 'HTTP/1.1' ) {
	        $protocol = 'HTTP/1.1';
	    }

	    header( $protocol . ' 503 Service Unavailable', true, 503 );
	    header( 'Retry-After: 3600' );

		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	
	$query = "SELECT id FROM ".$webjel."termekek WHERE rendelheto = 0 AND raktaron <= 0 AND sorrend >= 0";
	foreach ($pdo->query($query) as $value)
	{
		$pdo->exec("UPDATE ".$webjel."termekek SET regi_sorrend = sorrend, sorrend=-2 WHERE id=".$value['id']);
	}

	$query = "SELECT id FROM ".$webjel."termekek WHERE sorrend =-2 AND (rendelheto = 1 OR raktaron > 0)";
	foreach ($pdo->query($query) as $value)
	{
		$pdo->exec("UPDATE ".$webjel."termekek SET sorrend = regi_sorrend WHERE id=".$value['id']);
	}

	$pdo->exec("UPDATE ".$webjel."termekek SET sorrend=-3, rendelheto=0 WHERE ovip_torolt=1");

	$pdo->exec("UPDATE ".$webjel."termekek SET lathato=0 WHERE lathato=1 AND ovip_torolt=1 AND kifutott_eddig != '0000-00-00' AND kifutott_eddig < CURDATE()");