<?php
	include 'config.php';

  	// PDO
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	}
	catch (PDOException $e)
	{
	    $protocol = 'HTTP/1.0';

	    if ( $_SERVER['SERVER_PROTOCOL'] === 'HTTP/1.1' ) {
	        $protocol = 'HTTP/1.1';
	    }

	    header( $protocol . ' 503 Service Unavailable', true, 503 );
	    header( 'Retry-After: 3600' );

		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	$sorrend = 1;
	$elozo_termek = 0;

	$query = "SELECT * FROM ".$webjel."termek_kepek WHERE spec = 0 AND sorszam = 0 ORDER BY termek_id";
	foreach ($pdo->query($query) as $key => $value)
	{
		$jelenlegi_termek = $value['termek_id'];

		if ($jelenlegi_termek != $elozo_termek) {

			if ($elozo_termek > 0)
			{
				$updatecommand = "UPDATE ".$webjel."termek_kepek SET sorszam=? WHERE spec = 0 AND termek_id=?";
				$result = $pdo->prepare($updatecommand);
				$result->execute(array($sorrend,$elozo_termek));	
			}

			$sorrend = 1;
			$elozo_termek = $value['termek_id'];
		}
		else
		{
			$sorrend++;			
		}	
	}