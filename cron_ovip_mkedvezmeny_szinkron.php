<?php
	// folyamatában látszódjon a kiírása
	ob_implicit_flush(true);
	ob_end_flush();

	ini_set('xdebug.var_display_max_depth', -1);
	ini_set('xdebug.var_display_max_children', -1);
	ini_set('xdebug.var_display_max_data', -1);

	include 'config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	$options = array(
	  	'location' => $ovip_soap_link,
	 	'uri' => $ovip_soap_link,
	  	'encoding' => 'UTF-8',
	  	'trace' => 1 //csak hibakereséshez kell
	);	

	$request = 'GetQtyDiscount';

	$signature = hash('sha256', trim($ovip_ugyfel_id . $ovip_webshop_id . $ovip_authCode . $request . $ovip_ip_cim)); //hash generálása 

	$request = array(
		'request' => $request,
		'user_id' => $ovip_ugyfel_id,
		'signature' => $signature,
		'webshop_id' => $ovip_webshop_id
	);

	try {		

		$client = new SoapClient(NULL,$options);
		
		$tetelek = $client->getRequest($request);

		if (is_array($tetelek))
		{
			$pdo->exec("UPDATE ".$webjel."termek_darab_kedvezmeny SET lathato=0"); //láthatatlanná tesz mindent szinkron előtt

			//echo '<pre>';
			//print_r($tetelek);
			//echo '</pre>';

			foreach ($tetelek as $kedvezmeny)
			{
				$query = "SELECT id FROM ".$webjel."termek_darab_kedvezmeny WHERE ovip_id=".$kedvezmeny['ovip_quantity_id'];
				$res = $pdo->prepare($query);
				$res->execute();
				$row_kedvezmeny = $res -> fetch();		

				if (isset($row_kedvezmeny['id']))
				{
					$updatecommand = "UPDATE ".$webjel."termek_darab_kedvezmeny SET lathato=1 WHERE id=?";
					$result = $pdo->prepare($updatecommand);
					$result->execute(array($row_kedvezmeny['id']));						
				} else {
					//insert
					$insertcommand = "INSERT INTO ".$webjel."termek_darab_kedvezmeny (termek_db,termek_ar,tipus,ovip_arlista_id,ovip_termek_id,ovip_id,lathato) VALUES (:termek_db,:termek_ar,:tipus,:ovip_arlista_id,:ovip_termek_id,:ovip_id,:lathato)";
					$result = $pdo->prepare($insertcommand);
					$result->execute(array(':termek_db'=>$kedvezmeny['discount_from_quantity'],
									  ':termek_ar'=>$kedvezmeny['discount_value'],
									  ':tipus'=>$kedvezmeny['discount_type'],
									  ':ovip_arlista_id'=>$kedvezmeny['price_list_id'],
									  ':ovip_termek_id'=>$kedvezmeny['product_id'],
									  ':ovip_id'=>$kedvezmeny['ovip_quantity_id'],
									  ':lathato'=>1));				
				}
			}


		    echo "Mennyiségi kedvezmény kész<br/>";

		}
		else
		{
			echo "Mennyiségi kedvezmény üzenet:".$tetelek."<br/>";
		}

	} catch (Exception $e) {

		echo "Mennyiségi kedvezmény Szerver üzenet: ".$e->getMessage()."<br/>";

	}	


	$query = "SELECT id,ovip_termek_id,ovip_arlista_id FROM ".$webjel."termek_darab_kedvezmeny WHERE termek_id=0";
	foreach ($pdo->query($query) as $key => $value) {

		$query_term = "SELECT id FROM ".$webjel."termekek WHERE ovip_id=".$value['ovip_termek_id'];
		$res = $pdo->prepare($query_term);
		$res->execute();
		$row = $res -> fetch();

		if (isset($row['id'])) {
			$updatecommand = "UPDATE ".$webjel."termek_darab_kedvezmeny SET termek_id=".$row['id']." WHERE id=".$value['id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();			
		}		
	}


	$query = "SELECT id,ovip_termek_id,ovip_arlista_id FROM ".$webjel."termek_darab_kedvezmeny WHERE ovip_arlista_id > 0 AND arlista_id = 0";
	foreach ($pdo->query($query) as $key => $value) {

		$query = "SELECT id FROM ".$webjel."arlista WHERE ovip_id=".$value['ovip_arlista_id'];
		$res = $pdo->prepare($query);
		$res->execute();
		$row_arlista = $res -> fetch();

		if (isset($row_arlista['id'])) {
			$updatecommand = "UPDATE ".$webjel."termek_darab_kedvezmeny SET arlista_id=".$row_arlista['id']." WHERE id=".$value['id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();	
		}		
	}

?>

							  