<?php
	if (isset($_SESSION['login_id']))
	{
		
?>	
	<script>
		$(document).on('click', '.lenyito_sor', function(){
			var rendeles_id = $(this).attr('attr_rendeles_id');
			$('.lenyilo_sor').each(function() {
				if($(this).attr('attr_rendeles_id') == rendeles_id) {
					$(this).animate({
						height: [ "toggle", "swing" ]
					}, 300);
				}
			});	
		});
	</script>
	<div class="table-responsive">
		<table class="table table-bordered">
			<thead>
				<tr>
				<td>Azonosító</td>
				<td>Időpont</td>
				<td>Szállítási mód</td>
				<td>Ár</td>
				<?php /*<td>Státusz</td>*/ ?>
				</tr>
			</thead>
			<tbody>
<?php
		$query = "SELECT * FROM ".$webjel."rendeles WHERE user_id=".$_SESSION['login_id']." AND noreg=0 ORDER BY rendeles_id DESC";
		foreach ($pdo->query($query) as $row)
	{		if($row['teljesitve'] == 1)
			{
			$statusz = 'Teljesített';
			}
			else if($row['szallitva'] == 1)
			{
			$statusz = 'Szállítás alatt';
			}
			else
			{
			$statusz = 'Nyitott';
			}		
		?>
		<tr class="lenyito_sor" attr_rendeles_id="<?php echo $row['id']; ?>">
			<td><?php echo $row['id'];?></td>
			<td><?php echo $row['datum'];?></td>
			<td><?php echo $row['szall_mod'];?>
				<?php
					if (($row['szallitasi_dij'] + $row['utanvet']) > 0) {
						echo '('.($row['szallitasi_dij'] + $row['utanvet']).' Ft)';
					}
?>
			</td>
<?php
						$szumar = 0;
						$query_term = "SELECT * FROM ".$webjel."rendeles_tetelek WHERE rendeles_id=".$row['id'];
							foreach ($pdo->query($query_term) as $row_term)
							{
								if($row_term['term_akcios_ar'] == 0) //ha nem akciós
								{
									$ar = $row_term['term_ar'];
								}
								else //ha akciós
								{
									$ar = $row_term['term_akcios_ar'];
								}
								$db = $row_term['term_db'];
								$szumar += $ar * $db;
?>
							
<?php
							}	
			?>
			<td><?php echo number_format($szumar, 0, ',', ' ');?> Ft</td>
			<?php /*<td><?php echo $statusz;?></td>*/ ?>
		</tr>
		<?php
			$query_term = "SELECT * FROM ".$webjel."rendeles_tetelek WHERE rendeles_id=".$row['id'];
							foreach ($pdo->query($query_term) as $row_term)
							{
								if($row_term['term_akcios_ar'] == 0) //ha nem akciós
								{
									$term_ar = $row_term['term_ar'];
								}
								else //ha akciós
								{
									$term_ar = $row_term['term_akcios_ar'];
								}
								print '<tr class="lenyilo_sor" style="background-color: #fff" attr_rendeles_id="'.$row['id'].'">
									<td>'.$row_term['term_db'].' db</td>
									<td colspan="2">'.$row_term['term_nev'].'</td>
									<td>'.number_format($term_ar, 0, ',', ' ').' Ft / db</td>
								</tr>';
							} 
	}
?>
			</tbody>
		</table>
	</div>
<?php		
	}
	else
	{
		print '<p style="text-align:center;">Kérem jelentkezzen be!</p>';
	}
?>