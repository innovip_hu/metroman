<?php
	if(isset($_SESSION['login_id']))
	{
		$query = "SELECT * FROM ".$webjel."users where id=".$_SESSION['login_id'];
		$res = $pdo->prepare($query);
		$res->execute();
		$row  = $res -> fetch();
		$kotelezo_adatok = array('vezeteknev','email','telefon','szla_nev','cim_varos','cim_utca','cim_hszam','cim_irszam');

		$hibak_jelzese = array('vezeteknev' => '','email' => '','telefon' => '','szla_nev' => '','cim_varos' => '','cim_utca' => '','cim_hszam' => '','cim_irszam' => '');

		$volt_hianyzo_reg_adat = 0;
		foreach ($kotelezo_adatok as $adatok_check) {
			if (empty($row[$adatok_check])) {
				$hibak_jelzese[$adatok_check] = 'style="color: red;"';
				$volt_hianyzo_reg_adat = 1;
			}
		}

		print '<div class="row row-30 mt-2">';
		print '<div class="col-lg-6 col-md-6">
				<div class="row">
					<div class="col-lg-12"><h5>Személyes adatok</h5></div>
				</div>
				<div class="row mt-4" '.$hibak_jelzese['vezeteknev'].'>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">Kapcsolattartó név:</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'.$row['vezeteknev'].'</div>
				</div>
				<div class="row mt-2" '.$hibak_jelzese['email'].'>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">E-mail cím:</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'.$row['email'].'</div>
				</div>
				<div class="row mt-2" '.$hibak_jelzese['telefon'].'>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">Telefonszám:</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'.$row['telefon'].'</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="row">
					<div class="col-lg-12"><h5>Számlázási adatok</h5></div>
				</div>
				<div class="row mt-4" '.$hibak_jelzese['szla_nev'].'>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">Számlázási név:</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'.$row['szla_nev'].'</div>
				</div>				
				<div class="row mt-2" '.$hibak_jelzese['cim_varos'].'>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">Város:</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'.$row['cim_varos'].'</div>
				</div>
				<div class="row mt-2" '.$hibak_jelzese['cim_utca'].'>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">Utca:</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'.$row['cim_utca'].'</div>
				</div>
				<div class="row mt-2" '.$hibak_jelzese['cim_hszam'].'>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">Házszám:</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'.$row['cim_hszam'].'</div>
				</div>
				<div class="row mt-2" '.$hibak_jelzese['cim_irszam'].'>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">Irányítószám:</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'.$row['cim_irszam'].'</div>
				</div>
				<div class="row mt-2">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">Adószám:</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'.$row['adoszam'].'</div>
				</div>				
				<div class="row">';
					if ($row['cim_szall_varos'] == '')
					{
						print '<div class="col-lg-12"><b>A szállítási cím megegyezik<br>a számlázási címmel.</b></div>';
					}
					else
					{
						print '<div class="col-lg-12 margtop10"><h5>Szállítási cím</h5></div>';
					}
			print '</div>
			<div class="row mt-4">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">';
					if ($row['cim_szall_nev'] != '') { print 'Szállítási név:'; }
				print '</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">';
					if ($row['cim_szall_nev'] != '') { print $row['cim_szall_nev']; }
				print '</div>
			</div>			
			<div class="row mt-2">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">';
					if ($row['cim_szall_varos'] != '') { print 'Város:'; }
				print '</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">';
					if ($row['cim_szall_varos'] != '') { print $row['cim_szall_varos']; }
				print '</div>
			</div>			
			<div class="row mt-2">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">';
					if ($row['cim_szall_varos'] != '') { print 'Utca:'; }
				print '</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">';
					if ($row['cim_szall_varos'] != '') { print $row['cim_szall_utca']; }
				print '</div>
			</div>
			<div class="row mt-2">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">';
					if ($row['cim_szall_varos'] != '') { print 'Házszám:'; }
				print '</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">';
					if ($row['cim_szall_varos'] != '') { print $row['cim_szall_hszam']; }
				print '</div>
			</div>
			<div class="row mt-2">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">';
					if ($row['cim_szall_varos'] != '') { print 'Irányítószám:'; }
				print '</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">';
					if ($row['cim_szall_varos'] != '') { print $row['cim_szall_irszam']; }
				print '</div>
			</div>
		</div>';
		?>
		<?php if ($volt_hianyzo_reg_adat == 1): ?>
			<div class="col-md-12 pb-2"><p class="text-center" style="color: red;">A pirossal jelölt mezők hiányosak, vagy nincsenek kitölve.<br>Kérjük pótolja a rendeléshez az összes kötelező adatot a lenti módosít gomb segítségével.</p></div>
		<?php endif ?>
		<div class="col-md-12 pb-2">
			<div <?php if ($volt_hianyzo_reg_adat == 0) {echo 'class="pull-left"';} else { echo 'class="text-center"';} ?>>
				<button type="button" class="button button-default-outline button-sm" onclick="location.href='<?php print $domain; ?>/profil/';">Módosít</button>
			</div>
			<?php if ($volt_hianyzo_reg_adat == 0): ?>
			<div class="pull-right">
				<button type="button" id="adatok_tovabb_gomb" class="button button-sm button-gradient kassza_tovabb_gomb" attr_tart_id="szall_mod_tartalom" attr_horg="szall_mod_horgony">Tovább</button>
			</div>
			<?php endif ?>
		</div>
		</div>
		<input type="hidden" id="belepve" value="1"/>

		<input type="hidden" id="szla_nev" value="<?=$row['szla_nev']?>">
		<input type="hidden" id="cim_irszam" value="<?=$row['cim_irszam']?>">
		<input type="hidden" id="cim_varos" value="<?=$row['cim_varos']?>">
		<input type="hidden" id="cim_utca" value="<?=$row['cim_utca']?>">
		<input type="hidden" id="cim_hszam" value="<?=$row['cim_hszam']?>">
		<input type="hidden" id="cim_szall_nev" value="<?=$row['cim_szall_nev']?>">
		<input type="hidden" id="cim_szall_irszam" value="<?=$row['cim_szall_irszam']?>">
		<input type="hidden" id="cim_szall_varos" value="<?=$row['cim_szall_varos']?>">
		<input type="hidden" id="cim_szall_utca" value="<?=$row['cim_szall_utca']?>">
		<input type="hidden" id="cim_szall_hszam" value="<?=$row['cim_szall_hszam']?>">
		<?php
	}
	else
	{
		?>
		<input type="hidden" id="belepve" value="0"/>
		<div class="row row-30 mt-2">
		<div class="col-lg-6 col-md-6">
			<h5>Személyes adatok</h5>
			<div class="row mt-2">
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="nev_riaszt">A név nem lett megadva.</div>
				<div class="col-lg-4 col-md-4">Kapcsolattartó név:</div>
				<div class="col-lg-8 col-md-8"><input type="text" id="nev" name="nev" value="" class="form-control"></div>
			</div>
			<div class="row mt-2">
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="email_riaszt">Az email cím nem lett megadva.</div>
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="email_riaszt2">Az email nem megfelelő formátumú.</div>
				<div class="col-lg-4 col-md-4">E-mail cím:</div>
				<div class="col-lg-8 col-md-8"><input type="text" id="email" name="email" value="" class="form-control" ></div>
			</div>
			<div class="row mt-2" id="jelszo_div" style="display:none;">
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="jelszo_riaszt">A jelszó nem lett megadva.</div>
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="jelszo_riaszt2">A két jelszó nem egyezik!</div>
				<div class="col-lg-4 col-md-4">Jelszó:</div>
				<div class="col-lg-8 col-md-8"><input type="password" id="jelszo" name="jelszo" value="" class="form-control" ></div>
			</div>
			<div class="row mt-2" id="jelszo2_div" style="display:none;">
				<div class="col-lg-4 col-md-4">Jelszó újra:</div>
				<div class="col-lg-8 col-md-8"><input type="password" id="jelszo2" value="" class="form-control" ></div>
			</div>
			<div class="row mt-2">
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="telefon_riaszt">A telefonszám nem lett megadva.</div>
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="telefon_riaszt2">A telefonszám nem megfelelő formátumú!</div>
				<div class="col-lg-4 col-md-4">Telefonszám:</div>
				<div class="col-lg-8 col-md-8"><input type="text" id="telefon" name="telefon" value="" class="form-control" placeholder="Példa: +36301234567"></div>
			</div>
			<div class="row mt-2" id="feltetel_div" style="display:none;">
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="feltetel_riaszt">Az adatvédelmi nyilatkozat nem lett elfogadva.</div>
				<div class="col-lg-12 col-md-12">
	                <div class="form-wrap">
	                  <label class="checkbox-inline">
	                    <input name="feltetel" value="elfogad" id="feltetel" type="checkbox">Elolvastam és megértettem a Metroman Hungária Kft. <a style="text-decoration: underline;" href="<?=$domain?>/adatkezelesi-tajekoztato" target="_blank">Adatkezelési Tájékoztatóját és Szabályzatát</a>, és hozzájárulok ahhoz, hogy a megadott adataimat a szolgáltató, mint adatkezelő a Szabályzatban foglaltaknak megfelelően kezelje.
	                  </label>
	                </div>							
				</div>
			</div>
			<div class="row mt-2">
				<div class="col-lg-12 col-md-12">
	                <div class="form-wrap">
	                  <label class="checkbox-inline">
	                    <input name="regisztracio_checkbox" value="ok" id="regisztracio_checkbox" type="checkbox">Regisztráció
	                  </label>
	                </div>						
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6">
			<h5>Számlázási adatok</h5>
			<div class="row mt-2">
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="szla_nev_riaszt">A számlázási név nem lett megadva.</div>
				<div class="col-lg-4 col-md-4">Számlázási név:</div>
				<div class="col-lg-8 col-md-8"><input type="text" id="szla_nev" name="szla_nev" value="" class="form-control" ></div>
			</div>			
			<div class="row mt-2">
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="cim_irszam_riaszt">Az irányítószám nem lett megadva.</div>
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="cim_irszam_riaszt2">Az irányítószám csak számot tartalmazhat.</div>
				<div class="col-lg-4 col-md-4">Irányítószám:</div>
				<div class="col-lg-8 col-md-8"><input type="text" id="cim_irszam" name="cim_irszam" value="" class="form-control" autocomplete="off" data-mezo="cim_varos"></div>
			</div>
			<div class="row mt-2">
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="cim_varos_riaszt">A város nem lett megadva.</div>
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="cim_varos_riaszt2">Nem megfelelő karakterek.</div>
				<div class="col-lg-4 col-md-4">Város:</div>
				<div class="col-lg-8 col-md-8"><input type="text" id="cim_varos" name="cim_varos" value="" class="form-control" ></div>
			</div>
			<div class="row mt-2">
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="cim_utca_riaszt">Az utca nem lett megadva.</div>
				<div class="col-lg-4 col-md-4">Utca:</div>
				<div class="col-lg-8 col-md-8"><input type="text" id="cim_utca" name="cim_utca" value="" class="form-control" ></div>
			</div>
			<div class="row mt-2">
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="cim_hszam_riaszt">A házszám nem lett megadva.</div>
				<div class="col-lg-4 col-md-4">Házszám:</div>
				<div class="col-lg-8 col-md-8"><input type="text" id="cim_hszam" name="cim_hszam" value="" class="form-control" ></div>
			</div>
			<div class="row mt-2">
				<div class="col-lg-12 col-md-12">
	                <div class="form-wrap">
	                  <label class="checkbox-inline">
	                    <input name="cegadoszam" id="cegadoszam" type="checkbox" value="1">Cég esetén adószám megadása
	                  </label>
	                </div>						
				</div>
			</div>			
			<div class="row mt-2" id="adoszam_blokk" style="display: none;">
				<div class="col-lg-4 col-md-4">Adószám:</div>
				<div class="col-lg-8 col-md-8"><input type="text" id="adoszam" name="adoszam" value="" class="form-control" ></div>
				<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div text-right" id="adoszam_riaszt">Nem megfelelő adószám formátum!</div>
			</div>			
			<div class="row mt-2">
				<div class="col-lg-12 col-md-12">
	                <div class="form-wrap">
	                  <label class="checkbox-inline">
	                    <input name="mascim" id="mascim" type="checkbox" value="1">Eltérő szállítási cím házhozszállítás esetén
	                  </label>
	                </div>						
				</div>
			</div>
			<div id="eltero_cim" >
				<div class="row mt-2">
					<div id="div1" class="col-lg-4 col-md-4">Név:</div>
					<div id="div2" class="col-lg-8 col-md-8"><input type="text" id="cim_szall_nev" name="cim_szall_nev" value="" class="form-control" ></div>
				</div>
				<div class="row mt-2">
					<div id="div9" class="col-lg-4 col-md-4">Irányítószám:</div>
					<div id="div10" class="col-lg-8 col-md-8"><input type="text" id="cim_szall_irszam" name="cim_szall_irszam" value="" class="form-control"data-mezo="cim_szall_varos" autocomplete="off"></div>
				</div> 
				<div class="row mt-2">
					<div id="div3" class="col-lg-4 col-md-4">Város:</div>
					<div id="div4" class="col-lg-8 col-md-8"><input type="text" id="cim_szall_varos" name="cim_szall_varos" value="" class="form-control" ></div>
				</div>
				<div class="row mt-2">
					<div id="div5" class="col-lg-4 col-md-4">Utca:</div>
					<div id="div6" class="col-lg-8 col-md-8"><input type="text" id="cim_szall_utca" name="cim_szall_utca" value="" class="form-control" ></div>
				</div>
				<div class="row mt-2">
					<div id="div7" class="col-lg-4 col-md-4">Házszám:</div>
					<div id="div8" class="col-lg-8 col-md-8"><input type="text" id="cim_szall_hszam" name="cim_szall_hszam" value="" class="form-control" ></div>
				</div>				
			</div>
		</div>
		<div class="col-md-12 pb-2 text-center">
			<p><b>A szállítási címnél magyarországi címet szükséges megadni, külföldi szállítás nem elérhető.</b></p>
		</div>
		<div class="col-md-12 pb-2">
			<div class="pull-left">
				<a class="button button-default-outline button-sm" href="<?php print $domain; ?>/belepes/" >Belépés</a>
			</div>
			<div class="pull-right">
				<button type="button" id="adatok_tovabb_gomb" class="button button-sm button-gradient kassza_tovabb_gomb" attr_tart_id="szall_mod_tartalom" attr_horg="szall_mod_horgony">Tovább</button>
			</div>
		</div>
	</div>
		<?php
	}
?>