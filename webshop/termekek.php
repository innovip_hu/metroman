<?php
	if (isset($_GET['term_urlnev']) && $_GET['term_urlnev'] == '') {
        $szulkat = '';
        $szul_szulkat = '';
        $szul_szul_szulkat = '';
        $szulkat_nev = '';
        $szul_szulkat_nev = '';
        $szul_szul_szulkat_nev = '';
        if (isset($_GET['kat_urlnev']))
        {
          $query = "SELECT * FROM ".$webjel."term_csoportok where nev_url=?";
          $res = $pdo->prepare($query);
          $res->execute(array($_GET['kat_urlnev']));
          $row  = $res -> fetch();
          $szulkat = $row['csop_id'];
          $szulkat_nev = $row['nev'];
          $szulkat_url = $row['nev_url'];
          if ($szulkat > 0)
          {
            $query = "SELECT * FROM ".$webjel."term_csoportok where id=".$szulkat;
            $res = $pdo->prepare($query);
            $res->execute();
            $row  = $res -> fetch();
            $szul_szulkat = $row['csop_id'];
            $szul_szulkat_nev = $row['nev'];
            $szul_szulkat_url = $row['nev_url'];
            if ($szul_szulkat != 0 || $szul_szulkat != '')
            {
              $query = "SELECT * FROM ".$webjel."term_csoportok where id=".$szul_szulkat;
              $res = $pdo->prepare($query);
              $res->execute();
              $row  = $res -> fetch();
              $szul_szul_szulkat = $row['csop_id'];
              $szul_szul_szulkat_nev = $row['nev'];
              $szul_szul_szulkat_url = $row['nev_url'];
            }
          }
        }
        
	    print '<ul class="breadcrumbs-custom-path pb-4 d-none d-md-block">';
	        print '<li><a href="'.$domain.'">Főoldal</a></li>'; 
	        print '<li><a href="'.$domain.'/termekek/">Termékek</a></li>'; 
	      if($szul_szul_szulkat_nev != '' && $_GET['kat_urlnev'] != $szul_szul_szulkat_nev)
	      {
	        print '<li><a href="'.$domain.'/termekek/'.$szul_szul_szulkat_url.'/">'.$szul_szul_szulkat_nev.'</a></li>';
	      }
	      if($szul_szulkat_nev != '' && $_GET['kat_urlnev'] != $szul_szul_szulkat_nev)
	      {
	        print '<li><a href="'.$domain.'/termekek/'.$szul_szulkat_url.'/">'.$szul_szulkat_nev.'</a></li>';
	      }
	      if($szulkat_nev != '' && $_GET['kat_urlnev'] != $szul_szul_szulkat_nev)
	      {
	        print '<li><a href="'.$domain.'/termekek/'.$szulkat_url.'/">'.$szulkat_nev.'</a></li>';
	      }
	      if (!empty($_GET['term_urlnev']))
	      {
	        $res = $pdo->prepare("SELECT nev FROM ".$webjel."termekek where nev_url=?");
	        $res->execute(array($_GET['term_urlnev']));
	        $row  = $res -> fetch(); 

	        print '<li class="active">'.$row['nev'].'</li>';
	      }                 
	    print '</ul>';
	}

	// Szűrő
	if($oldal="termekek" && isset($_GET['kat_urlnev']) && $_GET['kat_urlnev'] != '' && $_GET['term_urlnev'] == '')
	{		
		include $gyoker.'/webshop/termek_csoport_termek_parameterek_szuro/view.php';
	}
?>

<div id="termekek_ajax_div">
<?php
	// Kategóriábam felsorolás (termék vagy további kategória)
	if (isset($_GET['kat_urlnev']) && isset($_GET['term_urlnev']) && $_GET['kat_urlnev'] != '' && $_GET['term_urlnev'] == '') 
	{
		include $gyoker.'/webshop/termek_kategoriak.php';
	}
//Termék infó
	else if (isset($_GET['term_urlnev'])) 
	{
		include $gyoker.'/webshop/termek_info.php';
	}
//KEZDŐLAP - TERMÉKEK
	else 
	{
		?>
                <ul class="breadcrumbs-custom-path">
                  <li><a href="<?=$domain?>">Főoldal</a></li>
                  <li class="active">Termékek</li>
                </ul> 

		<?php
		print '<link rel="stylesheet" href="'.$domain.'/termekek/termekek-oldal.css?v=2">';

		print '<div class="row kapcs_termekek row-30">';
				$query1 = "SELECT id,nev,kep_fo,nev_url FROM ".$webjel."term_csoportok WHERE csop_id = 0 AND lathato=1 ORDER BY sorrend asc";
				foreach ($pdo->query($query1) as $row1)
				{
					if ($row1['kep_fo'] == "")
					{
						$csop_kep_link = ''.$domain.'/webshop/images/noimage.png';
					}
					else
					{
						$csop_kep_link = $domain.'/images/termekek/'.$row1['kep_fo'];
					}
					?>

			            <div class="col-lg-3 col-sm-6">
			              <!-- Product-->
			              <article class="product">
			              	<a href="<?=$domain?>/termekek/<?=$row1['nev_url']?>/">
				                <div class="product-figure fels_termek_kep text-center">
				                	<img src="<?=$csop_kep_link?>" alt="<?=$row1['nev']?>" style="max-height: 200px; max-width: initial;" class="img-responsive"/>
				                </div>
				                <div class="fels_termek_adatok mb-0 bg-sajatv2">
					                <h6 class="product-title text-center"><?=$row1['nev']?></h6>
				                </div>
			                </a>
			               </article>

			                <?php

			                	$query2 = "SELECT id,nev,nev_url FROM ".$webjel."term_csoportok WHERE csop_id = ".$row1['id']." AND lathato=1 ORDER BY sorrend asc";
			                	foreach ($pdo->query($query2) as $row2) {

									$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id = ? AND lathato = 1");
									$res->execute(array($row2['id']));
									$rownum = $res->fetchColumn();

			                		echo '<h5 class="ptag fokat"><a href="'.$domain.'/termekek/'.$row2['nev_url'].'/">'.$row2['nev'].'</a></h5>';

			                		if ($rownum > 0) {
			                			echo '<ul>';

					                	$query3 = "SELECT id,nev,nev_url FROM ".$webjel."term_csoportok WHERE csop_id = ".$row2['id']." AND lathato=1 ORDER BY sorrend asc";
					                	foreach ($pdo->query($query3) as $row3) {			                			
					                		echo '<li class="alkateg"><a href="'.$domain.'/termekek/'.$row3['nev_url'].'/">'.$row3['nev'].' ></a></li>';
					                	}
			                			echo '</ul>';
			                		}
			                	}

			                ?>
			              
			            </div>

					<?php
				}
		print '</div>';
	}
?>
</div>
