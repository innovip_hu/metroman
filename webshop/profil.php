<!--eltűntető script-->
<script type="text/javascript">
	function showMe (box) {
		var chboxs = document.getElementsByName("mascim");
		var vis = "none";
		for(var i=0;i<chboxs.length;i++) { 
			if(chboxs[i].checked){
				 vis = "block";
					break;
			}
		}
		document.getElementById(box).style.display = vis;
	}
</script>
<?php
	if (isset($_SESSION['login_id']))
	{
		if (isset($_POST['command']) && $_POST['command'] == "MOD")
		{
			include $gyoker.'/webshop/profil_check.php';
			$jelszo = $_POST['jszo1'];
			if (profil_check($_POST) == "rendben")
			{
				if (isset($_POST['mascim']) && $_POST['mascim'] == 'ok')
				{
					$cim_szall_nev = $_POST['cim_szall_nev'];
					$cim_szall_varos = $_POST['cim_szall_varos'];
					$cim_szall_utca = $_POST['cim_szall_utca'];
					$cim_szall_hszam = $_POST['cim_szall_hszam'];
					$cim_szall_irszam = $_POST['cim_szall_irszam'];
				}
				else
				{
					$cim_szall_nev = '';
					$cim_szall_varos = '';
					$cim_szall_utca = '';
					$cim_szall_hszam = '';
					$cim_szall_irszam = '';
				}
				if($jelszo == "" )
				{
					$email = $_POST['email'];
					$telefon = $_POST['telefon'];
					$vezeteknev = $_POST['vezeteknev'];
					$cim_varos = $_POST['cim_varos'];
					$cim_utca = $_POST['cim_utca'];
					$cim_hszam = $_POST['cim_hszam'];
					$cim_irszam = $_POST['cim_irszam'];
					$id = $_SESSION['login_id'];
					$query = 'UPDATE '.$webjel.'users SET email=?, telefon=?, vezeteknev=?, cim_varos=?, cim_utca=?, cim_hszam=?, cim_irszam=?, cim_szall_nev=?, cim_szall_varos=?, cim_szall_utca=?, cim_szall_hszam=?, cim_szall_irszam=?, szla_nev=?, adoszam=? WHERE id=?';
					$result = $pdo->prepare($query);
					$result->execute(array($email,$telefon,$vezeteknev,$cim_varos,$cim_utca,$cim_hszam,$cim_irszam,$cim_szall_nev,$cim_szall_varos,$cim_szall_utca,$cim_szall_hszam,$cim_szall_irszam,$_POST['szla_nev'],$_POST['adoszam'],$id));
				}
				else
				{
					$email = $_POST['email'];
					$telefon = $_POST['telefon'];
					$vezeteknev = $_POST['vezeteknev'];
					$cim_varos = $_POST['cim_varos'];
					$cim_utca = $_POST['cim_utca'];
					$cim_hszam = $_POST['cim_hszam'];
					$cim_irszam = $_POST['cim_irszam'];
					$id = $_SESSION['login_id'];
					$jelszo = password_hash($jelszo, PASSWORD_DEFAULT);
					$query = 'UPDATE '.$webjel.'users SET email=?, telefon=?, vezeteknev=?, cim_varos=?, cim_utca=?, cim_hszam=?, cim_irszam=?, cim_szall_nev=?, cim_szall_varos=?, cim_szall_utca=?, cim_szall_hszam=?, cim_szall_irszam=?, jelszo=?, szla_nev=?, adoszam=? WHERE id=?';
					$result = $pdo->prepare($query);
					$result->execute(array($email,$telefon,$vezeteknev,$cim_varos,$cim_utca,$cim_hszam,$cim_irszam,$cim_szall_nev,$cim_szall_varos,$cim_szall_utca,$cim_szall_hszam,$cim_szall_irszam,$jelszo,$_POST['szla_nev'],$_POST['adoszam'],$id));
				}
				print '<div class="alert alert-success alert-dismissable bg-gradient-1 text-center"><p>Az adatok sikeresen elmentve!</p></div>';
				if (isset($_SESSION['kosar_id']))
				{
					header ('Location: '.$domain.'/kassza/');
				}
			}
			else
			{
				print '<div class="alert alert-danger alert-dismissable bg-gradient-1 text-center"><p>'.profil_check($_POST).'</p></div>';
			}
		}
		$query = "SELECT * FROM ".$webjel."users where id=".$_SESSION['login_id'];
		$res = $pdo->prepare($query);
		$res->execute();
		$row  = $res -> fetch();
?>
		<form id="mentesForm" action="" method="post">
		<div class="row row-30">
			<div class="col-lg-6 col-md-6">

				<h6 class="text-center pb-2">Személyes adatok</h6>

				<div class="form-wrap">
              		<input class="form-input" id="email" type="email" name="email" value="<?php print $row['email']; ?>" required>
              		<label class="form-label" for="email">E-mail cím *</label>
              		<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="email_riaszt">Add meg az email címet!</div>
				</div>

				<div class="form-wrap">
              		<input class="form-input" id="jszo1" type="password" name="jszo1" value="">
              		<label class="form-label" for="jszo1">Jelszó</label>
				</div>				
				
				<div class="form-wrap">
              		<input class="form-input" id="jszo2" type="password" name="jszo2" value="">
              		<label class="form-label" for="jszo2">Jelszó újra</label>
				</div>

				<div class="form-wrap">
              		<input class="form-input" id="vezeteknev" type="text" name="vezeteknev" value="<?php print $row['vezeteknev']; ?>" required>
              		<label class="form-label" for="vezeteknev">Név *</label>
				</div>
				
				<div class="form-wrap">
              		<input class="form-input" id="telefon" type="text" name="telefon" value="<?php print $row['telefon']; ?>" required>
              		<label class="form-label" for="telefon">Telefonszám *</label>
				</div>	

			</div>
			<div class="col-lg-6 col-md-6">

				<h6 class="text-center pb-2">Számlázási adatok</h6>

				<div class="form-wrap">
              		<input class="form-input" id="szla_nev" type="text" name="szla_nev" value="<?php print $row['szla_nev']; ?>" required>
              		<label class="form-label" for="szla_nev">Számlázási név *</label>
					<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="szla_nev_riaszt">Add meg a számlázási nevet!</div>
				</div>	

				<div class="form-wrap">
              		<input class="form-input" id="cim_varos" type="text" name="cim_varos" value="<?php print $row['cim_varos']; ?>" required>
              		<label class="form-label" for="cim_varos">Város *</label>
					<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="cim_varos_riaszt">Add meg a várost!</div>
				</div>	

				<div class="form-wrap">
              		<input class="form-input" id="cim_utca" type="text" name="cim_utca" value="<?php print $row['cim_utca']; ?>" required>
              		<label class="form-label" for="cim_utca">Utca *</label>
					<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="cim_utca_riaszt">Add meg az utcát!</div>
				</div>	

				
				<div class="form-wrap">
              		<input class="form-input" id="cim_hszam" type="text" name="cim_hszam" value="<?php print $row['cim_hszam']; ?>" required>
              		<label class="form-label" for="cim_hszam">Házszám *</label>
					<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="cim_hszam_riaszt">Add meg a házzszámot!</div>
				</div>	

				<div class="form-wrap">
              		<input class="form-input" id="cim_irszam" type="text" name="cim_irszam" value="<?php print $row['cim_irszam']; ?>" required>
              		<label class="form-label" for="cim_irszam">Irányítószám *</label>
					<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="cim_irszam_riaszt">Add meg az irányítószámot!</div>
				</div>	

				<div class="form-wrap">
              		<input class="form-input" id="adoszam" type="text" name="adoszam" value="<?php print $row['adoszam']; ?>">
              		<label class="form-label" for="adoszam">Adószám</label>
				</div>							

				<div class="form-wrap">
                	<label class="checkbox-inline">		
                    <input onclick="showMe('div1', this);" name="mascim" value="ok" id="mascim" type="checkbox" <?php if ($row['cim_szall_varos'] != '') {print 'checked';} ?>>Eltérő szállítási cím
                  </label>
                </div>		

                <?php if ($row['cim_szall_varos'] == '') {$display = 'style="display:none"';} else {$display = '';} ?>
                		

				<div id="div1" <?php print $display; ?> >
					<div class="form-wrap">
                  		<input class="form-input" id="cim_szall_nev" type="text" name="cim_szall_nev" value="<?php print $row['cim_szall_nev']; ?>">
                  		<label class="form-label" for="cim_szall_nev">Név</label>
					</div>	

					<div class="form-wrap">
                  		<input class="form-input" id="cim_szall_varos" type="text" name="cim_szall_varos" value="<?php print $row['cim_szall_varos']; ?>">
                  		<label class="form-label" for="cim_szall_varos">Város</label>
					</div>	

					<div class="form-wrap">
                  		<input class="form-input" id="cim_szall_utca" type="text" name="cim_szall_utca" value="<?php print $row['cim_szall_utca']; ?>">
                  		<label class="form-label" for="cim_szall_utca">Utca</label>
					</div>	

					<div class="form-wrap">
                  		<input class="form-input" id="cim_szall_hszam" type="text" name="cim_szall_hszam" value="<?php print $row['cim_szall_hszam']; ?>">
                  		<label class="form-label" for="cim_szall_hszam">Házszám</label>
					</div>	

					<div class="form-wrap">
                  		<input class="form-input" id="cim_szall_irszam" type="text" name="cim_szall_irszam" value="<?php print $row['cim_szall_irszam']; ?>">
                  		<label class="form-label" for="cim_szall_irszam">Irányítószám</label>
					</div>																															
				</div>

			</div>
			<div class="col-lg-12 col-md-12 text-center">
				<button class="button button-default-outline-2" type="submit">Mentés</button>
				<input type="hidden" name="command" value="MOD">
			</div>
		</div>
		</form>
<?php		
	}
	else
	{
		print '<p style="text-align:center;">Kérem jelentkezzen be!</p>';
	}
?>