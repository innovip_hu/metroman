<?php

	require_once "fbconfig.php";

	$redirectURL = $domain."/webshop/fb-callback.php";
	$permissions = ['email'];
	$loginURL = $helper->getLoginUrl($redirectURL,$permissions);

	if (isset($_SESSION['kosar_id']) && isset($_SESSION['login_id']))
	{
		header('Location: '.$domain.'/kassza/');
	}
	else if (isset($_SESSION['login_id']))
	{
		header('Location: '.$domain.'/');
	}
	else
	{
		?>
		<div class="belepes_box">
			<form id="loginForm" action="" method="post">
				<div class="belepes_header">
					Bejelentkezés
					<span>Ha vásároltál már nálunk</span>
				</div>
				<input type="text" name="email" value="" class="form-control" placeholder="E-mail">
				<input type="password" name="passwordText" value="" class="form-control" placeholder="Jelszó">
				<p>Elfelejtetted jelszavad? <a href="<?php print $domain; ?>/jelszo-emlekezteto/">Kattints egy újért</a></p>
				<div id="login_riaszt">
					<?php
						if(isset($uzenet))
						{
							print '<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								'.$uzenet.'
							</div>';
						}
					?>
				</div>
				<a onclick="document.getElementById('loginForm').submit();" class="btn btn-active g_zold" />Belépés</a>
				<input type="hidden" name="command" value="LOGIN">

					<?php //Facebook login
					if ($conf_facebook_belepes == 1) { ?>
							<div class="fb-login-button padtop20" data-max-rows="1" data-size="medium" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="true" onlogin="window.location = '<?php echo $loginURL; ?>';"></div>	

						<?php
					}
					?>


			</form>
		</div>
		<?php
	}
?>
