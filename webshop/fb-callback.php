<?php
	if(!session_id()) {
    session_start();
	}	
	include '../config.php';
	require_once "fbconfig.php";

	try { //Facebook szinkron hibakeresés
		$accessToken = $helper->getAccessToken();
	} catch (\Facebook\Exceptions\FacebookResponseException $e) {
		echo "Response exception: " . $e->getMessage();
		exit();
	} catch (\Facebook\Exceptions\FacebookSDKException $e) {
		echo "SDK exception: " . $e->getMessage();
		exit();
	}

	if (!$accessToken) {
		header('Location: ../belepes');
		exit();
	}

	$response = $FB->get("/me?fields=id,name,email",$accessToken);
	$userData = $response->getGraphNode()->asArray();

			$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
			try
			{
				$pdo = new PDO(
				$dsn, $dbuser, $dbpass,
				Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
				);
			}
			catch (PDOException $e)
			{
				die("Nem lehet kapcsolódni az adatbázishoz!");
			}

	$query = "SELECT * FROM ".$webjel."users WHERE email='".$userData['email']."'";
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	
	if ($row['email'] != '' && $row['facebook_id'] != '') { //ha email és facebook id is van, akkor simán belép
		$_SESSION['login_id'] = $row['id'];
		$_SESSION['login_nev'] = $row['vezeteknev'];
		$_SESSION['login_tipus'] = $row['tipus'];
		header('Location: '.$domain.'/');
		exit();
	} elseif ($row['email'] != '') { //ha csak email van facebook ID-t beilleszt
			$pdo->exec("UPDATE ".$webjel."users SET facebook_id=".$userData['id']." WHERE id=".$row['id']);
			$_SESSION['login_id'] = $row['id'];
			$_SESSION['login_nev'] = $row['vezeteknev'];
			$_SESSION['login_tipus'] = $row['tipus'];
			header('Location: '.$domain.'/');
			exit();
	} 
	else { //minden egyéb esetben létrehozza a felhasználót
				$passw = password_hash($userData['id'], PASSWORD_DEFAULT);
				$pdo->exec("INSERT INTO ".$webjel."users (facebook_id,vezeteknev,email,reg_datum,jelszo,tipus) VALUES ('".$userData['id']."','".$userData['name']."','".$userData['email']."',now(),'".$passw."','user')");
				$last_id = $pdo->lastInsertId();
				$_SESSION['login_id'] = $last_id;
				$_SESSION['login_nev'] = $row['vezeteknev'];
				$_SESSION['login_tipus'] = $row['tipus'];
				echo $last_id;
				header('Location: '.$domain.'/profil/'); //mivel új regisztráció, személyes adatokhoz átirányítás
				exit();
			}

?>