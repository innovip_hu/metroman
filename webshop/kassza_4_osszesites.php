<?php
	if (isset($_SESSION['login_id']))
	{
		$query4 = "SELECT * FROM ".$webjel."users where id=".$_SESSION['login_id'];
		$res = $pdo->prepare($query4);
		$res->execute();
		$row4  = $res -> fetch();
		$nev1 = $row4['szla_nev'] ? $row4['szla_nev'] : $row4['vezeteknev'];
		$nev2 = $row4['vezeteknev'];
		$telefon = $row4['telefon'];
		$user_email = $row4['email'];
		$cim1 = $row4['cim_irszam'].' '.$row4['cim_varos'];
		$cim2 = $row4['cim_utca'].' '.$row4['cim_hszam'];
		$adoszam = $row4['adoszam'];
		if ($row4['cim_szall_varos'] == '')
		{
			$szallcim1 = $row4['cim_irszam'].' '.$row4['cim_varos'];
			$szallcim2 = $row4['cim_utca'].' '.$row4['cim_hszam'];
		}
		else
		{
			$nev2 = $row4['cim_szall_nev'];
			$szallcim1 = $row4['cim_szall_irszam'].' '.$row4['cim_szall_varos'];
			$szallcim2 = $row4['cim_szall_utca'].' '.$row4['cim_szall_hszam'];
		}
	}
	else
	{
		$nev1 = '';
		$nev2 = '';
		$telefon = '';
		$user_email = '';
		$cim1 = '';
		$cim2 = '';
		$szallcim1 = '';
		$szallcim2 = '';
		$adoszam = '';
	}

	print '<table class="table table-bordered mt-3">';
	print '<thead>
			<tr>
				<td class="col-12 col-md-4">
					<b>Vevő</b>
					<br><span id="nev_span">'.$nev1.'</span>
					<br><span id="telefon_span">'.$telefon.'</span>
					<br><span id="email_span">'.$user_email.'</span>
					<div class="kosar_mobilon_elojon margtop10">
						<b>Számlázási cím</b>
						<br><span id="nev1_span2">'.$nev1.'</span>
						<br><span id="cim1_span2">'.$cim1.'</span>
						<br><span id="cim2_span2">'.$cim2.'</span>';
				print '<br><span id="cim2_adoszam1">'.$adoszam.'</span>';
				print '</div>
					<div class="kosar_mobilon_elojon margtop10">
						<b>Szállítási cím</b>
						<br><span id="nev2_span2">'.$nev2.'</span>
						<br><span id="szallcim1_span2">'.$szallcim1.'</span>
						<br><span id="szallcim2_span2">'.$szallcim2.'</span>
					</div>
				</td>
				<td class="kosar_mobilon_eltunik col-12 col-md-4">
					<b>Számlázási cím</b>
					<br><span id="nev1_span">'.$nev1.'</span>
					<br><span id="cim1_span">'.$cim1.'</span>
					<br><span id="cim2_span">'.$cim2.'</span>';
				print '<br><span id="cim2_adoszam2">'.$adoszam.'</span>';
				print '</td>';
				print '<td class="kosar_mobilon_eltunik col-12 col-md-4">
						<b>Szállítási cím</b>
						<br><span id="nev2_span">'.$nev2.'</span>
						<br><span id="szallcim1_span">'.$szallcim1.'</span>
						<br><span id="szallcim2_span">'.$szallcim2.'</span>
					</td>';
			print '</tr>
			</thead>
			<thead>
			<tr>';
	print '</table>';

	print '<table class="table table-bordered">';
		
			//Kosár tartalma
				$query = "SELECT * FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];
				$db = 0;
				$ar = 0;
				$id = 0;
				$items_google = array();
				$items_google_list = 0;				
				print '<td colspan="4" style="padding: 12px; border-color: #E1E1E1; background-color: #E1E1E1;">Név</td>
					<td colspan="2" style="padding: 12px; border-color: #E1E1E1; background-color: #E1E1E1;" class="kosar_mobilon_eltunik"></td>
					<td colspan="2" style="padding: 12px; border-color: #E1E1E1; background-color: #E1E1E1;" class="text-right kosar_mobilon_eltunik">Egységár</td>
					<td colspan="2" style="padding: 12px; border-color: #E1E1E1; background-color: #E1E1E1;" class="text-right kosar_mobilon_eltunik">Mennyiség</td>
					<td colspan="2" style="padding: 12px; border-color: #E1E1E1; background-color: #E1E1E1;" class="text-right">Összesen</td>
			</tr>
			</thead>
			<tbody>';
				foreach ($pdo->query($query) as $row)
				{
					$db = $db + 1;
					if($row['term_akcios_ar'] == 0) //ha nem akciós
					{
						$term_ar = $row['term_ar'];
					}
					else //ha akciós
					{
						$term_ar = $row['term_akcios_ar'];
					}
					//termék adatai
					$query_term = "SELECT * FROM ".$webjel."termekek where id=".$row['term_id'];
					$res = $pdo->prepare($query_term);
					$res->execute();
					$row_term  = $res -> fetch();
					// Ha OVIP termék
					if ($row_term['ovip_id'] !=0)
					{
						$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_term['id']." ORDER BY alap DESC LIMIT 1";
						$res = $pdo->prepare($query_kep);
						$res->execute();
						$row_kep = $res -> fetch();
						$alap_kep = $row_kep ? $row_kep['kep'] : "";
						if ($alap_kep == '') 
						{
							$kep_link = $domain.'/webshop/images/noimage.png';
						}
						else
						{
							if($row_kep['thumb'] != '')
							{
								$kep = $row_kep['thumb'];
							}
							else
							{
								$kep = $row_kep['kep'];
							}
							if ($row_kep['ovip_termek_id'] == 0)
							{
								$kep = $domain.'/images/termekek/'.$kep;
							}							
							$kep_link = $kep;
						}
						$kep = '<img src="'.$kep_link.'" style="min-width:90px" border="0">';
					}
					elseif ($row['term_kep'] != '')
					{
						$kep = '<img src="'.$domain.'/images/termekek/'.$row['term_kep'].'" style="min-width:90px" border="0">';
					}
					else
					{
						$kep = '';
					}
					
					$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_term['csop_id'];
					$res = $pdo->prepare($query_csop);
					$res->execute();
					$row_csop  = $res -> fetch();

      				$items_google[] = array('id' => $row['term_id'], 'name' => $row['term_nev'], 'list_name' => 'Kassza termékek', "category" => $row_csop['nev'], "list_position" => $items_google_list, "quantity" => $row['term_db'], "price" => $term_ar);

					print '<tr>
								<td colspan="4"><a href="'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row_term['nev_url'].'">
									<h6 class="margbot0">'.$row['term_nev'].'</h6>';
								// Jellemzők
								$query_jell = "SELECT * FROM ".$webjel."kosar_tetel_termek_parameter_ertek WHERE kosar_tetel_id = ".$row['id']." ORDER BY termek_parameter_nev ASC";
								$elso = 1;
								foreach ($pdo->query($query_jell) as $row_jell)
								{
									if($elso == 1) { print ''; $elso = 0; } else { print '<br/>'; }
									print $row_jell['termek_parameter_nev'].': '.$row_jell['termek_parameter_ertek_nev'];
								}						
								print '</a></td>
								<td colspan="2" class="text-center kosar_mobilon_eltunik" style="width: 90px;">'.$kep.'</td>
								<td colspan="2" class="text-right kosar_mobilon_eltunik">'.number_format($term_ar, 0, ',', ' ').' Ft</td>
								<td colspan="2" class="text-right kosar_mobilon_eltunik">'.$row['term_db'].'</td>
								<td colspan="2" class="text-right">'.number_format($term_ar * $row['term_db'], 0, ',', ' ').' Ft</td>
							</tr>';
					$ar = $ar + ($term_ar * $row['term_db']);
				}
				
				// beállítás
				$query_beall = 'SELECT * FROM '.$webjel.'beallitasok WHERE id=1';
				$res = $pdo->prepare($query_beall);
				$res->execute();
				$row_beall  = $res -> fetch();
				
				// Ha van ingyenes szállítási költség
				if ($row_beall['ingyenes_szallitas'] != 0 && $ar >= $row_beall['ingyenes_szallitas'])
				{
					$szall_kolts = 0;
				}
				// Kupon vizsgálat ingyen szállítás kedvezmény miatt
				$ar_kedvezmeny = 0;
				if($row['kupon_id'] > 0)
				{
					$query_kupon = "SELECT * FROM ".$webjel."kuponok where id=".$row['kupon_id'];
					$res = $pdo->prepare($query_kupon);
					$res->execute();
					$row_kupon = $res -> fetch();
					if($row_kupon['kupon_fajta'] == 'Ingyenes szállítás')
					{
						$szall_kolts = 0;
					}
					else if($row_kupon['kupon_fajta'] == 'Kedvezmény')
					{
						if ($row_kupon['kedv_tipus'] == 'szazalek')
						{
							$ar_kedvezmeny = round($ar * ($row_kupon['kedvezmeny'] / 100));
						}
						else if ($row_kupon['kedv_tipus'] == 'osszeg')
						{
							$ar_kedvezmeny = $row_kupon['kedvezmeny'];
						}
					}
					else if($row_kupon['kupon_fajta'] == 'Ajándék termék')
					{
						$query_ajandek = "SELECT * FROM ".$webjel."termekek WHERE id=".$row_kupon['termek_id'];
						$res = $pdo->prepare($query_ajandek);
						$res->execute();
						$row_ajandek = $res -> fetch();
						print '<tr>
									<td colspan="4">'.$row_ajandek['nev'].'</td>
									<td colspan="2" class="kosar_mobilon_eltunik"></td>
									<td colspan="2" class="kosar_mobilon_eltunik">0 Ft</td>
									<td colspan="2" class="kosar_mobilon_eltunik">1</td>
									<td colspan="2" class="text-right">0 Ft</td>
								</tr>';
					}
				}
				
				print '<tr>
							<td colspan="6" class="kosar_mobilon_eltunik" style="border-right:0;"></td>
							<td colspan="4" class="text-right td_border_bal_mobilon">Összesen</td>
							<td colspan="2" class="text-right">'.number_format($ar, 0, ',', ' ').' Ft</td>
						</tr>
						<input type="hidden" id="vegosszeghez_osszesen" value="'.$ar.'" />';
				
				if($ar_kedvezmeny > 0)
				{
					print '<tr>
								<td colspan="6" class="kosar_mobilon_eltunik" style="border-right:0;"></td>
								<td colspan="4" class="text-right td_border_bal_mobilon">Kedvezmény</td>
								<td colspan="2" class="text-right">-'.number_format($ar_kedvezmeny, 0, ',', ' ').' Ft</td>
							</tr>';
				}
				echo '<input type="hidden" id="vegosszeghez_ar_kedvezmeny" value="'.$ar_kedvezmeny.'" />';
						
				print '<tr>
							<td colspan="6" class="kosar_mobilon_eltunik" style="border-right:0;"></td>
							<td colspan="4" class="text-right td_border_bal_mobilon">Szállítási költség</td>
							<td colspan="2" class="text-right"><span id="szall_kolts_span">'.number_format($szall_kolts, 0, ',', ' ').'</span> Ft</td>
						</tr>';
				
				if (isset($utanvet) && $utanvet > 0) {
					$lathato_utanvet = '';
				} else {
					$utanvet = 0;
					$lathato_utanvet = ' style="display: none;"';
				}

				print '<tr '.$lathato_utanvet.' id="utanvet_sor">
							<td colspan="6" class="kosar_mobilon_eltunik" style="border-right:0;"></td>
							<td colspan="4" class="text-right td_border_bal_mobilon">Utánvét költség</td>
							<td colspan="2" class="text-right"><span id="utanvet_span">'.number_format($utanvet, 0, ',', ' ').'</span> Ft</td>
						</tr>';				

				print '<tr style="font-weight:bold;">
							<td colspan="6" class="kosar_mobilon_eltunik" style="border-right:0;"></td>
							<td colspan="4" class="text-right td_border_bal_mobilon">Fizetendő</td>
							<td colspan="2" class="text-right"><span id="fizetendo_span">'.number_format(($ar + $szall_kolts - $ar_kedvezmeny + $utanvet), 0, ',', ' ').'</span> Ft</td>
						</tr>';
			
			print '<tr>
				<td colspan="6" class="td_border_jobb_mobilon">
					<b>Fizetési mód:</b> <span id="fiz_mod_span">'.$alap_fiz_mod.'</span>
					<br/><b>Szállítás:</b> <span id="szall_mod_span">'.$alap_szall_mod.'</span>
					<br/>
					<span id="ppp_span"></span>
					<span id="pp_span"></span>
					<span id="gls_span"></span>';
				print '</td>
				<td colspan="6" class="kosar_mobilon_eltunik" style="border-left:0;"></td>
			</tr>
		<tbody></table>';
	$kod_otp = /* $row4['id']. */'a'.time().rand(1, 99999);
?>
	<input type="hidden" value='<?php echo json_encode($items_google); ?>' id="kosar_tartalma_google">

	<p style="text-align:center;">Megjegyzés:<br/><textarea style="width: 100%; height:60px" id="megjegyzes" name="megjegyzes"></textarea></p>
	

	<div class="col-lg-12 col-md-12 riaszt_div text-center mb-0" id="adatv_riaszt"><div class="alert alert-warning alert-danger text-primary border-primary">Az általános szerződési feltételek és Adatvédelmi tájékoztató nem lett elfogadva!<br>A jelölőnégyzet segítségével fogadhatja el a feltételeket.</div></div>
    <div class="form-wrap">
      <label class="checkbox-inline">
        <input name="adatv" value="elfogad" id="adatv" type="checkbox">Elfogadom az <a style="text-decoration: underline;" href="<?=$domain?>/aszf" target="_blank">Általános szerződési feltételeket</a> és az <a style="text-decoration: underline;" href="<?=$domain?>/adatkezelesi-tajekoztato" target="_blank">Adatkezelési tájékoztatót</a>. Tudomásul veszem, hogy a megrendelés elküldése fizetési kötelezettséggel jár.
      </label>
    </div>		

    <?php /*
	<div class="col-lg-12 col-md-12 riaszt_div text-center mb-0" id="vas_fel_riaszt"><div class="alert alert-warning alert-danger text-primary">A vásárlási feltétel nem lett elfogadva.</div></div>
    <div class="form-wrap">
      <label class="checkbox-inline">
        <input name="vas_fel" value="ok" id="vas_fel" type="checkbox">A <a href="<?php print $domain; ?>/aszf/" target="_blank" >vásárlási és fizetési feltételeket</a> elfogadom!
      </label>
    </div>	
    */ ?>	

    <div class="form-wrap mt-2">
      <label class="checkbox-inline">
        <input name="hirlevel_rend" value="igen" id="hirlevel_rend" type="checkbox">Hírlevél feliratkozás <a style="text-decoration: underline;" href="<?=$domain?>/adatkezelesi-tajekoztato" target="_blank">Adatkezelési Tájékoztató és Szabályzat</a>.
      </label>
    </div>	

    <?php if (isset($volt_hianyzo_reg_adat) && $volt_hianyzo_reg_adat == 1): ?>
    	<p class="text-center mb-4" style="color: red;">Kérjük töltse ki a rendeléshez az összes kötelező adatot.</p>
		<div style="text-align: center;">
			<button type="button" class="button button-default-outline button-sm" onclick="location.href='<?php print $domain; ?>/profil/';">Adatok módosítása</button>
		</div>    	
    <?php else: ?>
		<div id="rendeles_gomb_div" style="text-align: center;">
			<button type="button" id="rendeles_leadasa" class="button button-sm button-gradient">Rendelés leadása</button>
		</div>    	
    <?php endif ?>
<!-- OTP-hez -->
<?php /*
<input type="hidden" id="otp_fizetendo" value="<?php print round($fizetendo); ?>"/>
<input type="hidden" id="otp_kosar_id" value="<?php print $_SESSION['kosar_id']; ?>"/>
<input type="hidden" id="kod_otp" value="<?php print $kod_otp; ?>"/>
*/ ?>