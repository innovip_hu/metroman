
<!--eltűntető script-->
<script type="text/javascript">
	function showMe (box) {
		var chboxs = document.getElementsByName("mascim");
		var vis = "none";
		for(var i=0;i<chboxs.length;i++) { 
			if(chboxs[i].checked){
				 vis = "block";
					break;
			}
		}
		document.getElementById(box).style.display = vis;
	}

	function phonenumber(inputtxt)
	{
	  	var phoneno = /((?:\+?3|0)6)(?:|\()?(\d{1,2})(?:|\))?(\d{4})(\d{3,4})/g;
	  	if(inputtxt.value.match(phoneno))
     	{
	   		return true;      
 		}
   		else
 		{
	   		return false;
     	}
	}

	function varos(inputtxt)
	{
	  	var szamok = /\d/g;
	  	if(inputtxt.value.match(szamok))
     	{
	   		return false;      
	 	}
	   	else
     	{
	   		return true;
     	}
	}	

	function adoszam(adoszam)
	{
		var formatum = /^[0-9]{8}-[0-9]-[0-9]{2}$/;
	  	if(adoszam.value.match(formatum))
     	{
	   		return true;      
	 	}
	   	else
     	{
	   		return false;
     	}		
	}

	function irsz(inputtxt)
	{
	  var szamok = /^[0-9]+$/;
	  if(inputtxt.value.match(szamok))
	     {
		   return true;      
		 }
	   else
	     {
		   return false;
	     }
	}	

	function regisztracio() {
		document.getElementById("szla_nev_riaszt").style.display = 'none';
		document.getElementById("jszo1_riaszt").style.display = 'none';
		document.getElementById("jszo1_riaszt2").style.display = 'none';
		document.getElementById("jszo2_riaszt").style.display = 'none';
		document.getElementById("vezeteknev_riaszt").style.display = 'none';
		document.getElementById("email_riaszt").style.display = 'none';
		document.getElementById("telefon_riaszt").style.display = 'none';
		document.getElementById("telefon_riaszt2").style.display = 'none';
		document.getElementById("cim_varos_riaszt").style.display = 'none';
		document.getElementById("cim_varos_riaszt2").style.display = 'none';
		document.getElementById("cim_utca_riaszt").style.display = 'none';
		document.getElementById("cim_hszam_riaszt").style.display = 'none';
		document.getElementById("cim_irszam_riaszt").style.display = 'none';
		document.getElementById("cim_irszam_riaszt2").style.display = 'none';
		document.getElementById("feltetel_riaszt").style.display = 'none';
		document.getElementById("adoszam_riaszt").style.display = 'none';
		
		if(document.getElementById("jszo1").value == '')
		{
			document.getElementById("jszo1").focus();
			document.getElementById("jszo1_riaszt").style.display = 'block';
		}
		else if(document.getElementById("jszo2").value == '')
		{
			document.getElementById("jszo2").focus();
			document.getElementById("jszo2_riaszt").style.display = 'block';
		}
		else if(document.getElementById("jszo2").value != document.getElementById("jszo1").value)
		{
			document.getElementById("jszo1").focus();
			document.getElementById("jszo1_riaszt2").style.display = 'block';
		}
		else if(document.getElementById("vezeteknev").value == '')
		{
			document.getElementById("vezeteknev").focus();
			document.getElementById("vezeteknev_riaszt").style.display = 'block';
		}
		else if(document.getElementById("email").value == '')
		{
			document.getElementById("email").focus();
			document.getElementById("email_riaszt").style.display = 'block';
		}
		else if(document.getElementById("telefon").value == '')
		{
			document.getElementById("telefon").focus();
			document.getElementById("telefon_riaszt").style.display = 'block';
		}
		else if(phonenumber(document.getElementById("telefon")) == false)
		{
			document.getElementById("telefon").focus();
			document.getElementById("telefon_riaszt2").style.display = 'block';
		}		
		else if(document.getElementById("cim_varos").value == '')
		{
			document.getElementById("cim_varos").focus();
			document.getElementById("cim_varos_riaszt").style.display = 'block';
		}
		else if(varos(document.getElementById("cim_varos")) == false)
		{
			document.getElementById("cim_varos").focus();
			document.getElementById("cim_varos_riaszt2").style.display = 'block';
		}		
		else if(document.getElementById("cim_utca").value == '')
		{
			document.getElementById("cim_utca").focus();
			document.getElementById("cim_utca_riaszt").style.display = 'block';
		}
		else if(document.getElementById("cim_hszam").value == '')
		{
			document.getElementById("cim_hszam").focus();
			document.getElementById("cim_hszam_riaszt").style.display = 'block';
		}
		else if(document.getElementById("cim_irszam").value == '')
		{
			document.getElementById("cim_irszam").focus();
			document.getElementById("cim_irszam_riaszt").style.display = 'block';
		}
		else if(irsz(document.getElementById("cim_irszam")) == false)
		{
			document.getElementById("cim_irszam").focus();
			document.getElementById("cim_irszam_riaszt2").style.display = 'block';
		}		
		else if(document.getElementById("szla_nev").value == '')
		{
			document.getElementById("szla_nev").focus();
			document.getElementById("szla_nev_riaszt").style.display = 'block';
		}
		else if(adoszam(document.getElementById("adoszam")) == false && document.getElementById("adoszam").value != '')
		{
			document.getElementById("adoszam").focus();
			document.getElementById("adoszam_riaszt").style.display = 'block';
		}					
		else if(document.getElementById("feltetel").checked == false)
		{
			document.getElementById("feltetel_riaszt").style.display = 'block';
		}			
		else
		{
			gtag('event', 'click', {  
			   'event_category': 'registration',  
			   'event_label': 'registration_btn'
			}); 

	        if (document.getElementById("hirlevel_reg").checked == true)
	        {
	            gtag('event', 'click', {  
	               'event_category': 'newsletter',  
	               'event_label': 'registration_newsletter_checkbox'
	            });
	        }  			

			document.getElementById('regForm').submit();
		}
	}
</script>
<?php

		$reg_ok = '';
		if (isset($_POST['command']) && $_POST['command'] == "REG")
		{
			include  $gyoker.'/webshop/reg_check.php';
			if (reg_check($_POST) == "rendben")
			{
			//sql feltöltés
				$jelszo = $_POST['jszo1'];
				if (isset($_POST['mascim']) && $_POST['mascim'] == 'ok')
				{
					$cim_szall_nev = $_POST['cim_szall_nev'];
					$cim_szall_varos = $_POST['cim_szall_varos'];
					$cim_szall_utca = $_POST['cim_szall_utca'];
					$cim_szall_hszam = $_POST['cim_szall_hszam'];
					$cim_szall_irszam = $_POST['cim_szall_irszam'];
				}
				else
				{
					$cim_szall_nev = '';
					$cim_szall_varos = '';
					$cim_szall_utca = '';
					$cim_szall_hszam = '';
					$cim_szall_irszam = '';
				}
				$insertcommand = "INSERT INTO ".$webjel."users (email,telefon,vezeteknev,cim_varos,cim_utca,cim_hszam,cim_irszam,cim_szall_nev,cim_szall_varos,cim_szall_utca,cim_szall_hszam,cim_szall_irszam,tipus,jelszo,reg_datum,szla_nev,adoszam) VALUES ('".$_POST['email']."','".$_POST['telefon']."','".$_POST['vezeteknev']."','".$_POST['cim_varos']."','".$_POST['cim_utca']."','".$_POST['cim_hszam']."','".$_POST['cim_irszam']."','".$cim_szall_nev."','".$cim_szall_varos."','".$cim_szall_utca."','".$cim_szall_hszam."','".$cim_szall_irszam."','user','".password_hash($jelszo, PASSWORD_DEFAULT)."',NOW(),'".$_POST['szla_nev']."','".$_POST['adoszam']."')";
				$result = $pdo->prepare($insertcommand);
				$result->execute();
			//id meghatározása
				$query_id = "SELECT * FROM ".$webjel."users WHERE email='".$_POST['email']."'";
				$res = $pdo->prepare($query_id);
				$res->execute();
				$row_id = $res -> fetch();
				$id = $row_id['id'];
				
				$reg_ok = 'rendben';

				// Levélküldés
				$uzenet = '<p>Köszönjük regisztrációját! Ön az alábbi adatokkal regisztrált webáruházunkba:</p><br>
							<p>Számlázási cím: '.$_POST['szla_nev'].', '.$_POST['cim_irszam'].' '.$_POST['cim_varos'].', '.$_POST['cim_utca'].' '.$_POST['cim_hszam'];
				if (isset($_POST['mascim']) && $_POST['mascim'] == 'ok')
				{
					$uzenet .= '<br>Szállítási cím: '.$cim_szall_nev.', '.$cim_szall_irszam.' '.$cim_szall_varos.', '.$cim_szall_utca.' '.$cim_szall_hszam;	
				}
				else
				{
					$uzenet .= '<br>Szállítási cím: '.$_POST['szla_nev'].', '.$_POST['cim_irszam'].' '.$_POST['cim_varos'].', '.$_POST['cim_utca'].' '.$_POST['cim_hszam'];
				}
				$uzenet .= '<br>Kapcsolattartó: '.$_POST['vezeteknev'].'
							<br>Telefonszám: '.$_POST['telefon'].'
							<br>E-mail cím: '.$_POST['email'];

				if ($_POST['adoszam'] != '')
				{
					$uzenet .= '<br>Adószám: '.$_POST['adoszam'].'';
				}
				$uzenet .= '</p>';

				$uzenet .= '<br><p>Regisztrációját <a href="'.$domain.'/belepes">belépés</a> után bármikor módosíthatja a Profil menüben.</p>';

				$nev1 = $_POST['vezeteknev'];

				include $gyoker.'/module/mod_email_reg.php';
				//phpMailer
				require_once($gyoker.'/webshop/PHPMailer-master/PHPMailerAutoload.php');
				$mail = new PHPMailer();
				$mail->isHTML(true);
				$mail->CharSet = 'UTF-8';
				// $mail->addCustomHeader('MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n");
				if($conf_smtp == 1)
				{
					// SMTP
					$mail->IsSMTP(); // telling the class to use SMTP
					$mail->Host       = $smtp_host; // SMTP server
					$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
															   // 1 = errors and messages
															   // 2 = messages only
					$mail->SMTPAuth   = true;                  // enable SMTP authentication
					$mail->SMTPSecure = $smtp_protokol;
					$mail->Host       = $smtp_host; // sets the SMTP server
					$mail->Port       = $smtp_port;                    // set the SMTP port for the GMAIL server
					$mail->Username   = $smtp_user; // SMTP account username
					$mail->Password   = $smtp_pass;        // SMTP account password
					$mail->SetFrom($smtp_email, $smtp_name);
					$mail->AddReplyTo($smtp_email, $smtp_name);
				}
				else
				{
					$mail->SetFrom($email, $webnev);
				}
				$mail->AddAddress($_POST['email'], $_POST['vezeteknev']);
				$mail->Subject = "Sikeres regisztráció";
				$htmlmsg = $mess;
				$mail->Body = $htmlmsg;
				if(!$mail->Send()) {
				  echo "Mailer Error: " . $mail->ErrorInfo;
				}				

				if (isset($_POST['hirlevel_reg']) && $_POST['hirlevel_reg'] == 'igen')
				{
					$_POST['nev'] = $_POST['vezeteknev'];

					$kod = time().rand(1, 99999);
					
					// Levélküldés
					$uzenet = '<p>Köszönjük, hogy felíratkozott hírlevelünkre!</p>
								<p>Legyen kedves visszaigazolni ezen a <a href="'.$domain.'/hirlevel/?kod='.$kod.'" target="_blank">LINKEN</a> regisztrációját, hogy értesítést kaphasson legfrissebb híreinkről. </p>';
					include $gyoker.'/module/mod_email.php';
					//phpMailer
					require_once($gyoker.'/webshop/PHPMailer-master/PHPMailerAutoload.php');
					$mail = new PHPMailer();
					$mail->isHTML(true);
					$mail->CharSet = 'UTF-8';
					// $mail->addCustomHeader('MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n");
					if($conf_smtp == 1)
					{
						// SMTP
						$mail->IsSMTP(); // telling the class to use SMTP
						$mail->Host       = $smtp_host; // SMTP server
						$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
																   // 1 = errors and messages
																   // 2 = messages only
						$mail->SMTPAuth   = true;                  // enable SMTP authentication
						$mail->SMTPSecure = $smtp_protokol;
						$mail->Host       = $smtp_host; // sets the SMTP server
						$mail->Port       = $smtp_port;                    // set the SMTP port for the GMAIL server
						$mail->Username   = $smtp_user; // SMTP account username
						$mail->Password   = $smtp_pass;        // SMTP account password
						$mail->SetFrom($smtp_email, $smtp_name);
						$mail->AddReplyTo($smtp_email, $smtp_name);
					}
					else
					{
						$mail->SetFrom($email, $webnev);
					}
					$mail->AddAddress($_POST['email'], $_POST['nev']);
					$mail->Subject = "Hírlevél feliratkozás";
					$htmlmsg = $mess;
					$mail->Body = $htmlmsg;
					if(!$mail->Send()) {
					  echo "Mailer Error: " . $mail->ErrorInfo;
					}
					
					if(isset($_POST['webshop']) && $_POST['webshop'] == 'webshop')
					{
						$webshop = 1;
					}
					else
					{
						$webshop = 0;
					}
					if(isset($_POST['kozbeszerzes']) && $_POST['kozbeszerzes'] == 'kozbeszerzes')
					{
						$kozbeszerzes = 1;
					}
					else
					{
						$kozbeszerzes = 0;
					}

					
					$insertcommand = "INSERT INTO ".$webjel."hirlevel (nev,email,kod,webshop,kozbeszerzes) VALUES ('".$_POST['nev']."','".$_POST['email']."','".$kod."','".$webshop."','".$kozbeszerzes."')";
					$result = $pdo->prepare($insertcommand);
					$result->execute();	
				}			
				
				if ($oldal == 'kassza') // Ha a kasszánál van
				{
					$_SESSION['login_id'] = $id;
					$_SESSION['login_nev'] = $_POST['email'];
					$_SESSION['login_tipus'] = 'user';
					unset($_SESSION['noreg_id']);
					header('Location: '.$domain.'/kassza/');
				}
				else
				{
					print '<p align="center">A regisztrációd sikeresen megtörtént, kattints <a href="'.$domain.'/belepes/">IDE</a> a belépéshez.</p> <p align="center">Jó vásárlást kívánunk!<p>';
				}
			}
		}
		if ($reg_ok != 'rendben')
		{
			if (isset($_POST['command']) && $_POST['command'] == 'REG')
			{
				print '<div class="row"><div class="col-lg-12 col-md-12 "><div class="alert alert-danger bg-secondary" id="jszo1_riaszt2">'.reg_check($_POST).'</p></div></div></div>';
			}
			?>
			<h4 class="text-center">Új regisztráció</h4>
			<form id="regForm" action="" method="post">
			<div class="row row-30 pt-3">	
				
				
				<div class="col-lg-6 col-md-6">
					<h6 class="text-center pb-2">Személyes adatok</h6>

					<div class="form-wrap">
                  		<input class="form-input" id="email" type="email" name="email" value="<?php if(isset($_POST['email'])){print $_POST['email'];} ?>">
                  		<label class="form-label" for="email">E-mail cím</label>
                  		<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="email_riaszt">Add meg az email címet!</div>
					</div>

					<div class="form-wrap">
                  		<input class="form-input" id="jszo1" type="password" name="jszo1" value="">
                  		<label class="form-label" for="jszo1">Jelszó</label>
						<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="jszo1_riaszt">Add meg a jelszavad!</div>
						<div class="bg-secondary alert alert-danger riaszt_div mt-2 pt-2" id="jszo1_riaszt2">A két jelszó nem egyezik!</div>
					</div>				
					
					<div class="form-wrap">
                  		<input class="form-input" id="jszo2" type="password" name="jszo2" value="">
                  		<label class="form-label" for="jszo2">Jelszó újra</label>
						<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="jszo2_riaszt">Add meg a jelszavad újra!</div>
					</div>

					<div class="form-wrap">
                  		<input class="form-input" id="vezeteknev" type="text" name="vezeteknev" value="<?php if(isset($_POST['vezeteknev'])){print $_POST['vezeteknev'];} ?>">
                  		<label class="form-label" for="vezeteknev">Kapcsolattartó név</label>
						<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="vezeteknev_riaszt">Add meg a neved!</div>
					</div>
					
					<div class="form-wrap">
                  		<input class="form-input" id="telefon" type="text" name="telefon" value="<?php if(isset($_POST['telefon'])){print $_POST['telefon'];} ?>">
                  		<label class="form-label" for="telefon">Telefonszám (Példa: +36301234567)</label>
						<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="telefon_riaszt">Add meg a telefonszámot!</div>
						<div class="bg-secondary alert alert-danger riaszt_div mt-2 pt-2" id="telefon_riaszt2">Nem megfelelő telefonszám formátum!</div>
					</div>					
					
					
				</div>
				<div class="col-lg-6 col-md-6">
					<h6 class="text-center pb-2">Számlázási adatok</h6>

					<div class="form-wrap">
                  		<input class="form-input" id="szla_nev" type="text" name="szla_nev" value="<?php if(isset($_POST['szla_nev'])){print $_POST['szla_nev'];} ?>">
                  		<label class="form-label" for="szla_nev">Számlázási név</label>
						<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="szla_nev_riaszt">Add meg a nevet!</div>
					</div>

					<div class="form-wrap">
                  		<input class="form-input" id="cim_varos" type="text" name="cim_varos" value="<?php if(isset($_POST['cim_varos'])){print $_POST['cim_varos'];} ?>">
                  		<label class="form-label" for="cim_varos">Város</label>
						<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="cim_varos_riaszt">Add meg a várost!</div>
						<div class="bg-secondary alert alert-danger riaszt_div mt- pt-2" id="cim_varos_riaszt2">Nem megfelelő karakterek!</div>
					</div>	

					<div class="form-wrap">
                  		<input class="form-input" id="cim_utca" type="text" name="cim_utca" value="<?php if(isset($_POST['cim_utca'])){print $_POST['cim_utca'];} ?>">
                  		<label class="form-label" for="cim_utca">Utca</label>
						<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="cim_utca_riaszt">Add meg az utcát!</div>
					</div>	

					
					<div class="form-wrap">
                  		<input class="form-input" id="cim_hszam" type="text" name="cim_hszam" value="<?php if(isset($_POST['cim_hszam'])){print $_POST['cim_hszam'];} ?>">
                  		<label class="form-label" for="cim_hszam">Házszám</label>
						<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="cim_hszam_riaszt">Add meg a házzszámot!</div>
					</div>	

					<div class="form-wrap">
                  		<input class="form-input" id="cim_irszam" type="text" name="cim_irszam" value="<?php if(isset($_POST['cim_irszam'])){print $_POST['cim_irszam'];} ?>">
                  		<label class="form-label" for="cim_irszam">Irányítószám</label>
						<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="cim_irszam_riaszt">Add meg az irányítószámot!</div>
						<div class="bg-secondary alert alert-danger riaszt_div mt-2 pt-2" id="cim_irszam_riaszt2">Az irányítószám csak szám lehet!</div>
					</div>	

					<div class="form-wrap">
                  		<input class="form-input" id="adoszam" type="text" name="adoszam" value="<?php if(isset($_POST['adoszam'])){print $_POST['adoszam'];} ?>">
                  		<label class="form-label" for="adoszam">Adószám</label>
                  		<div class="bg-secondary alert alert-danger riaszt_div mt-2" id="adoszam_riaszt">Nem megfelelő adószám formátum!</div>
					</div>	

					<div class="form-wrap">
	                	<label class="checkbox-inline">		
	                    <input onclick="showMe('div1', this);" name="mascim" value="ok" id="mascim" type="checkbox" <?php if (isset($_POST['mascim']) && $_POST['mascim']=='ok') {print 'checked';} ?>>Eltérő szállítási cím
	                  </label>
	                </div>

					<?php if (!isset($_POST['mascim']) || (isset($_POST['mascim']) && $_POST['mascim']!='ok')) {$display = 'style="display:none"';}else{$display='';} ?>

					<div id="div1" <?php print $display; ?> >
						<div class="form-wrap">
	                  		<input class="form-input" id="cim_szall_nev" type="text" name="cim_szall_nev" value="<?php if(isset($_POST['cim_szall_nev'])){print $_POST['cim_szall_nev'];} ?>">
	                  		<label class="form-label" for="cim_szall_nev">Név</label>
						</div>	

						<div class="form-wrap">
	                  		<input class="form-input" id="cim_szall_varos" type="text" name="cim_szall_varos" value="<?php if(isset($_POST['cim_szall_varos'])){print $_POST['cim_szall_varos'];} ?>">
	                  		<label class="form-label" for="cim_szall_varos">Város</label>
						</div>	

						<div class="form-wrap">
	                  		<input class="form-input" id="cim_szall_utca" type="text" name="cim_szall_utca" value="<?php if(isset($_POST['cim_szall_utca'])){print $_POST['cim_szall_utca'];} ?>">
	                  		<label class="form-label" for="cim_szall_utca">Utca</label>
						</div>	

						<div class="form-wrap">
	                  		<input class="form-input" id="cim_szall_hszam" type="text" name="cim_szall_hszam" value="<?php if(isset($_POST['cim_szall_hszam'])){print $_POST['cim_szall_hszam'];} ?>">
	                  		<label class="form-label" for="cim_szall_hszam">Házszám</label>
						</div>	

						<div class="form-wrap">
	                  		<input class="form-input" id="cim_szall_irszam" type="text" name="cim_szall_irszam" value="<?php if(isset($_POST['cim_szall_irszam'])){print $_POST['cim_szall_irszam'];} ?>">
	                  		<label class="form-label" for="cim_szall_irszam">Irányítószám</label>
						</div>																															
					</div>	                
										

				</div>
				<div class="col-lg-12 col-md-12 text-center">

	                <div class="form-wrap">
	                  <label class="checkbox-inline">
	                    <input name="feltetel" value="elfogad" id="feltetel" type="checkbox">Elolvastam és megértettem a Metroman Hungária Kft. <a style="text-decoration: underline;" href="<?=$domain?>/adatkezelesi-tajekoztato" target="_blank">Adatkezelési Tájékoztatóját és Szabályzatát</a>, és hozzájárulok ahhoz, hogy a megadott adataimat a szolgáltató, mint adatkezelő a Szabályzatban foglaltaknak megfelelően kezelje.
	                  </label>
	                </div>		

	                <div class="bg-secondary alert alert-danger riaszt_div mt-2" id="feltetel_riaszt">A vásárlási feltétel nincs elfogadva!</div>

	                <div class="form-wrap mt-2">
	                  <label class="checkbox-inline">
	                    <input name="hirlevel_reg" value="igen" id="hirlevel_reg" type="checkbox">Hírlevél feliratkozás <a style="text-decoration: underline;" href="<?=$domain?>/adatkezelesi-tajekoztato" target="_blank">Adatkezelési Tájékoztató és Szabályzat</a>.
	                  </label>
	                </div>	                

				</div>
				<div class="col-lg-12 col-md-12 text-center">
					<div class="col-lg-12 col-md-12">
						<button onclick="regisztracio()" class="button button-gray-5" type="button">Regisztrálok</button>
						<input type="hidden" name="command" value="REG">
					</div>
						
				</div>
				
			</div>
			</form>
		<?php
		}
?>
