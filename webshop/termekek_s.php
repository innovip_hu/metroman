	<link rel="stylesheet" type="text/css" href="<?php print $domain; ?>/webshop/style.css" />
	<script type="text/javascript" src="<?php print $domain; ?>/webshop/scripts/clearbox.js"></script>
	<?php
		// Kívánság lista
		if (isset($_POST['command']) && $_POST['command'] == 'kivansag_lista_termekek')
		{
			if(isset($_SESSION['login_id']))
			{
				$rownum = 0;
				$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."kivansag_lista WHERE termek_id = ".$row['id']." AND user_id = ".$_SESSION['login_id']);
				$res->execute();
				$rownum = $res->fetchColumn();
				if ($rownum == 0) // Ha még nincs a listában
				{
					$insertcommand = "INSERT INTO ".$webjel."kivansag_lista (termek_id,user_id) VALUES (".$_POST['id'].",".$_SESSION['login_id'].")";
					$result = $pdo->prepare($insertcommand);
					$result->execute();
				}
				print '<div class="row"><div class="alert alert-success alert-dismissable margbot0"><p>A terméket sikeresen a kívánságlistádra helyezted. Reméljük, hamarosan átkerül a kosaradba.</p></div></div>';
			}
			else
			{
				print '<div class="row"><div class="alert alert-danger alert-dismissable margbot0"><p>Helyezd az általad választott terméket egyszerűen a kívánságlistádra és add le rendelésed akkor, amikor már biztos vagy a dolgodban. A kívánságlistádon elhelyezett termékeid aktuális állapotát érdemes figyelni nehogy lemaradj a rendelésről, hiszen termékkínálatunkat folyamatosan változtatjuk. A kívánságlista használatához regisztráció szükséges. <a href="'.$domain.'/regisztracio/">Regisztrálj!</a></p></div></div>';
			}
		}
		
		if (isset($_POST['osszehasonlitas'])) // Összehasonlítás
		{
			include $gyoker.'/webshop/module/mod_osszehasonlitas_oldal.php';
		}
		else
		{
			$csop_id = 0;
			$csop_leiras = '';
			if (isset($_GET['kat_urlnev'])) //Termék utáni csoportnév kiíratása
			{
				if ($config_ovip == 'I')
				{
					$res = $pdo->prepare("SELECT * FROM ".$ovipjel."term_csoportok where nev_url='".$_GET['kat_urlnev']."'");
				}
				else
				{
					$res = $pdo->prepare("SELECT * FROM ".$webjel."term_csoportok where nev_url='".$_GET['kat_urlnev']."'");
				}
				$res->execute();
				$row  = $res -> fetch();
				$csop_id = $row['id'];
				$csop_nev = $row['nev'];
				$csop_leiras = $row['leiras'];
				$csop_kep = $row['kep'];
			}
			// Oldalszám törlése, ha nem ugyanabba akategóriában van
			if (isset($_SESSION['csop_id']) && $_SESSION['csop_id'] != $csop_id)
			{
				unset($_SESSION['csop_id']);
				unset($_SESSION['oldszam']);
			}
			if ((($csop_id > 0 && $_GET['term_urlnev'] == '') || (isset($_POST['szuro_inditasa']) || isset($_SESSION['szuro_fooldal']))) && (!isset($_GET['term_urlnev']) || $_GET['term_urlnev'] == '')) //Termékek generálása
			{
					$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id = ".$csop_id." AND lathato=1 ORDER BY sorrend asc");
					$res->execute();
					$rownum = $res->fetchColumn();
					if ( $rownum > 0 && $csop_id > 0)
					{
						/* print '<div class="row">
								<div>
									<ul class="box-subcat">';
									$query1 = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id = ".$csop_id." AND lathato=1 ORDER BY sorrend asc";
									foreach ($pdo->query($query1) as $row1)
									{
										if ($row1['kep'] == "")
										{
											$csop_kep_link = ''.$domain.'/webshop/images/noimage.png';
										}
										else
										{
											$csop_kep_link = ''.$domain.'/images/termekek/'.$row1['kep'];
										}
													print '<li class="col-sm-3">
														<div class="image">
															<a href="'.$domain.'/termekek/'.$row1['nev_url'].'/"><img src="'.$csop_kep_link.'" alt="'.$row1['nev'].'">
															</a>
														</div>
														<div class="name subcatname">
															<a href="'.$domain.'/termekek/'.$row1['nev_url'].'/">'.$row1['nev'].'</a>
														</div>
													</li>';
									}
								print '</ul>
							</div>
						</div>'; */
						print '<div class="row kapcs_termekek">';
						$query1 = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id = ".$csop_id." AND lathato=1 ORDER BY sorrend asc";
						foreach ($pdo->query($query1) as $row1)
						{
							if ($row1['kep'] == "")
							{
								$csop_kep_link = ''.$domain.'/webshop/images/noimage.png';
							}
							else
							{
								$csop_kep_link = ''.$domain.'/images/termekek/'.$row1['kep'];
							}
										/* print '<li class="col-sm-3">
											<div class="image">
												<a href="'.$domain.'/termekek/'.$row1['nev_url'].'/"><img src="'.$csop_kep_link.'" alt="'.$row1['nev'].'">
												</a>
											</div>
											<div class="name subcatname">
												<a href="'.$domain.'/termekek/'.$row1['nev_url'].'/">'.$row1['nev'].'</a>
											</div>
										</li>'; */
							print '<div class="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12 fels_termek_div">
									<a href="'.$domain.'/termekek/'.$row1['nev_url'].'/"><div class="fels_termek_kep">
										<img alt="'.$row1['nev'].'" class="img-responsive lazy" data-src="'.$csop_kep_link.'" src="'.$csop_kep_link.'" style="opacity: 1;">
									</div></a>
									<div class="fels_termek_adatok">
										<h5 class="fels_termek_nev center"><a href="'.$domain.'/termekek/'.$row1['nev_url'].'/">'.$row1['nev'].'</a></h5>
									</div>
							</div>';
						}
						print '</div>';
					}
					else // Ha nincs alatta csoport, akkor a TERMÉKEKET írja ki
					{
						print '<div style="background-color: #F5F5F5;">';
					// Kategória leírása
						if ($csop_leiras != '')
						{
							print '<div class="row margbot20">';
                                if ($csop_kep != '')
								{
									print '<div class="col-sm-2">
										<div class="image">
											<img src="'.$domain.'/images/termekek/'.$csop_kep.'" class="img-thumbnail">
										</div>
									</div>
									<div class="col-sm-10">';
								}
								else
								{
									print '<div class="col-sm-12">';
								}
									print $csop_leiras.'
								</div>
                            </div>';
						}
					//oldalankénti darabszám
						if (isset($_POST['old_db']))
						{
							$_SESSION['old_db'] = $_POST['old_db'];
							unset($_SESSION['oldszam']);
						}
						if (isset($_SESSION['old_db']))
						{
							$db_per_oldal = $_SESSION['old_db'];
						}
						else
						{
							$db_per_oldal = 12; //alapból ennyi termék jelenik meg
						}
					// Szűrő
						$szuro_sql = '';
						$szuro_sql_2 = '';
						$szuro_sql_3 = '';
						if (isset($_SESSION['szuro_minimum_ar'])) // Ár szerinti szűrés
						{
							$szuro_sql = ' AND IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar) >= '.$_SESSION['szuro_minimum_ar'].' AND IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar) <= '.$_SESSION['szuro_maximum_ar'].' ';
						}
						
						if (isset($_SESSION['szuro_nem_ffi'])) 
						{
							$szuro_sql_2 .= ' AND ( nem_ffi = 1';
						}
						if (isset($_SESSION['szuro_nem_no'])) 
						{
							if($szuro_sql_2 == '') { $szuro_sql_2 .= ' AND ( nem_no = 1'; } // Ha még ez az első
							else { $szuro_sql_2 .= ' OR nem_no = 1'; }
						}
						if($szuro_sql_2 != '') { $szuro_sql_2 .= ')'; }
						
						if (isset($_SESSION['szuro_eletkor_fiatal']))
						{
							$szuro_sql_3 .= ' AND ( eletkor_fiatal = 1';
						}
						if (isset($_SESSION['szuro_eletkor_felnott']))
						{
							if($szuro_sql_3 == '') { $szuro_sql_3 .= ' AND ( eletkor_felnott = 1'; } // Ha még ez az első
							else { $szuro_sql_3 .= ' OR eletkor_felnott = 1'; }
						}
						if (isset($_SESSION['szuro_eletkor_idos']))
						{
							if($szuro_sql_3 == '') { $szuro_sql_3 .= ' AND ( eletkor_idos = 1'; } // Ha még ez az első
							else { $szuro_sql_3 .= ' OR eletkor_idos = 1'; }
						}
						if($szuro_sql_3 != '') { $szuro_sql_3 .= ')'; }
						
						if (isset($_SESSION['termek_parameter_ertek'])) {
							$szuro_termek_parameter_ertek = " AND (";
							foreach ($_SESSION['termek_parameter_ertek'] as $termek_parameter_id => $termek_parameter_ertekek) {
								if (reset($_SESSION['termek_parameter_ertek']) !== $termek_parameter_ertekek) {
									$szuro_termek_parameter_ertek .= " AND ";
								}
								$szuro_termek_parameter_ertek .= "EXISTS ("
										. "SELECT 1 FROM ".$webjel."termek_termek_parameter_ertekek ttpe WHERE ".$webjel."termekek.id=ttpe.termek_id AND (";
								$szuro_termek_parameter_ertek .= "ttpe.termek_parameter_id=".$termek_parameter_id." AND (";
								foreach ($termek_parameter_ertekek as $termek_parameter_ertek) {
									if (reset($termek_parameter_ertekek) !== $termek_parameter_ertek) {
										$szuro_termek_parameter_ertek .= " OR ";
									}
									if (strpos($termek_parameter_ertek, '|') === FALSE) {
										$szuro_termek_parameter_ertek .= "ttpe.ertek='".$termek_parameter_ertek."'";
									}
									else {
										$termek_csoport_termek_parameter_ertek_tipus = explode('|', $termek_parameter_ertek);
										$termek_csoport_termek_parameter_ertek_tipus = reset($termek_csoport_termek_parameter_ertek_tipus);
										$termek_parameter_ertek = explode('|', $termek_parameter_ertek);
										$termek_parameter_ertek = end($termek_parameter_ertek);
										if ($termek_csoport_termek_parameter_ertek_tipus == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_NAGYOBB) {
											$szuro_termek_parameter_ertek .= "ttpe.ertek>='".$termek_parameter_ertek."'";
										}
										elseif ($termek_csoport_termek_parameter_ertek_tipus == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KISEBB) {
											$szuro_termek_parameter_ertek .= "ttpe.ertek<='".$termek_parameter_ertek."'";
										}
										elseif ($termek_csoport_termek_parameter_ertek_tipus == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KOZOTT) {
											$termek_parameter_ertek = explode('-', $termek_parameter_ertek);
											$szuro_termek_parameter_ertek .= "(ttpe.ertek>='".$termek_parameter_ertek[0]."' AND ttpe.ertek<='".$termek_parameter_ertek[1]."')";
										}
									}
								}
								$szuro_termek_parameter_ertek .= ")))";
							}
							$szuro_termek_parameter_ertek .= ")";
						}
						else {
							$szuro_termek_parameter_ertek = "";
						}
						
					//rekordok száma
						if ((isset($_POST['szuro_inditasa']) && !isset($_GET['kat_urlnev'])) || isset($_SESSION['szuro_fooldal'])) // Ha főoldali szűrés
						{
							$_SESSION['szuro_fooldal'] = 'ok';
							$res = $pdo->prepare("SELECT COUNT(*), IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar) AS ar_sorrend FROM ".$webjel."termekek WHERE lathato=1".$szuro_sql.$szuro_sql_2.$szuro_sql_3);
						}
						else
						{
							$res = $pdo->prepare("SELECT COUNT(*), IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar) AS ar_sorrend FROM ".$webjel."termekek WHERE csop_id = ".$csop_id." AND lathato=1".$szuro_sql.$szuro_sql_2.$szuro_sql_3.$szuro_termek_parameter_ertek);
						}
						$res->execute();
						$rownum = $res->fetchColumn();
					//kezdés meghatározása
						if (isset($_POST['oldszam']))
						{
							$_SESSION['oldszam'] = $_POST['oldszam'];
							$_SESSION['csop_id'] = $csop_id;
						}
						if (isset($_SESSION['oldszam']))
						{
							$kezd = $_SESSION['oldszam'];
						}
						else
						{
							$kezd = 0;
						}
					//LAPOZÁS
						print '<div class="row lapozo_sav">';
							print	'<div class="col-lg-5 col-md-5 col-sm-12">';
								//Oldalankénti darabszám
										?>
											<script type="text/javascript">
												function old_db_form_script () {
													var frm = document.getElementById("old_db_form");
													frm.submit();
												}
											</script>
										<?php
										print '<span><form id="old_db_form" action="" method="POST">
												<select class="form-control" name="old_db" onchange="old_db_form_script()">';
												if (isset($_SESSION['old_db']) && $_SESSION['old_db'] == 12)
												{
													print '<option value="12" selected>12</option>';
												}
												else
												{
													print '<option value="12">12</option>';
												}
												if (isset($_SESSION['old_db']) && $_SESSION['old_db'] == 24)
												{
													print '<option value="24" selected>24</option>';
												}
												else
												{
													print '<option value="24">24</option>';
												}
												if (isset($_SESSION['old_db']) && $_SESSION['old_db'] == 48)
												{
													print '<option value="48" selected>48</option>';
												}
												else
												{
													print '<option value="48">48</option>';
												}
												if (isset($_SESSION['old_db']) && $_SESSION['old_db'] == 100)
												{
													print '<option value="100" selected>100</option>';
												}
												else
												{
													print '<option value="100">100</option>';
												}
												if (isset($_SESSION['old_db']) && $_SESSION['old_db'] == 500)
												{
													print '<option value="500" selected>500</option>';
												}
												else
												{
													print '<option value="500">500</option>';
												}
												print '</select>';
										print '</form></span>';
								//Sorrend
									
									if (isset($_POST['sorrend_uj']))
									{
										if ($_POST['sorrend_uj'] == 'nev_nov')
										{
											$_SESSION['sorr_tip'] = 'nev';
											$_SESSION['sorrend'] = 'asc';
										}
										else if ($_POST['sorrend_uj'] == 'nev_csokk')
										{
											$_SESSION['sorr_tip'] = 'nev';
											$_SESSION['sorrend'] = 'desc';
										}
										else if ($_POST['sorrend_uj'] == 'ar_nov')
										{
											$_SESSION['sorr_tip'] = 'ar_sorrend';
											$_SESSION['sorrend'] = 'asc';
										}
										else if ($_POST['sorrend_uj'] == 'ar_csokk')
										{
											$_SESSION['sorr_tip'] = 'ar_sorrend';
											$_SESSION['sorrend'] = 'desc';
										}
									}
									if (isset($_SESSION['sorr_tip']))
									{
										$sorr_tip = $_SESSION['sorr_tip'];
									}
									else
									{
										$sorr_tip = 'ar_sorrend'; // Alap rendezési feltétel
									}
									if (isset($_SESSION['sorrend']))
									{
										$sorrend = $_SESSION['sorrend'];
									}
									else
									{
										$sorrend = 'asc'; // Alap rendezési feltétel
									}
									
									?>
										<script type="text/javascript">
											function sorrend_form_script () {
												var frm = document.getElementById("sorrend_form");
												frm.submit();
											}
										</script>
									<?php
									print '<span><form id="sorrend_form" action="" method="POST">
													<select class="form-control" name="sorrend_uj" onchange="sorrend_form_script()">';
														if ($sorr_tip == 'ar_sorrend' && $sorrend == 'asc')
														{
															print '<option value="ar_nov" selected>Ár szerint növekvő</option>';
														}
														else
														{
															print '<option value="ar_nov">Ár szerint növekvő</option>';
														}
														if ($sorr_tip == 'ar_sorrend' && $sorrend == 'desc')
														{
															print '<option value="ar_csokk" selected>Ár szerint csökkenő</option>';
														}
														else
														{
															print '<option value="ar_csokk">Ár szerint csökkenő</option>';
														}
														if ($sorr_tip == 'nev' && $sorrend == 'asc')
														{
															print '<option value="nev_nov" selected>Név szerint növekvő</option>';
														}
														else
														{
															print '<option value="nev_nov">Név szerint növekvő</option>';
														}
														if ($sorr_tip == 'nev' && $sorrend == 'desc')
														{
															print '<option value="nev_csokk" selected>Név szerint csökkenő</option>';
														}
														else
														{
															print '<option value="nev_csokk">Név szerint csökkenő</option>';
														}
													print '</select>
												</form></span>
										</div>';
									
							//oldalszám kiírása
									print '<div class="col-lg-7 col-md-7 col-sm-12"><div class="lapozo_ikonok_div">';
									if (isset($_SESSION['sorrend'])) { $old_sorrend = '&sorrend='.$_SESSION['sorrend'];}
									if (isset($_SESSION['sorr_tip'])) { $old_sorr_tip = '&sorr_tip='.$_SESSION['sorr_tip'];}
									if (isset($_SESSION['old_db'])) { $old_old_db = '&old_db='.$_SESSION['old_db'];}
									
									$aktual_oldszam = ($kezd + $db_per_oldal) / $db_per_oldal;
									$oldal_elso = '';
									$oldal_minusz_1 = '';
									$oldal_minusz_2 = '';
									$oldal_minusz_3 = '';
									$pontok_eleje = '';
									$oldal_utolso = '';
									$pontok_vege = '';
									$oldal_plusz_1 = '';
									$oldal_plusz_2 = '';
									$oldal_plusz_3 = '';
									
								//Lapozó ikonok
									if (isset($_SESSION['oldszam']) &&  $_SESSION['oldszam'] > 0)
									{
										print '<span><form name="lapozForm" action="" method="POST">
												<input type="submit" title="" value="<" class="lapozo_szamok" id="balrabutton" />
												<input type="hidden" name="oldszam" value="'.($kezd - $db_per_oldal).'"/>
												</form></span>';
									}
									
									if (($aktual_oldszam) > 1)
									{
										$oldal_elso = '<span><form name="lapozForm" action="" method="POST">
													<input type="submit" value="1" src="'.$domain.'/webshop/images/g_mini_jobbra.png" class="lapozo_szamok"/>
													<input type="hidden" name="oldszam" value="0"/>
												</form></span>';

									}
									$oldal_aktualis = '<span><font class="lapozo_szamok_aktualis">'.$aktual_oldszam.'</font></span>';
									if (($aktual_oldszam - 1) > 1)
									{
										$oldal_minusz_1 = '<span><form name="lapozForm" action="" method="POST">
													<input type="submit" value="'.($aktual_oldszam - 1).'" src="'.$domain.'/webshop/images/g_mini_jobbra.png" class="lapozo_szamok"/>
													<input type="hidden" name="oldszam" value="'.(($aktual_oldszam - 2)*$db_per_oldal).'"/>
												</form></span>';
									}
									if (($aktual_oldszam - 2) > 1)
									{
										$oldal_minusz_2 = '<span><form name="lapozForm" action="" method="POST">
													<input type="submit" value="'.($aktual_oldszam - 2).'" src="'.$domain.'/webshop/images/g_mini_jobbra.png" class="lapozo_szamok"/>
													<input type="hidden" name="oldszam" value="'.(($aktual_oldszam - 3)*$db_per_oldal).'"/>
												</form></span>';
									}
									if (($aktual_oldszam - 3) > 1)
									{
										$oldal_minusz_3 = '<span><form name="lapozForm" action="" method="POST">
													<input type="submit" value="'.($aktual_oldszam - 3).'" src="'.$domain.'/webshop/images/g_mini_jobbra.png" class="lapozo_szamok"/>
													<input type="hidden" name="oldszam" value="'.(($aktual_oldszam - 4)*$db_per_oldal).'"/>
												</form></span>';
									}
									if (($aktual_oldszam) > 5)
									{
										$pontok_eleje = '<span>...</span>';
									}
									$utolso_oldszam = ceil($rownum / $db_per_oldal);
									if (($aktual_oldszam) < $utolso_oldszam)
									{
										// $oldal_utolso = $utolso_oldszam;
										$oldal_utolso = '<span><form name="lapozForm" action="" method="POST">
													<input type="submit" value="'.$utolso_oldszam.'" src="'.$domain.'/webshop/images/g_mini_jobbra.png" class="lapozo_szamok"/>
													<input type="hidden" name="oldszam" value="'.(($utolso_oldszam - 1)*$db_per_oldal).'"/>
												</form></span>';
									}
									if (($aktual_oldszam) < ($utolso_oldszam - 4))
									{
										$pontok_vege = '<span>...</span>';
									}
									if (($aktual_oldszam + 1) < $utolso_oldszam)
									{
										$oldal_plusz_1 = '<span><form name="lapozForm" action="" method="POST">
													<input type="submit" value="'.($aktual_oldszam + 1).'" src="'.$domain.'/webshop/images/g_mini_jobbra.png" class="lapozo_szamok"/>
													<input type="hidden" name="oldszam" value="'.(($aktual_oldszam + 0)*$db_per_oldal).'"/>
												</form></span>';
									}
									if (($aktual_oldszam + 2) < $utolso_oldszam)
									{
										$oldal_plusz_2 = '<span><form name="lapozForm" action="" method="POST">
													<input type="submit" value="'.($aktual_oldszam + 2).'" src="'.$domain.'/webshop/images/g_mini_jobbra.png" class="lapozo_szamok"/>
													<input type="hidden" name="oldszam" value="'.(($aktual_oldszam + 1)*$db_per_oldal).'"/>
												</form></span>';
									}
									if (($aktual_oldszam + 3) < $utolso_oldszam)
									{
										$oldal_plusz_3 = '<span><form name="lapozForm" action="" method="POST">
													<input type="submit" value="'.($aktual_oldszam + 3).'" src="'.$domain.'/webshop/images/g_mini_jobbra.png" class="lapozo_szamok"/>
													<input type="hidden" name="oldszam" value="'.(($aktual_oldszam + 2)*$db_per_oldal).'"/>
												</form></span>';
									}
									
									// print '<span>'.$oldal_elso.$pontok_eleje.$oldal_minusz_3.$oldal_minusz_2.$oldal_minusz_1.$oldal_aktualis.$oldal_plusz_1.$oldal_plusz_2.$oldal_plusz_3.$pontok_vege.$oldal_utolso.'</span><span></span>';
									print '<span>'.$oldal_elso.$pontok_eleje.$oldal_minusz_2.$oldal_minusz_1.$oldal_aktualis.$oldal_plusz_1.$oldal_plusz_2.$pontok_vege.$oldal_utolso.'</span><span></span>';
									
								//Lapozó ikonok
									if (isset($_SESSION['oldszam']) &&  $_SESSION['oldszam'] > 0)
									{
										print '<span>';
										if (($kezd + $db_per_oldal) < $rownum)
										{
											print	'<form name="lapozForm" action="" method="POST">
													<input type="submit" title="" value=">" class="lapozo_szamok" id="balrabutton" />
														<input type="hidden" name="oldszam" value="'.($kezd + $db_per_oldal).'"/>
														</form>';
										}
										print '</span>';
									}
									else
									{
										print '<span></span>';
										if (($kezd + $db_per_oldal) < $rownum)
										{
											print '<span width="24"><form name="lapozForm" action="" method="POST">
													<input type="submit" title="" value=">" class="lapozo_szamok" id="balrabutton" />
													<input type="hidden" name="oldszam" value="'.($kezd + $db_per_oldal).'"/>
													</form></span>';
										}
									}
								print	'</div></div></div>';
					//TERMÉKEK
						$datum = date('Y-m-d');
						$query = "SELECT *, IF(akciosar > 0 AND akcio_ig >= '".$datum."', akciosar, ar) AS ar_sorrend FROM ".$webjel."termekek WHERE csop_id=".$csop_id." AND lathato=1 ".$szuro_sql.$szuro_sql_2.$szuro_sql_3.$szuro_termek_parameter_ertek." ORDER BY ".$sorr_tip." ".$sorrend." LIMIT ".$kezd.",".$db_per_oldal;
						foreach ($pdo->query($query) as $row)
						{
							include $gyoker.'/webshop/termek_hosszu.php';
						}
						
					//LAPOZÁS az alján is
						print '<div class="row lapozo_sav"><div class="col-lg-12"><div class="lapozo_ikonok_div">';
									
								//Lapozó ikonok
									if (isset($_SESSION['oldszam']) &&  $_SESSION['oldszam'] > 0)
									{
										print '<span><form name="lapozForm" action="" method="POST">
												<input type="submit" title="" value="<" class="lapozo_szamok" id="balrabutton" />
												<input type="hidden" name="oldszam" value="'.($kezd - $db_per_oldal).'"/>
												</form></span>';
									}
									
								//oldalszám kiírása
									
									print '<span align="right" colspan="4">'.$oldal_elso.$pontok_eleje.$oldal_minusz_3.$oldal_minusz_2.$oldal_minusz_1.$oldal_aktualis.$oldal_plusz_1.$oldal_plusz_2.$oldal_plusz_3.$pontok_vege.$oldal_utolso.'<span></span>';
									
								//Lapozó ikonok
									if (isset($_SESSION['oldszam']) &&  $_SESSION['oldszam'] > 0)
									{
										print '<span>';
										if (($kezd + $db_per_oldal) < $rownum)
										{
											print	'<form name="lapozForm" action="" method="POST">
													<input type="submit" title="" value=">" class="lapozo_szamok" id="balrabutton" />
														<input type="hidden" name="oldszam" value="'.($kezd + $db_per_oldal).'"/>
														</form>';
										}
										print '</span>';
									}
									else
									{
										if (($kezd + $db_per_oldal) < $rownum)
										{
											print '<span width="24"><form name="lapozForm" action="" method="POST">
													<input type="submit" title="" value=">" class="lapozo_szamok" id="balrabutton" />
													<input type="hidden" name="oldszam" value="'.($kezd + $db_per_oldal).'"/>
													</form></span>';
										}
									}
							print '</div></div></div>';
						print '</div>';
					}
			}
			else if (isset($_GET['term_urlnev'])) //Termék infó
			{
				include $gyoker.'/webshop/termek_info.php';
			}
			else //KEZDŐLAP - TERMÉKEK
			{
				print '<div class="row">
					<div>
						<ul class="box-subcat">';
						$query1 = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id = 0 AND lathato=1 ORDER BY sorrend asc";
						foreach ($pdo->query($query1) as $row1)
						{
							if ($row1['kep'] == "")
							{
								$csop_kep_link = ''.$domain.'/webshop/images/noimage.png';
							}
							else
							{
								$csop_kep_link = ''.$domain.'/images/termekek/'.$row1['kep'];
							}
							print '<li class="col-sm-3">
								<div class="image">
									<a href="'.$domain.'/termekek/'.$row1['nev_url'].'/"><img src="'.$csop_kep_link.'" alt="'.$row1['nev'].'">
									</a>
								</div>
								<div class="name subcatname">
									<a href="'.$domain.'/termekek/'.$row1['nev_url'].'/">'.$row1['nev'].'</a>
								</div>
							</li>';
						}
					print '</ul>
					</div>
				</div>';
			}
			
			
		}
?>
