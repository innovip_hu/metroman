<div class="row mt-2 row-30">
<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div" id="fiz_mod_riaszt"><div class="alert alert-warning alert-danger">Válassz fizetési módot.</div></div>
<?php
		$query_beall = 'SELECT * FROM '.$webjel.'beallitasok WHERE id=1';
		$res = $pdo->prepare($query_beall);
		$res->execute();
		$row_beall = $res -> fetch();
	//Előre utalás
		echo '<div class="col-md-6"><h5 class="mb-2">Fizetés</h5>
			<select name="fiz_mod" id="fiz_mod_select" class="kassza_select">';
				$query2 = "SELECT * FROM ".$webjel."kassza_fiz_mod WHERE lathato=1 ORDER BY sorrend ASC";
				foreach ($pdo->query($query2) as $row2)
				{
					$select = '';
					if($row2['alap'] == 1)
					{
						$select = 'selected';
						$fiz_leiras = $row2['leiras'];
						$fiz_utanvet = $row2['utanvet'];
						$alap_fiz_mod = $row2['nev'];
					}

					$disabled = '';

					if ($row2['id'] == 4)
					{
						$disabled = 'disabled';
					}
					echo '<option value="'.$row2['id'].'" attr_utanvet="'.$row2['utanvet'].'" attr_leiras="'.htmlentities($row2['leiras']).'" '.$select.' '.$disabled.'>'.$row2['nev'].'</option>';
				}
			echo '</select>
			<p id="fiz_leiras_p">'.$fiz_leiras.'</p>
		</div>';
		echo '<div class="col-md-6 order-first"><h5 class="mb-2">Szállítás</h5>
			<select name="szall_mod" id="szall_mod_select" class="kassza_select">';
				$query2 = "SELECT * FROM ".$webjel."kassza_szall_mod WHERE lathato=1 ORDER BY sorrend ASC";
				foreach ($pdo->query($query2) as $row2)
				{				
					$query_kosar = "SELECT SUM(IF(term_akcios_ar > 0, (term_akcios_ar*term_db), (term_ar*term_db))) AS szumma FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];
					$res = $pdo->prepare($query_kosar);
					$res->execute();
					$row_kosar = $res -> fetch();	

					$res = $pdo->prepare('SELECT * FROM '.$webjel.'kassza_szall_mod_savok WHERE sav <= '.$row_kosar['szumma'].' AND szall_mod_id = "'.$row2['id'].'" ORDER BY sav DESC LIMIT 1 ');
					$res->execute();
					$row_sav  = $res -> fetch();							
					
					$select = '';
					if($row2['alap'] == 1)
					{
						$select = 'selected';
						$szall_leiras = $row2['leiras'];
						$alap_szall_mod = $row2['nev'];
			
						if($fiz_utanvet == 1)
						{
							$utanvet = $row_sav['ar_utanvet'] - $row_sav['ar'];
						
							$szall_kolts = $row_sav['ar_utanvet'] - $utanvet;

							$szall_koltseg_szoveg = number_format($szall_kolts, 0, ',', ' ')." Ft + ".number_format($utanvet, 0, ',', ' ')." Ft utánvét kezelési díj";
						}
						else
						{
							$szall_kolts = $row_sav['ar'];

							$szall_koltseg_szoveg = number_format($szall_kolts, 0, ',', ' ')." Ft";
						}

						if ($row2['ingyenes_szallitas'] > 0 && $row_kosar['szumma'] >= $row2['ingyenes_szallitas'])
						{
							$szall_kolts = 0;

							$szall_koltseg_szoveg = "0 Ft";
						}						
					}

					if ($row2['ingyenes_szallitas'] > 0 && $row_kosar['szumma'] >= $row2['ingyenes_szallitas'])
					{
						$szall_kolts_sor = 0;
					} else {
						$szall_kolts_sor = $row_sav['ar'];
					}					

					$lathato_szallitas = 1;

					if ($row2['korlatozott'] == 1)
					{
						$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE ppp_tiltas = 1 AND id IN (SELECT term_id FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'].")");
						$res->execute();
						$rownum = $res->fetchColumn();	


						if ($rownum > 0 || (date('Y-m-d') < '2022-01-01'))
						{
							$lathato_szallitas = 0;
						}					
					}

					if ($lathato_szallitas == 1)
					{
						echo '<option value="'.$row2['id'].'" attr_automata="'.$row2['automata'].'" attr_leiras="'.$row2['leiras'].'" '.$select.'>'.$row2['nev'].' ('.number_format($szall_kolts_sor,0,',',' ').' Ft)</option>';
					}
					
				}
			echo '</select>
			<p id="szall_leiras_p">'.$szall_leiras.'</p>
			<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div mb-0" id="ppp_riaszt"><div class="alert alert-warning alert-danger">Válassz Pick Pack Pont átvételi pontot.</div></div>
			<iframe id="ppp_iframe" width="100%" height="650px" style="display: none; margin-bottom: 10px; margin-top: 10px;" src="https://online.sprinter.hu/terkep/#/"></iframe>';

		?>
				<div id="pp_riaszt"></div>
				<div id="pp_valaszto" class="mx-0 mt-2">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 ">
							<select id="pp_megye" onChange="varosok()">
								<option value="">Válassz megyét</option>
								<?php
									$query = "SELECT DISTINCT megye FROM ".$webjel."varos_megye ORDER BY megye ASC";
									foreach ($pdo->query($query) as $row)
									{
										print '<option value="'.$row['megye'].'">'.$row['megye'].'</option>';
									}
								?>
							</select>
							<div id="pp_varos_div" class="mt-4">
								<select id="pp_varos">
									<option value="">Válassz várost</option>
								</select>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 ">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
									<input onClick="ppKeres()" type="radio" id="pp_posta" name="pp_tipus" value="ok"><label for="pp_posta"><img src="<?php print $domain; ?>/webshop/images/pp_MagyarPostaLogisztika.png"></label>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
									<input onClick="ppKeres()" type="radio" id="pp_coop" name="pp_tipus" value="ok"><label for="pp_coop"><img src="<?php print $domain; ?>/webshop/images/pp_coop_logo.png"></label>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
									<input onClick="ppKeres()" type="radio" id="pp_ossz" name="pp_tipus" value="ok" checked><label style="margin-top: 14px;" for="pp_ossz">Összes</label>
								</div>
							</div>
							<div class="row mt-1">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
									<input onClick="ppKeres()" type="radio" id="pp_mol" name="pp_tipus" value="ok"><label for="pp_mol"><img src="<?php print $domain; ?>/webshop/images/pp_MOL_logo_color.png"></label>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
									<input onClick="ppKeres()" type="radio" id="pp_autom" name="pp_tipus" value="ok"><label for="pp_autom"><img src="<?php print $domain; ?>/webshop/images/pp_csomagautomata_logo.png"></label>
								</div>
							</div>
						</div>
					</div>
					<div id="pp_talalat_div"></div>
				</div>

				<div id="gls_riaszt"></div>
				<div id="gls_valaszto" class="mx-0 my-2">
					<div class="row row-20 align-items-center">
						<div class="col-lg-6 col-12 ">
							<select id="gls_megye" onChange="varosok_gls()">
								<option value="">Válassz megyét</option>
								<?php
									$query = "SELECT DISTINCT megye FROM ".$webjel."varos_megye ORDER BY megye ASC";
									foreach ($pdo->query($query) as $row)
									{
										print '<option value="'.$row['megye'].'">'.$row['megye'].'</option>';
									}
								?>
							</select>
							<div id="gls_varos_div" class="mt-2">
								<select id="gls_varos">
									<option value="">Válassz várost</option>
								</select>
							</div>
						</div>
						<div class="col-lg-6 col-12 order-first order-lg-last"><img src="<?php print $domain; ?>/webshop/images/gls-csomagpont-2023.jpg"></div>						
					</div>
					<div id="gls_talalat_div"></div>
				</div>			

				<div id="fp_riaszt"></div>
				<div id="foxpost_valaszto" style="display: none;" class="mx-0 my-2">
					<div class="row row-20">
						<div class="col-lg-6 col-12 ">
							<select id="foxpost_megye" onChange="varosok_foxpost()" class="kassza_select">
								<option value="">Válassz megyét</option>';
								<?php
									$query = "SELECT DISTINCT megye FROM ".$webjel."varos_megye ORDER BY megye ASC";
									foreach ($pdo->query($query) as $row)
									{
										print '<option value="'.$row['megye'].'">'.$row['megye'].'</option>';
									}
								?>
							</select>
							<div id="foxpost_varos_div" class="mt-2">
								<select id="foxpost_varos" class="kassza_select">
									<option value="">Válassz várost</option>
								</select>
							</div>
						</div>
						<div class="col-lg-6 col-12 order-first order-lg-last"><img src="<?=$domain?>/webshop/images/foxpost_ikon.jpg"></div>
						<div class="col-lg-12" id="foxpost_talalat_div"></div>
					</div>
				</div>					

		<?php

		echo '</div>';
?>
	<div class="col-md-12 text-center mt-5">
		<span class="kassza_szall_kolts_info">Szállítási költség: <span id="fiz_szall_mod_koltseg"><?=$szall_koltseg_szoveg?></span></span>
	</div>

	<div class="col-md-12 pb-2">
		<div class="pull-right">
			<button type="button" id="fiz_mod_tovabb_gomb" class="button button-sm button-gradient kassza_tovabb_gomb" attr_tart_id="osszesites_tartalom" attr_horg="osszes_horgony">Tovább</button>
		</div>		
	</div>
	<input type="hidden" name="valasztott_gls_cspont" id="valasztott_gls_cspont" value="">
	<input type="hidden" name="valasztott_gls_cspont_azonosito" id="valasztott_gls_cspont_azonosito" value="">
	
	<input type="hidden" name="valasztott_postapont" id="valasztott_postapont" value="">

	<input type="hidden" id="ppp_shopCode" name="ppp_shopCode" value=""/>
	<input type="hidden" id="ppp_shopType" name="ppp_shopType" value=""/>
	<input type="hidden" id="ppp_city" name="ppp_city" value=""/>
	<input type="hidden" id="ppp_address" name="ppp_address" value=""/>

	<input type="hidden" name="valasztott_foxpost_cspont" id="valasztott_foxpost_cspont" value="">

	<input type="hidden" id="csomagpont_nev" name="csomagpont_nev" value="">
	<input type="hidden" id="csomagpont_varos" name="csomagpont_varos" value="">
	<input type="hidden" id="csomagpont_irsz" name="csomagpont_irsz" value="">
	<input type="hidden" id="csomagpont_cim" name="csomagpont_cim" value="">
</div>