<?php
// Szállítás
	$query = "SELECT * FROM ".$webjel."rendeles WHERE id=".$rendeles_id_etracking;
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	$szall_kolts = $row['szallitasi_dij'];
	$szall_kolts_afa = $row['szallitasi_dij_afa'];
	$rendeles_id = $row['id'];
	$rendeles_rendeles_id = $row['rendeles_id'];

// Rendelés adatai
$query = "SELECT * FROM ".$webjel."rendeles_tetelek WHERE rendeles_id=".$rendeles_id;
$ar = 0;
$items = Array();
foreach ($pdo->query($query) as $row)
{
	$query_term = 'SELECT * FROM '.$webjel.'termekek WHERE id='.$row['term_id'];
	$res = $pdo->prepare($query_term);
	$res->execute();
	$row_term = $res -> fetch();
	
	$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_term['csop_id'];
	$res = $pdo->prepare($query_csop);
	$res->execute();
	$row_csop = $res -> fetch();
	if($row['term_akcios_ar'] == 0) //ha nem akciós
	{
		$items[] = array('sku'=>$row_term['cikkszam'], 'name'=>$row_term['nev'], 'category'=>$row_csop['nev'], 'price'=>$row['term_ar'], 'quantity'=>$row['term_db'], 'id' => $row['term_id']);
		
		$ar = $ar + ($row['term_ar'] * $row['term_db']);
	}
	else //ha akciós
	{
		$items[] = array('sku'=>$row_term['cikkszam'], 'name'=>$row_term['nev'], 'category'=>$row_csop['nev'], 'price'=>$row['term_akcios_ar'], 'quantity'=>$row['term_db'], 'id' => $row['term_id']);
		
		$ar = $ar + ($row['term_akcios_ar'] * $row['term_db']);
	}
}

$ar = $ar + $szall_kolts;
$afa = $ar - (round($ar / (1+($szall_kolts_afa/100))));
										
// Transaction Data
$trans = array('id'=>$rendeles_id, 'affiliation'=>$webnev,
               'revenue'=>$ar, 'shipping'=>$szall_kolts, 'tax'=>$afa);


function getTransactionJs(&$trans) {
  return <<<HTML
ga('ecommerce:addTransaction', {
  'id': '{$trans['id']}',
  'affiliation': '{$trans['affiliation']}',
  'revenue': '{$trans['revenue']}',
  'shipping': '{$trans['shipping']}',
  'tax': '{$trans['tax']}'
});
HTML;
}

function getItemJs(&$transId, &$item, &$darab) {
  return <<<HTML
{
  "id": "{$item['id']}",
  "publicSku": "{$item['sku']}",
  'name': '{$item['name']}',
  "list_name": "Search Results",
  "brand": "",
  'category': '{$item['category']}',
  "variant": "",
  "list_position": $darab,
  'price': '{$item['price']}',
  'quantity': '{$item['quantity']}'
},
HTML;
}

/*
?>
<!-- Begin HTML -->
<script>
ga('require', 'ecommerce');

<?php
echo getTransactionJs($trans);

foreach ($items as &$item) {
  echo getItemJs($trans['id'], $item);
}
?>

ga('ecommerce:send');
</script>

*/
?>
<script type="text/javascript">
		gtag('event', 'purchase', {
		  "transaction_id": "<?=$trans['id']?>",
		  "affiliation": "<?=$trans['affiliation']?>",
		  "value": <?=$trans['revenue']?>,
		  "currency": "HUF",
		  "tax": <?=$trans['tax']?>,
		  "shipping": <?=$trans['shipping']?>,
		  "items": [
		  	<?php
		  	$darab = 1;
			foreach ($items as &$item) {
			  echo getItemJs($trans['id'], $item, $darab);
			  $darab++;
			}
			?>	  
			]
		});

		var _gravity = _gravity || [];
  	<?php
			foreach ($items as &$item) { ?>	  
				_gravity.push({
				type: "event",
				eventType: "BUY",
				itemId: "<?=$item['id']?>",
				unitPrice: "<?=$item['price']?>",
				quantity: "<?=$item['quantity']?>",
				orderId: "<?=$trans['id']?>"
				});	
		<?php
			}
		?>			
</script>

<script>
        window.uetq = window.uetq || [];
        window.uetq.push("event", "", {"revenue_value":<?=$trans['revenue']?>,"currency":"HUF"});
</script>