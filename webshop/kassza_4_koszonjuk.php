<?php
	// rendelés és simplepay átirányítás ide

if (isset($_GET['rendeles_id'])) {

	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."rendeles WHERE koszonjuk=1 AND id=?");
	$res->execute(array($_GET['rendeles_id']));
	$rownum = $res->fetchColumn();	

	if ($rownum > 0) {
		// volt már feldolgozott rendelés, mehet a domain-re
		header('Location: '.$domain);
		exit();
	}

	if (isset($_GET['tranzakcio_azonosito'])) {
    	print '<h4 style="text-align:center;"">Sikeres tranzakció!</h4>';
    	print '<p style="text-align:center;" class="mb-5">SimplePay tranzakció azonosító: '.$_GET['tranzakcio_azonosito'].'</p>';
	}

	?>
		<h3 style="text-align: center;"><i class="mdi mdi-checkbox-marked-circle text-green"></i> Rendelésedet sikeresen rögzítettük!</h3>
		<h4 style="text-align: center;">Hamarosan e-mail-ben is megküldjük a vásárlásod adatait.</h4>
		<h4 style="text-align: center;">További szép napot kívánunk!</h4>
		<p style="text-align: center;" class="py-3 border-top border-bottom">Ha még nem tetted, kövess minket a Facebookon is, hogy ne maradj le akcióinkról és újdonságainkról!
		<br><a href="https://www.facebook.com/MetromanHungariaKft/" target="_blank"><img src="<?php print $domain; ?>/images/facebook.png" style="width: 40px; margin-top: 10px;" alt="Facebook"></a></p>
		<?php	

		echo '<div class="mt-5">';
		echo '<h4 class="text-center mb-3 h5">Ezeket a termékeket vásároltad meg</h4>';


		$query = "SELECT
					t.id,
					t.nev,
					t.csop_id,
					t.cikkszam,
					t.nev_url,
					csop.nev_url as csopnev_url
		FROM ".$webjel."termekek t
		INNER JOIN ".$webjel."term_csoportok csop
		ON csop.id = t.csop_id
		INNER JOIN ".$webjel."rendeles_tetelek rt
		ON rt.term_id = t.id
		WHERE rt.rendeles_id = ?
		GROUP BY rt.id";
		$result = $pdo->prepare($query);
		$result->execute(array($_GET['rendeles_id']));		
		foreach ($result as $key => $value) {

		$link = $domain.'/termekek/'.$value['csopnev_url'].'/'.$value['nev_url'];

		$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$value['id']." AND spec=0 ORDER BY alap DESC LIMIT 1";
		$res = $pdo->prepare($query_kep);
		$res->execute();
		$row_kep = $res -> fetch();
		$alap_kep = $row_kep ? $row_kep['kep'] : "";
		if ($alap_kep == '') 
		{
			$kep_link = $domain.'/webshop/images/noimage.png';
		}
		elseif ($row_kep['ovip_termek_id'] !=0)
		{
			if($row_kep['thumb'] != '')
			{
				$kep = $row_kep['thumb'];
			}
			else
			{
				$kep = $row_kep['kep'];
			}
			$kep_link = $kep;
		}
		else
		{
			if($row_kep['thumb'] != '')
			{
				$kep = $row_kep['thumb'];
			}
			else
			{
				$kep = $row_kep['kep'];
			}
			$kep_link = $domain.'/images/termekek/'.$kep;
		}   

		if ($key != 0)
		{
				echo '<hr class="my-3">';
			}           			

			include $gyoker.'/webshop/termek_csik_rendeles.php';
		}		

		echo '</div>';
		
		$rendeles_id_etracking = $_GET['rendeles_id'];
		include $gyoker.'/webshop/ecommerce_tracking.php';		

		$updatecommand = "UPDATE ".$webjel."rendeles SET koszonjuk=1 WHERE id=?";
		$result = $pdo->prepare($updatecommand);
		$result->execute(array($_GET['rendeles_id']));		
		
}
else {
	header('Location: '.$domain);
	exit();
}