<?php
	// Utoljára megnézett termékek
	if (isset($_GET['term_urlnev']) && $_GET['term_urlnev'] != '' )
	{
		if(isset($_SESSION['ut_term_1']) && $_GET['term_urlnev'] == $_SESSION['ut_term_1'])
		{}
		else if(isset($_SESSION['ut_term_2']) && $_GET['term_urlnev'] == $_SESSION['ut_term_2'])
		{}
		else if(isset($_SESSION['ut_term_3']) && $_GET['term_urlnev'] == $_SESSION['ut_term_3'])
		{}
		else
		{
			if(isset($_SESSION['ut_term_2']))
			{
				$_SESSION['ut_term_3'] = $_SESSION['ut_term_2'];
			}
			if(isset($_SESSION['ut_term_1']))
			{
				$_SESSION['ut_term_2'] = $_SESSION['ut_term_1'];
			}
			$_SESSION['ut_term_1'] = $_GET['term_urlnev'];
		}
	}
	
	if(isset($_SESSION['ut_term_1']))
	{
		print '<div class="row utnez_term_divek">';
		$query_ut_term = "SELECT * FROM ".$webjel."termekek where nev_url='".$_SESSION['ut_term_1']."'";
		$res = $pdo->prepare($query_ut_term);
		$res->execute();
		$row_ut_term = $res -> fetch();
		$query_csop_ut = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_ut_term['csop_id'];
		$res_csop = $pdo->prepare($query_csop_ut);
		$res_csop->execute();
		$row_csop_ut  = $res_csop -> fetch();
		$link = ''.$domain.'/termekek/'.$row_csop_ut['nev_url'].'/'.$row_ut_term['nev_url'];
		$link_ut = ''.$domain.'/termekek/'.$row_csop_ut['nev_url'].'/'.$row_ut_term['nev_url'];
		$_SESSION['link_ut'] = ''.$domain.'/termekek/'.$row_csop_ut['nev_url'].'/'.$row_ut_term['nev_url'];
		// Kép
		print '<div class="col-lg-4 col-md-4 col-sm-4 col-sx-4"><a href="'.$link.'" >';
			$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_ut_term['id']." ORDER BY alap DESC LIMIT 1";
			$res = $pdo->prepare($query_kep);
			$res->execute();
			$row_kep = $res -> fetch();
			$alap_kep = $row_kep['kep'];
			if ($alap_kep == '') 
			{
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$domain.'/webshop/images/noimage.png" />';
			} elseif ($row_kep['ovip_termek_id'] != 0) {
				if($row_kep['thumb'] != '')
				{
					$kep = $row_kep['thumb'];
				}
				else
				{
					$kep = $row_kep['kep'];
				}
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$kep.'" />';
			}
			else
			{
				if($row_kep['thumb'] != '')
				{
					$kep = $row_kep['thumb'];
				}
				else
				{
					$kep = $row_kep['kep'];
				}
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$domain.'/images/termekek/'.$kep.'" />';
			}
		print '</a></div>';
		// Név
		print '<div class="col-lg-8 col-md-8 col-sm-8 col-sx-3">';
			print '<p><a href="'.$link.'">';
				if (strlen($row_ut_term['nev']) > 46)
				{
					print mb_substr($row_ut_term['nev'],0,46,'UTF-8').'...';
				}
				else
				{
					print $row_ut_term['nev'];
				}
			print '</a></p>';
			// Ár
			$datum = date('Y-m-d');
			if ($row_ut_term['akcio_ig'] >= $datum && $row_ut_term['akcio_tol'] <= $datum) //Akciós
			{
				print '<span class="price-new" style="display:inline;">'.number_format($row_ut_term['akciosar'], 0, ',', ' ').' Ft</span> <span class="price-old" style="display:inline;">'.number_format($row_ut_term['ar'], 0, ',', ' ').' Ft</span>';
			}
			else //nem akciós
			{
				print '<span class="price-normal">'.number_format($row_ut_term['ar'], 0, ',', ' ').' Ft</span>';
			}
		print '</div>';
		print '</div>';
	}
	
	if(isset($_SESSION['ut_term_2']))
	{
		print '<div class="row utnez_term_divek">';
		$query_ut_term = "SELECT * FROM ".$webjel."termekek where nev_url='".$_SESSION['ut_term_2']."'";
		$res = $pdo->prepare($query_ut_term);
		$res->execute();
		$row_ut_term = $res -> fetch();
		$query_csop_ut = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_ut_term['csop_id'];
		$res_csop = $pdo->prepare($query_csop_ut);
		$res_csop->execute();
		$row_csop_ut  = $res_csop -> fetch();
		$link = ''.$domain.'/termekek/'.$row_csop_ut['nev_url'].'/'.$row_ut_term['nev_url'];
		// Kép
		print '<div class="col-lg-4 col-md-4 col-sm-4 col-sx-4"><a href="'.$link.'" >';
			$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_ut_term['id']." ORDER BY alap DESC LIMIT 1";
			$res = $pdo->prepare($query_kep);
			$res->execute();
			$row_kep = $res -> fetch();
			$alap_kep = $row_kep['kep'];
			if ($alap_kep == '') 
			{
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$domain.'/webshop/images/noimage.png" />';
			} elseif ($row_kep['ovip_termek_id'] != 0) {
				if($row_kep['thumb'] != '')
				{
					$kep = $row_kep['thumb'];
				}
				else
				{
					$kep = $row_kep['kep'];
				}
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$kep.'" />';
			}
			else
			{
				if($row_kep['thumb'] != '')
				{
					$kep = $row_kep['thumb'];
				}
				else
				{
					$kep = $row_kep['kep'];
				}
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$domain.'/images/termekek/'.$kep.'" />';
			}
		print '</a></div>';
		// Név
		print '<div class="col-lg-8 col-md-8 col-sm-8 col-sx-3">';
			print '<p><a href="'.$link.'">';
				if (strlen($row_ut_term['nev']) > 46)
				{
					print mb_substr($row_ut_term['nev'],0,46,'UTF-8').'...';
				}
				else
				{
					print $row_ut_term['nev'];
				}
			print '</a></p>';
			// Ár
			$datum = date('Y-m-d');
			if ($row_ut_term['akcio_ig'] >= $datum && $row_ut_term['akcio_tol'] <= $datum) //Akciós
			{
				print '<span class="price-new" style="display:inline;">'.number_format($row_ut_term['akciosar'], 0, ',', ' ').' Ft</span> <span class="price-old" style="display:inline;">'.number_format($row_ut_term['ar'], 0, ',', ' ').' Ft</span>';
			}
			else //nem akciós
			{
				print '<span class="price-normal">'.number_format($row_ut_term['ar'], 0, ',', ' ').' Ft</span>';
			}
		print '</div>';
		print '</div>';
	}
	
	if(isset($_SESSION['ut_term_3']))
	{
		print '<div class="row utnez_term_divek">';
		$query_ut_term = "SELECT * FROM ".$webjel."termekek where nev_url='".$_SESSION['ut_term_3']."'";
		$res = $pdo->prepare($query_ut_term);
		$res->execute();
		$row_ut_term = $res -> fetch();
		$query_csop_ut = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_ut_term['csop_id'];
		$res_csop = $pdo->prepare($query_csop_ut);
		$res_csop->execute();
		$row_csop_ut  = $res_csop -> fetch();
		$link = ''.$domain.'/termekek/'.$row_csop_ut['nev_url'].'/'.$row_ut_term['nev_url'];
		// Kép
		print '<div class="col-lg-4 col-md-4 col-sm-4 col-sx-4"><a href="'.$link.'" >';
			$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_ut_term['id']." ORDER BY alap DESC LIMIT 1";
			$res = $pdo->prepare($query_kep);
			$res->execute();
			$row_kep = $res -> fetch();
			$alap_kep = $row_kep['kep'];
			if ($alap_kep == '') 
			{
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$domain.'/webshop/images/noimage.png" />';
			} elseif ($row_kep['ovip_termek_id'] != 0) {
				if($row_kep['thumb'] != '')
				{
					$kep = $row_kep['thumb'];
				}
				else
				{
					$kep = $row_kep['kep'];
				}
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$kep.'" />';
			}
			else
			{
				if($row_kep['thumb'] != '')
				{
					$kep = $row_kep['thumb'];
				}
				else
				{
					$kep = $row_kep['kep'];
				}
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$domain.'/images/termekek/'.$kep.'" />';
			}
		print '</a></div>';
		// Név
		print '<div class="col-lg-8 col-md-8 col-sm-8 col-sx-3">';
			print '<p><a href="'.$link.'">';
				if (strlen($row_ut_term['nev']) > 46)
				{
					print mb_substr($row_ut_term['nev'],0,46,'UTF-8').'...';
				}
				else
				{
					print $row_ut_term['nev'];
				}
			print '</a></p>';
			// Ár
			$datum = date('Y-m-d');
			if ($row_ut_term['akcio_ig'] >= $datum && $row_ut_term['akcio_tol'] <= $datum) //Akciós
			{
				print '<span class="price-new" style="display:inline;">'.number_format($row_ut_term['akciosar'], 0, ',', ' ').' Ft</span> <span class="price-old" style="display:inline;">'.number_format($row_ut_term['ar'], 0, ',', ' ').' Ft</span>';
			}
			else //nem akciós
			{
				print '<span class="price-normal">'.number_format($row_ut_term['ar'], 0, ',', ' ').' Ft</span>';
			}
			print '</div>';
			print '</div>';
		}
			
	?>
