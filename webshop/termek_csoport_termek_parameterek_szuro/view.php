<?php 

	$termek_csoport_id = $pdo->query("SELECT id FROM ".$webjel."term_csoportok WHERE nev_url='".$_GET['kat_urlnev']."'")->fetchColumn();
	$query = "SELECT DISTINCT
			tp.id, tp.nev
		FROM ".$webjel."term_csoportok_parameter_ertek tpe 
		JOIN ".$webjel."term_csoportok_parameter tp ON tp.id=tpe.param_id
		JOIN ".$webjel."termekek t ON (t.csop_id = tp.csop_id OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=t.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id = tp.csop_id ))
		WHERE t.lathato=1 AND tp.csop_id = ".$termek_csoport_id." 
		AND (REPLACE(t.rovid_leiras,'&nbsp;',' ') LIKE CONCAT('%', tpe.ertek2, '%') OR REPLACE(t.rovid_leiras,'&nbsp;',' ') LIKE CONCAT('%', tpe.ertek, '%') OR t.nev LIKE CONCAT('%', tpe.ertek, '%'))
		ORDER BY tp.nev ASC";
	$res = $pdo->prepare($query);
	$res->execute();
	$parameterk_szurobe = $res -> fetchAll(PDO::FETCH_ASSOC);

	if (count($parameterk_szurobe) > 0)
	{
?>
		<div class="term_cim sidebar_term_cim mb-3" id="szuro_lenyito">Szűrő <span class="mobil_kat_lenyito_allando"><i class="fa fa-bars" style="font-style: normal;"></i></span></div>
		<div id="szuro_div_section" class="mb-3">
			<div class="szuro_mobil_title">Szűrő<img id="szuro_bezar_gomb" src="<?php print $domain; ?>/webshop/images/delete-button.png" class="szuro_bezar_gomb"/></div>
			<div id="szuro_div">

				<h6 style="margin-bottom: 10px" class="d-none d-md-block">Termékek szűrése:</h6>

				<form id="szuro_form" action="<?php if($oldal != 'termekek'){print ''.$domain.'/termekek/';} ?>" method="GET" >
					<div class="row row-10">
					
					<?php

					foreach ($parameterk_szurobe as $row)
					{
						echo '<div class="col-12 col-md-4">';
						if (isset($_SESSION['termek_parameter_ertek'][$row['id']]))
						{
							$sima_paramater_nem_latszik = 'zarva-asztal';
							$chevron = "fa-chevron-down";
						}
						else
						{
							$sima_paramater_nem_latszik = 'zarva-asztal';	
							$chevron = "fa-chevron-down";
						}

							$rendezeshez = 'tpe.ertek ASC';

						echo '<div class="filter_box accordion szuro_divek">
							<div class="headerFilter">
								<h3 class="mb-0">'.$row['nev'].'</h3>
								<span class="fa '.$chevron.' text-primary"></span>
							</div>
							<div class="belso-szuro '.$sima_paramater_nem_latszik.'" id="belso-szuro-'.$row['id'].'">';
								$query2 = "SELECT tpe.id, tpe.ertek 
									FROM ".$webjel."term_csoportok_parameter_ertek tpe 
									JOIN ".$webjel."term_csoportok_parameter tp ON tp.id=tpe.param_id
									JOIN ".$webjel."termekek t ON (t.csop_id = tp.csop_id OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=t.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id = tp.csop_id))
									WHERE t.lathato=1 AND tpe.param_id=".$row['id']." AND tp.csop_id = ".$termek_csoport_id."
									AND (t.rovid_leiras LIKE CONCAT('%', tpe.ertek2, '%') OR t.rovid_leiras LIKE CONCAT('%', tpe.ertek, '%') OR t.nev LIKE CONCAT('%', tpe.ertek, '%'))
									GROUP BY tpe.ertek 
									ORDER BY ".$rendezeshez."";
								foreach ($pdo->query($query2) as $row2)
								{
									if (isset($_SESSION['termek_parameter_ertek'][$row['id']]) AND in_array($row2['ertek'], $_SESSION['termek_parameter_ertek'][$row['id']])) { $checked = 'checked'; }
									else { $checked = ''; }							
									echo '<div><label class="checkbox-inline"><input type="checkbox" data-id="'.$row['id'].'" name="termek_parameter_ertek['.$row['id'].'][]" value="'.$row2['ertek'].'" '.$checked.'>'.$row2['ertek'].'</label></div>';
								}
							echo '</div>
						</div>
						</div>';
					}
					?>

					<div class="col-12 col-md-4 d-block d-md-none">
						<button type="button" class="szuro_bezar_gomb button button-block button-primary button-xs">Szűrés</button>
						<a href="<?=$domain?>/termekek/<?=$_GET['kat_urlnev']?>/" class="button button-block button-default-outline button-xs mt-2">Szűrő törlése</a>
					</div>

					</div>
				</form>
			</div>
		</div>

		


<?php 

	}

?>