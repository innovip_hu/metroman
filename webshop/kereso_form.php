		<form class="search_form" action="<?php print $domain; ?>/kereso/" method="GET">
	        <div class="input-group">
	          <input style="height: auto;" type="search" class="form-control" aria-label="Nem találod? Keresd itt." aria-describedby="button-addon2" id="keres_nev" name="keres_nev" value="<?php if(isset($_GET['keres_nev'])) {print $_GET['keres_nev'];} else if(isset($_SESSION['keres_nev'])) {print $_SESSION['keres_nev'];} ?>" placeholder="Nem találod? Keresd itt." autocomplete="off">
	          <div class="input-group-append">
	            <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><span class="fa fa-search"></span></button>
	          </div>
	        </div> 			
		</form>
		<div id="kereso_ac_doboz" class="small"></div>
