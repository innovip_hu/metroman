<?php
	session_start();
	include '../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	$query_f = "SELECT * FROM ".$webjel."kassza_fiz_mod WHERE id=".$_POST['fiz_mod'];
	$res = $pdo->prepare($query_f);
	$res->execute();
	$row_f = $res -> fetch();

	$query_sz = "SELECT * FROM ".$webjel."kassza_szall_mod WHERE id=".$_POST['szall_mod'];
	$res = $pdo->prepare($query_sz);
	$res->execute();
	$row_sz = $res -> fetch();

	$query_kosar = "SELECT SUM(IF(term_akcios_ar > 0, (term_akcios_ar*term_db), (term_ar*term_db))) AS szumma FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];
	$res = $pdo->prepare($query_kosar);
	$res->execute();
	$row_kosar = $res -> fetch();		

	$res = $pdo->prepare('SELECT * FROM '.$webjel.'kassza_szall_mod_savok WHERE sav <= '.$row_kosar['szumma'].' AND szall_mod_id = '.$row_sz['id'].' ORDER BY sav DESC LIMIT 1 ');
	$res->execute();
	$row_sav  = $res -> fetch();	

	if ($row_sz['ingyenes_szallitas'] > 0 && $row_kosar['szumma'] >= $row_sz['ingyenes_szallitas'])
	{
		echo 0;
		$eredmeny = [
			"szallitas" => 0,
			"utanvet" => 0,
			"ar" => 0
		];		
	}	
	elseif($row_f['utanvet'] == 1)
	{
		$eredmeny = [
			"szallitas" => $row_sav['ar_utanvet'] - ($row_sav['ar_utanvet']-$row_sav['ar']),
			"utanvet" => $row_sav['ar_utanvet']-$row_sav['ar'],
			"ar" => $row_sav['ar']
		];		
	}
	else
	{
		$eredmeny = [
			"szallitas" => $row_sav['ar'],
			"utanvet" => 0,
			"ar" => $row_sav['ar']
		];			
	}

	echo json_encode($eredmeny);