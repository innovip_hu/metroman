<?php

function profil_check($adatok)
{
	include '../config.php';
		
		if($adatok['email'] == "" | $adatok['telefon'] == "" | $adatok['vezeteknev'] == "" | $adatok['szla_nev'] == "")
		{
		return "Csillagozott mező kitöltése kötelező!";
		}
		else if (strlen($_POST['jszo1']) < 8 && $_POST['jszo1'] != '') {
			return "A jelszónak minimum 8 karakternek kell lennie!";
		}	 
		else if(!preg_match("/^[0-9]{8}-[0-9]-[0-9]{2}$/", $_POST['adoszam']) && $_POST['adoszam'] != '')
		{
			return "Az adószám nem megfelelő!";
		}	  
		else if(!preg_match("/((?:\+?3|0)6)(?:|\()?(\d{1,2})(?:|\))?(\d{4})(\d{3,4})/", $adatok['telefon']))
		{
			return "A telefonszám nem megfelelő!";
		}			     
		else if($_POST['jszo1'] != $_POST['jszo2'] )
		{
			return "A két jelszó nem egyezik!";
		}
		else
		{
			return "rendben";
		}
}