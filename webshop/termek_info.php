<?php	
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	?><link rel="stylesheet" href="<?= $domain ?>/webshop/css/swipebox.css"><?php
	// VÁSÁRLÁS
	if (isset($_POST['command']) && $_POST['command'] == 'VASARLAS')
	{	
		$kod = time().rand(1, 9999);
		$term_id = $_POST['term_id'];
		$term_db = $_POST['darab'] > 0 ? $_POST['darab'] : 1;
		$datum = date('Y-m-d');
		$term_ar = $_POST['term_ar'];
		$term_akcios_ar = $_POST['term_akcios_ar'];
		$ido = date('H:i:s');
		$term_nev = $_POST['term_nev'];
		$term_kep = $_POST['term_kep'];
		$term_afa = $_POST['term_afa'];

		if (isset($_SESSION['kosar_id'])) //ha nem üres a kosár
		{
			if (isset($_SESSION['login_id']))
			{
				$user_kosar = $_SESSION['login_id'];
			}
			else
			{
				$user_kosar = 0;
			}	

			$kosar_id = $_SESSION['kosar_id'];
			$uid = $_SESSION['uid'];

			$query = "SELECT id FROM ".$webjel."kosar WHERE kosar_id = ".$kosar_id." AND term_id=".$term_id;
			$res = $pdo->prepare($query);
			$res->execute();
			$row = $res -> fetch();

			if (isset($row['id']))
			{
				$updatecommand = "UPDATE ".$webjel."kosar SET term_db = term_db + ".$term_db." WHERE id=".$row['id'];
				$result = $pdo->prepare($updatecommand);
				$result->execute();				
				$kosar_tetel_id = $row['id'];
			}
			else
			{
				$query = "INSERT INTO ".$webjel."kosar (term_id,term_db,datum,term_ar,term_akcios_ar,ido,kosar_id,term_nev,term_kep,term_afa,uid,user_id) VALUES (:term_id,:term_db,:datum,:term_ar,:term_akcios_ar,:ido,:kosar_id,:term_nev,:term_kep,:term_afa,:uid,:user_id)";
				$result = $pdo->prepare($query);
				$result->execute(array(':term_id'=>$term_id,
									':term_db'=>$term_db,
									':datum'=>$datum,
									':term_ar'=>$term_ar,
									':term_akcios_ar'=>$term_akcios_ar,
									':ido'=>$ido,
									':kosar_id'=>$kosar_id,
									':term_nev'=>$term_nev,
									':term_kep'=>$term_kep,
									':term_afa'=>$term_afa,
									':uid'=>$uid,
									':user_id'=>$user_kosar));
				$kosar_tetel_id = $pdo->lastInsertId();				
			}

			setcookie( "kosar_id", $uid, strtotime( '+7 days' ), "/" );
		}
		else //ha üres a kosár
		{
			if (isset($_SESSION['login_id']))
			{
				$user_kosar = $_SESSION['login_id'];
			}
			else
			{
				$user_kosar = 0;
			}
			$query = "INSERT INTO ".$webjel."kosar (term_id,term_db,datum,term_ar,term_akcios_ar,ido,kod,term_nev,term_kep,term_afa,user_id) VALUES (:term_id,:term_db,:datum,:term_ar,:term_akcios_ar,:ido,:kod,:term_nev,:term_kep,:term_afa,:user_id)";
			$result = $pdo->prepare($query);
			$result->execute(array(':term_id'=>$term_id,
								':term_db'=>$term_db,
								':datum'=>$datum,
								':term_ar'=>$term_ar,
								':term_akcios_ar'=>$term_akcios_ar,
								':ido'=>$ido,
								':kod'=>$kod,
								':term_nev'=>$term_nev,
								':term_kep'=>$term_kep,
								':term_afa'=>$term_afa,
								':user_id'=>$user_kosar));
			$kosar_tetel_id = $pdo->lastInsertId();
									
			$kosar_id = $kosar_tetel_id;

			$uid = md5(microtime(true).mt_Rand().$kosar_id);
			
			$query = "UPDATE ".$webjel."kosar SET kosar_id=?, uid = ? WHERE id=?";
			$result = $pdo->prepare($query);
			$result->execute(array($kosar_id,$uid,$kosar_id));
									
			$_SESSION['kosar_id'] = $kosar_id;
			$_SESSION['uid'] = $uid;
			setcookie( "kosar_id", $uid, strtotime( '+7 days' ), "/" );
		}
		if (isset($_POST['termek_parameter'])) {
			foreach ($_POST['termek_parameter'] as $termek_parameter_id => $termek_parameter_ertek_id) {
				$insert = $pdo->prepare("INSERT INTO ".$webjel."kosar_tetel_termek_parameter_ertek ("
						. "kosar_tetel_id, "
						. "termek_parameter_id, "
						. "termek_parameter_nev, "
						. "termek_parameter_ertek_id, "
						. "termek_parameter_ertek_nev"
						. ") values ("
						. "".$kosar_tetel_id.", "
						. "".$termek_parameter_id.", "
						. "'".$pdo->query("SELECT nev FROM ".$webjel."termek_uj_parameterek WHERE id=".$termek_parameter_id)->fetchColumn()."', "
						. "".$termek_parameter_ertek_id.", "
						. "'".$pdo->query("SELECT ertek 
									FROM ".$webjel."termek_uj_parameter_ertekek 
									WHERE id=".$termek_parameter_ertek_id)->fetchColumn()."'"
						. ")");
				$insert->execute();
			}
		}
		
		// Ha kívánságlistából jött, akkor törölni kell onnan
		if (isset($_POST['command2']) && $_POST['command2'] == 'kivansaglistabol')
		{
			$deletecommand = "DELETE FROM ".$webjel."kivansag_lista WHERE termek_id =".$_POST['term_id']." AND user_id=".$_SESSION['login_id'];
			$result = $pdo->prepare($deletecommand);
			$result->execute();
		}
		// Kupon törlése
		$updatecommand = "UPDATE ".$webjel."kosar SET kupon_id=0 WHERE kosar_id=".$_SESSION['kosar_id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
	}
	
	// print '<pre>'.print_r ($_POST,true).'</pre>';

	print '<input type="hidden" id="kat_urlnev" value="'.$_GET['kat_urlnev'].'" />';
	print '<input type="hidden" id="term_urlnev" value="'.$_GET['term_urlnev'].'" />';
	
	// Kívánság lista
	if (isset($_POST['command']) && $_POST['command'] == 'kivansag_lista')
	{
		if(isset($_SESSION['login_id']))
		{
			$rownum = 0;
			$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."kivansag_lista WHERE termek_id = ".$_POST['id']." AND user_id = ".$_SESSION['login_id']);
			$res->execute();
			$rownum = $res->fetchColumn();
			if ($rownum == 0) // Ha még nincs a listában
			{
				$insertcommand = "INSERT INTO ".$webjel."kivansag_lista (termek_id,user_id) VALUES (".$_POST['id'].",".$_SESSION['login_id'].")";
				$result = $pdo->prepare($insertcommand);
				$result->execute();
			}
			print '<div class="alert alert-success alert-dismissable"><p>A terméket sikeresen a kívánságlistádra helyezted. Reméljük, hamarosan átkerül a kosaradba.</p></div>';
		}
		else
		{
			print '<div class="alert alert-danger alert-dismissable">'
				. '<p>'
					. 'Helyezd az általad választott terméket egyszerűen a kívánságlistádra és add le rendelésed akkor, amikor már biztos vagy a dolgodban.<br>'
					. 'A kívánságlistádon elhelyezett termékeid aktuális állapotát érdemes figyelni nehogy lemaradj a rendelésről, hiszen termékkínálatunkat folyamatosan változtatjuk.<br>'
					. '<b>A kívánságlista használatához regisztráció szükséges. <a href="'.$domain.'/regisztracio/">Regisztrálj!</a></b>'
				. '</p>'
			. '</div>';
		}
	}

	if (!empty($_GET['kat_urlnev']) && !empty($_GET['term_urlnev'])) {
		// új átirányítási mód a termékekhez.
		$query = "SELECT id,honnan,hova,tipus FROM ".$webjel."termekek WHERE honnan=?";
		$res = $pdo->prepare($query);
		$res->execute(array($_GET['kat_urlnev'].'/'.$_GET['term_urlnev']));
		$row_redirect = $res -> fetch();

		if (isset($row_redirect['id']) && $row_redirect['honnan'] != '' && $row_redirect['hova'] != '' && $row_redirect['tipus'] > 0) {
		
			$atiranyitas_honnan = explode('/', $row_redirect['honnan']);
			$atiranyitas_hova = explode('/', $row_redirect['hova']);

			header('Location: '.$domain.'/termekek/'.$atiranyitas_hova[0].'/'.$atiranyitas_hova[1], true, $row_redirect['tipus']);
			die();
		}
	}

	$query = "SELECT *, ".$webjel."afa.afa as term_afa, ".$webjel."termekek.id as id 
		FROM ".$webjel."termekek 
		INNER JOIN ".$webjel."afa 
		ON ".$webjel."termekek.afa=".$webjel."afa.id 
		WHERE ".$webjel."termekek.nev_url=?";
	$res = $pdo->prepare($query);
	$res->execute(array($_GET['term_urlnev']));
	$row  = $res -> fetch();
	if(isset($row['id']) && $row['lathato'] == 1)
	{
        $szulkat = '';
        $szul_szulkat = '';
        $szul_szul_szulkat = '';
        $szulkat_nev = '';
        $szul_szulkat_nev = '';
        $szul_szul_szulkat_nev = '';
        if (isset($_GET['kat_urlnev']))
        {
          $query = "SELECT * FROM ".$webjel."term_csoportok where nev_url=?";
          $res = $pdo->prepare($query);
          $res->execute(array($_GET['kat_urlnev']));
          $row_1  = $res -> fetch();
          $szulkat = $row_1['csop_id'];
          $szulkat_nev = $row_1['nev'];
          $szulkat_url = $row_1['nev_url'];
          if ($szulkat > 0)
          {
            $query = "SELECT * FROM ".$webjel."term_csoportok where id=".$szulkat;
            $res = $pdo->prepare($query);
            $res->execute();
            $row_2  = $res -> fetch();
            $szul_szulkat = $row_2['csop_id'];
            $szul_szulkat_nev = $row_2['nev'];
            $szul_szulkat_url = $row_2['nev_url'];
            if ($szul_szulkat != 0 || $szul_szulkat != '')
            {
              $query = "SELECT * FROM ".$webjel."term_csoportok where id=".$szul_szulkat;
              $res = $pdo->prepare($query);
              $res->execute();
              $row_3  = $res -> fetch();
              $szul_szul_szulkat = $row_3['csop_id'];
              $szul_szul_szulkat_nev = $row_3['nev'];
              $szul_szul_szulkat_url = $row_3['nev_url'];
            }
          }
        }
        
        // if(!isset($_GET['term_urlnev']) || $_GET['term_urlnev'] == '')
        // {
          //print '<h2 class="pull-left">'.$row['nev'].'</h2>';
        // }
        print '<ul class="breadcrumbs-custom-path pb-4 d-none d-md-block">';
            print '<li><a href="'.$domain.'">Főoldal</a></li>'; 
            print '<li><a href="'.$domain.'/termekek/">Termékek</a></li>'; 
          if($szul_szul_szulkat_nev != '' && $_GET['kat_urlnev'] != $szul_szul_szulkat_nev)
          {
            print '<li><a href="'.$domain.'/termekek/'.$szul_szul_szulkat_url.'/">'.$szul_szul_szulkat_nev.'</a></li>';
          }
          if($szul_szulkat_nev != '' && $_GET['kat_urlnev'] != $szul_szul_szulkat_nev)
          {
            print '<li><a href="'.$domain.'/termekek/'.$szul_szulkat_url.'/">'.$szul_szulkat_nev.'</a></li>';
          }
          if($szulkat_nev != '' && $_GET['kat_urlnev'] != $szul_szul_szulkat_nev)
          {
            print '<li><a href="'.$domain.'/termekek/'.$szulkat_url.'/">'.$szulkat_nev.'</a></li>';
          }
          if (!empty($_GET['term_urlnev']))
          {
            print '<li class="active">'.$row['nev'].'</li>';
          }                 
        print '</ul>';

		print '<div class="row row-30">';

		//KÉP
			print '<div class="col-lg-6 col-md-12">';
			print '<div class="slick-product slick-vertical">';

			 ?>	

				<div class="termek_plecsnik">
					<?php
						$query = "SELECT * 
						FROM ".$webjel."termek_uj_parameter_ertekek
						INNER JOIN ".$webjel."termek_uj_parameterek
						ON ".$webjel."termek_uj_parameter_ertekek.parameter_id = ".$webjel."termek_uj_parameterek.id
						WHERE termek_id=".$row['id']." AND ".$webjel."termek_uj_parameterek.plecsni = 1";
						foreach ($pdo->query($query) as $value) { ?>

							<img src="<?=$domain?>/images/termekek/<?=$value['ikon']?>" alt="<?=$value['ikon']?>" style="margin: 5px 0; max-width: 60px; height: auto;"><br>
							
					<?php	 }
					?>
				</div>		
				<?php

				$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." AND spec=0 ORDER BY alap DESC LIMIT 1";
				$res = $pdo->prepare($query_kep);
				$res->execute();
				$row_kep = $res -> fetch();	

				if (!empty($row_kep['thumb']))
				{
					$alap_kep = $row_kep['thumb'];
				}
				else
				{
					$alap_kep = "";	
				}
				

				print '<input type="hidden" id="kep_reloadhoz" value="'.$alap_kep.'">';
			
				if ($alap_kep == '') 
				{					
					print '<img id="ez" src="'.$domain.'/webshop/images/noimage.png" alt="nincs kép">';
				}
				else
				{
					print '<div class="slick-slider carousel-parent lightGallery" id="carousel-parent" data-items="1" data-swipe="true" data-child="#child-carousel" data-for="#child-carousel">';

					$query_kepek = "SELECT ".$webjel."termek_kepek.kep, ".$webjel."termek_kepek.thumb, ".$webjel."termek_kepek.ovip_termek_id
							FROM ".$webjel."termek_kepek
							WHERE termek_id=".$row['id']." AND spec=0
							ORDER BY ".$webjel."termek_kepek.alap DESC, ".$webjel."termek_kepek.id ASC";
				
					$elso = 1;
					$kiskepek = 0;
					foreach ($pdo->query($query_kepek) as $row_kepek)
					{
						if ($row_kepek['ovip_termek_id'] !=0)
						{
							$kep_link = $row_kepek['kep'];
						}
						else
						{
							$kep_link = $domain.'/images/termekek/'.$row_kepek['kep'];
						}


						if ($elso == 1)
						{ ?>
		                  <div class="item">
		                    <div class="slick-product-figure"><a href="<?=$kep_link?>"><img src="<?=$kep_link?>" alt="<?=$row['nev']?>" id="ez" class="nagykep-fix"/></a>
		                    </div>
		                  </div>
		                  <?php
		                  $elso = 0;
						} else
						{ ?>
		                  <div class="item">
		                    <div class="slick-product-figure"><a href="<?=$kep_link?>"><img src="<?=$kep_link?>" alt="<?=$row['nev']?>" class="nagykep-fix"/></a>
		                    </div>
		                  </div>
		                  <?php
						}
						?>
								


						<?php

					}

					print '</div>';

					print '<div class="slick-slider child-carousel slick-nav-1" id="child-carousel" data-arrows="true" data-items="3" data-sm-items="3" data-md-items="3" data-lg-items="3" data-xl-items="3" data-xxl-items="3" data-md-vertical="true" data-for="#carousel-parent">';
				
					$query_kepek = "SELECT ".$webjel."termek_kepek.kep, ".$webjel."termek_kepek.thumb, ".$webjel."termek_kepek.ovip_termek_id
							FROM ".$webjel."termek_kepek
							WHERE termek_id=".$row['id']." AND spec=0
							ORDER BY ".$webjel."termek_kepek.alap DESC, ".$webjel."termek_kepek.id ASC";

					$elso = 1;
					$kiskepek = 0;
					foreach ($pdo->query($query_kepek) as $row_kepek)
					{
	

						if ($row_kepek['ovip_termek_id'] !=0)
						{
							if ($row_kepek['thumb'] != '')
							{
								$kiskep = $row_kepek['thumb'];
							}
							else
							{
								$kiskep = $row_kepek['kep'];
							}							
							$kep_link = $kiskep;
						}
						else
						{
							if ($row_kepek['thumb'] != '')
							{
								$kiskep = $row_kepek['thumb'];
							}
							else
							{
								$kiskep = $row_kepek['kep'];
							}							
							$kep_link = $domain.'/images/termekek/'.$kiskep;
						}						

						?>
								
			              <div class="item">
			                <div class="slick-product-figure"><img src="<?=$kep_link?>" alt="<?=$row['nev']?>" class="kiskep-fix"/>
			                </div>
			              </div>

						<?php

					}

					print '</div>';					

				}			

			print '</div>';
		print '</div>';
				
		//ADATOK
		print '<div class="col-lg-6 col-md-12 temek_info">';
		print '<div class="single-product">';

			print '
                <ul class="list-description">
                  <li><span>Cikkszám:</span><span>'.$row['cikkszam'].'</span></li>
                </ul>';

			if ($row['nev'] != '')
			{
				print '<h1 class="single-product-title heading-3">'.$row['nev'].'</h1>';
			}

			?>

          	<?php 
          		if ($row['raktaron'] > 0)
          		{
          			$keszleten = 'Raktáron';
          			$class_r = 'raktaron';
          		}
          		elseif ($row['rendelheto'] == 1)
          		{
          			$keszleten = 'Rendelhető';
          			$class_r = 'raktaron-rendelheto';
          		}
          		elseif ($row['ovip_torolt'] == 1)
          		{
          			$keszleten = 'Kifutott, a továbbiakban nem rendelhető.';
          			$class_r = 'nemrendelheto-kifutott';
          		}          		
          		elseif ($row['kifutott'] == 1)
          		{
          			$keszleten = 'Nem rendelhető';
          			$class_r = 'nemrendelheto-kifutott';
          		}            		
          		else
          		{
          			$keszleten = 'Nem rendelhető';	
          			$class_r = 'nemrendelheto-kifutott';
          		}
          	?>
      		


			<?php
				$query = "SELECT hatarido FROM ".$webjel."beszallito WHERE hatarido != '' AND id = ".$row['beszallito_id'];
				$res = $pdo->prepare($query);
				$res->execute();
				$row_beszall = $res -> fetch();



		// Összehasonlítás
			if ($config_osszehasonlito == 'I')
			{
				if ((isset($_SESSION['osszahas_1']) && $_SESSION['osszahas_1'] == $row['id']) || (isset($_SESSION['osszahas_2']) && $_SESSION['osszahas_2'] == $row['id']) || (isset($_SESSION['osszahas_3']) && $_SESSION['osszahas_3'] == $row['id']) || (isset($_SESSION['osszahas_4']) && $_SESSION['osszahas_4'] == $row['id']))
				{
					$osszeh_check = 'checked';
				}
				else
				{
					$osszeh_check = '';
				}
				print '<p><form action="" method="POST">
							összehasonlítás <input type="checkbox" onclick="form.submit()" '.$osszeh_check.'>';
							if ($osszeh_check == '')
							{
								print '<input type="hidden" name="osszahas_id" value="'.$row['id'].'">';
							}
							else
							{
								print '<input type="hidden" name="no_osszahas_id" value="'.$row['id'].'">';
							}
				print'</form></p>';
			}
			
			$datum = date("Y-m-d");
			
			if ($row['rovid_leiras'] != '')
			{
				print '<div class="tagfix_import mt-3">'.$row['rovid_leiras'].'</div>';
				print '<hr class="hr-24 hr-gray-100">';
			}

		//ÁR
			print '<div class="row row-10 align-items-center mt-0 mb-3">';
			print '<div class="col-12 col-md-7">';

			$van_arlista = 0;

			if (isset($_SESSION['arlista']))
			{
				$query_arlista = "SELECT 
							".$webjel."arlista_arak.ar, 
							".$webjel."arlista_arak.ar_akcios,
							".$webjel."arlista_arak.akcio_tol,
							".$webjel."arlista_arak.akcio_ig,
							".$webjel."termekek.ar as term_ar
						FROM ".$webjel."arlista
						INNER JOIN ".$webjel."arlista_arak
						ON ".$webjel."arlista_arak.arlista_id = ".$webjel."arlista.id
						INNER JOIN ".$webjel."termekek
						ON ".$webjel."arlista_arak.termek_id = ".$webjel."termekek.id
						WHERE ".$webjel."arlista.id = ".$_SESSION['arlista']." AND ".$webjel."termekek.id=".$row['id'];
				$res = $pdo->prepare($query_arlista);
				$res->execute();
				$row_arlista = $res -> fetch();

				if (isset($row_arlista['ar']))
				{
					$van_arlista = 1;
				}
			}	
			$arszin_felulir = '';

			if ($row['raktaron'] < 1 && $row['rendelheto'] != 1) {
				$arszin_felulir = 'style="color: #787878;"';
			}

			if ($van_arlista == 1)
			{
				if ($row_arlista['akcio_ig'] >= $datum && $row_arlista['akcio_tol'] <= $datum)
				{
					print '<div class="text-muted"><span class="text-strike">'.number_format($row_arlista['ar'], 0, ',', ' ').' Ft</span>';
					?>
							<span class="text-body" data-toggle="tooltip" data-placement="bottom" title="Az áthúzott ár az árcsökkentés alkalmazását megelőző 30 nap legalacsonyabb eladási ára." data-template='<div class="tooltip" role="tooltip" style="max-width: 50px;"><div class="arrow"></div><div class="tooltip-inner bg-primary small text-bold text-left p-2"></div></div>'><span class="fa fa-info-circle"></span>
					<?php
					print '</div>';					
					print '<div class="heading-5 single-product-price akcios-szin" '.$arszin_felulir.'><b>AKCIÓ: '.number_format($row_arlista['ar_akcios'], 0, ',', ' ').' Ft</b></div>';

					$datetime1 = new DateTime($row_arlista['akcio_tol']);
					$datetime2 = new DateTime($row_arlista['akcio_ig']);
					$interval = $datetime1->diff($datetime2);
					$kulonbseg = $interval->format('%a');

					if ($kulonbseg > 90)
					{
						$keszlet_szoveg = 'a készlet erejéig.';
					}
					else
					{
						$keszlet_szoveg = $row_arlista['akcio_ig'];
					}

					$akcios_szoveg =  '<p class="small mb-0 mt-2">Akció időtartama: '.$row_arlista['akcio_tol']. ' - '.$keszlet_szoveg.'</p>';
					$term_akcios_ar = $row_arlista['ar_akcios'];
					$term_ara = $row_arlista['ar_akcios'];
				}
				else
				{
					print '<div class="heading-5 single-product-price" id="ar_szines"><b>'.number_format($row_arlista['ar'], 0, ',', ' ').' Ft</b></div>';
					$term_akcios_ar = 0;
					$term_ara = $row_arlista['ar'];
				}			
				print '<div class="small">Alap eladási ár: '.number_format($row['ar'], 0, ',', ' ').' Ft </div>';

			}
			elseif ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) //ha akciós
			{
				print '<div class="text-muted"><span class="text-strike">'.number_format($row['ar'], 0, ',', ' ').' Ft</span>';
				?>
						<span class="text-body" data-toggle="tooltip" data-placement="bottom" title="Az áthúzott ár az árcsökkentés alkalmazását megelőző 30 nap legalacsonyabb eladási ára." data-template='<div class="tooltip" role="tooltip" style="max-width: 50px;"><div class="arrow"></div><div class="tooltip-inner bg-primary small text-bold text-left p-2"></div></div>'><span class="fa fa-info-circle"></span>
				<?php
				print '</div>';
				print '<div class="heading-5 single-product-price akcios-szin" '.$arszin_felulir.'><b>AKCIÓ: '.number_format($row['akciosar'], 0, ',', ' ').' Ft</b></div>';

				$datetime1 = new DateTime($row['akcio_tol']);
				$datetime2 = new DateTime($row['akcio_ig']);
				$interval = $datetime1->diff($datetime2);
				$kulonbseg = $interval->format('%a');

				if ($kulonbseg > 90)
				{
					$keszlet_szoveg = 'a készlet erejéig.';
				}
				else
				{
					$keszlet_szoveg = $row['akcio_ig'];
				}

				$akcios_szoveg =  '<p class="small mb-0 mt-2">Akció időtartama: '.$row['akcio_tol']. ' - '.$keszlet_szoveg.'</p>';
				$term_akcios_ar = $row['akciosar'];
				$term_ara = $row['akciosar'];
			}
			else //ha nem akciós
			{
				print '<div class="heading-5 single-product-price" id="ar_szines" '.$arszin_felulir.'><b>'.number_format($row['ar'], 0, ',', ' ').' Ft</b></div>';
				$term_akcios_ar = 0;
				$term_ara = $row['ar'];
			}


			if (isset($akcios_szoveg))
			{
				echo $akcios_szoveg;
			}

			print '</div>';

			print '<div class="col-12 col-md-5 text-left order-first order-md-last"><span class="product-price '.$class_r.' p-1 mt-2">'.$keszleten.'</span>';

			if (isset($row_beszall['hatarido']) && ($keszleten == 'Rendelhető'))
			{
				?>

				<p class="mt-1 small"><span class="text-body" data-toggle="tooltip" data-placement="bottom" title="Tájékoztató jellegű információ, mely alatt az átlagos beszerzési idő értendő, amennyiben a termék a beszállítónál készleten van! Egyes esetekben a szállítási idő ettől jelentősen eltérhet!" data-template='<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner bg-primary small text-bold text-left p-2"></div></div>'><b>Várható szállítási idő: <?=$row_beszall['hatarido']?> <span class="fa fa-info-circle big"></span></b></span></p>

				<?php
			}	
			
			print '</div>';		
			print '</div>';		


					$termek_parameterek_kosar = $pdo->query("SELECT *, ".$webjel."termek_uj_parameterek.id AS id 
						FROM ".$webjel."termek_uj_parameterek 
						INNER JOIN ".$webjel."termek_uj_parameter_ertekek
						ON ".$webjel."termek_uj_parameterek.id = ".$webjel."termek_uj_parameter_ertekek.parameter_id 
							AND  ".$webjel."termek_uj_parameter_ertekek.termek_id = ".$row['id']."
						WHERE valaszthato=1 
						GROUP BY ".$webjel."termek_uj_parameterek.id 
						ORDER BY nev ASC")->fetchAll();
				?>
				<?php
					if($row['raktaron'] > 0 || $row['rendelheto'] == 1)
					{
						?>
						<form name="kosarba_form" id="kosarba_form" action="" method="post" >
							<?php
								if($config_keszlet_kezeles == 'I')
								{
									print '<input type="hidden" name="raktaron" id="raktaron" value="'.$row['raktaron'].'"/>';
									if ($row['rendelheto'] == 1)
									{
										$max_darab = 1000;
									}
									else
									{
										$max_darab = $row['raktaron'];
									}
								}
								else
								{
									print '<input type="hidden" name="raktaron" id="raktaron" value="999999999"/>';
								}								
									foreach ($termek_parameterek_kosar as $termek_parameter)
									{
										?>
										<div class="pt-3">
										<div class="alert alert-danger" style="padding:6px 15px; margin-bottom:0; display: none;">Kötelező kiválasztani!</div>
										<h6 class="text-secondary text-transform-none pb-1"><?php echo $termek_parameter['nev']; ?></h6>
										<select attr_id="<?= $termek_parameter['id'] ?>" id="termek_parameter<?= $termek_parameter['id'] ?>" name="termek_parameter[<?= $termek_parameter['id'] ?>]" class="form-input termek_parameterek_kosar">
											<option value="" data-felar="0">Válasszon</option>
													<?php
													$termek_parameter_ertekek = $pdo->query("SELECT * 
																FROM ".$webjel."termek_uj_parameter_ertekek 
																WHERE parameter_id=".$termek_parameter['id']."  AND termek_id = ".$row['id']."
																ORDER BY ertek"
													)->fetchAll();
													foreach ($termek_parameter_ertekek as $termek_parameter_ertek)
													{
														?>
														<option
															value="<?php echo $termek_parameter_ertek['id']; ?>"
															data-felar="<?php echo $termek_parameter_ertek['felar']; ?>"
														>
															<?php echo $termek_parameter_ertek['ertek']; ?>
															<?php if ($termek_parameter_ertek['felar']): ?>
															(<?php echo number_format($termek_parameter_ertek['felar'], 0, ',', ' '); ?> Ft felár)
															<?php endif; ?>
														</option>
														<?php
													}
													?>
										</select>
										</div>
										<?php 
									}

							?>									
							
							<input type='hidden' id='term_id' name='term_id' value='<?php print $row['id']; ?>'/>
							<input type='hidden' id='sku' name='sku' value='<?php print $row['cikkszam']; ?>'/>
							<input type='hidden' id='term_nev'  name='term_nev' value='<?php print $row['nev']; ?>'/>
							<input type='hidden' id='term_kep' name='term_kep' value='<?php print $alap_kep; ?>'/>
							<input type='hidden' id='term_ar' name='term_ar' value='<?php print $term_ara; ?>'/>
							<input type='hidden' id='term_afa' name='term_afa' value='<?php print $row["term_afa"]; ?>'/>
							<input type='hidden' id='term_akcios_ar' name='term_akcios_ar' value='<?php print $term_akcios_ar; ?>'/>
							<input type='hidden' id='command' name='command' value='VASARLAS'/>

							<div class="pt-3" id="parameter_riaszt" style="display:none;">
								<div class="alert alert-warning alert-danger" style="margin-bottom:0;"><p>Válasszon paramétert!</p></div>
							</div>

			                <div class="group-md group-middle mt-0">
			                  <div class="stepper-style-1">
			                    <input type="number" data-zeros="true" value="1" min="1" max="<?=$max_darab?>" name="darab" id="darab">
			                  </div>
			                  <div>
			                  	<button type="button" class="button button-lg button-gradient" id="kosarba_gomb">Kosárba</button>
			                  </div>
			                </div>		
			      
			                <div class="mt-4">
			                  <div>
			                    <ul class="list-social list-inline list-inline-md">
			                    	<li>
			                    		<a href="javascript:void(0)" style="font-size: 35px;" class="icon fa fa-heart text-muted" onclick="document.getElementById('kivansag_lista_form').submit();" data-yuspaddtofavorites ="<?php print $row['id']; ?>"></a>
			                    	</li>
			                      <li>
			                      	<a style="font-size: 35px;" class="icon fa fa-envelope text-muted" href="mailto:?subject=Metroman.hu: <?=htmlentities($row['nev'])?>&amp;body=Ez a terméket találtam a Metroman.hu oldalon: <?=$domain?>/termekek/<?=$_GET['kat_urlnev']?>/<?=$_GET['term_urlnev']?>" title="Metroman.hu termék megosztása" target="_blank"></a>
			                      </li>			                    	
			                      <li>
			                      	<a style="font-size: 35px;" class="icon fa fa-facebook-official text-muted" href="http://www.facebook.com/sharer.php?u=<?=$domain?>/termekek/<?=$_GET['kat_urlnev']?>/<?=$_GET['term_urlnev']?>" target="_blank"></a>
			                      </li>
			                    </ul>
			                  </div>		                  
			                </div>			                		                					

			                <hr class="hr-30 hr-gray-100">

						</form>	

						<form id="kivansag_lista_form" action="" method="post" >
							<input type='hidden' name='command' value='kivansag_lista'/>
							<input type='hidden' name='id' value='<?php print $row['id']; ?>'/>
						</form>
						<?php

					//DARAB kedvezmény
								if ($van_arlista == 1) {
									$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termek_darab_kedvezmeny WHERE lathato=1 AND arlista_id=".$_SESSION['arlista']." AND termek_id=".$row['id']);						
								} else {
									$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termek_darab_kedvezmeny WHERE lathato=1 AND arlista_id=0 AND termek_id=".$row['id']);
								}
									$res->execute();
									$rownum_kedv = $res->fetchColumn();						

								if ($rownum_kedv > 0)
								{
									//echo '<hr class="hr-24 hr-gray-100">';

									echo '<div>';		
									echo '<h5>Mennyiségi kedvezmény</h5>';
									if ($van_arlista == 1) {
										$query_kedv = "SELECT * FROM ".$webjel."termek_darab_kedvezmeny WHERE lathato=1 AND arlista_id=".$_SESSION['arlista']." AND termek_id=".$row['id'];
									} else {
										$query_kedv = "SELECT * FROM ".$webjel."termek_darab_kedvezmeny WHERE lathato=1 AND arlista_id=0 AND termek_id=".$row['id'];
									}
									$elso_kedvezmeny = 1;		
									echo '<div class="table-responsive">';	
									echo '<table class="table table-bordered table-striped mt-3">';	
									echo '<thead>';
									echo '<tr>';
									echo '<th>Mennyiség</th>';
									echo '<th class="text-center">Kedvezmény</th>';
									echo '<th class="text-right">Termék ára</th>';
									echo '</tr>';
									echo '</thead>';
									echo '<tbody>';
									foreach ($pdo->query($query_kedv) as $key => $value)
									{
										if ($value['tipus'] == "szazalek") {
											$mennyiseg_kedvezmeny = $term_ara - ($term_ara * ($value['termek_ar'] / 100));
											$kedvezmeny_merteke = '<b>'.$value['termek_ar'] . '</b> %';
											$mennyiseg_kedvezmeny =  $mennyiseg_kedvezmeny + (5 -  $mennyiseg_kedvezmeny % 5);
										} else {
											$mennyiseg_kedvezmeny = $term_ara - $value['termek_ar'];
											$kedvezmeny_merteke = '<b>'.$value['termek_ar'] . '</b> Ft';
										}

										if ($elso_kedvezmeny == 1)
										{
											echo '<tr><td><b>1</b> db vagy több</td><td class="text-center">&#8212;</td><td class="text-right"><b>'.number_format($term_ara,0,',',' ').'</b> Ft/db</td></tr>';
											$elso_kedvezmeny = 0;
										}

										echo '<tr><td><b>'.$value['termek_db'].'</b> db vagy több</td><td class="text-center">'.$kedvezmeny_merteke.'</td><td class="text-right"><b>'.number_format($mennyiseg_kedvezmeny,0,',',' ').'</b> Ft/db</td></tr>';
										
									}
									echo '</tbody>';
									echo '</table>';			
									echo '</div>';		
									echo '</div>';		

										
								}						
					}
					else
					{
						print '<a href="'.$domain.'/termekek/'.$_GET['kat_urlnev'].'/" class="mt-0 button button-primary button-xs">Megnézem a hasonlókat <span class="fa fa-chevron-right"></span></a>';
					}
					?>
			</div></div>
		</div>
		<?php 
		
			$csop_id = $row['csop_id'];
			$termek_id = $row['id'];
		
			$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termek_kepek WHERE spec=1 AND termek_id=".$row['id']);
			$res->execute();
			$rownum_pdf = $res->fetchColumn();

			$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek_csomagok WHERE csomag_termek_id=".$row['id']);
			$res->execute();
			$rownum_csomag = $res->fetchColumn();	

			$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek_csomagok WHERE termek_id=".$row['id']);
			$res->execute();
			$rownum_csomagban_is = $res->fetchColumn();	

		?>

          <div class="tabs-custom tabs-modern">
            <!-- Nav tabs-->
            <div class="nav-tabs-wrap">
              <ul class="nav nav-tabs justify-content-md-start">
              	<li class="nav-item" role="presentation"><a class="nav-link active" href="#leiras" data-toggle="tab">Ismertető</a></li>
              	<?php if ($rownum_csomag > 0): ?>
              	<li class="nav-item" role="presentation"><a class="nav-link" href="#csomag" data-toggle="tab">A csomag tartalma</a></li>          		
              	<?php endif ?>
                <?php /*
                <li class="nav-item" role="presentation"><a class="nav-link" href="#technikai_adatok" data-toggle="tab">Technikai adatok</a></li>
                */ ?>
				<?php if ($rownum_pdf > 0): ?>
				<li class="nav-item" role="presentation"><a class="nav-link" href="#letoltheto-dokumentumok" data-toggle="tab">Használati útmutató</a></li>	
				<?php endif ?>                
				<li class="nav-item" role="presentation"><a class="nav-link" href="#szallitas" data-toggle="tab">Szállítás</a></li>
				<?php if ($rownum_csomagban_is > 0): ?>
					<li class="nav-item" role="presentation"><a class="nav-link" href="#csomagban_is" data-toggle="tab">Csomagban is elérhető</a></li>
				<?php endif ?>
              </ul>
            </div>
            <!-- Tab panes-->
            <div class="tab-content tab-content-1">

          	<?php if ($rownum_csomag > 0): ?>
              <div class="tab-pane fade" id="csomag">

              </div>
          	<?php endif ?>
              <div class="tab-pane fade show active" id="leiras">
              	<div class="mb-3 termek_hosszu_leiras_fix"> 
					<?= $row["leiras"] ?>
				</div>
              </div>          	

              <?php /*
              <div class="tab-pane fade" id="technikai_adatok">
					<?php
						$query_jell = "SELECT * 
							FROM ".$webjel."termek_uj_parameter_ertekek 
							INNER JOIN ".$webjel."termek_uj_parameterek 
							ON ".$webjel."termek_uj_parameterek.id=".$webjel."termek_uj_parameter_ertekek.parameter_id 
							WHERE termek_id=".$row['id']." AND valaszthato=0";
						echo '<div class="table-responsive"><table class="table table-bordered table-striped"><tbody>';
						$tr = 1;
						foreach ($pdo->query($query_jell) as $row_jell)
						{
							if($tr == 1)
							{
								echo '<tr>';
								$tr = 0;
							}
							else { $tr = 1; }
							echo '<td style="width:50%;">';
								if($row_jell['ikon'] != '')
								{
									echo '<img src="'.$domain.'/images/termekek/'.$row_jell['ikon'].'" />';
								}
								echo '<b>'.$row_jell['nev'].':</b>
								<span>'.$row_jell['ertek'].' '.$row_jell['mertekegyseg'].'</span>
							</td>';
							if($tr == 1) { echo '</tr>'; }
						}
						echo '</tbody></table></div>';

					?>
              </div>
              */ ?>
				<div class="tab-pane fade" id="letoltheto-dokumentumok">
					<?php

						if ($rownum_pdf > 0)
						{
							echo '<h6 class="pt-3 pb-2 text-center">Csatolt dokumentumok</h6>';
							echo '<div class="table-responsive"><table class="table table-bordered">
									<thead><tr><th width="80%">Név</th><th class="text-center">Formátum</th></tr></thead>
							<tbody>';
							$query_csatolt = "SELECT * FROM ".$webjel."termek_kepek WHERE spec=1 AND termek_id=".$row['id'];
							foreach ($pdo->query($query_csatolt) as $key => $value)
							{
								echo '<tr><td><a href="'.$domain.'/dokumentumok/termekek/'.$value['kep'].'" target="_blank">'.$value['kep'].'</a></td><td class="text-center"><a href="'.$domain.'/dokumentumok/termekek/'.$value['kep'].'" target="_blank"><img src="'.$domain.'/images/'.strtolower($value['extension']).'.svg" style="max-width: 50px;"></a></td></tr>';
							}
							echo '</tbody></table></div>';
							echo '<p><b>A használati útmutatók letöltése csak saját használatra megengedett. Kereskedelmi célú felhasználása, nyilvános közzététele interneten vagy nyomtatott formában tilos!</b></p>';
						}					
					?>
				</div>
				<div class="tab-pane fade" id="szallitas">
					<div class="row">
						<div class="col-md-4">
							<h5 class="text-primary">Otthon maradnál?</h5>
							<p>Szeretnéd kényelmesen, otthonodban átvenni a megrendelt terméket? Válaszd a házhoz szállítást, és megrendelésedet mindössze 1350 Ft-tól, vagy 100 000 Ft felett ingyenesen házhoz szállítja a futárszolgálat.</p>
							<a href="<?=$domain?>/szallitasi-feltetelek">Bővebben</a>							
						</div>
						<div class="col-md-4">
							<h5 class="text-primary">GLS csomagautomata - <span class="d-inline-block">24 órás</span> kényelmes átvétel</h5>
							<p>Élvezd a csomagautomaták adta szabadságot, vedd át megrendelésed napközben vagy akár éjszaka! A csomagautomatába szállítás díja csupán 990 Ft, 60 ezer Ft felett pedig ingyenes.</p>
							<a href="<?=$domain?>/szallitasi-feltetelek">Bővebben</a>								
						</div>
						<div class="col-md-4">
							<h5 class="text-primary">GLS Csomagpont</h5>
							<p>Válaszd ki a hozzád legközelebb található GLS csomagpontot 1190 ft-ért. Kényelmesen, az otthonodhoz közel, az üzlet nyitva tartási idejében tudod átvenni megrendelésed. 60 000 Ft feletti rendelésnél a szállítás ingyenes.</p>
							<a href="<?=$domain?>/szallitasi-feltetelek">Bővebben</a>								
						</div>
					</div>
				</div>	

				<?php if ($rownum_csomagban_is > 0): ?>
				<div class="tab-pane fade" id="csomagban_is">
					<?php
						$termekek_csomagban = $pdo->query("SELECT GROUP_CONCAT(csomag_termek_id) FROM ".$webjel."termekek_csomagok WHERE termek_id=".$termek_id)->fetchColumn();

						$query = "SELECT * FROM `".$webjel."termekek` WHERE id IN (".$termekek_csomagban.") AND lathato = 1";
						print '<div class="row row-30 kapcs_termekek">';
						foreach ($pdo->query($query) as $row)
						{
							include $gyoker.'/webshop/termek.php';
						}
						print '</div>';
					?>
				</div>
				<?php endif ?>							
            </div>
          </div>

	<?php if ($rownum_csomag > 0): ?>
          <div id="kapcs_div" class="mb-5">
          	<?php 

		  			$query = "SELECT
		  					SUM(IF(akciosar > 0 AND akcio_ig >= CURDATE(), akciosar, ar) * mennyiseg) as szumma
					FROM ".$webjel."termekek t
		  			INNER JOIN ".$webjel."termekek_csomagok tcs
		  			ON tcs.termek_id = t.id
		  			INNER JOIN ".$webjel."term_csoportok csop
		  			ON csop.id = t.csop_id
		  			WHERE tcs.csomag_termek_id = ".$termek_id;
					$res = $pdo->prepare($query);
					$res->execute();
					$row_szumma = $res -> fetch();	

					$sporoljon = $row_szumma['szumma'] - $term_ara;	  			

          	?>
			<span class="float-md-right float-none text-center p-2 bg-kek d-block d-md-inline">
				<b>Spóroljon: <?php echo number_format($sporoljon,0,'',' ') ?> Ft-ot!</b>
			</span>           	
          	<h4 class="my-3">A csomag tartalma</h4>         	
      		<?php 
		  			$sporoljon = 0;

		  			$query = "SELECT
		  					t.id,
		  					t.nev,
		  					t.csop_id,
		  					t.cikkszam,
		  					t.raktaron,
		  					t.rendelheto,
		  					t.beszallito_id,
		  					t.nev_url,
		  					csop.nev_url as csopnev_url,
		  					IF(akciosar > 0 AND akcio_ig >= CURDATE(), akciosar, ar) AS termek_ara,
		  					tcs.mennyiseg
					FROM ".$webjel."termekek t
		  			INNER JOIN ".$webjel."termekek_csomagok tcs
		  			ON tcs.termek_id = t.id
		  			INNER JOIN ".$webjel."term_csoportok csop
		  			ON csop.id = t.csop_id
		  			WHERE tcs.csomag_termek_id = ".$termek_id."
		  			GROUP BY t.id";
		  			foreach ($pdo->query($query) as $key => $value) {

						$link = $domain.'/termekek/'.$value['csopnev_url'].'/'.$value['nev_url'];

		  				$sporoljon+= $value['termek_ara'] * $value['mennyiseg'];

						$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$value['id']." AND spec=0 ORDER BY alap DESC LIMIT 1";
						$res = $pdo->prepare($query_kep);
						$res->execute();
						$row_kep = $res -> fetch();
						$alap_kep = $row_kep['kep'];
						if ($alap_kep == '') 
						{
							$kep_link = $domain.'/webshop/images/noimage.png';
						}
						elseif ($row_kep['ovip_termek_id'] !=0)
						{
							if($row_kep['thumb'] != '')
							{
								$kep = $row_kep['thumb'];
							}
							else
							{
								$kep = $row_kep['kep'];
							}
							$kep_link = $kep;
						}
						else
						{
							if($row_kep['thumb'] != '')
							{
								$kep = $row_kep['thumb'];
							}
							else
							{
								$kep = $row_kep['kep'];
							}
							$kep_link = $domain.'/images/termekek/'.$kep;
						}   

						if ($key != 0)
						{
						      echo '<hr class="my-3">';
		       			}           			

		       			include $gyoker.'/webshop/termek_csik.php';
				}
				?>
          </div>
    <?php endif ?>

	<?php 
		$query = "SELECT pe.ertek FROM ".$webjel."termekek t INNER JOIN ".$webjel."termek_uj_parameter_ertekek pe ON pe.termek_id = t.id WHERE pe.parameter_id = 7 AND t.id = ".$termek_id." GROUP BY t.id";
		$res = $pdo->prepare($query);
		$res->execute();
		$row_kapcs = $res -> fetch();
		if (isset($row_kapcs['ertek']))
		{		
			?>
				<h4 class="heading-4 pt-5 text-center">Kapcsolódó termékek</h4>
				<?php include $gyoker.'/webshop/kapcsolodo_termekek.php'; ?>
		<?php	
		} else { ?>
				<h4 class="heading-4 pt-5 text-center">Hasonló termékek</h4>
				<?php include $gyoker.'/webshop/hasonlo_termekek.php'; ?>				
		<?php
		}
	}
	else
	{
		ob_end_clean();
		header('Location: '.$domain.'/404.php');
		die();
		print '<h5>Sajnos nincs ilyen termékünk, vagy már korábban megszűnt.</h5>';
	}
?>