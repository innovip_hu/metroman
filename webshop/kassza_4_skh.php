<?php
	// OTP visszatérés
	if(isset($_GET['txid']))
	{
		if ($_GET['txid'] == '')
		{
			print '<p style="text-align:center; color:#D27290;">Hiányzik a rendelési azonosító!</p>';
		}
		else
		{
			$url = 'https://ebank.khb.hu/PaymentGatewayTest/PGResult?mid=1210&txid='.$_GET['txid'];
		    $ch = curl_init($url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    $response = curl_exec($ch);
		    curl_close($ch);
			
			$eredmeny_tomb = explode("\n", $response);

			$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."rendeles WHERE id='".$_GET['txid']."'");
			$res->execute();
			$rownum = $res->fetchColumn();
			if($rownum > 0)
			{
				print '<h3 style="text-align: center;">Rendelésedet sikeresen rögzítettük!</h3>';
				print '<h4 style="text-align: center;">Hamarosan e-mail-ben is megküldjük a vásárlásod adatait.</h4>';
				print '<h4 style="text-align: center;">További szép napot kívánunk!</h4>
						<p style="text-align: center;"><b>A fizetés eredménye:</b></p>';

		      	if (trim($eredmeny_tomb[0]) == 'ACK')
		      	{				
					// Fizetés visszaigazolása
					$updatecommand = "UPDATE ".$webjel."rendeles SET fizetve_otp=1 WHERE id='".$_GET['txid']."'";
					$result = $pdo->prepare($updatecommand);
					$result->execute();					

					echo '<p style="color: green;"><b>Sikeres fizetés!</b></p>';
				}
				elseif (trim($eredmeny_tomb[0]) == 'PEN')
				{
					// Fizetés visszaigazolása
					$updatecommand = "UPDATE ".$webjel."rendeles SET fizetve_otp=2 WHERE id='".$_GET['txid']."'";
					$result = $pdo->prepare($updatecommand);
					$result->execute();					

					echo '<p><b>Függőben</b></p>';
				}
				else
				{
					echo '<p style="color: red;"><b>Sikertelen!</b></p>';
				}

				print '<p style="text-align: center;">Tranzakció azonosító: '.$_GET['txid'].'<br>Állapot: '.$eredmeny_tomb[0].'<br>Üzenet: '.$eredmeny_tomb[2].'<br>Banki engedélyszám: '.$eredmeny_tomb[3].'</p>
									<p style="text-align: center;">Ha még nem tetted, kövess minket a Facebookon is, hogy ne maradj le akcióinkról és újdonságainkról!
							<br><a href="https://www.facebook.com/MetromanHungariaKft/" target="_blank"><img src="'.$domain.'/images/facebook.png" style="width: 40px; margin-top: 10px;" alt="Facebook"></a></p>';


			}
			else
			{
				print '<p style="text-align:center; color:#D27290;">Érvénytelen rendelési azonosító!</p>';
			}
		}
	}
	// Rendelés leadása
	else if (isset($_POST['command']) && $_POST['command'] == 'rendeles_leadas' && (isset($_SESSION['kosar_id'])))
	{
		// Ha be van lépve
		if (isset($_SESSION['login_id'])) //REG
		{
			$query4 = "SELECT * FROM ".$webjel."users where id=".$_SESSION['login_id'];
			$user_id = $_SESSION['login_id'];
			$noreg = 0;
			$res = $pdo->prepare($query4);
			$res->execute();
			$row4 = $res -> fetch();
			
			$user_email = $row4['email'];
			$nev1 = $row4['vezeteknev'].' '.$row4['keresztnev'];
			$nev2 = $row4['vezeteknev'].' '.$row4['keresztnev'];
			$nev3 = $row4['vezeteknev'].' '.$row4['keresztnev'];
			$telefon = $row4['telefon'];
			$adoszam = $row4['adoszam'];
			$cim1 = $row4['cim_irszam'].' '.$row4['cim_varos'];
			$cim2 = $row4['cim_utca'].' '.$row4['cim_hszam'];
			$szla_vasarlo_nev = $row4['szla_nev'];
			$szla_vasarlo_email = $row4['email'];
			$szla_vasarlo_irszam = $row4['cim_irszam'];
			$szla_vasarlo_varos = $row4['cim_varos'];
			$szla_vasarlo_cim = $row4['cim_utca'].' '.$row4['cim_hszam'];
			if ($row4['cim_szall_varos'] == '')
			{
				$szallcim1 = $row4['cim_irszam'].' '.$row4['cim_varos'];
				$szallcim2 = $row4['cim_utca'].' '.$row4['cim_hszam'];
			}
			else
			{
				$nev3 = $row4['cim_szall_nev'];
				$szallcim1 = $row4['cim_szall_irszam'].' '.$row4['cim_szall_varos'];
				$szallcim2 = $row4['cim_szall_utca'].' '.$row4['cim_szall_hszam'];
			}

			$cim_szall_nev = $row4['cim_szall_nev'];
			$cim_szall_varos = $row4['cim_szall_varos'];
			$cim_szall_utca = $row4['cim_szall_utca'];
			$cim_szall_hszam = $row4['cim_szall_hszam'];
			$cim_szall_irszam = $row4['cim_szall_irszam'];				
		}
		else // Nincs belépve
		{
			$_POST['nev'] = str_replace("'", "`", $_POST['nev']);
			$user_email = $_POST['email'];
			$nev1 = $_POST['nev'];
			$nev2 = $_POST['nev'];
			$nev3 = $_POST['nev'];
			$telefon = $_POST['telefon'];
			$adoszam = $_POST['adoszam'];
			$cim1 = $_POST['cim_irszam'].' '.$_POST['cim_varos'];
			$cim2 = $_POST['cim_utca'].' '.$_POST['cim_hszam'];
			$szla_vasarlo_nev = $_POST['szla_nev'];
			$szla_vasarlo_email = $_POST['email'];
			$szla_vasarlo_irszam = $_POST['cim_irszam'];
			$szla_vasarlo_varos = $_POST['cim_varos'];
			$szla_vasarlo_cim = $_POST['cim_utca'].' '.$_POST['cim_hszam'];
			if ($_POST['cim_szall_varos'] == '')
			{
				$szallcim1 = $_POST['cim_irszam'].' '.$_POST['cim_varos'];
				$szallcim2 = $_POST['cim_utca'].' '.$_POST['cim_hszam'];
			}
			else
			{
				$nev3 = $_POST['cim_szall_nev'];
				$szallcim1 = $_POST['cim_szall_irszam'].' '.$_POST['cim_szall_varos'];
				$szallcim2 = $_POST['cim_szall_utca'].' '.$_POST['cim_szall_hszam'];
			}
			if(isset($_POST['regisztracio_checkbox']) && $_POST['regisztracio_checkbox'] == 'ok') // Regisztráció
			{
				$jelszo = $_POST['jelszo'];
				
				$cim_szall_nev = $_POST['cim_szall_nev'];
				$cim_szall_varos = $_POST['cim_szall_varos'];
				$cim_szall_utca = $_POST['cim_szall_utca'];
				$cim_szall_hszam = $_POST['cim_szall_hszam'];
				$cim_szall_irszam = $_POST['cim_szall_irszam'];
					
				$insertcommand = "INSERT INTO ".$webjel."users (email,telefon,vezeteknev,cim_varos,cim_utca,cim_hszam,cim_irszam,cim_szall_nev,cim_szall_varos,cim_szall_utca,cim_szall_hszam,cim_szall_irszam,tipus,jelszo,reg_datum,szla_nev,adoszam) VALUES ('".$_POST['email']."','".$_POST['telefon']."','".$_POST['nev']."','".$_POST['cim_varos']."','".$_POST['cim_utca']."','".$_POST['cim_hszam']."','".$_POST['cim_irszam']."','".$cim_szall_nev."','".$cim_szall_varos."','".$cim_szall_utca."','".$cim_szall_hszam."','".$cim_szall_irszam."','user','".password_hash($jelszo, PASSWORD_DEFAULT)."',NOW(),'".$szla_vasarlo_nev."','".$adoszam."')";
				$result = $pdo->prepare($insertcommand);
				$result->execute();
				//id meghatározása
				$user_id = $pdo->lastInsertId();
				$noreg = 0;
			}
			else
			{
				$vezeteknev = $_POST['nev'];
				$keresztnev = '';
				$user_email = $_POST['email'];
				$telefon = $_POST['telefon'];
				$cim_varos = $_POST['cim_varos'];
				$cim_utca = $_POST['cim_utca'];
				$cim_hszam = $_POST['cim_hszam'];
				$cim_irszam = $_POST['cim_irszam'];
				$cim_szall_nev = $_POST['cim_szall_nev'];
				$cim_szall_varos = $_POST['cim_szall_varos'];
				$cim_szall_utca = $_POST['cim_szall_utca'];
				$cim_szall_hszam = $_POST['cim_szall_hszam'];
				$cim_szall_irszam = $_POST['cim_szall_irszam'];
				$query = "INSERT INTO ".$webjel."users_noreg (vezeteknev,keresztnev,email,telefon,cim_varos,cim_utca,cim_hszam,cim_irszam,cim_szall_nev,cim_szall_varos,cim_szall_utca,cim_szall_hszam,cim_szall_irszam,szla_nev,adoszam) VALUES (:vezeteknev,:keresztnev,:email,:telefon,:cim_varos,:cim_utca,:cim_hszam,:cim_irszam,:cim_szall_nev,:cim_szall_varos,:cim_szall_utca,:cim_szall_hszam,:cim_szall_irszam,:szla_nev,:adoszam)";
				$result = $pdo->prepare($query);
				$result->execute(array(':vezeteknev'=>$vezeteknev,
									':keresztnev'=>$keresztnev,
									':email'=>$user_email,
									':telefon'=>$telefon,
									':cim_varos'=>$cim_varos,
									':szla_nev'=>$szla_vasarlo_nev,
									':adoszam'=>$adoszam,
									':cim_utca'=>$cim_utca,
									':cim_hszam'=>$cim_hszam,
									':cim_irszam'=>$cim_irszam,
									':cim_szall_nev'=>$cim_szall_nev,
									':cim_szall_varos'=>$cim_szall_varos,
									':cim_szall_utca'=>$cim_szall_utca,
									':cim_szall_hszam'=>$cim_szall_hszam,
									':cim_szall_irszam'=>$cim_szall_irszam));
				
				$user_id = $pdo->lastInsertId();
				$noreg = 1;
			}
		}
		//szállítási költség meghatározása
		$query_f = "SELECT * FROM ".$webjel."kassza_fiz_mod WHERE id=".$_POST['fiz_mod'];
		$res = $pdo->prepare($query_f);
		$res->execute();
		$row_f = $res -> fetch();
		$fiz_mod = $row_f['nev'];

		$query_sz = "SELECT * FROM ".$webjel."kassza_szall_mod WHERE id=".$_POST['szall_mod'];
		$res = $pdo->prepare($query_sz);
		$res->execute();
		$row_sz = $res -> fetch();
		$szall_mod = $row_sz['nev'];
		// Rendelés
						
						// Weboldal beállításai
							$query_beall = 'SELECT * FROM '.$webjel.'beallitasok WHERE id=1';
							$res = $pdo->prepare($query_beall);
							$res->execute();
							$row_beall = $res -> fetch();
							
						// Kosár tartalmának összege, hogy meghatározzuk ingyenes-e a szállítás
							$ingy_szall = 0;
							$query = "SELECT * FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];
							$ar_ossz = 0;
							foreach ($pdo->query($query) as $row)
							{
								if($row['term_akcios_ar'] == 0) //ha nem akciós
								{
									$ar_ossz = $ar_ossz + ($row['term_ar'] * $row['term_db']);
								}
								else //ha akciós
								{
									$ar_ossz = $ar_ossz + ($row['term_akcios_ar'] * $row['term_db']);
								}
							}
							if ($row_sz['ingyenes_szallitas'] != 0 && $ar_ossz >= $row_sz['ingyenes_szallitas'])
							{
								$ingy_szall = 1;
							}
							// Kupon vizsgálat ingyen szállítás kedvezmény miatt
							$kupon_kedv = 0;
							$kupon_termek_id = 0;
							$kupon_minusz_tetel = 0;
							if($row['kupon_id'] > 0)
							{
								$query_kupon = "SELECT * FROM ".$webjel."kuponok where id=".$row['kupon_id'];
								$res = $pdo->prepare($query_kupon);
								$res->execute();
								$row_kupon = $res -> fetch();
								if($row_kupon['kupon_fajta'] == 'Ingyenes szállítás')
								{
									$ingy_szall = 1;
								}
								else if($row_kupon['kupon_fajta'] == 'Kedvezmény')
								{
									if ($row_kupon['kedv_tipus'] == 'szazalek')
									{
										$kupon_kedv = $row_kupon['kedvezmeny'];
										$kupon_minusz_tetel = round($ar_ossz * ($kupon_kedv / 100))*(-1);
									}
									else if ($row_kupon['kedv_tipus'] == 'osszeg')
									{
										$kupon_minusz_tetel = $row_kupon['kedvezmeny']*(-1);
									}
								}
								else if($row_kupon['kupon_fajta'] == 'Ajándék termék')
								{
									$kupon_termek_id = $row_kupon['termek_id'];
								}
							}

							if($row_f['bankkartya'] == 1 && isset($row4['id']))
							{
								$kod_otp = $row4['id'].'a'.time().rand(1, 99999);
							}
							else if($row_f['bankkartya'] == 1)
							{
								$kod_otp = '1a'.time().rand(1, 99999);
							}
							else
							{
								$kod_otp = '';
							}
							
						//rendelés létrehozása
							$insertcommand = "INSERT INTO ".$webjel."rendeles (user_id, datum, ido, rendeles_id, fiz_mod, szall_mod, noreg, ingy_szall, megjegyzes, ajandek_termek_id, postapont, pickpackpont, kod_otp, vasarlo_irszam, vasarlo_varos, vasarlo_cim, vasarlo_nev, vasarlo_email, glscspont, fp_csomagpont) 
							VALUES (:user_id, :datum, :ido, :rendeles_id, :fiz_mod, :szall_mod, :noreg, :ingy_szall, :megjegyzes, :ajandek_termek_id, :postapont, :pickpackpont, :kod_otp, :vasarlo_irszam, :vasarlo_varos, :vasarlo_cim, :vasarlo_nev, :vasarlo_email, :glscspont, :fp_csomagpont)";
							$result = $pdo->prepare($insertcommand);
							$result->execute(array(':user_id'=>$user_id,
					  							   ':datum'=>date('Y-m-d'),
					  							   ':ido'=>date('H:i:s'),
					  							   ':rendeles_id'=>$_SESSION['kosar_id'],
					  							   ':fiz_mod'=>$fiz_mod,
					  							   ':szall_mod'=>$szall_mod,
					  							   ':noreg'=>$noreg,
					  							   ':ingy_szall'=>$ingy_szall,
					  							   ':megjegyzes'=>$_POST['megjegyzes'],
					  							   ':ajandek_termek_id'=>$kupon_termek_id,
					  							   ':postapont'=>$_POST['valasztott_postapont'],
					  							   ':glscspont'=>$_POST['valasztott_gls_cspont'],
					  							   ':fp_csomagpont'=>$_POST['valasztott_foxpost_cspont'],
					  							   ':pickpackpont'=>$_POST['ppp_shopType'].' '.$_POST['ppp_address'],
					  							   ':kod_otp'=>$kod_otp,
					  							   ':vasarlo_irszam'=>$szla_vasarlo_irszam,
					  							   ':vasarlo_varos'=>$szla_vasarlo_varos,
					  							   ':vasarlo_cim'=>$szla_vasarlo_cim,
					  							   ':vasarlo_nev'=>$szla_vasarlo_nev,
					  							   ':vasarlo_email'=>$szla_vasarlo_email));
							$last_rendeles_id = $pdo->lastInsertId();

							if ($last_rendeles_id > 0)
							{
								$insertcommand = "INSERT INTO ".$webjel."rendeles_cimek (rendeles_id, vezeteknev, email, telefon, szla_nev, cim_varos, cim_utca, cim_hszam, cim_irszam, cim_szall_nev, cim_szall_varos, cim_szall_utca, cim_szall_hszam, cim_szall_irszam, adoszam) 
								VALUES (:rendeles_id, :vezeteknev, :email, :telefon, :szla_nev, :cim_varos, :cim_utca, :cim_hszam, :cim_irszam, :cim_szall_nev, :cim_szall_varos, :cim_szall_utca, :cim_szall_hszam, :cim_szall_irszam, :adoszam)";
								$result = $pdo->prepare($insertcommand);
								$result->execute(array(':rendeles_id'=>$last_rendeles_id,
						  							   ':vezeteknev'=>$nev1,
						  							   ':email'=>$szla_vasarlo_email,
						  							   ':telefon'=>$telefon,
						  							   ':szla_nev'=>$szla_vasarlo_nev,
						  							   ':adoszam'=>$adoszam,
						  							   ':cim_varos'=>$szla_vasarlo_varos,
						  							   ':cim_varos'=>$szla_vasarlo_varos,
						  							   ':cim_utca'=>$szla_vasarlo_cim,
						  							   ':cim_hszam'=>'',
						  							   ':cim_irszam'=>$szla_vasarlo_irszam,
						  							   ':cim_szall_nev'=>$cim_szall_nev,
						  							   ':cim_szall_varos'=>$cim_szall_varos,
						  							   ':cim_szall_utca'=>$cim_szall_utca,
						  							   ':cim_szall_hszam'=>$cim_szall_hszam,
						  							   ':cim_szall_irszam'=>$cim_szall_irszam));							

							//Tábla a rendelésről
								$tabla_tetelek = '<table align="left" cellpadding="6" cellspacing="0" style="background-color: #FFFFFF; font-family: calibri, sans-serif;" width="100%">
											<tr><td colspan="5"><b>Rendelés adatai</p></td></tr>
											<tr style="border: 0px solid #c9c9c9;">
												<td colspan="2" align="left" style="border: 0px solid #c9c9c9;">Megrendelés száma</td>
												<td colspan="3"><b style="background-color: #3E5878; color: #ffffff; padding: 0 5px;">'.$last_rendeles_id.'</b></td>
											</tr>
											<tr>
												<td colspan="2" align="left">
													Rendelés időpontja
												</td>
												<td colspan="3" align="left">
													'.date('Y.m.d').'&nbsp;'.date('H:i:s').'
												</td>
											</tr>
											<tr>
												<td colspan="2" align="left">
													Név
												</td>
												<td colspan="3" align="left">
													'.$nev1.'
												</td>
											</tr>										
											<tr>
												<td colspan="2" align="left">
													Számlázási cím
												</td>
												<td colspan="3" align="left">
													'.$cim1.','.$cim2.'
												</td>
											</tr>
											<tr>
												<td colspan="2" align="left">
													Szállítási Név
												</td>
												<td colspan="3" align="left">
													'.$nev3.'
												</td>
											</tr>										
											<tr>
												<td colspan="2" align="left">
													Szállítási cím
												</td>
												<td colspan="3" align="left">
													'.$szallcim1.','.$szallcim2.'
												</td>
											</tr>										
											<tr>
												<td colspan="2" align="left">
													Email
												</td>
												<td colspan="3" align="left">
													'.$user_email.'
												</td>
											</tr>
											<tr>
												<td colspan="2" align="left">
													Telefon
												</td>
												<td colspan="3" align="left">
													'.$telefon.'
												</td>
											</tr>																													
											<tr><td colspan="5">';

						
							//Kosár tartalma
								$query = "SELECT * FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];
								$ar = 0;
								$id = 0;
								$tabla_tetelek.= '<table align="left" cellpadding="6" cellspacing="0" style="font-family: calibri, sans-serif;" width="100%"">';
								$tabla_tetelek.= '<tr align="left" style="font-weight:bold;" style="background-color: #3E5878; color: #FFFFFF;">
											<td width="175" colspan="2" style="background-color: #3E5878; color: #FFFFFF;">Név</td>
											<td style="background-color: #3E5878; color: #FFFFFF;">Mennyiség</td>
											<td style="background-color: #3E5878; color: #FFFFFF;">Egységár</td>
											<td align="right" style="background-color: #3E5878; color: #FFFFFF;">Összesen</td>
										</tr>'; //colspan ki, ha van kép
								foreach ($pdo->query($query) as $row)
								{
									if($row['term_akcios_ar'] == 0) //ha nem akciós
									{
										// $term_ar = $row['term_ar'] - (round($row['term_ar'] * ($kupon_kedv / 100)));
										$term_ar = $row['term_ar'];
									}
									else //ha akciós
									{
										// $term_ar = $row['term_akcios_ar'] - (round($row['term_akcios_ar'] * ($kupon_kedv / 100)));
										$term_ar = $row['term_akcios_ar'];
									}
									//termék adatai
									$query_term = "SELECT * FROM ".$webjel."termekek where id=".$row['term_id'];
									$res = $pdo->prepare($query_term);
									$res->execute();
									$row_term = $res -> fetch();
									if ($row_term['kep'] != '')
									{
										$kep = '<img src="'.$domain.'/images/termekek/'.$row_term['kep'].'" width="30" border="0">';
									}
									else
									{
										$kep = '';
									}
									$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_term['csop_id'];
									$res = $pdo->prepare($query_csop);
									$res->execute();
									$row_csop = $res -> fetch();
									
									//rendelés tétel létrehozása
									$insertcommand = "INSERT INTO ".$webjel."rendeles_tetelek (term_id,term_db,term_ar,term_akcios_ar,rendeles_id,term_nev,afa) VALUES ('".$row['term_id']."','".$row['term_db']."','".$row['term_ar']."','".$row['term_akcios_ar']."','".$last_rendeles_id."',:term_nev,'".$row['term_afa']."')";
									$result = $pdo->prepare($insertcommand);
									// $result->execute();
									$result->execute(array(':term_nev'=>$row['term_nev']));
									
									$last_id = $pdo->lastInsertId();
									
									// Készlet csökkentése
									if($config_keszlet_kezeles == 'I')
									{
										$pdo->exec("UPDATE ".$webjel."termekek SET raktaron=(raktaron - ".$row['term_db'].") WHERE id=".$row['term_id']);
									}
									
									$tabla_tetelek.= '<tr>
												<td><a href="'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row_term['nev_url'].'">
													<h5 class="margbot10">'.$row['term_nev'].'</h5>';
												// Jellemzők
												$query_jell = "SELECT * FROM ".$webjel."kosar_tetel_termek_parameter_ertek WHERE kosar_tetel_id = ".$row['id']." ORDER BY termek_parameter_nev ASC";
												$elso = 1;
												foreach ($pdo->query($query_jell) as $row_jell)
												{
													if($elso == 1) { $tabla_tetelek.= ''; $elso = 0; } else { $tabla_tetelek.= '<br/>'; }
													$tabla_tetelek.= $row_jell['termek_parameter_nev'].': '.$row_jell['termek_parameter_ertek_nev'];
													
													//rendelés tétel paraméterek
													$insertcommand = "INSERT INTO ".$webjel."rendeles_tetelek_termek_parameterek 
															(rendeles_tetel_id, termek_parameter_nev, termek_parameter_ertek_nev) 
															VALUES (".$last_id.", '".$row_jell['termek_parameter_nev']."', '".$row_jell['termek_parameter_ertek_nev']."')";
													$result = $pdo->prepare($insertcommand);
													$result->execute();
												}						
												//rendelés tétel paraméterek törlése
												$deletecommand = 'DELETE FROM '.$webjel.'kosar_tetel_termek_parameter_ertek WHERE kosar_tetel_id='.$row['id'];
												$result = $pdo->prepare($deletecommand);
												$result->execute();
												
												$tabla_tetelek.= '</a></td>
												<td></td>
												<td>'.$row['term_db'].'</td>
												<td>'.number_format($term_ar, 0, ',', ' ').' Ft</td>
												<td align="right">'.number_format($term_ar * $row['term_db'], 0, ',', ' ').' Ft</td>
											</tr>';
									$ar = $ar + ($term_ar * $row['term_db']);
									$kupon_id = $row['kupon_id'];
								}
								if($kupon_minusz_tetel != 0) // Ha van kedvezményes összeg, akkor minuszos tétel
								{
									// <td>'.$kep.'</td> <td></td> helyett
	 								$tabla_tetelek.= '<tr>
												<td>Kupon</td>
												<td></td>
												<td>1</td>
												<td>'.number_format($kupon_minusz_tetel, 0, ',', ' ').' Ft</td>
												<td align="right">'.number_format($kupon_minusz_tetel, 0, ',', ' ').' Ft</td>
											</tr>';
									$ar = $ar + ($kupon_minusz_tetel);
									// SQl
									$insertcommand = "INSERT INTO ".$webjel."rendeles_tetelek (term_id,term_db,term_ar,term_akcios_ar,rendeles_id,term_nev) VALUES ('0','1','".$kupon_minusz_tetel."','0','".$last_rendeles_id."','Kupon')";
									$result = $pdo->prepare($insertcommand);
									$result->execute();
								}

								$res = $pdo->prepare('SELECT * FROM '.$webjel.'kassza_szall_mod_savok WHERE sav <= '.$ar.' AND szall_mod_id='.$_POST['szall_mod'].' ORDER BY sav DESC LIMIT 1 ');
								$res->execute();
								$row_sav  = $res -> fetch();

								//szállítási költség meghatározása
								if($row_f['utanvet'] == 1)
								{
									$szall_kolts = $row_sav['ar_utanvet'];
								}
								else
								{
									$szall_kolts = $row_sav['ar'];
								}
								// Ha van ingyenes szállítási költség
								
								if ($row_sz['ingyenes_szallitas'] != 0 && $ar >= $row_sz['ingyenes_szallitas'])
								{
									$szall_kolts = 0;
								}
								if($ingy_szall == 1)
								{
									$szall_kolts = 0;
								}
								
								// Rendelések táblába beírni a szállítási költséget + kupon 
								$updatecommand = "UPDATE ".$webjel."rendeles SET szallitasi_dij=?, szallitasi_dij_afa=?, kupon_id=? WHERE rendeles_id=?";
								$result = $pdo->prepare($updatecommand);
								$result->execute(array($szall_kolts,$row_beall['szallitas_afa'],$kupon_id,$_SESSION['kosar_id']));
								
								// Kupon kedvezmény
								$ar_kedvezmeny = 0;
								if($kupon_kedv > 0)
								{
									$ar_kedvezmeny = round($ar * ($row_kupon['kedvezmeny'] / 100));
								}
								
								if($kupon_termek_id > 0) // Ha kuponból ajándék
								{
									$query_ajandek = "SELECT * FROM ".$webjel."termekek WHERE id=".$kupon_termek_id;
									$res = $pdo->prepare($query_ajandek);
									$res->execute();
									$row_ajandek = $res -> fetch();
									$tabla_tetelek.= '<tr>
												<td>'.$row_ajandek['nev'].'</td>
												<td></td>
												<td>1</td>
												<td>0 Ft</td>
												<td align="right">0 Ft</td>
											</tr>';
								}
								
								$tabla_tetelek.= '<tr>
											<td colspan="3"></td>
											<td>Összesen</td>
											<td align="right">'.number_format($ar, 0, ',', ' ').' Ft</td>
										</tr>';
										
								$tabla_tetelek.= '<tr>
											<td colspan="3"></td>
											<td align="left">Szállítási költség</td>
											<td align="right">'.number_format($szall_kolts, 0, ',', ' ').' Ft</td>
										</tr>';
								
								$fizetendo = $ar + $szall_kolts;
								$tabla_tetelek.= '<tr style="font-weight:bold;">
											<td colspan="3"></td>
											<td>Fizetendő</td>
											<td align="right">'.number_format($fizetendo, 0, '.', ' ').' Ft</td>
										</tr>';
								$tabla_tetelek.= '</table>';
							
							$tabla_tetelek.= '</td></tr>';
							$tabla_tetelek.= '<tr>
								<td colspan="4">
									<b>Szállítás:</b> '.$szall_mod.'
									<br><b>Fizetési mód:</b> '.$fiz_mod;
									if ($fiz_mod == 'Banki átutalás')
									{
										$tabla_tetelek.= '<br>Az átutaláshoz szükséges adatok a következők:
													<br>Számlaszám: '.$bankszamla.'
													<br>Közlemény: Rendelésszám '.$last_rendeles_id;
									}
									if ($_POST['szall_mod'] == 4)
									{
										$tabla_tetelek.= '<br><b>A kiválasztott Pick Pack Pont:</b> '.$_POST['ppp_shopType'].' '.$_POST['ppp_address'];
									}
									else if ($_POST['szall_mod'] == 3)
									{
										$tabla_tetelek.= '<br><b>A kiválasztott PostaPont:</b> '.$_POST['valasztott_postapont'];
									}
									else if ($_POST['szall_mod'] == 6)
									{
										$tabla_tetelek.= '<br><b>A kiválasztott GLS csomagpont:</b> '.$_POST['valasztott_gls_cspont'];
									}									
								$tabla_tetelek.= '</td>
							</tr>
						</table>';
					//E-MAIL
						//üzenet
						$mess =  '<table align="center" cellpadding="12" cellspacing="0" border="0" style="background-color: #fff;">
									<tr>
										<td>
											<a href="'.$domain.'" style="color: blue;" target="_blank"><img src="'.$domain.'/images/logo.png" width="200" title="'.$webnev.'" /></a>
										</td>
									</tr>
									<tr>
										<td style="text-align:left;">
											<p><b>Kedves '.$nev1.'!</b></p>
											<p>Az alábbi rendelésed adtad le webáruházunkban:</p>
										</td>
									</tr>
										<td>
											'.$tabla_tetelek.'
										</td>
									<tr>
										<td style="text-align:left;">
											<p>Vásárlásod ezúton is köszönjük!</p>
											<p>
												Üdvözlettel:
												<br>
												'.$webnev.'
											</p>
											<p>Ha még nem tetted, kövess minket a Facebookon is, hogy ne maradj le akcióinkról és újdonságainkról!
											<br><a href="https://www.facebook.com/MetromanHungariaKft/" target="_blank"><img src="'.$domain.'/images/facebook.png" style="width: 40px; margin-top: 10px;" alt="Facebook"></a></p>
										</td>
									</tr>
								</table>';
						//phpMailer
						require_once($gyoker.'/webshop/PHPMailer-master/PHPMailerAutoload.php');
						$mail = new PHPMailer();
						$mail->isHTML(true);
						$mail->CharSet = 'UTF-8';
						// $mail->addCustomHeader('MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n");
						if($conf_smtp == 1)
						{
							// SMTP
							$mail->IsSMTP(); // telling the class to use SMTP
							$mail->Host       = $smtp_host; // SMTP server
							$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
																	   // 1 = errors and messages
																	   // 2 = messages only
							$mail->SMTPAuth   = true;                  // enable SMTP authentication
							$mail->Host       = $smtp_host; // sets the SMTP server
							$mail->Port       = $smtp_port;                    // set the SMTP port for the GMAIL server
							$mail->Username   = $smtp_user; // SMTP account username
							$mail->Password   = $smtp_pass;        // SMTP account password
							$mail->SetFrom($smtp_email, $smtp_name);
							$mail->AddReplyTo($smtp_email, $smtp_name);
						}
						else
						{
							$mail->SetFrom($email, $webnev);
						}
						$mail->AddAddress($user_email, $nev1);
						$mail->Subject = "Vásárlás";
						$htmlmsg = '<html><body style="background-color: #d2d2d2">'.$mess.'</body></html>';
						$mail->Body = $htmlmsg;
						if(!$mail->Send()) {
						  echo "Mailer Error: " . $mail->ErrorInfo;
						}
						
					//E-mail a tulajnak
						if ($config_email_vasar_tulaj == "I")
						{
							//phpMailer
							require_once($gyoker.'/webshop/PHPMailer-master/PHPMailerAutoload.php');
							$mail = new PHPMailer();
							$mail->isHTML(true);
							$mail->CharSet = 'UTF-8';
							// $mail->addCustomHeader('MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n");
							if($conf_smtp == 1)
							{
								// SMTP
								$mail->IsSMTP(); // telling the class to use SMTP
								$mail->Host       = $smtp_host; // SMTP server
								$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
																		   // 1 = errors and messages
																		   // 2 = messages only
								$mail->SMTPAuth   = true;                  // enable SMTP authentication
								$mail->Host       = $smtp_host; // sets the SMTP server
								$mail->Port       = $smtp_port;                    // set the SMTP port for the GMAIL server
								$mail->Username   = $smtp_user; // SMTP account username
								$mail->Password   = $smtp_pass;        // SMTP account password
								$mail->SetFrom($smtp_email, $smtp_name);
								$mail->AddReplyTo($smtp_email, $smtp_name);
							}
							else
							{
								$mail->SetFrom($email, $webnev);
							}
							$mail->AddAddress($email, 'WebShop');
							$mail->Subject = "Vásárlás";
							$htmlmsg = '<html>
												<body>
													<table align="left" cellpadding="12" cellspacing="0" border="0">
														<tr>
															<td>
																<p>Tisztelt Üzemeltető</p>
															</td>
														</tr>
														<tr>
															<td>
																<p>A Webáruházban az alábbi vásárlás történt, melyet megtekinthet az <a href="'.$domain.'/adm/">adminisztrációs felületen</a>.</p>
															</td>
														</tr>
														<tr>
															<td>
																'.$tabla_tetelek.'
															</td>
														</tr>
														<tr>
															<td>
																<p>Üdvözlettel:<br><a href="'.$domain.'" style="color: blue;" target="_blank"><img src="'.$domain.'/images/logo.png" width="200" title="'.$webnev.'" /></a></p>
															</td>
														</tr>
													</table>
												</body>
											</html>';
							$mail->Body = $htmlmsg;
							if(!$mail->Send()) {
							  echo "Mailer Error: " . $mail->ErrorInfo;
							}
						}
			
						//Kosár ürítése
							$deletecommand = 'DELETE FROM '.$webjel.'kosar WHERE kosar_id='.$_SESSION['kosar_id'];
							$result = $pdo->prepare($deletecommand);
							$result->execute();
							$kosar_id_otphez = $_SESSION['kosar_id'];
							// $rendeles_id = $_SESSION['kosar_id'];
							$rendeles_id = $last_rendeles_id;
							setcookie( "kosar_id", $_SESSION['uid'], 1, "/" );
							unset($_SESSION['kosar_id']);
							unset($_SESSION['uid']);
							unset($_SESSION['noreg_id']);
							
							if($ovip_authCode != '') // OVIP-pal összekapcsolt
							{
								include '_ovip_rendeles_leadas.php';
							}
						?>
						<h3 style="text-align: center;">Rendelésedet sikeresen rögzítettük!</h3>
						<h4 style="text-align: center;">Hamarosan e-mail-ben is megküldjük a vásárlásod adatait.</h4>
						<h4 style="text-align: center;">További szép napot kívánunk!</h4>
						<p style="text-align: center;">Ha még nem tetted, kövess minket a Facebookon is, hogy ne maradj le akcióinkról és újdonságainkról!
						<br><a href="https://www.facebook.com/MetromanHungariaKft/" target="_blank"><img src="<?php print $domain; ?>/images/facebook.png" style="width: 40px; margin-top: 10px;" alt="Facebook"></a></p>
						<?php		
			
			
			if($config_ecommerce == 1)
			{
				$rendeles_id_etracking = $last_rendeles_id;
				include $gyoker.'/webshop/ecommerce_tracking.php';
			}
			
			
			// Ha OTP, akkor tovább kell küldeni
			if($row_f['bankkartya'] == 1)
			{
				include $gyoker.'/webshop/kartya_start.php';
			}

		}
		else
		{ ?>

		<h3 style="text-align: center;">A rendelés során rendszer hiba lépett fel!</h3>
		<h4 style="text-align: center;">Kérjük, keress minket elérhetőségeinken, amennyiben a hibát továbbra is tapasztalod!</h4>
		<h4 style="text-align: center;">További szép napot kívánunk!</h4>
		<a href="<?=$domain?>/kosar" class="button">Vissza a kosárhoz</a>

		<?php
		}		
	}
	// Kassza
	else if (isset($_SESSION['kosar_id']))
	{
		?>
		<div class="kassza">
			<form method="POST" action="" id="rendeles_leadas_form">
				<a id="adatok_horgony"></a>
				<div class="row mt-0">
					<div class="col-md-12">
						<div class="kassza_title" attr_tart_id="adatok_tartalom">
							1. Adatok <span class="kassza_ceruza" ><img src="<?php print $domain; ?>/webshop/images/g_kassza_mod.png"/></span>
						</div>
					</div>
					<div class="col-md-12 margtop10" id="adatok_tartalom">
						<?php
							include $gyoker.'/webshop/kassza_4_adatok.php';
						?>
					</div>
				</div>
				<a id="szall_mod_horgony"></a>
				<div class="row mt-0">
					<div class="col-md-12">
						<div class="kassza_title" attr_tart_id="szall_mod_tartalom">
							2. Szállítási és fizetési módok <span class="kassza_ceruza" ><img src="<?php print $domain; ?>/webshop/images/g_kassza_mod.png"/></span>
						</div>
					</div>
					<div class="col-md-12 margtop10" id="szall_mod_tartalom" style="display:none;">
						<?php
							include $gyoker.'/webshop/kassza_4_fiz_szall_mod.php';
						?>
					</div>
				</div>
				<a id="osszes_horgony"></a>
				<div class="row mt-0">
					<div class="col-md-12">
						<div class="kassza_title" attr_tart_id="osszesites_tartalom">
							3. Rendelés összesítése
						</div>
					</div>
					<div class="col-md-12 margtop10" id="osszesites_tartalom" style="display:none;">
						<?php
							include $gyoker.'/webshop/kassza_4_osszesites.php';
						?>
					</div>
				</div>
				<input type="hidden" name="command" value="rendeles_leadas"/>
			</form>
		</div>
		<?php
	}
	else
	{
		print '<p>A kosár tartalma üres.</p>';
	}
?>
