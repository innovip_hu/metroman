<?php
	//$rendeles_id = 49; // ezt a változót meg kell adni az include előtt

	$query = "SELECT ovip_id FROM ".$webjel."rendeles WHERE id=?";
	$res = $pdo->prepare($query);
	$res->execute(array($rendeles_id));
	$row_rendeles = $res -> fetch();

	if (isset($row_rendeles['ovip_id']) && $row_rendeles['ovip_id'] > 0) {

		$request = 'paymentStatusUpdate';

		$signature = hash('sha256', trim($ovip_ugyfel_id . $ovip_webshop_id . $ovip_authCode . $request . $ovip_ip_cim)); //hash generálása 

		$options = array(
		  	'location' => $ovip_soap_link,
		 	'uri' => $ovip_soap_link,
		  	'encoding' => 'UTF-8',
		  	'trace' => 1
		);

		$request = array(
			'extra_data' => $row_rendeles['ovip_id'],
			'request' => $request,
			'user_id' => $ovip_ugyfel_id,
			'signature' => $signature,
			'webshop_id' => $ovip_webshop_id
		);

		try {

			$client = new SoapClient(NULL,$options);
			
			$client->getRequest($request);

		} catch (Exception $e) {

			$hiba = $e->getMessage();
		}	
	}