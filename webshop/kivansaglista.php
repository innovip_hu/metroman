<?php 
	if (isset($_POST['command']) && $_POST['command'] == 'VASARLAS')
	{	
		$kod = time().rand(1, 9999);
		$term_id = $_POST['term_id'];
		$term_db = $_POST['darab'] > 0 ? $_POST['darab'] : 1;
		$datum = date('Y-m-d');
		$term_ar = $_POST['term_ar'];
		$term_akcios_ar = $_POST['term_akcios_ar'];
		$ido = date('H:i:s');
		$term_nev = $_POST['term_nev'];
		$term_kep = $_POST['term_kep'];
		$term_afa = $_POST['term_afa'];
		if (isset($_SESSION['kosar_id'])) //ha nem üres a kosár
		{
			if (isset($_SESSION['login_id']))
			{
				$user_kosar = $_SESSION['login_id'];
			}
			else
			{
				$user_kosar = 0;
			}			
			$kosar_id = $_SESSION['kosar_id'];
			$uid = $_SESSION['uid'];
			$query = "INSERT INTO ".$webjel."kosar (term_id,term_db,datum,term_ar,term_akcios_ar,ido,kosar_id,term_nev,term_kep,term_afa,uid,user_id) VALUES (:term_id,:term_db,:datum,:term_ar,:term_akcios_ar,:ido,:kosar_id,:term_nev,:term_kep,:term_afa,:uid,:user_id)";
			$result = $pdo->prepare($query);
			$result->execute(array(':term_id'=>$term_id,
								':term_db'=>$term_db,
								':datum'=>$datum,
								':term_ar'=>$term_ar,
								':term_akcios_ar'=>$term_akcios_ar,
								':ido'=>$ido,
								':kosar_id'=>$kosar_id,
								':term_nev'=>$term_nev,
								':term_kep'=>$term_kep,
								':term_afa'=>$term_afa,
								':uid'=>$uid,
								':user_id'=>$user_kosar));
			$kosar_tetel_id = $pdo->lastInsertId();
			setcookie( "kosar_id", $uid, strtotime( '+7 days' ), "/" );
		}
		else //ha üres a kosár
		{
			if (isset($_SESSION['login_id']))
			{
				$user_kosar = $_SESSION['login_id'];
			}
			else
			{
				$user_kosar = 0;
			}
			$query = "INSERT INTO ".$webjel."kosar (term_id,term_db,datum,term_ar,term_akcios_ar,ido,kod,term_nev,term_kep,term_afa,user_id) VALUES (:term_id,:term_db,:datum,:term_ar,:term_akcios_ar,:ido,:kod,:term_nev,:term_kep,:term_afa,:user_id)";
			$result = $pdo->prepare($query);
			$result->execute(array(':term_id'=>$term_id,
								':term_db'=>$term_db,
								':datum'=>$datum,
								':term_ar'=>$term_ar,
								':term_akcios_ar'=>$term_akcios_ar,
								':ido'=>$ido,
								':kod'=>$kod,
								':term_nev'=>$term_nev,
								':term_kep'=>$term_kep,
								':term_afa'=>$term_afa,
								':user_id'=>$user_kosar));
			$kosar_tetel_id = $pdo->lastInsertId();
									
			$kosar_id = $kosar_tetel_id;

			$uid = md5(microtime(true).mt_Rand().$kosar_id);
			
			$query = "UPDATE ".$webjel."kosar SET kosar_id=?, uid = ? WHERE id=?";
			$result = $pdo->prepare($query);
			$result->execute(array($kosar_id,$uid,$kosar_id));
									
			$_SESSION['kosar_id'] = $kosar_id;
			$_SESSION['uid'] = $uid;
			setcookie( "kosar_id", $uid, strtotime( '+7 days' ), "/" );
		}
		if (isset($_POST['termek_parameter'])) {
			foreach ($_POST['termek_parameter'] as $termek_parameter_id => $termek_parameter_ertek_id) {
				$insert = $pdo->prepare("INSERT INTO ".$webjel."kosar_tetel_termek_parameter_ertek ("
						. "kosar_tetel_id, "
						. "termek_parameter_id, "
						. "termek_parameter_nev, "
						. "termek_parameter_ertek_id, "
						. "termek_parameter_ertek_nev"
						. ") values ("
						. "".$kosar_tetel_id.", "
						. "".$termek_parameter_id.", "
						. "'".$pdo->query("SELECT nev FROM ".$webjel."termek_uj_parameterek WHERE id=".$termek_parameter_id)->fetchColumn()."', "
						. "".$termek_parameter_ertek_id.", "
						. "'".$pdo->query("SELECT ertek 
									FROM ".$webjel."termek_uj_parameter_ertekek 
									WHERE id=".$termek_parameter_ertek_id)->fetchColumn()."'"
						. ")");
				$insert->execute();
			}
		}
		
		// Ha kívánságlistából jött, akkor törölni kell onnan
		if (isset($_POST['command2']) && $_POST['command2'] == 'kivansaglistabol')
		{
			$deletecommand = "DELETE FROM ".$webjel."kivansag_lista WHERE termek_id =".$_POST['term_id']." AND user_id=".$_SESSION['login_id'];
			$result = $pdo->prepare($deletecommand);
			$result->execute();
		}
		// Kupon törlése
		$updatecommand = "UPDATE ".$webjel."kosar SET kupon_id=0 WHERE kosar_id=".$_SESSION['kosar_id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
	}
?>

	<div class="table-responsive margtop20" id="kosar_elemei">
		<table class="table table-bordered">
			<thead>
				<tr>
					<td class="text-center">Kép</td>
					<td class="text-left">Termék neve</td>
					<td class="text-right">Termék ára</td>
					<td></td>
					<td></td>
				</tr>
			</thead>
			<tbody>
		<?php
			// $query = "SELECT * FROM ".$webjel."kivansag_lista WHERE user_id=".$_SESSION['login_id'];
			$query = "SELECT ".$webjel."kivansag_lista.id as kivlist_id, 
							".$webjel."kivansag_lista.user_id, 
							".$webjel."kivansag_lista.termek_id, 
							".$webjel."termekek.csop_id, 
							".$webjel."termekek.id, 
							".$webjel."termekek.nev_url, 
							".$webjel."termekek.kep, 
							".$webjel."termekek.ar, 
							".$webjel."termekek.akciosar, 
							".$webjel."termekek.akcio_tol, 
							".$webjel."termekek.akcio_ig, 
							".$webjel."termekek.nev,
							".$webjel."afa.afa as term_afa
				FROM ".$webjel."kivansag_lista 
				INNER JOIN ".$webjel."termekek 
				ON ".$webjel."kivansag_lista.termek_id=".$webjel."termekek.id 
				INNER JOIN ".$webjel."afa 
				ON ".$webjel."termekek.afa=".$webjel."afa.id 				
				WHERE ".$webjel."kivansag_lista.user_id=".$_SESSION['login_id']." 
				ORDER BY ".$webjel."termekek.nev ASC";
			$datum = date('Y-m-d');
			foreach ($pdo->query($query) as $row)
			{
				$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row['csop_id'];
				$res_csop = $pdo->prepare($query_csop);
				$res_csop->execute();
				$row_csop  = $res_csop -> fetch();

				$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
				$res = $pdo->prepare($query_kep);
				$res->execute();
				$row_kep = $res -> fetch();				

				if ($row_kep['kep'] == "")
				{
					$kep = $domain.'/webshop/images/noimage.png';
				}
				else
				{
					$kep = $domain.'/images/termekek/'.$row_kep['kep'];
				}
				
				print '<tr>
					<td class="text-center align-middle">
						<div class="image"><a href="'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row['nev_url'].'"><img style="width: 80px;" src="'.$kep.'" alt="'.$row['nev'].'" title="'.$row['nev'].'" class="img-thumbnail"></a></div>
					</td>
					<td class="text-left align-middle"><a href="'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row['nev_url'].'">'.$row['nev'].'</a></td>
					<td class="text-right align-middle">
						<div class="price-section">';
						if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) //ha akciós
						{
							print '<div class="product-price" style="color: #A20000;">'.number_format($row['akciosar'], 0, ',', ' ').' Ft</div> <div class="text-strike small">'.number_format($row['ar'], 0, ',', ' ').' Ft</div>';
							$term_akcios_ar = $row['akciosar'];
						}
						else //ha nem akciós
						{
							print '<div class="product-price">'.number_format($row['ar'], 0, ',', ' ').' Ft</div>';
							$term_akcios_ar = 0;
						}
					print '</div></td>
					<td style="width:42px;" class="align-middle">
						<form id="kosarbaForm_'.$row['kivlist_id'].'" action="?command=kivansaglista" method="post">';
							?>
							<button class="button button-default button-xs" type="submit">Kosárba</button>
							<?php
							print '<input type="hidden" name="term_id" value="'.$row['id'].'"/>
							<input type="hidden" name="darab" value="1"/>
							<input type="hidden" name="term_nev" value="'.$row['nev'].'"/>
							<input type="hidden" name="term_kep" value="'.$row_kep['kep'].'"/>
							<input type="hidden" name="term_ar" value="'.$row['ar'].'"/>
							<input type="hidden" name="term_afa" value="'.$row['term_afa'].'"/>
							<input type="hidden" name="term_akcios_ar" value="'.$term_akcios_ar.'"/>
							<input type="hidden" name="command" value="VASARLAS"/>
							<input type="hidden" name="command2" value="kivansaglistabol"/>
						</form>
					</td>
					<td style="width:42px;" class="align-middle">
						<form id="kivLivtTorlesForm_'.$row['kivlist_id'].'" action="?command=kivansaglista" method="post">';
							?>
							<a onClick="document.getElementById('kivLivtTorlesForm_<?php print $row['kivlist_id']; ?>').submit();" class="btn gomb_normal" data-yuspremovefromfavorites="<?=$row['id']?>"/><span class="fa fa-trash"></span></a>
							<?php
							print '<input type="hidden" name="id" value="'.$row['kivlist_id'].'"/>
							<input type="hidden" name="command" value="kivlistTORLES"/>
						</form>
					</td>
				</tr>';
			}
		?>
		</tbody></table>
	</div>