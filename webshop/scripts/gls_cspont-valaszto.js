// Városok generálása
	function varosok_gls() {
		let automata = $('#szall_mod_select option:selected').attr('attr_automata');

		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("gls_varos_div").innerHTML=xmlhttp.responseText;
				$('#gls_varos').select2();
			}
		  }
		xmlhttp.open("POST","../webshop/module/glscsomagpont_varosok.php?script=ok&automata="+automata,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("megye="+encodeURIComponent(document.getElementById("gls_megye").value));
	}
// Pontok keresése
	function glsKeres() {
		let automata = $('#szall_mod_select option:selected').attr('attr_automata');

		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("gls_talalat_div").innerHTML=xmlhttp.responseText;
				$('#gls_varos').select2();
			}
		  }
		xmlhttp.open("POST","../webshop/module/glscsomagpont_talalatok.php?script=ok&automata="+automata,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("varos="+encodeURIComponent(document.getElementById("gls_varos").value));
	}
// Pont kiválasztása
	function glsKivalaszt(id) {
		var azonosito = $("#gls_adat_"+id).data('id');
		document.getElementById("valasztott_gls_cspont").value = document.getElementById("gls_adat_"+id).value;
		document.getElementById("valasztott_gls_cspont_azonosito").value = azonosito;

		document.getElementById("csomagpont_nev").value = $("#gls_adat_"+id).data('nev');
		document.getElementById("csomagpont_varos").value = $("#gls_adat_"+id).data('varos');
		document.getElementById("csomagpont_irsz").value = $("#gls_adat_"+id).data('irsz');
		document.getElementById("csomagpont_cim").value = $("#gls_adat_"+id).data('cim');		

		// Bele kell írni címbe.

		$( "#szallcim1_span" ).html($("#gls_adat_"+id).data('irsz') + " " + $("#gls_adat_"+id).data('varos'));
		$( "#szallcim2_span" ).html($("#gls_adat_"+id).data('cim'));
		$( "#szallcim1_span2" ).html($("#gls_adat_"+id).data('irsz') + " " + $("#gls_adat_"+id).data('varos'));
		$( "#szallcim2_span2" ).html($("#gls_adat_"+id).data('cim'));		
		$( "#nev2_span2" ).html($("#gls_adat_"+id).data('nev'));
		$( "#nev2_span" ).html($("#gls_adat_"+id).data('nev'));		
	}
// Kereső megnyitása
	function glsKeresoNyitas() {
		$( "#gls_valaszto" ).slideDown( 300, function() {

		});
	}
// Kereső zárása
	function glsKeresoZaras() {
		$( "#gls_valaszto" ).slideUp( 300, function() { });
		document.getElementById("valasztott_gls_cspont").value = '';
		document.getElementById("valasztott_gls_cspont_azonosito").value = '';
		document.getElementById("gls_varos").value = 0;
		$('#gls_megye').val("").trigger("change");
		document.getElementById("gls_talalat_div").innerHTML='';
		document.getElementById("gls_riaszt").innerHTML='';

		if ($( "#cim_szall_nev" ).val() != '') {
			$( "#szallcim1_span" ).html($("#cim_szall_irszam").val() + " " + $("#cim_szall_varos").val());
			$( "#szallcim2_span" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
			$( "#szallcim1_span2" ).html($("#cim_szall_irszam").val() + " " + $("#cim_szall_varos").val());		
			$( "#szallcim2_span2" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());		
			$( "#nev2_span2" ).html($( "#cim_szall_nev" ).val());
			$( "#nev2_span" ).html($( "#cim_szall_nev" ).val());				
		} else {
			$( "#szallcim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
			$( "#szallcim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
			$( "#szallcim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());		
			$( "#szallcim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());		
			$( "#nev2_span2" ).html($( "#szla_nev" ).val());
			$( "#nev2_span" ).html($( "#szla_nev" ).val());				
		}

	}
// Ellenőrzés a gls_cspont miatt
	function gls_elenorzes() {
		let automata = $('#szall_mod_select option:selected').attr('attr_automata');
		let szoveg = "CsomagPontot";

		if (automata == 1) {
			szoveg = "Automatát";
		} 

		if(document.getElementById("gls_cspont").checked == true && document.getElementById("valasztott_gls_cspont").value == '') {
			document.getElementById("gls_riaszt").innerHTML='<div class="alert alert-danger alert-dismissable">Válassz GLS "'+szoveg+'"!</p></div>';
			return false;
		}
		else
		{
			return true;
		}
	}
