/* 1. Noreg gomb */
	$(document).on("click", "#vasarlas_reg_nelkul_tovabb", function() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("adatok_tartalom").innerHTML = xmlhttp.responseText;
				$( "#bejelntkezes_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
			}
		  }
		xmlhttp.open("GET","../webshop/kassza_uj_noreg_reg.php?script=ok",true);
		xmlhttp.send();
	});
/* 1. Regisztráció gomb */
	$(document).on("click", "#regisztracio_tovabb", function() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("adatok_tartalom").innerHTML = xmlhttp.responseText;
				$( "#bejelntkezes_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
			}
		  }
		xmlhttp.open("GET","../webshop/kassza_uj_regisztracio.php?script=ok",true);
		xmlhttp.send();
	});
/* 1. Bejelentkezés ceruza */
	$(document).on("click", "#bejelntkezes_ceruza", function() {
		$( "#bejelntkezes_tartalom" ).slideDown( 300, function() { });
		$( "#adatok_tartalom" ).slideUp( 300, function() { });
		$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
		$( "#rendeles_tartalom" ).slideUp( 300, function() { });
		$( "#fizetes_tartalom" ).slideUp( 300, function() { });
	});
/* 1. Belépés gomb */
	$(document).on("click", "#login_gomb", function() {
		document.getElementById("email_riaszt").innerHTML = '';
		document.getElementById("passwordText_riaszt").innerHTML = '';
		if(document.getElementById("email").value == '')
		{
			document.getElementById("email").focus();
			document.getElementById("email_riaszt").innerHTML = 'Add meg az e-mail címed!';
		}
		else if(document.getElementById("passwordText").value == '')
		{
			document.getElementById("passwordText").focus();
			document.getElementById("passwordText_riaszt").innerHTML = 'Add meg a jelszavad!';
		}
		else
		{
			// felhasználó egyezőség vizsgálata
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var vane = parseFloat(xmlhttp.responseText); // Át kell alakítani számmá a kapott értéket
					if(vane != 1) // van már ilyen felhasználónév
					{
						document.getElementById("email").focus();
						document.getElementById("email_riaszt").innerHTML = 'Az e-mail/jelszó<br>kombináció nem megfelelő!';
					}
					else
					{
						if (window.XMLHttpRequest)
						  {// code for IE7+, Firefox, Chrome, Opera, Safari
						  xmlhttp=new XMLHttpRequest();
						  }
						else
						  {// code for IE6, IE5
						  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
						  }
						xmlhttp.onreadystatechange=function()
						  {
						  if (xmlhttp.readyState==4 && xmlhttp.status==200)
							{
								document.getElementById("adatok_tartalom").innerHTML = xmlhttp.responseText;
								document.getElementById("bejelntkezes_ceruza").style.display = 'none';
								document.getElementById("adatok_ceruza").style.display = 'block';
								// Nyitás/csukás
								$( "#bejelntkezes_tartalom" ).slideUp( 300, function() { });
								$( "#adatok_tartalom" ).slideDown( 300, function() { });
							}
						  }
						xmlhttp.open("POST","../webshop/kassza_uj_reg_adatok.php?script=ok",true);
						xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
						xmlhttp.send("command=belepes"+"&email="+encodeURIComponent(document.getElementById("email").value)+"&passwordText="+encodeURIComponent(document.getElementById("passwordText").value));
					}
				}
			  }
			xmlhttp.open("POST","../webshop/kassza_uj_belepes_ell.php?script=ok",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("command=belepes"+"&email="+encodeURIComponent(document.getElementById("email").value)+"&passwordText="+encodeURIComponent(document.getElementById("passwordText").value));
		}
	});
	
/* 2. Bejelentkezés gomb */
	$(document).on("click", "#adatok_tovabb", function() {
		$( "#adatok_tartalom" ).slideUp( 300, function() { });
		$( "#szall_mod_tartalom" ).slideDown( 300, function() { });
		 document.getElementById("szall_mod_ceruza").style.display = 'block';
	});
/* 2. Bejelentkezés ceruza */
	$(document).on("click", "#adatok_ceruza", function() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("adatok_tartalom").innerHTML = xmlhttp.responseText;
				$( "#bejelntkezes_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#rendeles_tartalom" ).slideUp( 300, function() { });
				$( "#fizetes_tartalom" ).slideUp( 300, function() { });
			}
		  }
		xmlhttp.open("GET","../webshop/kassza_uj_adatok.php?script=ok",true);
		xmlhttp.send();
	});
/* 2. Eltérő cím nyítás/csukás */
	$(document).on("click", "#mascim", function() {
		if(document.getElementById("mascim").checked == true )
		{
			$( "#eltero_cim" ).slideDown( 300, function() { });
		}
		else
		{
			$( "#eltero_cim" ).slideUp( 300, function() { });
			document.getElementById("cim_szall_nev").value = '';
			document.getElementById("cim_szall_varos").value = '';
			document.getElementById("cim_szall_utca").value = '';
			document.getElementById("cim_szall_hszam").value = '';
			document.getElementById("cim_szall_irszam").value = '';
		}
	});
/* 2. Regisztráció */
	$(document).on("click", "#regisztracio_gomb", function() {
		document.getElementById("jszo1_riaszt").innerHTML = '';
		document.getElementById("jszo2_riaszt").innerHTML = '';
		document.getElementById("nev_riaszt").innerHTML = '';
		document.getElementById("email_reg_riaszt").innerHTML = '';
		document.getElementById("telefon_riaszt").innerHTML = '';
		document.getElementById("cim_varos_riaszt").innerHTML = '';
		document.getElementById("cim_utca_riaszt").innerHTML = '';
		document.getElementById("cim_hszam_riaszt").innerHTML = '';
		document.getElementById("cim_irszam_riaszt").innerHTML = '';
		document.getElementById("feltetel_riaszt").innerHTML = '';
		if(document.getElementById("email_reg").value == '')
		{
			document.getElementById("email_reg").focus();
			document.getElementById("email_reg_riaszt").innerHTML = 'Add meg az email címed!';
		}
		else if(document.getElementById("jszo1").value == '')
		{
			document.getElementById("jszo1").focus();
			document.getElementById("jszo1_riaszt").innerHTML = 'Add meg a jelszavad!';
		}
		else if(document.getElementById("jszo2").value == '')
		{
			document.getElementById("jszo2").focus();
			document.getElementById("jszo2_riaszt").innerHTML = 'Add meg a jelszavad újra!';
		}
		else if(document.getElementById("jszo2").value != document.getElementById("jszo1").value)
		{
			document.getElementById("jszo1").focus();
			document.getElementById("jszo1_riaszt").innerHTML = 'A két jelszó nem egyezik!';
		}
		else if(document.getElementById("nev").value == '')
		{
			document.getElementById("nev").focus();
			document.getElementById("nev_riaszt").innerHTML = 'Add meg a nevedet!';
		}
		else if(document.getElementById("telefon").value == '')
		{
			document.getElementById("telefon").focus();
			document.getElementById("telefon_riaszt").innerHTML = 'Add meg a telefonszámod!';
		}
		else if(document.getElementById("cim_varos").value == '')
		{
			document.getElementById("cim_varos").focus();
			document.getElementById("cim_varos_riaszt").innerHTML = 'Add meg a címedet!';
		}
		else if(document.getElementById("cim_utca").value == '')
		{
			document.getElementById("cim_utca").focus();
			document.getElementById("cim_utca_riaszt").innerHTML = 'Add meg a címedet!';
		}
		else if(document.getElementById("cim_hszam").value == '')
		{
			document.getElementById("cim_hszam").focus();
			document.getElementById("cim_hszam_riaszt").innerHTML = 'Add meg a címedet!';
		}
		else if(document.getElementById("cim_irszam").value == '')
		{
			document.getElementById("cim_irszam").focus();
			document.getElementById("cim_irszam_riaszt").innerHTML = 'Add meg a címedet!';
		}
		else if(document.getElementById("feltetel").checked == false)
		{
			document.getElementById("feltetel").focus();
			document.getElementById("feltetel_riaszt").innerHTML = 'A regisztrációs feltétel nincs elfogadva!';
		}
		else
		{
			document.getElementById("jszo1_riaszt").innerHTML = '';
			document.getElementById("jszo2_riaszt").innerHTML = '';
			document.getElementById("nev_riaszt").innerHTML = '';
			document.getElementById("email_reg_riaszt").innerHTML = '';
			document.getElementById("telefon_riaszt").innerHTML = '';
			document.getElementById("cim_varos_riaszt").innerHTML = '';
			document.getElementById("cim_utca_riaszt").innerHTML = '';
			document.getElementById("cim_hszam_riaszt").innerHTML = '';
			document.getElementById("cim_irszam_riaszt").innerHTML = '';
			// felhasználó egyezőség vizsgálata
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var vane = parseFloat(xmlhttp.responseText); // Át kell alakítani számmá a kapott értéket
					if(vane > 0) // van már ilyen felhasználónév
					{
						document.getElementById("email_reg").focus();
						document.getElementById("email_reg_riaszt").innerHTML = 'Az email cím foglalt!';
					}
					else
					{
						if (window.XMLHttpRequest)
						  {// code for IE7+, Firefox, Chrome, Opera, Safari
						  xmlhttp=new XMLHttpRequest();
						  }
						else
						  {// code for IE6, IE5
						  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
						  }
						xmlhttp.onreadystatechange=function()
						  {
						  if (xmlhttp.readyState==4 && xmlhttp.status==200)
							{
								document.getElementById("szall_mod_tartalom").innerHTML = xmlhttp.responseText;
								document.getElementById("bejelntkezes_ceruza").style.display = 'none';
								document.getElementById("adatok_ceruza").style.display = 'block';
								document.getElementById("szall_mod_ceruza").style.display = 'block';
								// Nyitás/csukás
								$( "#adatok_tartalom" ).slideUp( 300, function() { });
								$( "#szall_mod_tartalom" ).slideDown( 300, function() { });
							}
						  }
						xmlhttp.open("POST","../webshop/kassza_uj_szall_mod.php?script=ok",true);
						xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
						xmlhttp.send("command=regisztracio"
							+"&jszo1="+encodeURIComponent(document.getElementById("jszo1").value)
							+"&nev="+encodeURIComponent(document.getElementById("nev").value)
							+"&email="+encodeURIComponent(document.getElementById("email_reg").value)
							+"&telefon="+encodeURIComponent(document.getElementById("telefon").value)
							+"&cim_varos="+encodeURIComponent(document.getElementById("cim_varos").value)
							+"&cim_utca="+encodeURIComponent(document.getElementById("cim_utca").value)
							+"&cim_hszam="+encodeURIComponent(document.getElementById("cim_hszam").value)
							+"&cim_irszam="+encodeURIComponent(document.getElementById("cim_irszam").value)
							+"&cim_szall_nev="+encodeURIComponent(document.getElementById("cim_szall_nev").value)
							+"&cim_szall_varos="+encodeURIComponent(document.getElementById("cim_szall_varos").value)
							+"&cim_szall_utca="+encodeURIComponent(document.getElementById("cim_szall_utca").value)
							+"&cim_szall_hszam="+encodeURIComponent(document.getElementById("cim_szall_hszam").value)
							+"&cim_szall_irszam="+encodeURIComponent(document.getElementById("cim_szall_irszam").value));
					}
				}
			  }
			xmlhttp.open("POST","../webshop/kassza_uj_reg_ell.php?script=ok",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("command=belepes"+"&email="+encodeURIComponent(document.getElementById("email_reg").value));
		}
	});
/* 2. Noreg regisztráció */
	$(document).on("click", "#noreg_regisztracio_gomb", function() {
		document.getElementById("nev_riaszt").innerHTML = '';
		document.getElementById("email_riaszt").innerHTML = '';
		document.getElementById("telefon_riaszt").innerHTML = '';
		document.getElementById("cim_varos_riaszt").innerHTML = '';
		document.getElementById("cim_utca_riaszt").innerHTML = '';
		document.getElementById("cim_hszam_riaszt").innerHTML = '';
		document.getElementById("cim_irszam_riaszt").innerHTML = '';
		if(document.getElementById("nev").value == '')
		{
			document.getElementById("nev").focus();
			document.getElementById("nev_riaszt").innerHTML = 'Add meg a nevedet!';
		}
		else if(document.getElementById("email").value == '')
		{
			document.getElementById("email").focus();
			document.getElementById("email_riaszt").innerHTML = 'Add meg az email címed!';
		}
		else if(document.getElementById("telefon").value == '')
		{
			document.getElementById("telefon").focus();
			document.getElementById("telefon_riaszt").innerHTML = 'Add meg a telefonszámod!';
		}
		else if(document.getElementById("cim_varos").value == '')
		{
			document.getElementById("cim_varos").focus();
			document.getElementById("cim_varos_riaszt").innerHTML = 'Add meg a címedet!';
		}
		else if(document.getElementById("cim_utca").value == '')
		{
			document.getElementById("cim_utca").focus();
			document.getElementById("cim_utca_riaszt").innerHTML = 'Add meg a címedet!';
		}
		else if(document.getElementById("cim_hszam").value == '')
		{
			document.getElementById("cim_hszam").focus();
			document.getElementById("cim_hszam_riaszt").innerHTML = 'Add meg a címedet!';
		}
		else if(document.getElementById("cim_irszam").value == '')
		{
			document.getElementById("cim_irszam").focus();
			document.getElementById("cim_irszam_riaszt").innerHTML = 'Add meg a címedet!';
		}
		else
		{
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("szall_mod_tartalom").innerHTML = xmlhttp.responseText;
					document.getElementById("bejelntkezes_ceruza").style.display = 'none';
					document.getElementById("adatok_ceruza").style.display = 'block';
					document.getElementById("szall_mod_ceruza").style.display = 'block';
					// Nyitás/csukás
					$( "#adatok_tartalom" ).slideUp( 300, function() { });
					$( "#szall_mod_tartalom" ).slideDown( 300, function() { });
				}
			  }
			xmlhttp.open("POST","../webshop/kassza_uj_szall_mod.php?script=ok",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("command=noreg_regisztracio"
				+"&nev="+encodeURIComponent(document.getElementById("nev").value)
				+"&email="+encodeURIComponent(document.getElementById("email").value)
				+"&telefon="+encodeURIComponent(document.getElementById("telefon").value)
				+"&cim_varos="+encodeURIComponent(document.getElementById("cim_varos").value)
				+"&cim_utca="+encodeURIComponent(document.getElementById("cim_utca").value)
				+"&cim_hszam="+encodeURIComponent(document.getElementById("cim_hszam").value)
				+"&cim_irszam="+encodeURIComponent(document.getElementById("cim_irszam").value)
				+"&cim_szall_nev="+encodeURIComponent(document.getElementById("cim_szall_nev").value)
				+"&cim_szall_varos="+encodeURIComponent(document.getElementById("cim_szall_varos").value)
				+"&cim_szall_utca="+encodeURIComponent(document.getElementById("cim_szall_utca").value)
				+"&cim_szall_hszam="+encodeURIComponent(document.getElementById("cim_szall_hszam").value)
				+"&cim_szall_irszam="+encodeURIComponent(document.getElementById("cim_szall_irszam").value));
		}
	});
/* 2. Noreg mentés */
	$(document).on("click", "#noreg_mentes_gomb", function() {
		document.getElementById("nev_riaszt").innerHTML = '';
		document.getElementById("email_riaszt").innerHTML = '';
		document.getElementById("telefon_riaszt").innerHTML = '';
		document.getElementById("cim_varos_riaszt").innerHTML = '';
		document.getElementById("cim_utca_riaszt").innerHTML = '';
		document.getElementById("cim_hszam_riaszt").innerHTML = '';
		document.getElementById("cim_irszam_riaszt").innerHTML = '';
		if(document.getElementById("nev").value == '')
		{
			document.getElementById("nev").focus();
			document.getElementById("nev_riaszt").innerHTML = 'Add meg a nevedet!';
		}
		else if(document.getElementById("email").value == '')
		{
			document.getElementById("email").focus();
			document.getElementById("email_riaszt").innerHTML = 'Add meg az email címed!';
		}
		else if(document.getElementById("telefon").value == '')
		{
			document.getElementById("telefon").focus();
			document.getElementById("telefon_riaszt").innerHTML = 'Add meg a telefonszámod!';
		}
		else if(document.getElementById("cim_varos").value == '')
		{
			document.getElementById("cim_varos").focus();
			document.getElementById("cim_varos_riaszt").innerHTML = 'Add meg a címedet!';
		}
		else if(document.getElementById("cim_utca").value == '')
		{
			document.getElementById("cim_utca").focus();
			document.getElementById("cim_utca_riaszt").innerHTML = 'Add meg a címedet!';
		}
		else if(document.getElementById("cim_hszam").value == '')
		{
			document.getElementById("cim_hszam").focus();
			document.getElementById("cim_hszam_riaszt").innerHTML = 'Add meg a címedet!';
		}
		else if(document.getElementById("cim_irszam").value == '')
		{
			document.getElementById("cim_irszam").focus();
			document.getElementById("cim_irszam_riaszt").innerHTML = 'Add meg a címedet!';
		}
		else
		{
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("szall_mod_tartalom").innerHTML = xmlhttp.responseText;
					document.getElementById("bejelntkezes_ceruza").style.display = 'none';
					document.getElementById("adatok_ceruza").style.display = 'block';
					document.getElementById("szall_mod_ceruza").style.display = 'block';
					// Nyitás/csukás
					$( "#adatok_tartalom" ).slideUp( 300, function() { });
					$( "#szall_mod_tartalom" ).slideDown( 300, function() { });
				}
			  }
			xmlhttp.open("POST","../webshop/kassza_uj_szall_mod.php?script=ok",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("command=noreg_mentes"
				+"&nev="+encodeURIComponent(document.getElementById("nev").value)
				+"&email="+encodeURIComponent(document.getElementById("email").value)
				+"&telefon="+encodeURIComponent(document.getElementById("telefon").value)
				+"&cim_varos="+encodeURIComponent(document.getElementById("cim_varos").value)
				+"&cim_utca="+encodeURIComponent(document.getElementById("cim_utca").value)
				+"&cim_hszam="+encodeURIComponent(document.getElementById("cim_hszam").value)
				+"&cim_irszam="+encodeURIComponent(document.getElementById("cim_irszam").value)
				+"&cim_szall_nev="+encodeURIComponent(document.getElementById("cim_szall_nev").value)
				+"&cim_szall_varos="+encodeURIComponent(document.getElementById("cim_szall_varos").value)
				+"&cim_szall_utca="+encodeURIComponent(document.getElementById("cim_szall_utca").value)
				+"&cim_szall_hszam="+encodeURIComponent(document.getElementById("cim_szall_hszam").value)
				+"&cim_szall_irszam="+encodeURIComponent(document.getElementById("cim_szall_irszam").value));
		}
	});
	
/* 3. Szállítás gomb */
	$(document).on("click", "#szallitas_tovabb", function() {
		var szall_mod;
		$('[name="szall_mod"]').each(function () {
			if ($(this).is(':checked')) {
				szall_mod = this.value;
			}
		});
		
		if(document.getElementById("postapont").checked == true && document.getElementById("valasztott_postapont").value == '')
		{
			document.getElementById("pp_riaszt").innerHTML='<div class="alert alert-danger alert-dismissable">Válassz Posta Pontot!</p></div>';
		}
		else if(document.getElementById("gls_cspont").checked == true && document.getElementById("valasztott_gls_cspont").value == '')
		{
			document.getElementById("gls_riaszt").innerHTML='<div class="alert alert-danger alert-dismissable">Válassz GLS CsomagPontot!</p></div>';
		}
		else
		{
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("fizetes_tartalom").innerHTML = xmlhttp.responseText;
					$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
					$( "#fizetes_tartalom" ).slideDown( 300, function() {
						/* $('html, body').animate({
								scrollTop: $("#rendeles_fejlec").offset().top
							}, 300); */
					});
				}
			  }
			xmlhttp.open("POST","../webshop/kassza_uj_fiz_mod.php?script=ok",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("szall_mod="+szall_mod+"&valasztott_postapont="+encodeURIComponent(document.getElementById("valasztott_postapont").value)+"&valasztott_gls_cspont="+encodeURIComponent(document.getElementById("valasztott_gls_cspont").value));
		}
	});
	
/* 4. Fizetés gomb */
	$(document).on("click", "#fizetes_tovabb", function() {
		
		var fiz_mod;
		$('[name="fiz_mod"]').each(function () {
			if ($(this).is(':checked')) {
				fiz_mod = this.value;
			}
		});
		var szall_mod;
		$('[name="szall_mod"]').each(function () {
			if ($(this).is(':checked')) {
				szall_mod = this.value;
			}
		});
		
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("rendeles_tartalom").innerHTML = xmlhttp.responseText;
				$( "#fizetes_tartalom" ).slideUp( 300, function() { });
				$( "#rendeles_tartalom" ).slideDown( 300, function() {
					/* $('html, body').animate({
							scrollTop: $("#rendeles_fejlec").offset().top
						}, 300); */
				});
			}
		  }
		xmlhttp.open("POST","../webshop/kassza_uj_osszesites.php?script=ok",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("fiz_mod="+fiz_mod+"&szall_mod="+szall_mod+"&valasztott_postapont="+encodeURIComponent(document.getElementById("valasztott_postapont").value)+"&valasztott_gls_cspont="+encodeURIComponent(document.getElementById("valasztott_gls_cspont").value));
	});
/* 3. Bejelentkezés ceruza */
	$(document).on("click", "#szall_mod_ceruza", function() {
		$( "#bejelntkezes_tartalom" ).slideUp( 300, function() { });
		$( "#adatok_tartalom" ).slideUp( 300, function() { });
		$( "#szall_mod_tartalom" ).slideDown( 300, function() { });
		$( "#rendeles_tartalom" ).slideUp( 300, function() { });
		$( "#fizetes_tartalom" ).slideUp( 300, function() { });
	});
	
/* 4. Rendelés összesítése */
	$(document).on("click", "#rendeles_leadasa", function() {
		if(document.getElementById("vas_fel").checked == true )
		{
			// preloader
			document.getElementById("rendeles_gomb_div").innerHTML = '<img src="../webshop/images/preloader.GIF" style="width:46px;" />';
			
			var fiz_mod;
			$('[name="fiz_mod"]').each(function () {
				if ($(this).is(':checked')) {
					fiz_mod = this.value;
				}
			});
			
			var szall_mod;
			$('[name="szall_mod"]').each(function () {
				if ($(this).is(':checked')) {
					szall_mod = this.value;
				}
			});
			
			var postapont = '';
			if(szall_mod == 'PostaPont') {
				postapont = encodeURIComponent(document.getElementById("valasztott_postapont").value);
			}
			var glscspont = '';
			if(szall_mod == 'GLS CsomagPont') {
				glscspont = encodeURIComponent(document.getElementById("valasztott_gls_cspont").value);
			}
			else if(fiz_mod == 'Bankkártya') {
				kod_otp = encodeURIComponent(document.getElementById("kod_otp").value);
				otp_fizetendo = encodeURIComponent(document.getElementById("otp_fizetendo").value);
				otp_kosar_id = encodeURIComponent(document.getElementById("otp_kosar_id").value);
			}
			
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("kassza_div").innerHTML = xmlhttp.responseText;
					
					if(fiz_mod == 'Bankkártya') {
						window.location.href = +domain+'/otpwebshop/web_demo/fiz3.php?tranzakcioAzonosito='+kod_otp+'&posId=%2302299991&osszeg='+otp_fizetendo+'&devizanem=HUF&nyelvkod=hu&nevKell=true&shopMegjegyzes=Rendeles azonosito: '+otp_kosar_id+'&backURL=http://'+domain+'/otpwebshop/web_demo/fiz3.php?func=fiz3';
					}
					else {
						var body = $("html, body");
						body.stop().animate({scrollTop:0}, '500', 'swing', function() { });
					}
				}
			  }
			xmlhttp.open("POST","../webshop/kassza_uj_rendeles.php?script=ok",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("fiz_mod="+fiz_mod+"&szall_mod="+szall_mod+"&megjegyzes="+encodeURIComponent(document.getElementById("megjegyzes").value)+"&kod_otp="+encodeURIComponent(document.getElementById("kod_otp").value)+"&postapont="+postapont+"&glscspont="+glscspont);
		}
		else
		{
			document.getElementById("vas_fel_riaszt").innerHTML='<div class="alert alert-danger alert-dismissable">A vásárlási feltétel nem lett elfogadva!</p></div>';
		}
	});
