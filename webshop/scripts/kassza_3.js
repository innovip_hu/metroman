// Tartalmak nyitása zárása
	$(document).on("click", ".kassza_title", function() {
		$( "#adatok_tartalom" ).slideUp( 300, function() { });
		$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
		$( "#fiz_mod_tartalom" ).slideUp( 300, function() { });
		$( "#osszesites_tartalom" ).slideUp( 300, function() { });
		$( "#"+$(this).attr('attr_tart_id') ).slideDown( 300, function() { });
	});
// Tovább gombok
	$(document).on("click", ".kassza_tovabb_gomb", function() {
		var tartalom_id = $(this).attr('attr_tart_id');
		var horgony_id = $(this).attr('attr_horg');
		$( "#adatok_tartalom" ).slideUp( 300, function() { });
		$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
		$( "#fiz_mod_tartalom" ).slideUp( 300, function() { });
		$( "#osszesites_tartalom" ).slideUp( 300, function() { });
		$( "#" + tartalom_id).slideDown( 300, function() {
			$('html, body').animate({
				scrollTop: $("#"+horgony_id).offset().top - 80
				// scrollTop: $("#fiz_mod_horgony").offset().top - 80
			}, 500);
		});
	});
// Regisztráció checkbox
	$(document).on("click", "#regisztracio_checkbox", function() {
		if ($(this).is(":checked")) {
			$( "#jelszo_div" ).slideDown( 300, function() { });
			$( "#jelszo2_div" ).slideDown( 300, function() { });
			$( "#feltetel_div" ).slideDown( 300, function() { });
		}
		else {
			$( "#jelszo_div" ).slideUp( 300, function() { });
			$( "#jelszo2_div" ).slideUp( 300, function() { });
			$( "#feltetel_div" ).slideUp( 300, function() { });
			$( "#jelszo" ).val('');
			$( "#jelszo2" ).val('');
		}
	});
// Eltérő cím nyítás/csukás
	$(document).on("click", "#mascim", function() {
		if ($(this).is(":checked")) {
			$( "#eltero_cim" ).slideDown( 300, function() { });
			$( "#szallcim1_span" ).html($("#cim_szall_irszam").val() + " " + $("#cim_szall_varos").val());
			$( "#szallcim2_span" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
			$( "#szallcim1_span2" ).html($("#cim_szall_irszam").val() + " " + $("#cim_szall_varos").val());
			$( "#szallcim2_span2" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
		}
		else
		{
			$( "#eltero_cim" ).slideUp( 300, function() { });
			$( "#cim_szall_nev" ).val('');
			$( "#cim_szall_varos" ).val('');
			$( "#cim_szall_utca" ).val('');
			$( "#cim_szall_hszam" ).val('');
			$( "#cim_szall_irszam" ).val('');
			$( "#szallcim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
			$( "#szallcim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
			$( "#szallcim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
			$( "#szallcim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
			$( "#nev2_span" ).html('');
			$( "#nev2_span2" ).html('');
		}
	});
// Adatok beírása az összesítésbe
	// Név
	$(document).on("change", "#nev", function() {
		$( "#nev_span" ).html($(this).val());
		$( "#nev1_span" ).html($(this).val());
		$( "#nev1_span2" ).html($(this).val());
	});
	// Email
	$(document).on("change", "#email", function() {
		$( "#email_span" ).html($(this).val());
	});
	// Telefon
	$(document).on("change", "#telefon", function() {
		$( "#telefon_span" ).html($(this).val());
	});
	// cim_varos
	$(document).on("change", "#cim_varos", function() {
		$( "#cim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		$( "#cim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		if ($("#mascim").not(":checked")) {
			$( "#szallcim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
			$( "#szallcim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		}
	});
	// cim_irszam
	$(document).on("change", "#cim_irszam", function() {
		$( "#cim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		$( "#cim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		if ($("#mascim").not(":checked")) {
			$( "#szallcim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
			$( "#szallcim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		}
	});
	// cim_utca
	$(document).on("change", "#cim_utca", function() {
		$( "#cim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		$( "#cim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		if ($("#mascim").not(":checked")) {
			$( "#szallcim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
			$( "#szallcim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		}
	});
	// cim_hszam
	$(document).on("change", "#cim_hszam", function() {
		$( "#cim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		$( "#cim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		if ($("#mascim").not(":checked")) {
			$( "#szallcim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
			$( "#szallcim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		}
	});
	// Száll Név
	$(document).on("change", "#cim_szall_nev", function() {
		$( "#nev2_span" ).html($(this).val());
		$( "#nev2_span2" ).html($(this).val());
	});
	// cim_varos
	$(document).on("change", "#cim_szall_varos", function() {
		$( "#szallcim1_span" ).html($("#cim_szall_irszam").val() + " " + $("#cim_szall_varos").val());
	});
	// cim_irszam
	$(document).on("change", "#cim_szall_irszam", function() {
		$( "#szallcim1_span" ).html($("#cim_szall_irszam").val() + " " + $("#cim_szall_varos").val());
	});
	// cim_utca
	$(document).on("change", "#cim_szall_utca", function() {
		$( "#szallcim2_span" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
		$( "#szallcim2_span2" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
	});
	// cim_hszam
	$(document).on("change", "#cim_szall_hszam", function() {
		$( "#szallcim2_span" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
		$( "#szallcim2_span2" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
	});
	// Szállítási mód	
	$(document).on("click", ".szall_mod_checkbox", function() {
		$( "#szall_mod_span" ).html($(this).val());
		if($(this).attr('id') != 'postapont'){
			$( "#szall_mod_pp_span" ).html('');
		}
		else if($(this).attr('id') != 'gls_cspont'){
			$( "#szall_mod_gls_span" ).html('');
		}
	});
	$(document).on("click", ".pp_talalat", function() {
		$( "#szallcim1_span" ).html($("#valasztott_postapont").val());
		$( "#szallcim2_span" ).html('');
		$( "#szallcim1_span2" ).html($("#valasztott_postapont").val());
		$( "#szallcim2_span2" ).html('');
		$( "#gls_span" ).html('');
		$( "#pp_span" ).html('<b>Választott PostaPont:</b> ' + $("#valasztott_postapont").val());
	});
	$(document).on("click", ".gls_talalat", function() {
		$( "#szallcim1_span" ).html($("#valasztott_gls_cspont").val());
		$( "#szallcim2_span" ).html('');
		$( "#szallcim1_span2" ).html($("#valasztott_gls_cspont").val());
		$( "#szallcim2_span2" ).html('');
		$( "#pp_span" ).html('');
		$( "#gls_span" ).html('<b>Választott GLS csomagpont:</b> ' + $("#valasztott_gls_cspont").val());
	});
	// Fizetése mód	
	$(document).on("click", ".fiz_mod_checkbox", function() {
		$( "#fiz_mod_span" ).html($(this).val());
	});
	// Postapont
	/* $(document).on("click", ".pp_talalat", function() {
		$( "#szall_mod_pp_span" ).html("<br/>" + $(this).val());
	}); */

// Szállítási árak meghatározása
	// Futárszolgálattal
	$(document).on("click", "#hazhoz", function() {
		$( ".szall_kolts_arak" ).css('display', 'none');
		$( "#szall_kolts_ar_futar_fiz_elore" ).css('display', 'inline');
		$( "#szall_kolts_ar_futar_fiz_utanv" ).css('display', 'inline');
		$( "#szall_kolts_ar_futar_fiz_bankk" ).css('display', 'inline');
		$( "#szall_kolts_ar_futar_fiz_uzlet" ).css('display', 'inline');
		// Előreutalás
		if ($("#eloreutal").is(":checked")) {
			$( ".ossz_szall_kolts_ar_futar" ).css('display', 'inline');
		}
		// Utánvét
		if ($("#utanvet").is(":checked")) {
			$( ".ossz_szall_kolts_ar_futar_utanv" ).css('display', 'inline');
		}
		// Bankkártyás
		if ($("#bankkartya").is(":checked")) {
			$( ".ossz_szall_kolts_ar_futar" ).css('display', 'inline');
		}
		// Fizetés üzletben
		if ($("#bankkartya").is(":checked")) {
			$( ".ossz_szall_kolts_ar_futar" ).css('display', 'inline');
		}
	});
	// Egyedi
	$(document).on("click", "#egyedi", function() {
		$( ".szall_kolts_arak" ).css('display', 'none');
		$( "#szall_kolts_ar_egyedi_fiz_elore" ).css('display', 'inline');
		$( "#szall_kolts_ar_egyedi_fiz_utanv" ).css('display', 'inline');
		$( "#szall_kolts_ar_egyedi_fiz_bankk" ).css('display', 'inline');
		$( "#szall_kolts_ar_egyedi_fiz_uzlet" ).css('display', 'inline');
		// Előreutalás
		if ($("#eloreutal").is(":checked")) {
			$( ".ossz_szall_kolts_ar_egyedi" ).css('display', 'inline');
		}
		// Utánvét
		if ($("#utanvet").is(":checked")) {
			$( ".ossz_szall_kolts_ar_egyedi_utanv" ).css('display', 'inline');
		}
		// Bankkártyás
		if ($("#bankkartya").is(":checked")) {
			$( ".ossz_szall_kolts_ar_egyedi" ).css('display', 'inline');
		}
		// Fizetés üzletben
		if ($("#szematvet").is(":checked")) {
			$( ".ossz_szall_kolts_ar_szematvet" ).css('display', 'inline');
		}
	});
	// Postapont
	$(document).on("click", "#postapont", function() {
		$( ".szall_kolts_arak" ).css('display', 'none');
		$( "#szall_kolts_ar_pp_fiz_elore" ).css('display', 'inline');
		$( "#szall_kolts_ar_pp_fiz_utanv" ).css('display', 'inline');
		$( "#szall_kolts_ar_pp_fiz_bankk" ).css('display', 'inline');
		$( "#szall_kolts_ar_pp_fiz_uzlet" ).css('display', 'inline');
		// Előreutalás
		if ($("#eloreutal").is(":checked")) {
			$( ".ossz_szall_kolts_ar_pp" ).css('display', 'inline');
		}
		// Utánvét
		if ($("#utanvet").is(":checked")) {
			$( ".ossz_szall_kolts_ar_pp_utanv" ).css('display', 'inline');
		}
		// Bankkártyás
		if ($("#bankkartya").is(":checked")) {
			$( ".ossz_szall_kolts_ar_pp" ).css('display', 'inline');
		}
		// Fizetés üzletben
		if ($("#szematvet").is(":checked")) {
			$( ".ossz_szall_kolts_ar_szematvet" ).css('display', 'inline');
		}
	});
	// Postapont
	$(document).on("click", "#gls_cspont", function() {
		$( ".szall_kolts_arak" ).css('display', 'none');
		$( "#szall_kolts_ar_gls_fiz_elore" ).css('display', 'inline');
		$( "#szall_kolts_ar_gls_fiz_utanv" ).css('display', 'inline');
		$( "#szall_kolts_ar_gls_fiz_bankk" ).css('display', 'inline');
		$( "#szall_kolts_ar_gls_fiz_uzlet" ).css('display', 'inline');
		// Előreutalás
		if ($("#eloreutal").is(":checked")) {
			$( ".ossz_szall_kolts_ar_gls" ).css('display', 'inline');
		}
		// Utánvét
		if ($("#utanvet").is(":checked")) {
			$( ".ossz_szall_kolts_ar_gls_utanv" ).css('display', 'inline');
		}
		// Bankkártyás
		if ($("#bankkartya").is(":checked")) {
			$( ".ossz_szall_kolts_ar_gls" ).css('display', 'inline');
		}
		// Fizetés üzletben
		if ($("#szematvet").is(":checked")) {
			$( ".ossz_szall_kolts_ar_szematvet" ).css('display', 'inline');
		}
	});
	// Üzletben átvétel
	$(document).on("click", "#szematvet", function() {
		$( ".szall_kolts_arak" ).css('display', 'none');
		$( ".ossz_szall_kolts_ar_szematvet" ).css('display', 'inline');
	});
	// Szállítási módok
	// Előre utalás
	$(document).on("click", "#eloreutal", function() {
		$( ".ossz_szall_kolts_arak" ).css('display', 'none');
		// Futárszolgálat
		if ($("#hazhoz").is(":checked")) {
			$( ".ossz_szall_kolts_ar_futar" ).css('display', 'inline');
		}
		else if ($("#egyedi").is(":checked")) {
			$( ".ossz_szall_kolts_ar_egyedi" ).css('display', 'inline');
		}
		else if ($("#postapont").is(":checked")) {
			$( ".ossz_szall_kolts_ar_pp" ).css('display', 'inline');
		}
		else if ($("#gls_cspont").is(":checked")) {
			$( ".ossz_szall_kolts_ar_gls" ).css('display', 'inline');
		}
		else if ($("#szematvet").is(":checked")) {
			$( ".ossz_szall_kolts_ar_szematvet" ).css('display', 'inline');
		}
	});
	// Utánvétel
	$(document).on("click", "#utanvet", function() {
		$( ".ossz_szall_kolts_arak" ).css('display', 'none');
		// Futárszolgálat
		if ($("#hazhoz").is(":checked")) {
			$( ".ossz_szall_kolts_ar_futar_utanv" ).css('display', 'inline');
		}
		else if ($("#egyedi").is(":checked")) {
			$( ".ossz_szall_kolts_ar_egyedi_utanv" ).css('display', 'inline');
		}
		else if ($("#postapont").is(":checked")) {
			$( ".ossz_szall_kolts_ar_pp_utanv" ).css('display', 'inline');
		}
		else if ($("#gls_cspont").is(":checked")) {
			$( ".ossz_szall_kolts_ar_gls_utanv" ).css('display', 'inline');
		}
		else if ($("#szematvet").is(":checked")) {
			$( ".ossz_szall_kolts_ar_szematvet" ).css('display', 'inline');
		}
	});
	// Bankkártyás fizetés
	$(document).on("click", "#bankkartya", function() {
		$( ".ossz_szall_kolts_arak" ).css('display', 'none');
		// Futárszolgálat
		if ($("#hazhoz").is(":checked")) {
			$( ".ossz_szall_kolts_ar_futar" ).css('display', 'inline');
		}
		else if ($("#egyedi").is(":checked")) {
			$( ".ossz_szall_kolts_ar_egyedi" ).css('display', 'inline');
		}
		else if ($("#postapont").is(":checked")) {
			$( ".ossz_szall_kolts_ar_pp" ).css('display', 'inline');
		}
		else if ($("#gls_cspont").is(":checked")) {
			$( ".ossz_szall_kolts_ar_gls" ).css('display', 'inline');
		}
		else if ($("#szematvet").is(":checked")) {
			$( ".ossz_szall_kolts_ar_szematvet" ).css('display', 'inline');
		}
	});
	// Fizetés üzletben
	$(document).on("click", "#szemfiz", function() {
		$( ".ossz_szall_kolts_arak" ).css('display', 'none');
		// Futárszolgálat
		if ($("#hazhoz").is(":checked")) {
			$( ".ossz_szall_kolts_ar_futar" ).css('display', 'inline');
		}
		else if ($("#egyedi").is(":checked")) {
			$( ".ossz_szall_kolts_ar_egyedi" ).css('display', 'inline');
		}
		else if ($("#postapont").is(":checked")) {
			$( ".ossz_szall_kolts_ar_pp" ).css('display', 'inline');
		}
		else if ($("#gls_cspont").is(":checked")) {
			$( ".ossz_szall_kolts_ar_gls" ).css('display', 'inline');
		}
		else if ($("#szematvet").is(":checked")) {
			$( ".ossz_szall_kolts_ar_szematvet" ).css('display', 'inline');
		}
	});
// Rendelés leadása
	$(document).on("click", "#rendeles_leadasa", function() {
		$( ".riaszt_div" ).css('display', 'none');
		if($('#belepve').val() == 0)
		{
			if($('#nev').val() == ''){
				$( "#nev_riaszt" ).css('display', 'block');
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#fiz_mod_tartalom" ).slideUp( 300, function() { });
				$( "#osszesites_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#nev" ).focus();
				return false;
			}
			else if($('#email').val() == ''){
				$( "#email_riaszt" ).css('display', 'block');
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#fiz_mod_tartalom" ).slideUp( 300, function() { });
				$( "#osszesites_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#email" ).focus();
				return false;
			}
			else if($('#telefon').val() == ''){
				$( "#telefon_riaszt" ).css('display', 'block');
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#fiz_mod_tartalom" ).slideUp( 300, function() { });
				$( "#osszesites_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#telefon" ).focus();
				return false;
			}
			else if($('#cim_irszam').val() == ''){
				$( "#cim_irszam_riaszt" ).css('display', 'block');
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#fiz_mod_tartalom" ).slideUp( 300, function() { });
				$( "#osszesites_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#cim_irszam" ).focus();
				return false;
			}
			else if($('#cim_varos').val() == ''){
				$( "#cim_varos_riaszt" ).css('display', 'block');
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#fiz_mod_tartalom" ).slideUp( 300, function() { });
				$( "#osszesites_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#cim_varos" ).focus();
				return false;
			}
			else if($('#cim_utca').val() == ''){
				$( "#cim_utca_riaszt" ).css('display', 'block');
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#fiz_mod_tartalom" ).slideUp( 300, function() { });
				$( "#osszesites_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#cim_utca" ).focus();
				return false;
			}
			else if($('#cim_hszam').val() == ''){
				$( "#cim_hszam_riaszt" ).css('display', 'block');
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#fiz_mod_tartalom" ).slideUp( 300, function() { });
				$( "#osszesites_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#cim_hszam" ).focus();
				return false;
			}
			// Ha regisztrál
			else if ($("#regisztracio_checkbox").is(":checked")) {
				if($('#jelszo').val() == ''){
					$( "#jelszo_riaszt" ).css('display', 'block');
					$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
					$( "#fiz_mod_tartalom" ).slideUp( 300, function() { });
					$( "#osszesites_tartalom" ).slideUp( 300, function() { });
					$( "#adatok_tartalom" ).slideDown( 300, function() { });
					$( "#jelszo" ).focus();
					return false;
				}
				else if($('#jelszo').val() != $('#jelszo2').val()){
					$( "#jelszo_riaszt2" ).css('display', 'block');
					$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
					$( "#fiz_mod_tartalom" ).slideUp( 300, function() { });
					$( "#osszesites_tartalom" ).slideUp( 300, function() { });
					$( "#adatok_tartalom" ).slideDown( 300, function() { });
					$( "#jelszo" ).focus();
					return false;
				}
				else if($('#feltetel').is(":checked") != true){
					$( "#feltetel_riaszt" ).css('display', 'block');
					$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
					$( "#fiz_mod_tartalom" ).slideUp( 300, function() { });
					$( "#osszesites_tartalom" ).slideUp( 300, function() { });
					$( "#adatok_tartalom" ).slideDown( 300, function() { });
					$( "#feltetel" ).focus();
					return false;
				}
			};
		}
		// Szállítási mód ellenőrzése
		var anySzallmodChecked = false;
		$('.szall_mod_checkbox').each(function() {
			if ($(this).is(":checked")) {
				anySzallmodChecked = true;
			}
		});
		if (anySzallmodChecked == false) {
			// alert('Válasz legalább egy lehetőséget');
			$( "#szall_mod_riaszt" ).css('display', 'block');
			$( "#fiz_mod_tartalom" ).slideUp( 300, function() { });
			$( "#osszesites_tartalom" ).slideUp( 300, function() { });
			$( "#adatok_tartalom" ).slideUp( 300, function() { });
			$( "#szall_mod_tartalom" ).slideDown( 300, function() { });
			$('html, body').animate({
				scrollTop: $('#szall_mod_horgony').offset().top - 80
			}, 500);
			return false;
		}
		// Postapont
		if($("#postapont").is(":checked") && $('#valasztott_postapont').val() == '')
		{
			// document.getElementById("pp_riaszt").innerHTML='<div class="alert alert-danger alert-dismissable">Válassz Posta Pontot!</p></div>';
			$( "#postapont_riaszt" ).css('display', 'block');
			$( "#fiz_mod_tartalom" ).slideUp( 300, function() { });
			$( "#osszesites_tartalom" ).slideUp( 300, function() { });
			$( "#adatok_tartalom" ).slideUp( 300, function() { });
			$( "#szall_mod_tartalom" ).slideDown( 300, function() { });
			$('html, body').animate({
				scrollTop: $('#postapont_horgony').offset().top - 80
			}, 500);
			return false;
		}
		// GLS pont
		if(document.getElementById("gls_cspont").checked == true && document.getElementById("valasztott_gls_cspont").value == '')
		if($("#gls_cspont").is(":checked") && $('#valasztott_gls_cspont').val() == '')
		{
			// document.getElementById("pp_riaszt").innerHTML='<div class="alert alert-danger alert-dismissable">Válassz Posta Pontot!</p></div>';
			$( "#glspont_riaszt" ).css('display', 'block');
			$( "#fiz_mod_tartalom" ).slideUp( 300, function() { });
			$( "#osszesites_tartalom" ).slideUp( 300, function() { });
			$( "#adatok_tartalom" ).slideUp( 300, function() { });
			$( "#szall_mod_tartalom" ).slideDown( 300, function() { });
			$('html, body').animate({
				scrollTop: $('#glspont_horgony').offset().top - 80
			}, 500);
			return false;
		}
		// Fizetésp mód ellenőrzése
		var anyFizmodChecked = false;
		$('.fiz_mod_checkbox').each(function() {
			if ($(this).is(":checked")) {
				anyFizmodChecked = true;
			}
		});
		if (anyFizmodChecked == false) {
			// alert('Válasz legalább egy lehetőséget');
			$( "#fiz_mod_riaszt" ).css('display', 'block');
			$( "#osszesites_tartalom" ).slideUp( 300, function() { });
			$( "#adatok_tartalom" ).slideUp( 300, function() { });
			$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
			$( "#fiz_mod_tartalom" ).slideDown( 300, function() { });
			$('html, body').animate({
				scrollTop: $('#fiz_mod_horgony').offset().top - 80
			}, 500);
			return false;
		}
		if($('#vas_fel').is(":checked") != true){
			$( "#vas_fel_riaszt" ).css('display', 'block');
			return false;
		}		
		
		// Minden OK
		$("#rendeles_gomb_div").html('<div class="loader"></div>');
		$("#rendeles_leadas_form").submit();
	});

	