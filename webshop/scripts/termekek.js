// Az oldalon megjelenő termékek mennyisége
	$(document).on("change", "#old_db", function() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
				termekekIgazitasa();
			}
		  }
		xmlhttp.open("GET",domain+"/webshop/termek_kategoriak.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&old_db="+encodeURIComponent(document.getElementById("old_db").value),true);
		xmlhttp.send();
	});
// Sorba rendezés
	$(document).on("change", "#sorrend_uj", function() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
				var select = $('#sorrend_uj');
				select.select2( {
					dropdownParent:          select.parent(),
					placeholder:             select.attr( 'data-placeholder' ) || null,
					minimumResultsForSearch: select.attr( 'data-minimum-results-search' ) || Infinity,
					containerCssClass:       select.attr( 'data-container-class' ) || null,
					dropdownCssClass:        select.attr( 'data-dropdown-class' ) || null
				} );				
				termekekIgazitasa();
			}
		  }
		xmlhttp.open("GET",domain+"/webshop/termek_kategoriak.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&sorrend_uj="+encodeURIComponent(document.getElementById("sorrend_uj").value),true);
		xmlhttp.send();
	});
// Lapozás
	function lapozas(oldszam) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
				var select = $('#sorrend_uj');
				select.select2( {
					dropdownParent:          select.parent(),
					placeholder:             select.attr( 'data-placeholder' ) || null,
					minimumResultsForSearch: select.attr( 'data-minimum-results-search' ) || Infinity,
					containerCssClass:       select.attr( 'data-container-class' ) || null,
					dropdownCssClass:        select.attr( 'data-dropdown-class' ) || null
				} );				
				termekekIgazitasa();
				$('html,body').scrollTop(0);
				// var stateObj = { foo: "webshop" };
				// history.pushState(stateObj, "", domain+"/webshop/termek_kategoriak.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&oldszam="+oldszam);
			}
		  }
		xmlhttp.open("GET",domain+"/webshop/termek_kategoriak.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&oldszam="+oldszam,true);
		xmlhttp.send();
	};
// Összehasonlításba tesz, vagy kivesz
	$(document).on("click", ".osszeh_check", function() {
		var osszeh_termek = jQuery(this).attr( "attr_term_id" );
		var osszeh_checked = jQuery(this).attr( "attr_checked" );
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
				termekekIgazitasa();
				// Összehasonlító div újratöltése
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("osszehas_ajax_div").innerHTML = xmlhttp.responseText;
					}
				  }
				xmlhttp.open("GET",domain+"/webshop/module/mod_osszehasonlitas_kicsi.php?script=ok",true);
				xmlhttp.send();
			}
		  }
		xmlhttp.open("GET",domain+"/webshop/termek_kategoriak.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&osszeh_termek="+osszeh_termek+"&osszeh_checked="+osszeh_checked,true);
		xmlhttp.send();
	});
// Összehasonlítás megnyitása
	$(document).on("click", "#osszeh_tovabb", function() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",domain+"/webshop/module/mod_osszehasonlitas_oldal.php?script=ok",true);
		xmlhttp.send();
	});
// Kívánságlista
	function kivansaglistabaRakas(kivansaglista_termek) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
				termekekIgazitasa();
			}
		  }
		xmlhttp.open("GET",domain+"/webshop/termek_kategoriak.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&kivansaglista_termek="+kivansaglista_termek,true);
		xmlhttp.send();
	};
// Szűrő
	function szures(id) {
		if (document.getElementById("term_urlnev").value == '') {
			var str = $( "#szuro_form" ).serialize();
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					if ($('#szuro_lenyito').css('display') == 'none') { 
						$('#belso-szuro-'+id).animate({
							opacity: [ "toggle" ]
						}, 200);				
						if ($('#belso-szuro-'+id).parent().find('.headerFilter span').hasClass('fa-chevron-up'))
						{
							$('#belso-szuro-'+id).parent().find('.headerFilter span').removeClass('fa-chevron-up').addClass('fa-chevron-down');
						}
						else
						{
							$('#belso-szuro-'+id).parent().find('.headerFilter span').removeClass('fa-chevron-down').addClass('fa-chevron-up');
						}
					}
					document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;					
					window.history.replaceState("valami", "Termékek", domain+"/termekek/"+encodeURIComponent(document.getElementById("kat_urlnev").value)+"/?"+str);
					var select = $('#sorrend_uj');
					select.select2( {
						dropdownParent:          select.parent(),
						placeholder:             select.attr( 'data-placeholder' ) || null,
						minimumResultsForSearch: select.attr( 'data-minimum-results-search' ) || Infinity,
						containerCssClass:       select.attr( 'data-container-class' ) || null,
						dropdownCssClass:        select.attr( 'data-dropdown-class' ) || null
					} );	

				    $("body,html").animate(
				      {
				        scrollTop: $('#termekek_ajax_div').offset().top - $('.rd-navbar').height() - 55
				      },
				      400 //speed
				    );						
					termekekIgazitasa();
				}
			  }
			/* xmlhttp.open("POST",domain+"/webshop/termek_kategoriak.php?script=ok&command=szures&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value),true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send($("#szuro_form").serialize()); */
			xmlhttp.open("POST",domain+"/webshop/termek_kategoriak.php?script=ok&command=szures&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&"+str,true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("szures_most=ok");
		}
		else
		{
			document.getElementById('szuro_form').submit();
		}
	};
// Nézet módosítása
	$(document).on("click", "#nezet_gomb", function() {
		var nezet_uj = jQuery(this).attr( "attr_nezet" );
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
				termekekIgazitasa();
			}
		  }
		xmlhttp.open("GET",domain+"/webshop/termek_kategoriak.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&nezet_uj="+nezet_uj,true);
		xmlhttp.send();
	});
// Kosárba helyezés
	$(document).on("click", "#kosarba_gomb", function() {

		event.preventDefault();

		var formData = new FormData();
		// Paraméterek
		var nincs_param = 0;
		var felar = 0;
		$(".termek_parameterek_kosar").each(function(){
			formData.append($(this).attr('id'), ($(this).val()));
			if($(this).val() == '')
			{
				nincs_param = 1;
			}
			felar = felar + Number($(this).children('option[value="'+$(this).val()+'"]').attr('data-felar'));
			
			par_id = $(this).attr('attr_id');
			formData.append("termek_parameter["+par_id+"]", (document.getElementById("termek_parameter"+par_id).value));
		});
		formData.append("term_id", (document.getElementById("term_id").value));
		formData.append("term_nev", (document.getElementById("term_nev").value));
		formData.append("term_kep", (document.getElementById("term_kep").value));
		formData.append("term_ar", (Number(document.getElementById("term_ar").value) + felar));
		formData.append("term_afa", (document.getElementById("term_afa").value));
		if(document.getElementById("term_akcios_ar").value == 0) // Nem akciós
		{
			formData.append("term_akcios_ar", (document.getElementById("term_akcios_ar").value));
			var googlePrice = document.getElementById("term_ar").value;
		}
		else // Akciós
		{
			formData.append("term_akcios_ar", (Number(document.getElementById("term_akcios_ar").value) + felar));
			var googlePrice = document.getElementById("term_akcios_ar").value;
		}
		formData.append("command", (document.getElementById("command").value));
		formData.append("darab", (document.getElementById("darab").value));

		for (var elemek of formData.entries()) {
		    console.log(elemek[0]+ ', ' + elemek[1]); //formdata elemeinek kiírása console-ba
		}	

		var googleID = document.getElementById("term_id").value;
		var googleSKU = document.getElementById("sku").value;
		var googleName = document.getElementById("term_nev").value;
		var googleQuantity = document.getElementById("darab").value;		

		if(nincs_param == 1)
		{
			$('#parameter_riaszt').show();
		}
		else
		{
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					$('#kosarba_felugro_ablak').modal('show');
					//Kinyíló kosár újra generálása
					if (window.XMLHttpRequest)
					  {// code for IE7+, Firefox, Chrome, Opera, Safari
					  xmlhttp=new XMLHttpRequest();
					  }
					else
					  {// code for IE6, IE5
					  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
					  }
					xmlhttp.onreadystatechange=function()
					  {
					  if (xmlhttp.readyState==4 && xmlhttp.status==200)
						{
							//document.getElementById("kinyilo_kosar_div").innerHTML=xmlhttp.responseText;

							gtag('event', 'add_to_cart', {  
							   "items": [ 
							     {  
							       "id": googleID,  
							       "publicSku": googleSKU,  
							       "name": googleName,  
							       "quantity": googleQuantity,  
							       "price":  googlePrice
							     }  
							   ]  
							});  							
						}
					  }
					xmlhttp.open("GET",domain+"/webshop/kinyilo_kosar.php?script=ok",true);
					xmlhttp.send();
					termekekIgazitasa();

					$.get(domain+'/webshop/kosar_darabszam.php?script=ok',function(response,status){ // Required Callback Function
				    		$('#kosar-darabszam').html(response);
					}); 					
					
				}
				console.log("Error", xmlhttp.statusText);  
			  }
			xmlhttp.open("POST",domain+"/webshop/termek_info.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value),true);
			xmlhttp.send(formData);			
		}
	});
// Szűrő nyitása
	$(document).on("click", "#szuro_lenyito", function() {
		/* $("#szuro_div_section").css('display', 'block'); */
		$("#szuro_div_section").css('z-index', '999999');
		$("#szuro_div_section").animate({
			opacity: '1',
		});
	});
// Szűrő zárása
	$(document).on("click", ".szuro_bezar_gomb", function() {
		/* $("#szuro_div_section").css('display', 'none'); */
		$("#szuro_div_section").animate({
			opacity: '0',
		}
			, function() {
				$("#szuro_div_section").css('z-index', '-1');
			}
		);
		/* $("#szuro_div_section").animate({
			opacity: '0',
		}, 500); */
		/* $("#szuro_div_section").css('z-index', '-1'); */
	});
