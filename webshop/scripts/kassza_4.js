	function googleLista()
	{
		var lista = JSON.parse(document.getElementById("kosar_tartalma_google").value);

		var arr = new Array();

		for (var i = 0; i < lista.length; i++) {
			arr.push(lista[i]);
		}

		gtag('event', 'checkout_progress', { "items" : lista });
	}	

// Tartalmak nyitása zárása
	$(document).on("click", ".kassza_title", function() {
		var tartalom_id = $(this).attr('attr_tart_id');
		var horgony_id = $(this).attr('attr_horg');		
		$( "#adatok_tartalom" ).slideUp( 300, function() { });
		$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
		$( "#osszesites_tartalom" ).slideUp( 300, function() { });
		$( "#" + tartalom_id).slideDown( 300, function() {
			$('html, body').animate({
				scrollTop: $("#"+horgony_id).offset().top - 80
			}, 500);
		});		

		if (tartalom_id == 'adatok_tartalom')
		{
			gtag('event', 'checkout_progress', {
			  "checkout_step": 2
			});					
		}

		if (tartalom_id == 'szall_mod_tartalom')
		{
			gtag('event', 'checkout_progress', {
			  "checkout_step": 3
			});			

			googleLista();	
		}	
		
		if (tartalom_id == 'osszesites_tartalom')
		{
			gtag('event', 'checkout_progress', {
			  "checkout_step": 4
			});				
		}			
	});
// Tovább gombok
	$(document).on("click", ".kassza_tovabb_gomb", function() {
		var tartalom_id = $(this).attr('attr_tart_id');
		var horgony_id = $(this).attr('attr_horg');
		$( "#adatok_tartalom" ).slideUp( 300, function() { });
		$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
		$( "#osszesites_tartalom" ).slideUp( 300, function() { });
		$( "#" + tartalom_id).slideDown( 300, function() {
			$('html, body').animate({
				scrollTop: $("#"+horgony_id).offset().top - 80
				// scrollTop: $("#fiz_mod_horgony").offset().top - 80
			}, 500);
		});

		if (tartalom_id == 'adatok_tartalom')
		{
			gtag('event', 'checkout_progress', {
			  "checkout_step": 2
			});					
		}

		if (tartalom_id == 'szall_mod_tartalom')
		{
			gtag('event', 'checkout_progress', {
			  "checkout_step": 3
			});			

			googleLista();	
		}	
		
		if (tartalom_id == 'osszesites_tartalom')
		{
			gtag('event', 'checkout_progress', {
			  "checkout_step": 4
			});				
		}
	});
// Regisztráció checkbox
	$(document).on("click", "#regisztracio_checkbox", function() {
		if ($(this).is(":checked")) {
			$( "#jelszo_div" ).slideDown( 300, function() { });
			$( "#jelszo2_div" ).slideDown( 300, function() { });
			$( "#feltetel_div" ).slideDown( 300, function() { });  			
		}
		else {
			$( "#jelszo_div" ).slideUp( 300, function() { });
			$( "#jelszo2_div" ).slideUp( 300, function() { });
			$( "#feltetel_div" ).slideUp( 300, function() { });
			$( "#jelszo" ).val('');
			$( "#jelszo2" ).val('');
		}
	});
// Eltérő cím nyítás/csukás
	$(document).on("click", "#mascim", function() {
		if ($(this).is(":checked")) {
			$( "#eltero_cim" ).slideDown( 300, function() { });
			$( "#szallcim1_span" ).html($("#cim_szall_irszam").val() + " " + $("#cim_szall_varos").val());
			$( "#szallcim2_span" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
			$( "#szallcim1_span2" ).html($("#cim_szall_irszam").val() + " " + $("#cim_szall_varos").val());
			$( "#szallcim2_span2" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
		}
		else
		{
			$( "#eltero_cim" ).slideUp( 300, function() { });
			$( "#cim_szall_nev" ).val('');
			$( "#cim_szall_varos" ).val('');
			$( "#cim_szall_utca" ).val('');
			$( "#cim_szall_hszam" ).val('');
			$( "#cim_szall_irszam" ).val('');
			$( "#szallcim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
			$( "#szallcim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
			$( "#szallcim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
			$( "#szallcim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
			$( "#nev2_span" ).html('');
			$( "#nev2_span2" ).html('');
		}
	});

	$(document).on("click", "#cegadoszam", function() {
		if ($(this).is(":checked")) {
			$( "#adoszam_blokk" ).slideDown( 300, function() { });			
		}
		else
		{
			$( "#adoszam_blokk" ).slideUp( 300, function() { });
			$('#adoszam').val('');
		}
	});	
// Adatok beírása az összesítésbe
	// Név
	$(document).on("change", "#nev", function() {
		$( "#nev_span" ).html($(this).val());
	});
	$(document).on("change", "#szla_nev", function() {
		var szla_nev = $(this).val();
		$( "#nev1_span2" ).html(szla_nev);
		$( "#nev1_span" ).html(szla_nev);
		if ($("#mascim").not(":checked")) 
		{
			$( "#nev2_span2" ).html(szla_nev);
			$( "#nev2_span" ).html(szla_nev);
		}
	});	
	// Email
	$(document).on("change", "#email", function() {
		$( "#email_span" ).html($(this).val());
	});
	// Adószám
	$(document).on("change", "#adoszam", function(){
		var adoszam = $(this).val();
			$('#cim2_adoszam1').html(adoszam);
			$('#cim2_adoszam2').html(adoszam);
	});
	// Telefon
	$(document).on("change", "#telefon", function() {
		$( "#telefon_span" ).html($(this).val());
	});
	// cim_varos
	$(document).on("change", "#cim_varos", function() {
		$( "#cim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		$( "#cim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		if ($("#mascim").not(":checked")) {
			$( "#szallcim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
			$( "#szallcim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		}
	});
	// cim_irszam
	$(document).on("change", "#cim_irszam", function() {
		$( "#cim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		$( "#cim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		if ($("#mascim").not(":checked")) {
			$( "#szallcim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
			$( "#szallcim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		}
	});
	// cim_utca
	$(document).on("change", "#cim_utca", function() {
		$( "#cim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		$( "#cim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		if ($("#mascim").not(":checked")) {
			$( "#szallcim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
			$( "#szallcim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		}
	});
	// cim_hszam
	$(document).on("change", "#cim_hszam", function() {
		$( "#cim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		$( "#cim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		if ($("#mascim").not(":checked")) {
			$( "#szallcim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
			$( "#szallcim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		}
	});
	// Száll Név
	$(document).on("change", "#cim_szall_nev", function() {
		$( "#nev2_span" ).html($(this).val());
		$( "#nev2_span2" ).html($(this).val());
	});
	// cim_varos
	$(document).on("change", "#cim_szall_varos", function() {
		$( "#szallcim1_span" ).html($("#cim_szall_irszam").val() + " " + $("#cim_szall_varos").val());
	});
	// cim_irszam
	$(document).on("change", "#cim_szall_irszam", function() {
		$( "#szallcim1_span" ).html($("#cim_szall_irszam").val() + " " + $("#cim_szall_varos").val());
	});
	// cim_utca
	$(document).on("change", "#cim_szall_utca", function() {
		$( "#szallcim2_span" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
		$( "#szallcim2_span2" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
	});
	// cim_hszam
	$(document).on("change", "#cim_szall_hszam", function() {
		$( "#szallcim2_span" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
		$( "#szallcim2_span2" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
	});
	// Szállítási mód	
	/* $(document).on("click", ".szall_mod_checkbox", function() {
		$( "#szall_mod_span" ).html($(this).val());
		if($(this).attr('id') != 'postapont'){
			$( "#szall_mod_pp_span" ).html('');
		}
		else if($(this).attr('id') != 'gls_cspont'){
			$( "#szall_mod_gls_span" ).html('');
		}
	}); */
	$(document).on("click", ".pp_talalat", function() {
		$( "#szallcim1_span" ).html($("#valasztott_postapont").val());
		$( "#szallcim2_span" ).html('');
		$( "#szallcim1_span2" ).html($("#valasztott_postapont").val());
		$( "#szallcim2_span2" ).html('');
		$( "#gls_span" ).html('');
		$( "#ppp_span" ).html('');
		$( "#pp_span" ).html('<b>Választott PostaPont:</b> ' + $("#valasztott_postapont").val());
	});
	$(document).on("click", ".gls_talalat", function() {
		$( "#szallcim1_span" ).html($("#valasztott_gls_cspont").val());
		$( "#szallcim2_span" ).html('');
		$( "#szallcim1_span2" ).html($("#valasztott_gls_cspont").val());
		$( "#szallcim2_span2" ).html('');
		$( "#pp_span" ).html('');
		$( "#gls_span" ).html('<b>Választott GLS csomagpont vagy automata:</b> ' + $("#valasztott_gls_cspont").val());
	});
	// Fizetése mód	
	/* $(document).on("click", ".fiz_mod_checkbox", function() {
		$( "#fiz_mod_span" ).html($(this).val());
	}); */
	
// Fizetési mód kiválasztása	
	$(document).on("change", "#fiz_mod_select, #szall_mod_select", function() {
		// alert(8);
		$.post(domain+'/webshop/kassza_4_mod_szall_koltseg_meghat.php',{
				fiz_mod : $('#fiz_mod_select').val(),
				szall_mod : $('#szall_mod_select').val()
			},function(response,status){ // Required Callback Function
				let valasz = JSON.parse(response);								
				var szall_koltseg = valasz.szallitas;
				var osszesen = $('#vegosszeghez_osszesen').val();
				var ar_kedvezmeny = $('#vegosszeghez_ar_kedvezmeny').val();
				if (valasz.utanvet > 0) {
					var fiezetendo = Number(szall_koltseg) + Number(osszesen) - Number(ar_kedvezmeny) + Number(valasz.utanvet);
					let normalAr = valasz.szallitas.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " Ft";
					let utanvetAr = valasz.utanvet.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " Ft utánvét kezelési díj";

					$('#fiz_szall_mod_koltseg').html(`${normalAr} + ${utanvetAr}`);	
					$('#utanvet_span').html(valasz.utanvet.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
					$('#utanvet_sor').show();
				} else {
					$('#utanvet_sor').hide();
					$('#utanvet_span').html("0");	
					var fiezetendo = Number(szall_koltseg) + Number(osszesen) - Number(ar_kedvezmeny);
					$('#fiz_szall_mod_koltseg').html(szall_koltseg.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " Ft");	
				}
				// Összesítéshez
				$('#szall_kolts_span').html(szall_koltseg.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
				$('#fizetendo_span').html(fiezetendo.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
				$('#fiz_mod_span').html($('#fiz_mod_select option:selected').text());
				$('#szall_mod_span').html($('#szall_mod_select option:selected').text());

			});

	});
	$(document).on("change", "#fiz_mod_select", function() {
		var ertek = $('#fiz_mod_select').val();
		if (ertek == 4)
		{
			$('#szall_mod_select').val('5');

			$('#szall_mod_select option[value="1"]').attr('disabled','disabled');
			$('#szall_mod_select option[value="4"]').attr('disabled','disabled');
			$('#szall_mod_select option[value="6"]').attr('disabled','disabled');
			$('#szall_mod_select option[value="8"]').attr('disabled','disabled');

			$('#szall_mod_select').select2({minimumResultsForSearch: -1});

			ppKeresoZaras();
			$( "#ppp_iframe" ).slideUp( 300, function() { });
			glsKeresoZaras();
			$( "#foxpost_valaszto" ).slideUp( 300, function() { });		

			$('#szall_leiras_p').html($('#szall_mod_select option[value="5"]').attr('attr_leiras'));	

		}
		else
		{
			$('#szall_mod_select option[value="1"]').removeAttr('disabled');			
			$('#szall_mod_select option[value="4"]').removeAttr('disabled');			
			$('#szall_mod_select option[value="6"]').removeAttr('disabled');
			$('#szall_mod_select option[value="8"]').removeAttr('disabled');

			if ($('#szall_mod_select').val() == 5)
			{
				$('#szall_mod_select').val('1');
				$('#szall_leiras_p').html($('#szall_mod_select option[value="1"]').attr('attr_leiras'));
			}
			
			$('#szall_mod_select').select2({minimumResultsForSearch: -1});			
		}

		$( "#fiz_leiras_p" ).animate({
			height: [ "toggle", "swing" ]
		}, 300, function(){
			$('#fiz_leiras_p').html($('#fiz_mod_select option:selected').attr('attr_leiras'));
			$( "#fiz_leiras_p" ).animate({
				height: [ "toggle", "swing" ]
			}, 300);
		});
	});
	$(document).on("change", "#szall_mod_select", function() {
		var ertek = $('#szall_mod_select').val();
		if (ertek == 5)
		{
			$('#fiz_mod_select option[value="4"]').removeAttr('disabled');
			$('#fiz_mod_select option[value="2"]').attr('disabled','disabled');
			$('#fiz_mod_select option[value="3"]').attr('disabled','disabled');

			$('#fiz_mod_select').val('4');

			$('#fiz_mod_select').select2({minimumResultsForSearch: -1});	

			$('#fiz_leiras_p').html($('#fiz_mod_select option[value="4"]').attr('attr_leiras'));		
		}
		else
		{
			if ($('#fiz_mod_select').val() == '4')
			{
				$('#fiz_mod_select').val('2');
				$('#fiz_leiras_p').html($('#fiz_mod_select option[value="2"]').attr('attr_leiras'));
			}	

			$('#fiz_mod_select option[value="2"]').removeAttr('disabled');
			$('#fiz_mod_select option[value="3"]').removeAttr('disabled');
			$('#fiz_mod_select option[value="4"]').attr('disabled','disabled');		

			$('#fiz_mod_select').select2({minimumResultsForSearch: -1});		
		}	

		$( "#szall_leiras_p" ).animate({
			height: [ "toggle", "swing" ]
		}, 300, function(){
			$('#szall_leiras_p').html($('#szall_mod_select option:selected').attr('attr_leiras'));
			$( "#szall_leiras_p" ).animate({
				height: [ "toggle", "swing" ]
			}, 300);
			if ($('#szall_mod_select option:selected').val() == 4)
			{
				$( "#ppp_iframe" ).slideDown( 300, function() { });
				$( "#ppp_span" ).html('');
				ppKeresoZaras();
				glsKeresoZaras();
				$( "#foxpost_valaszto" ).slideUp( 300, function() { });
			}
			else if ($('#szall_mod_select option:selected').val() == 3)
			{
				document.getElementById('ppp_shopCode').value = '';
				document.getElementById('ppp_shopType').value = '';
				document.getElementById('ppp_city').value = '';
				document.getElementById('ppp_address').value = '';
				$( "#ppp_span" ).html('');				
				ppKeresoNyitas();
				$( "#ppp_iframe" ).slideUp( 300, function() { });
				glsKeresoZaras();
				$( "#foxpost_valaszto" ).slideUp( 300, function() { });
			}	
			else if ($('#szall_mod_select option:selected').val() == 6 || $('#szall_mod_select option:selected').val() == 8)
			{
				document.getElementById('ppp_shopCode').value = '';
				document.getElementById('ppp_shopType').value = '';
				document.getElementById('ppp_city').value = '';
				document.getElementById('ppp_address').value = '';
				$( "#ppp_span" ).html('');				
				ppKeresoZaras();
				$( "#ppp_iframe" ).slideUp( 300, function() { });
				$( "#ppp_span" ).html('');
				glsKeresoZaras();
				glsKeresoNyitas();
				$( "#foxpost_valaszto" ).slideUp( 300, function() { });
			}	
			else if ($('#szall_mod_select option:selected').val() == 7)
			{
				document.getElementById('ppp_shopCode').value = '';
				document.getElementById('ppp_shopType').value = '';
				document.getElementById('ppp_city').value = '';
				document.getElementById('ppp_address').value = '';
				$( "#ppp_span" ).html('');				
				$( "#foxpost_valaszto" ).slideDown( 300, function() { });
				$( "#ppp_iframe" ).slideUp( 300, function() { });
				$( "#ppp_span" ).html('');
				ppKeresoZaras();
				glsKeresoZaras();
			}							
			else
			{
				$( "#ppp_iframe" ).slideUp( 300, function() { });
				document.getElementById('ppp_shopCode').value = '';
				document.getElementById('ppp_shopType').value = '';
				document.getElementById('ppp_city').value = '';
				document.getElementById('ppp_address').value = '';
				$( "#ppp_span" ).html('');		
				ppKeresoZaras();
				glsKeresoZaras();	
				$( "#foxpost_valaszto" ).slideUp( 300, function() { });	

				// segéd mezők
				document.getElementById("csomagpont_nev").value='';
				document.getElementById("csomagpont_varos").value='';
				document.getElementById("csomagpont_irsz").value='';
				document.getElementById("csomagpont_cim").value='';				
			}				
		});
	});
	
	
	function phonenumber(inputtxt)
	{
	  var phoneno = /((?:\+?3|0)6)(?:|\()?(\d{1,2})(?:|\))?(\d{4})(\d{3,4})/g;
	  if(inputtxt.value.match(phoneno))
	     {
		   return true;      
		 }
	   else
	     {
		   return false;
	     }
	}	

	function varos(inputtxt)
	{
	  var szamok = /\d/g;
	  if(inputtxt.value.match(szamok))
	     {
		   return false;      
		 }
	   else
	     {
		   return true;
	     }
	}	

	function adoszam(adoszam)
	{
		var formatum = /^[0-9]{8}-[0-9]-[0-9]{2}$/;
	  	if(adoszam.value.match(formatum))
     	{
	   		return true;      
	 	}
	   	else
     	{
	   		return false;
     	}		
	}	

	function irsz(inputtxt)
	{
	  var szamok = /^[0-9]+$/;
	  if(inputtxt.value.match(szamok))
	     {
		   return true;      
		 }
	   else
	     {
		   return false;
	     }
	}	

	function validateEmail(email) 
    {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }	

	$(document).on("input", "#cim_irszam, #cim_szall_irszam", function() {
		var irsz = $(this).val();
		var mezo = $(this).data('mezo');

		if (irsz.length == 4) {
			$.post(domain+'/webshop/irsz_kereso.php?script=ok',{
					irsz : irsz
				},function(response,status){ // Required Callback Function
					if (response != '') {
						$('#' + mezo).val(response);
					}
			});
		}
	});

// Rendelés leadása
	$(document).on("click", "#rendeles_leadasa", function() {
		$( ".riaszt_div" ).css('display', 'none');
		var telefon_check = document.getElementById("telefon");
		var varos_check = document.getElementById("cim_varos");
		var adoszam_check = document.getElementById("adoszam");
		var irszam_check = document.getElementById("cim_irszam");

		var hiba = '';

		if($('#belepve').val() == 0)
		{
			if($('#nev').val() == ''){
				$( "#nev_riaszt" ).css('display', 'block');
				if (hiba == '') { hiba = 'nev'; }
			}
			if($('#email').val() == ''){
				$( "#email_riaszt" ).css('display', 'block');
				if (hiba == '') { hiba = 'email'; }
			}
			if($('#telefon').val() == ''){
				$( "#telefon_riaszt" ).css('display', 'block');
				if (hiba == '') { hiba = 'telefon'; }
			}
			if(phonenumber(telefon_check) == false){
				$( "#telefon_riaszt2" ).css('display', 'block');
				if (hiba == '') { hiba = 'telefon'; }
			}			
			if($('#szla_nev').val() == ''){
				$( "#szla_nev_riaszt" ).css('display', 'block');
				if (hiba == '') { hiba = 'szla_nev'; }
			}			
			if($('#cim_irszam').val() == ''){
				$( "#cim_irszam_riaszt" ).css('display', 'block');
				if (hiba == '') { hiba = 'cim_irszam'; }
			}
			if(irsz(irszam_check) == false){
				$( "#cim_irszam_riaszt2" ).css('display', 'block');
				if (hiba == '') { hiba = 'cim_irszam'; }
			}			
			if($('#cim_varos').val() == ''){
				$( "#cim_varos_riaszt" ).css('display', 'block');
				if (hiba == '') { hiba = 'cim_varos'; }
			}
			if(varos(varos_check) == false){
				$( "#cim_varos_riaszt2" ).css('display', 'block');
				if (hiba == '') { hiba = 'cim_varos'; }
			}			
			if($('#cim_utca').val() == ''){
				$( "#cim_utca_riaszt" ).css('display', 'block');
				if (hiba == '') { hiba = 'cim_utca'; }
			}
			if($('#cim_hszam').val() == ''){
				$( "#cim_hszam_riaszt" ).css('display', 'block');
				if (hiba == '') { hiba = 'cim_hszam'; }
			}
			if(adoszam(adoszam_check) == false && $('#adoszam').val() != '' && $('#cegadoszam').is(":checked")){
				$( "#adoszam_riaszt" ).css('display', 'block');
				if (hiba == '') { hiba = 'adoszam'; }
			}			
			// Ha regisztrál
			if ($("#regisztracio_checkbox").is(":checked")) {
				if($('#jelszo').val() == ''){
					$( "#jelszo_riaszt" ).css('display', 'block');
					if (hiba == '') { hiba = 'jelszo'; }
				}
				if($('#jelszo').val() != $('#jelszo2').val()){
					$( "#jelszo_riaszt2" ).css('display', 'block');
					if (hiba == '') { hiba = 'jelszo'; }
				}
				if($('#feltetel').is(":checked") != true){
					$( "#feltetel_riaszt" ).css('display', 'block');
					if (hiba == '') { hiba = 'feltetel'; }
				}
			}
			
	        var x = $('#email').val();
	        //var atpos = x.indexOf("@");
	        //var dotpos = x.lastIndexOf(".");

	        if (validateEmail(x) == false)
	        {
	          	$( "#email_riaszt2" ).css('display', 'block');
	          	if (hiba == '') { hiba = 'email'; }
	        }

	        if (hiba != '') {
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#osszesites_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#" + hiba ).focus();
				$('html, body').animate({
					scrollTop: $("#adatok_horgony").offset().top - 80
				}, 500);				
				return false;
	        }	        
		}
		// Postapont
		if($('#szall_mod_select').val() == 3 && $('#valasztott_postapont').val() == '')
		{
			document.getElementById("pp_riaszt").innerHTML='<div class="alert alert-danger alert-dismissable text-primary mb-0">Válassz Posta Pontot!</p></div>';
			$( "#osszesites_tartalom" ).slideUp( 300, function() { });
			$( "#adatok_tartalom" ).slideUp( 300, function() { });
			$( "#szall_mod_tartalom" ).slideDown( 300, function() { });
			$('html, body').animate({
				scrollTop: $('#szall_mod_horgony').offset().top - 80
			}, 500);
			return false;
		} 

		//Pickpackpont
		if($('#szall_mod_select').val() == 4 && ($('#ppp_shopType').val() == '' || $('#ppp_city').val() == '' || $('#ppp_address').val() == '' || $('#ppp_shopCode').val() == ''))
		{
			$( "#ppp_riaszt" ).css('display', 'block');
			$( "#osszesites_tartalom" ).slideUp( 300, function() { });
			$( "#adatok_tartalom" ).slideUp( 300, function() { });
			$( "#szall_mod_tartalom" ).slideDown( 300, function() { });
			$('html, body').animate({
				scrollTop: $('#szall_mod_horgony').offset().top - 80
			}, 500);
			return false;
		}

		//GLS csomagpont
		if(($('#szall_mod_select').val() == 6 || $('#szall_mod_select').val() == 8) && $('#valasztott_gls_cspont').val() == '')
		{
			document.getElementById("gls_riaszt").innerHTML='<div class="alert alert-danger alert-dismissable text-primary mb-0">Válassz GLS CsomagPontot vagy Automatát!</p></div>';
			$( "#osszesites_tartalom" ).slideUp( 300, function() { });
			$( "#adatok_tartalom" ).slideUp( 300, function() { });
			$( "#szall_mod_tartalom" ).slideDown( 300, function() { });
			$('html, body').animate({
				scrollTop: $('#szall_mod_horgony').offset().top - 80
			}, 500);
			return false;
		}	

		//Foxpost
		if($('#szall_mod_select').val() == 7 && $('#valasztott_foxpost_cspont').val() == '')
		{
			document.getElementById("fp_riaszt").innerHTML='<div class="alert alert-danger alert-dismissable text-primary mb-0">Válassz Foxpost átvételi pontot.</p></div>';
			$( "#osszesites_tartalom" ).slideUp( 300, function() { });
			$( "#adatok_tartalom" ).slideUp( 300, function() { });
			$( "#szall_mod_tartalom" ).slideDown( 300, function() { });
			$('html, body').animate({
				scrollTop: $('#szall_mod_horgony').offset().top - 80
			}, 500);
			return false;
		}			

		//if($('#vas_fel').is(":checked") != true){
			//$( "#vas_fel_riaszt" ).css('display', 'block');
			//return false;
		//}	

		if($('#adatv').is(":checked") != true){
			$( "#adatv_riaszt" ).css('display', 'block');
			return false;
		}				
		
		// Minden OK
		$.post(domain+'/webshop/keszlet_check.php?script=ok',function(response,status){ // Required Callback Function

			if (response == 0)
			{
				$("#rendeles_gomb_div").html('<div class="loader"></div>');
				$("#rendeles_leadas_form").submit();
			}
			else
			{
				window.location.href = domain+'/kosar/';
			}

		});	
	});

	function varosok_foxpost() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("foxpost_varos_div").innerHTML=xmlhttp.responseText;
				$('#foxpost_varos').select2();
			}
		  }
		xmlhttp.open("POST","../webshop/module/foxpost_varosok.php?script=ok",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("megye="+encodeURIComponent(document.getElementById("foxpost_megye").value));
	}
// Pontok keresése
	function foxpostKeres() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("foxpost_talalat_div").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("POST","../webshop/module/foxpost_talalatok.php?script=ok",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("varos="+encodeURIComponent(document.getElementById("foxpost_varos").value));
	}

	function foxpostKivalaszt(id) {
		document.getElementById("valasztott_foxpost_cspont").value = document.getElementById("foxpost_adat_"+id).value;
	}	

	function receiveMessage(event)
	{
		var data = jQuery.parseJSON(event.data);
		// alert (data.shopType+data.city+data.address);
		if (data.hasOwnProperty('shopType') && data.hasOwnProperty('city') && data.hasOwnProperty('address')) {
			jQuery('input#ppp_shopCode').val(data.shopCode);
			jQuery('input#ppp_shopType').val(data.shopType);
			jQuery('input#ppp_city').val(data.city);
			jQuery('input#ppp_address').val(data.address);
			$( "#ppp_span" ).html('<b>Választott Pick Pack Pont:</b> ' + $("#ppp_shopType").val() + ' ' + $('#ppp_address').val());
		}
	}
	
	window.addEventListener("message", receiveMessage, false);	