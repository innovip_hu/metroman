	<?php
		// Időzóna beállítás
		date_default_timezone_set('Europe/Budapest');
	
		if (!isset($_SESSION['login_id']))
		{
			if (isset($_POST['login-password']))
			{

				$email= addslashes($_POST['login-email']);
				$query = "SELECT * FROM ".$webjel."users WHERE email = ?";
				$res = $pdo->prepare($query);
				$res->execute(array($email));
				$row  = $res -> fetch();
				$userpw = password_verify($_POST['login-password'], $row['jelszo']);
				if ($userpw == true)
				{
					$_SESSION['login_id'] = $row['id'];
					$_SESSION['login_nev'] = $row['email'];
					if ($row['tipus'] == 'admin')
					{
						$_SESSION['login_tipus'] = 'admin';
					}
					else
					{
						$_SESSION['login_tipus'] = 'user';
					}
					unset($_SESSION['noreg_id']);

					$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."arlista WHERE ervenyes = 1 AND id=".$row['arlista_id']);
					$res->execute();
					$rownum = $res->fetchColumn();					

					if ($rownum > 0)
					{
						$_SESSION['arlista'] = $row['arlista_id'];
					}	

					if (!isset($_SESSION['kosar_id']))
					{
						$query = "SELECT kosar_id,uid FROM ".$webjel."kosar WHERE user_id = ? LIMIT 1";
						$res = $pdo->prepare($query);
						$res->execute(array($row['id']));
						$row_kosar_suti = $res -> fetch();	

						if (isset($row_kosar_suti['kosar_id']))
						{
							setcookie( "kosar_id", $row_kosar_suti['uid'], strtotime( '+7 days' ), "/" );
							$_SESSION['kosar_id'] = $row_kosar_suti['kosar_id'];
							$_SESSION['uid'] = $row_kosar_suti['uid'];
						}
					}
				}
				else
				{
					$uzenet = 'Hibás azonosító!';
				}
			}
		}
	
		// Kívánságlista törlése
		if (isset($_POST['command']) && $_POST['command'] == 'kivlistTORLES')
		{
			$deletecommand = "DELETE FROM ".$webjel."kivansag_lista WHERE id =".$_POST['id'];
			$result = $pdo->prepare($deletecommand);
			$result->execute();
		}
		
		//Kosár ürítése
		if (isset($_POST['command']) && $_POST['command'] == 'KOSAR_URIT')
		{
				$deletecommand = 'DELETE FROM '.$webjel.'kosar WHERE kosar_id='.$_SESSION['kosar_id'];
				$result = $pdo->prepare($deletecommand);
				$result->execute();
				setcookie( "kosar_id", $_SESSION['uid'], 1, "/" );
				unset($_SESSION['kosar_id']);
				unset($_SESSION['uid']);
		}
		
	// Szűrés
		if (isset($_GET['termek_parameter_ertek']) || isset($_SESSION['termek_parameter_ertek'])) // Ár szerinti szűrés
		{
			if(isset($_GET['termek_parameter_ertek']))
			{
				$_SESSION['termek_parameter_ertek'] = $_GET['termek_parameter_ertek'];
			}
			else
			{
				unset($_SESSION['termek_parameter_ertek']);
			}
			// Vissza az első oldalra
			$_SESSION['oldszam'] = 0;
		}
		else if ($oldal != 'termekek') // Ár szerinti szűrés
		{
			unset($_SESSION['termek_parameter_ertek']);
		}
		
	?>