<?php
if (isset($_GET['script']))
{
	session_start();
	include '../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	$van_hiba = 0;

	if (isset($_SESSION['kosar_id']))
	{
		$query = "SELECT term_id,term_db FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];
		foreach ($pdo->query($query) as $value)
		{
			$query_termek = "SELECT raktaron FROM ".$webjel."termekek WHERE rendelheto = 0 AND id=".$value['term_id'];
			$res = $pdo->prepare($query_termek);
			$res->execute();
			$row_termek = $res -> fetch();		

			if (isset($row_termek['raktaron']) && $value['term_db'] > $row_termek['raktaron'])
			{
				$van_hiba = 1;
			}
		}
	}
	else
	{
		$van_hiba = 1;
	}

	echo $van_hiba;
}
?>

