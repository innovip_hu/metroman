<p><b>Válassz Posta Pontot.</b></p>
<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	// Szűrők
	$szuro = '';
	if($_POST['pp_posta'] == 'true')
	{
		$szuro = " AND tipus='posta' ";
	}
	else if($_POST['pp_mol'] == 'true')
	{
		$szuro = " AND tipus='mol' ";
	}
	else if($_POST['pp_autom'] == 'true')
	{
		$szuro = " AND tipus='automata' ";
	}
	else if($_POST['pp_coop'] == 'true')
	{
		$szuro = " AND tipus='coop' ";
	}

	$query = "SELECT * FROM ".$webjel."postapontok WHERE varos='".$_POST['varos']."' ".$szuro." ORDER BY nev ASC";
	$elso = 1;
	foreach ($pdo->query($query) as $row)
	{
		if($row['tipus'] == 'posta')
		{
			$kep = 'pp_MagyarPostaLogisztika.png';
		}
		else if($row['tipus'] == 'mol')
		{
			$kep = 'pp_MOL_logo_color.png';
		}
		else if($row['tipus'] == 'automata')
		{
			$kep = 'pp_csomagautomata_logo.png';
		}
		else if($row['tipus'] == 'coop')
		{
			$kep = 'pp_coop_logo.png';
		}
		
		if ($elso == 1)
		{
			print '<div class="pp_talalatok" style="border-top: 1px solid #ccc;">';
			$elso = 0;
		}
		else
		{
			print '<div class="pp_talalatok">';
		}
			
			// print '<option value="'.$row['varos'].'">'.$row['varos'].'</option>';
			print '<input onClick="ppKivalaszt('.$row['id'].')" type="radio" id="pp_talalat_'.$row['id'].'" name="pp_talalat" class="pp_talalat" value="'.$row['id'].'">
			<label for="pp_talalat_'.$row['id'].'">
				<h5>
				<img src="'.$domain.'/webshop/images/'.$kep.'">'.$row['nev'].' <span>'.$row['cim'].'</span></h5>
			</label>';
			print '<input type="hidden" id="pp_adat_'.$row['id'].'" value="'.$row['nev'].' - '.$row['irszam'].' '.$row['varos'].', '.$row['cim'].'" />';
		print '</div>';
	}
	
	
?>
