<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	$automata = 0;

	if (isset($_GET['automata'])) {
		$automata = $_GET['automata'];
	}

	print '<select id="gls_varos" onChange="glsKeres()">
			<option value="">Válassz várost</option>';
		if ($_POST['megye'] == 'Pest') {
			$query = "SELECT ".$webjel."varos_megye.megye, 
						".$webjel."varos_megye.varos, 
						".$webjel."gls_csomagpontok.varos 
				FROM ".$webjel."varos_megye 
				INNER JOIN ".$webjel."gls_csomagpontok 
				ON ".$webjel."gls_csomagpontok.varos LIKE CONCAT('%', ".$webjel."varos_megye.varos, '%')
				WHERE ".$webjel."varos_megye.megye='".$_POST['megye']."' AND ".$webjel."gls_csomagpontok.automata = $automata
				GROUP BY ".$webjel."gls_csomagpontok.varos 
				ORDER BY ".$webjel."gls_csomagpontok.varos ASC";
		}
		else
		{
			$query = "SELECT ".$webjel."varos_megye.megye, 
						".$webjel."varos_megye.varos, 
						".$webjel."gls_csomagpontok.varos 
				FROM ".$webjel."varos_megye 
				INNER JOIN ".$webjel."gls_csomagpontok 
				ON ".$webjel."varos_megye.varos=".$webjel."gls_csomagpontok.varos 
				WHERE ".$webjel."varos_megye.megye='".$_POST['megye']."' AND ".$webjel."gls_csomagpontok.automata = $automata
				GROUP BY ".$webjel."gls_csomagpontok.varos 
				ORDER BY ".$webjel."gls_csomagpontok.varos ASC";
		}
		foreach ($pdo->query($query) as $row)
		{
			print '<option value="'.$row['varos'].'">'.$row['varos'].'</option>';
		}
	print '</select>';