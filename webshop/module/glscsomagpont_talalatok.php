<?php 
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}

	$automata = 0;

	if (isset($_GET['automata'])) {
		$automata = $_GET['automata'];
	}	
?>
<div class="row mt-2">
	<div class="col-md-12 margbot20">
	<?php if ($automata == 0): ?>
		<p><b>Válassz GLS CsomagPontot.</b></p>
	<?php else: ?>	
		<p><b>Válassz GLS Automatát.</b></p>
	<?php endif ?>
	
	<?php
		if($_POST['varos'] == 'Budapest')
		{
			$query = "SELECT * FROM ".$webjel."gls_csomagpontok WHERE automata=".$automata." AND varos LIKE '%".$_POST['varos']."%' ORDER BY nev ASC";
		}
		else
		{
			$query = "SELECT * FROM ".$webjel."gls_csomagpontok WHERE automata=".$automata." AND varos='".$_POST['varos']."' ORDER BY nev ASC";
		}
		$elso = 1;
		foreach ($pdo->query($query) as $row)
		{
			
			if ($elso == 1)
			{
				print '<div class="gls_talalatok" style="border-top: 1px solid #ccc;">';
				$elso = 0;
			}
			else
			{
				print '<div class="gls_talalatok">';
			}

				print '<input onClick="glsKivalaszt('.$row['id'].')" type="radio" id="gls_talalat_'.$row['id'].'" name="gls_talalat" value="'.$row['id'].'">
				<label for="gls_talalat_'.$row['id'].'" style="width: 90% !important;">
					<h5>
					<img src="'.$domain.'/webshop/images/gls-csomagpont-2023.jpg">'.$row['nev'].' <span>'.$row['cim'].'</span></h5>
				</label>';
				print '<input type="hidden" id="gls_adat_'.$row['id'].'" value="'.$row['nev'].' - '.$row['irszam'].' '.$row['varos'].' '.$row['cim'].'" data-id="'.$row['azonosito'].'" data-irsz="'.$row['irszam'].'" data-nev="'.$row['nev'].'" data-varos="'.$row['varos'].'" data-cim="'.$row['cim'].'" />';
			print '</div>';
		}
		
		
	?>
	</div>

</div>