<?php
	if (isset($_GET['id']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
		if (isset($_GET['darab']))
		{
			$term_db = $_GET['darab'] > 0 ? $_GET['darab'] : 1;
			$id = $_GET['id'];
			$query = "UPDATE ".$webjel."kosar SET term_db=? WHERE id=?";
			$result = $pdo->prepare($query);
			$result->execute(array($term_db,$id));
			// Kupon törlése
			$updatecommand = "UPDATE ".$webjel."kosar SET kupon_id=0 WHERE kosar_id=".$_SESSION['kosar_id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();
		}
		else if ($_GET['muvelet'] == 'plusz')
		{
			$term_db = $_GET['regidarab'] + 1;
			$id = $_GET['id'];
			$query = "UPDATE ".$webjel."kosar SET term_db=? WHERE id=?";
			$result = $pdo->prepare($query);
			$result->execute(array($term_db,$id));
			// Kupon törlése
			$updatecommand = "UPDATE ".$webjel."kosar SET kupon_id=0 WHERE kosar_id=".$_SESSION['kosar_id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();
		}
		else if ($_GET['muvelet'] == 'minusz')
		{
			$term_db = $_GET['regidarab'] - 1;
			$id = $_GET['id'];
			$query = "UPDATE ".$webjel."kosar SET term_db=? WHERE id=?";
			$result = $pdo->prepare($query);
			$result->execute(array($term_db,$id));
			// Kupon törlése
			$updatecommand = "UPDATE ".$webjel."kosar SET kupon_id=0 WHERE kosar_id=".$_SESSION['kosar_id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();
		}
	}
				$eltero_darabszam = array();

				$query = "SELECT term_id,term_db,id FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];
				foreach ($pdo->query($query) as $value)
				{
					$query_termek = "SELECT raktaron FROM ".$webjel."termekek WHERE rendelheto = 0 AND id=".$value['term_id'];
					$res = $pdo->prepare($query_termek);
					$res->execute();
					$row_termek = $res -> fetch();		

					if (isset($row_termek['raktaron']) && $value['term_db'] > $row_termek['raktaron'])
					{
						$eltero_darabszam[] = $value['term_id'];
					}

					$query_kos = "SELECT t.id, IF(t.akciosar > 0 AND t.akcio_ig >= NOW() AND t.akcio_tol <= NOW(), t.akciosar, 0) as akciosar, t.ar as normal_ar
									FROM ".$webjel."termekek t
									WHERE t.id=".$value['term_id'];
					$res = $pdo->prepare($query_kos);
					$res->execute();
					$row_termek = $res -> fetch();	

					$term_ara = $row_termek['akciosar'] > 0 ? $row_termek['akciosar'] : $row_termek['normal_ar'];
					$term_akcios_ar = $row_termek['akciosar'];	

					$arlista_azonosito = 0;

					if (isset($_SESSION['arlista']))
					{
						$query_arlista = "SELECT 
									".$webjel."arlista_arak.ar, 
									".$webjel."arlista_arak.ar_akcios,
									".$webjel."arlista_arak.akcio_tol,
									".$webjel."arlista_arak.akcio_ig,
									".$webjel."termekek.ar as term_ar
								FROM ".$webjel."arlista
								INNER JOIN ".$webjel."arlista_arak
								ON ".$webjel."arlista_arak.arlista_id = ".$webjel."arlista.id
								INNER JOIN ".$webjel."termekek
								ON ".$webjel."arlista_arak.termek_id = ".$webjel."termekek.id
								WHERE ".$webjel."arlista.id = ".$_SESSION['arlista']." AND ".$webjel."termekek.id=".$value['term_id'];
						$res = $pdo->prepare($query_arlista);
						$res->execute();
						$row_arlista = $res -> fetch();

						if (isset($row_arlista['ar']))
						{
							if ($row_arlista['akcio_ig'] >= date('Y-m-d') && $row_arlista['akcio_tol'] <= date('Y-m-d'))
							{
								$term_akcios_ar = $row_arlista['ar_akcios'];
								$term_ara = $row_arlista['ar_akcios'];
							}
							else
							{
								$term_akcios_ar = 0;
								$term_ara = $row_arlista['ar'];
							}

							$arlista_azonosito = $_SESSION['arlista'];
						}
					}						

					$query = "SELECT * FROM ".$webjel."termek_darab_kedvezmeny WHERE lathato=1 AND arlista_id=".$arlista_azonosito." AND termek_id=".$value['term_id']." AND `termek_db` <= ".$value['term_db']." ORDER BY termek_db DESC LIMIT 1";
					$res = $pdo->prepare($query);
					$res->execute();
					$row_kedvezmeny = $res -> fetch();

					if (isset($row_kedvezmeny['termek_ar']))
					{
						if ($row_kedvezmeny['tipus'] == "szazalek") {
							$mennyiseg_kedvezmeny = $term_ara - ($term_ara * ($row_kedvezmeny['termek_ar'] / 100));
							$mennyiseg_kedvezmeny =  $mennyiseg_kedvezmeny + (5 -  $mennyiseg_kedvezmeny % 5);
						} else {
							$mennyiseg_kedvezmeny = $term_ara - $row_kedvezmeny['termek_ar'];
						}	
											
						$query = "UPDATE ".$webjel."kosar SET term_akcios_ar=? WHERE id=?";
						$result = $pdo->prepare($query);
						$result->execute(array($mennyiseg_kedvezmeny,$value['id']));
					}	
					else
					{
						$query = "UPDATE ".$webjel."kosar SET term_akcios_ar=? WHERE id=?";
						$result = $pdo->prepare($query);
						$result->execute(array($term_akcios_ar,$value['id']));				
					}					
				}

				$query = "SELECT *, ".$webjel."kosar.id as id FROM ".$webjel."kosar 
						LEFT JOIN ".$webjel."kosar_tetel_termek_parameter_ertek 
						ON ".$webjel."kosar.id=".$webjel."kosar_tetel_termek_parameter_ertek.kosar_tetel_id 
						WHERE kosar_id=".$_SESSION['kosar_id']."
						GROUP BY ".$webjel."kosar.id";
				$db = 0;
				$ar = 0;
				$kupon_kedvezmeny = 0;
				print '<div class="table-responsive d-none d-lg-block">';
				print '<table class="table-custom table-custom-kek table-cart">
					<thead>
						<tr>
							<th class="text-left kosar_mobilon_eltunik"></th>
							<th class="text-left">Termék név</th>
							<th class="text-left kosar_mennyiseg_oszlop">Mennyiség</th>
							<th class="text-right kosar_mobilon_eltunik">Egységár</th>
							<th class="text-right kosar_mobilon_eltunik">Összesen</th>
							<th style="border-right: 1px solid #e1e1e1;"></td>
						</tr>
					</thead>
					<tbody>';
				$items_google = array();
				$items_google_list = 0;
				foreach ($pdo->query($query) as $row)
				{
					$items_google_list++;
					//termék adatai
					$query_term = "SELECT * FROM ".$webjel."termekek WHERE id=".$row['term_id'];
					$res_term = $pdo->prepare($query_term);
					$res_term->execute();
					$row_term  = $res_term -> fetch();
					
					$db = $db + 1;
					// Ha OVIP termék
					if ($row_term['ovip_id'] !=0)
					{
						$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_term['id']." ORDER BY alap DESC LIMIT 1";
						$res = $pdo->prepare($query_kep);
						$res->execute();
						$row_kep = $res -> fetch();
						$alap_kep = $row_kep ? $row_kep['kep'] : "";
						if ($alap_kep == '') 
						{
							$kep_link = $domain.'/webshop/images/noimage.png';
						}
						else
						{
							if($row_kep['thumb'] != '')
							{
								$kep = $row_kep['thumb'];
							}
							else
							{
								$kep = $row_kep['kep'];
							}
							if ($row_kep['ovip_termek_id'] == 0)
							{
								$kep = $domain.'/images/termekek/'.$kep;
							}
							$kep_link = $kep;
						}
						$kep = $kep_link;
					}
					elseif ($row['term_kep'] == "")
					{
						$kep = $domain.'/webshop/images/noimage.png';
					}
					else
					{
						$kep = $domain.'/images/termekek/'.$row['term_kep'];
					}
					if($row['term_akcios_ar'] == 0) //ha nem akciós
					{
						$term_ar = $row['term_ar'];
					}
					else //ha akciós
					{
						$term_ar = $row['term_akcios_ar'];
					}				

					$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_term['csop_id'];
					$res_csop = $pdo->prepare($query_csop);
					$res_csop->execute();
					$row_csop  = $res_csop -> fetch();

      				$items_google[] = array('id' => $row['term_id'], 'name' => $row['term_nev'], 'list_name' => 'Kosár termékek', "category" => $row_csop['nev'], "list_position" => $items_google_list, "quantity" => $row['term_db'], "price" => $term_ar);					
					
					print '<tr>
						<td style="min-width: auto; width: 10%;" class="kosar_mobilon_eltunik"><a class="table-cart-figure" href="'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row_term['nev_url'].'"><img src="'.$kep.'" alt="'.$row['term_nev'].'"/></a></td>
						<td style="width: 25%; text-align: left;"><a style="white-space: normal;" class="table-cart-link" href="'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row_term['nev_url'].'">'.$row['term_nev'].'
							<div class="kosar_mobilon_ar"><div class="price">'.number_format($term_ar, 0, ',', ' ').' Ft</div></div>
							';
						// Jellemzők
						$query_jell = "SELECT * FROM ".$webjel."kosar_tetel_termek_parameter_ertek WHERE kosar_tetel_id = ".$row['id']." ORDER BY termek_parameter_nev ASC";
						$elso = 1;
						foreach ($pdo->query($query_jell) as $row_jell)
						{
							if($elso == 1) { print ''; $elso = 0; } else { print '<br/>'; }
							print '<p class="small mt-0">';
							print $row_jell['termek_parameter_nev'].': '.$row_jell['termek_parameter_ertek_nev'];
							print '</p>';
						}		

						if (in_array($row['term_id'], $eltero_darabszam))
						{
							print '<span style="color: #e74c3c; font-size: 14px; display: block;">Az adott termékből maximum rendelhető: '.$row_term['raktaron'].'</span>';
						}									
							
						print '</a></td>
						<td class="text-left">';
							if($config_keszlet_kezeles == 'I')
							{
								print '<input type="hidden" name="raktaron" id="raktaron_'.$row['id'].'" value="'.$row_term['raktaron'].'"/>';
								if ($row_term['rendelheto'] == 1)
								{
									$max_darab = 1000;
								}
								else
								{
									$max_darab = $row_term['raktaron'];
								}
								
							}
							else
							{
								print '<input type="hidden" name="raktaron" id="raktaron_'.$row['id'].'" value="999999999"/>';
								$max_darab = 1000;
							}
							?>
		                    <div class="stepper-style-2">
		                      <input type="number" data-zeros="true" name="darab" id="darab_<?=$row['id']?>" value="<?=$row['term_db']?>" min="1" max="<?=$max_darab?>" onchange="showTetelek(<?=$row['id']?>)">
		                    </div>

							<?php
							/*
								print '<div class="input-group input-group-sm mb-3">
								  <div class="input-group-prepend">
								    <a onclick="showTetelekPlusz('.$row['id'].')" class="button button-outline button-xs sajat-gombhover"><span class="fa fa-plus"></span></a>
								    <a onclick="showTetelekMinusz('.$row['id'].')" class="button button-outline button-xs sajat-gombhover" style="margin-top: 0px;"><span class="fa fa-minus"></span></a>
								  </div>
								  <input onchange="showTetelek('.$row['id'].')" type="text" name="darab" id="darab_'.$row['id'].'" value="'.$row['term_db'].'" class="form-input" readonly style="min-width: 70px; text-align: center;"><div class="kosar_mobilon_torol_gomb">';
								  ?><a onclick="document.getElementById('tartalomTorlese_<?php print $row['id']; ?>').submit();" class="btn" ><span class="fa fa-trash text-secondary"></span></a><?php
								print '</div></div>';

								*/

							print '
						</td>
						<td class="text-right kosar_mobilon_eltunik" style="white-space: nowrap;"><div class="price">'.number_format($term_ar, 0, ',', ' ').' Ft</div></td>
						<td class="text-right kosar_mobilon_eltunik" style="white-space: nowrap;"><div class="price price-total" style="color: #151515;">'.number_format($term_ar * $row['term_db'], 0, ',', ' ').' Ft</div></td>
						<td style="border-right: 1px solid #e1e1e1;"><form id="tartalomTorlese_'.$row['id'].'" action="" method="post">';
								?>
									<a onclick="document.getElementById('tartalomTorlese_<?php print $row['id']; ?>').submit();" class="button button-xs" data-yuspremovefromcart="<?=$row['term_id']?>"><span class="fa fa-trash text-secondary" style="    font-size: 16px;"></span></a>
								<?php
								print '<input type="hidden" name="id" value="'.$row['id'].'"/>
								<input type="hidden" name="command" value="TORLES"/>
								</form></td>
					</tr>';
					$ar = $ar + ($term_ar * $row['term_db']);
				}
				print '<input type="hidden" id="kosar_erteke_kuponhoz" value="'.$ar.'" />';
				
				$ingyen_szalitas = 0;
				$kedv_ar = 0;
				if($row['kupon_id'] > 0)
				{
					print '<tr>
						<td style="min-width: auto; width: 10%;"></td>
						<td class="text-left">
							Kupon kedvezmény:';
							$query_kupon = "SELECT * FROM ".$webjel."kuponok WHERE id=".$row['kupon_id'];
							$res = $pdo->prepare($query_kupon);
							$res->execute();
							$row_kupon = $res -> fetch();
							if($row_kupon['kupon_fajta'] == 'Ingyenes szállítás')
							{
								print 'Ingyenes szállítás';
								$kedv_ar = 0;
							}
							else if($row_kupon['kupon_fajta'] == 'Kedvezmény')
							{
								print '-'.$row_kupon['kedvezmeny'];
								if ($row_kupon['kedv_tipus'] == 'szazalek')
								{
									print '% kedvezmény';
									$kedv_ar = $ar*$row_kupon['kedvezmeny']/100*(-1);
								}
								else if ($row_kupon['kedv_tipus'] == 'osszeg')
								{
									print 'Ft kedvezmény';
									$kedv_ar = $row_kupon['kedvezmeny']*(-1);
								}
							}
							else if($row_kupon['kupon_fajta'] == 'Ajándék termék')
							{
								$query_term = "SELECT * FROM ".$webjel."termekek WHERE id=".$row_kupon['termek_id'];
								$res = $pdo->prepare($query_term);
								$res->execute();
								$row_term = $res -> fetch();
								$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_term['csop_id'];
								$res_csop = $pdo->prepare($query_csop);
								$res_csop->execute();
								$row_csop  = $res_csop -> fetch();
								print 'Ajándék: <a href="'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row_term['nev_url'].'">'.$row_term['nev'].'</a>';
								$kedv_ar = 0;
							}
							else
							{
								print $row_kupon['kupon_fajta'];
							}
						
						print '</td>
						<td></td>
						<td class="text-right"><div class="price">'.number_format($kedv_ar, 0, ',', ' ').' Ft</div></td>
						<td class="text-right"><div class="price price-total">'.number_format($kedv_ar, 0, ',', ' ').' Ft</div></td>
						<td></td>
					</tr>';
				}
				
				print '<tr>
							<td colspan="6" class="text-right bg-kek"><div class="price price-total big"><b>Fizetendő: '.number_format($ar+$kedv_ar, 0, ',', ' ').' Ft</b></div></td>
						</tr>';

				print '</tbody></table>';
				print '</div>';		


?>
			<input type="hidden" value='<?php echo json_encode($items_google); ?>' id="kosar_tartalma_google">

			<div class="d-lg-none">
				<h6 class="text-center bg-kek py-2">A kosár tartalma</h6>
			<?php
				$query = "SELECT *, ".$webjel."kosar.id as id FROM ".$webjel."kosar 
						LEFT JOIN ".$webjel."kosar_tetel_termek_parameter_ertek 
						ON ".$webjel."kosar.id=".$webjel."kosar_tetel_termek_parameter_ertek.kosar_tetel_id 
						WHERE kosar_id=".$_SESSION['kosar_id']."
						GROUP BY ".$webjel."kosar.id";
				$db = 0;
				$ar = 0;
				$kupon_kedvezmeny = 0;

				foreach ($pdo->query($query) as $row)
				{
					//termék adatai
					$query_term = "SELECT * FROM ".$webjel."termekek WHERE id=".$row['term_id'];
					$res_term = $pdo->prepare($query_term);
					$res_term->execute();
					$row_term  = $res_term -> fetch();
					
					$db = $db + 1;
					// Ha OVIP termék
					if ($row_term['ovip_id'] !=0)
					{
						$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_term['id']." ORDER BY alap DESC LIMIT 1";
						$res = $pdo->prepare($query_kep);
						$res->execute();
						$row_kep = $res -> fetch();
						$alap_kep = $row_kep['kep'];
						if ($alap_kep == '') 
						{
							$kep_link = $domain.'/webshop/images/noimage.png';
						}
						else
						{
							if($row_kep['thumb'] != '')
							{
								$kep = $row_kep['thumb'];
							}
							else
							{
								$kep = $row_kep['kep'];
							}

							if ($row_kep['ovip_termek_id'] == 0)
							{
								$kep = $domain.'/images/termekek/'.$kep;
							}
														
							$kep_link = $kep;
						}
						$kep = $kep_link;
					}
					elseif ($row['term_kep'] == "")
					{
						$kep = $domain.'/webshop/images/noimage.png';
					}
					else
					{
						$kep = $domain.'/images/termekek/'.$row['term_kep'];
					}
					if($row['term_akcios_ar'] == 0) //ha nem akciós
					{
						$term_ar = $row['term_ar'];
					}
					else //ha akciós
					{
						$term_ar = $row['term_akcios_ar'];
					}

					$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_term['csop_id'];
					$res_csop = $pdo->prepare($query_csop);
					$res_csop->execute();
					$row_csop  = $res_csop -> fetch();
												
					if($config_keszlet_kezeles == 'I')
					{
						if ($row_term['rendelheto'] == 1)
						{
							$max_darab = 1000;
						}
						else
						{
							$max_darab = $row_term['raktaron'];
						}
						
					}
					else
					{
						$max_darab = 1000;
					}

					$ar = $ar + ($term_ar * $row['term_db']);

					?>
						<div class="row row-10">
								<div class="col-4 align-self-center"><img src="<?=$kep?>"></div>
								<div class="col-8 mobil-kosar-elemek">
									<div class="row row-10">
										<div class="col-12"><b><a style="white-space: normal;" class="table-cart-link text-bold" href="<?=$domain?>/termekek/<?=$row_csop['nev_url']?>/<?=$row_term['nev_url']?>"><?=$row['term_nev']?></a></b>
											<?php 
												if (in_array($row['term_id'], $eltero_darabszam))
												{
													print '<span style="color: #e74c3c; font-size: 14px; display: block;">Az adott termékből maximum rendelhető: '.$row_term['raktaron'].'</span>';
												}	
											?>
										</div>
										<div class="col-12">
						                    <div class="stepper-style-2">
						                      <input type="number" data-zeros="true" name="darab" id="darab2_<?=$row['id']?>" value="<?=$row['term_db']?>" min="1" max="<?=$max_darab?>" onchange="showTetelek_mobil(<?=$row['id']?>)">
						                    </div>
											<a onclick="document.getElementById('tartalomTorlese_<?php print $row['id']; ?>').submit();" class="button-xs mt-0" data-yuspremovefromcart="<?=$row['term_id']?>"><span class="fa fa-trash text-secondary" style="    font-size: 16px;"></span></a></div>
										<div class="col-12">Egységár: <?=number_format($term_ar, 0, ',', ' ')?> Ft</div>
										<div class="col-12">Összesen: <?=number_format($term_ar * $row['term_db'], 0, ',', ' ')?> Ft</div>
									</div>
								</div>
							</div>

					<?php
				}

				$ingyen_szalitas = 0;
				$kedv_ar = 0;
				if($row['kupon_id'] > 0)
				{
					print '<div class="row row-10">
						<div class="col-4"></div>
						<div class="col-8 mobil-kosar-elemek">
						<div class="row row-10">
							<div class="col-12">
							<b>Kupon kedvezmény:</b>';
							$query_kupon = "SELECT * FROM ".$webjel."kuponok WHERE id=".$row['kupon_id'];
							$res = $pdo->prepare($query_kupon);
							$res->execute();
							$row_kupon = $res -> fetch();
							if($row_kupon['kupon_fajta'] == 'Ingyenes szállítás')
							{
								print 'Ingyenes szállítás';
								$kedv_ar = 0;
							}
							else if($row_kupon['kupon_fajta'] == 'Kedvezmény')
							{
								print '-'.$row_kupon['kedvezmeny'];
								if ($row_kupon['kedv_tipus'] == 'szazalek')
								{
									print '% kedvezmény';
									$kedv_ar = $ar*$row_kupon['kedvezmeny']/100*(-1);
								}
								else if ($row_kupon['kedv_tipus'] == 'osszeg')
								{
									print 'Ft kedvezmény';
									$kedv_ar = $row_kupon['kedvezmeny']*(-1);
								}
							}
							else if($row_kupon['kupon_fajta'] == 'Ajándék termék')
							{
								$query_term = "SELECT * FROM ".$webjel."termekek WHERE id=".$row_kupon['termek_id'];
								$res = $pdo->prepare($query_term);
								$res->execute();
								$row_term = $res -> fetch();
								$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_term['csop_id'];
								$res_csop = $pdo->prepare($query_csop);
								$res_csop->execute();
								$row_csop  = $res_csop -> fetch();
								print 'Ajándék: <a href="'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row_term['nev_url'].'">'.$row_term['nev'].'</a>';
								$kedv_ar = 0;
							}
							else
							{
								print $row_kupon['kupon_fajta'];
							}
						
						print '</div>
						<div class="col-12">Egységár: '.number_format($kedv_ar, 0, ',', ' ').' Ft</div>
						<div class="col-12">Összesen: '.number_format($kedv_ar, 0, ',', ' ').' Ft</div>
							</div>
						</div>
					</div>';

				}

				?>

				<div class="col-12 text-center mt-3 bg-kek py-2">
							Fizetendő: <?=number_format($ar+$kedv_ar, 0, ',', ' ')?> Ft
				</div>

			</div>

	
<?php
/*
				print '<div class="table-responsive">';
				print '<table class="table-custom table-custom-secondary table-cart mt-4">
					<thead>
						<tr>
							<th colspan="6" class="text-center">Szállítási költségek</th>
						</tr>
					</thead>
					<tbody>';								

				$query = "SELECT *,IF (EXISTS (SELECT 1 FROM ".$webjel."kassza_szall_mod_savok WHERE szall_mod_id = ".$webjel."kassza_szall_mod.id AND ar = 0),1,0) as van_ingyenes FROM ".$webjel."kassza_szall_mod WHERE lathato = 1 AND id != 5 ORDER BY sorrend ASC";	
				//echo $query;
				// AND (ingyenes_szallitas > 0 OR EXISTS (SELECT 1 FROM ".$webjel."kassza_szall_mod_savok WHERE szall_mod_id = ".$webjel."kassza_szall_mod.id AND ar = 0))
				foreach ($pdo->query($query) as $value)
				{
					$ingyszall_szoveg = '';
					if ($value['ingyenes_szallitas'] > 0)
					{
						$kosar_kulonbseg = $value['ingyenes_szallitas'] - ($ar+$kedv_ar);
					}
					else
					{
						$res = $pdo->prepare('SELECT sav FROM '.$webjel.'kassza_szall_mod_savok WHERE szall_mod_id = "'.$value['id'].'" AND ar = 0');
						$res->execute();
						$row_sav  = $res -> fetch();						
						$kosar_kulonbseg = $row_sav['sav'] - ($ar+$kedv_ar);						
					}
					if($kosar_kulonbseg > 0)
					{
						$ingy_szallhoz = number_format($kosar_kulonbseg, 0, ',', ' ');
						$ingyszall_szoveg = 'Az ingyenes kiszállításhoz még '.$ingy_szallhoz.' Ft-ért válassz terméket.';
					}
					elseif ($value['ingyenes_szallitas'] > 0 || $value['van_ingyenes'] > 0)
					{
						$ingyszall_szoveg = 'A kiszállítás ingyenes.';
					}

					$res = $pdo->prepare('SELECT * FROM '.$webjel.'kassza_szall_mod_savok WHERE sav <= '.($ar+$kedv_ar).' AND szall_mod_id = '.$value['id'].' ORDER BY sav DESC LIMIT 1 ');
					$res->execute();
					$row_sav  = $res -> fetch();					

					if ($value['korlatozott'] == 1)
					{
						$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE ppp_tiltas = 1 AND id IN (SELECT term_id FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'].")");
						$res->execute();
						$rownum = $res->fetchColumn();	


						if ($rownum > 0)
						{
							$ingyszall_szoveg = 'Szállítás nem lehetséges!';
						}					
					}

					if ($ingyszall_szoveg == 'A kiszállítás ingyenes.')
					{
						$row_sav['ar'] = 0;
					}

					$ikon = '';
					if ($value['ikon'] != '')
					{
						$ikon = '<img src="'.$domain.'/webshop/images/'.$value['ikon'].'"/>';
					}

					print '<tr class="border-top">
								<td class="ikon-kosar">'.$ikon.'</td>
								<td class="text-left"><span>'.$value['nev'].'</span></td>
								<td colspan="4" class="text-right"><div class="price price-total" >'.number_format($row_sav['ar'],0,',',' ').' Ft</div></td>
							</tr>';
					if ($ingyszall_szoveg != '')
					{
						print '<tr>
									<td colspan="6" class="text-center"><div class="price price-total" >'.$ingyszall_szoveg.'</div></td>
								</tr>';
					}

				}
				
				print '</tbody></table>';
				print '</div>';

				print '<div class="row row-50 justify-content-center mt-4 kosar_kupon">
				            <div class="col-md-10 col-lg-5 text-center">
				            	<div class="box-radio" style="text-align: inherit;">
						            <h6>Kuponkód beváltása</h6>
									<form method="POST" action="" id="kupon_form">
										<input type="text" name="kupon_kod" value="" placeholder="Kupon kód" id="input-coupon" class="form-input mt-2" style="width:100%;">
										<input type="hidden" id="kosar_erteke" name="kosar_erteke" value="'.$ar.'" />
										<button type="submit" class="button button-xs button-gray-8 mt-3">Bevált</button>
									</form>
								</div>
				            </div>
						</div>';

*/

						print '<div class="row row-30">';
						
						print '<div class="col-lg-8 col-12">';

						print '<div class="border p-3 h-100">';

						print '<h6>Szállítási költségek</h6>';


						$query = "SELECT *,IF (EXISTS (SELECT 1 FROM ".$webjel."kassza_szall_mod_savok WHERE szall_mod_id = ".$webjel."kassza_szall_mod.id AND ar = 0),1,0) as van_ingyenes FROM ".$webjel."kassza_szall_mod WHERE lathato = 1 AND id != 5 ORDER BY sorrend ASC";	
						//echo $query;
						// AND (ingyenes_szallitas > 0 OR EXISTS (SELECT 1 FROM ".$webjel."kassza_szall_mod_savok WHERE szall_mod_id = ".$webjel."kassza_szall_mod.id AND ar = 0))
						foreach ($pdo->query($query) as $value)
						{
							$ingyszall_szoveg = '';
							if ($value['ingyenes_szallitas'] > 0)
							{
								$kosar_kulonbseg = $value['ingyenes_szallitas'] - ($ar+$kedv_ar);
							}
							else
							{
								$res = $pdo->prepare('SELECT sav FROM '.$webjel.'kassza_szall_mod_savok WHERE szall_mod_id = "'.$value['id'].'" AND ar = 0');
								$res->execute();
								$row_sav  = $res -> fetch();						
								$kosar_kulonbseg = $row_sav['sav'] - ($ar+$kedv_ar);						
							}
							if($kosar_kulonbseg > 0)
							{
								$ingy_szallhoz = number_format($kosar_kulonbseg, 0, ',', ' ');
								$ingyszall_szoveg = '<span class="text-primary">Az ingyenes kiszállításhoz még '.$ingy_szallhoz.' Ft-ért válassz terméket.</span>';
							}
							elseif ($value['ingyenes_szallitas'] > 0 || $value['van_ingyenes'] > 0)
							{
								//$ingyszall_szoveg = 'A kiszállítás ingyenes.';
								$ingyen_szalitas = 1;
							}

							$res = $pdo->prepare('SELECT * FROM '.$webjel.'kassza_szall_mod_savok WHERE sav <= '.($ar+$kedv_ar).' AND szall_mod_id = '.$value['id'].' ORDER BY sav DESC LIMIT 1 ');
							$res->execute();
							$row_sav  = $res -> fetch();					

							if ($value['korlatozott'] == 1)
							{
								$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE ppp_tiltas = 1 AND id IN (SELECT term_id FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'].")");
								$res->execute();
								$rownum = $res->fetchColumn();	


								if ($rownum > 0)
								{
									$ingyszall_szoveg = '<span class="text-primary">Szállítás nem lehetséges!</span>';
								}					
							}

							$sav_ar = number_format($row_sav['ar'],0,',',' ').' Ft';

							if ($ingyen_szalitas == 1)
							{
								$sav_ar = 'ingyenes';
							}

							print '<p class="small">
										<span>'.$value['nev'].'</span>
										<span class="pull-right">'.$sav_ar.'</span>';

							if ($ingyszall_szoveg != '')
							{
								print '<br>'.$ingyszall_szoveg;
							}										
							
							print '</p>';

						}

						print '<p class="small"><b>Szállítás kiválasztása a következő oldalon történik!</b></p>';


						print '</div>';
						print '</div>';

				        print '<div class="col-lg-4 col-12 text-center kosar_kupon">
				            	<div class="box-radio h-100 justify-content-center d-flex flex-column" style="text-align: inherit;">
				            		<h6>Kuponkód beváltása</h6>
				            		<div id="kupontLenyito"><p><a href="javascript:void(0)" onclick="nezetValt()" class="small">Ha rendelkezel kuponkóddal, ide kattintva érvényesítheted.</a></p></div>
				            		<div id="kupontBevaltDiv" class="d-none">
				            			<p class="small">Add meg az alábbi mezőben a kuponkódod, majd érvényesítsd a gomb segítségével.</p>
										<form method="POST" action="" id="kupon_form">
											<input type="text" name="kupon_kod" value="" placeholder="Kupon kód" id="input-coupon" class="form-input mt-2" style="width:100%;">
											<input type="hidden" id="kosar_erteke" name="kosar_erteke" value="'.$ar.'" />
											<button type="submit" class="button button-xs button-gray-8 mt-3">Bevált</button>
										</form>
									</div>
								</div>
				            </div>';

						
						print '</div>';