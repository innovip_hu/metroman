<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	print '<div style="float:left; width:100%; margin-top: 10px;"><a href="" class="reszletek_gomb" style="margin-bottom:4px;">Vissza</a></div>';

	// Összehasonlított termék számának meghatározása
	$termek_szam = 0;
	if (isset($_SESSION['osszahas_1']))
	{
		$termek_szam = $termek_szam + 1;
		if ($config_ovip == 'I')
		{
			$query_1 = 'SELECT * FROM '.$ovipjel.'termekek WHERE id='.$_SESSION['osszahas_1'];
		}
		else
		{
			$query_1 = 'SELECT * FROM '.$webjel.'termekek WHERE id='.$_SESSION['osszahas_1'];
		}
		$res_1 = $pdo->prepare($query_1);
		$res_1->execute();
		$row_1  = $res_1 -> fetch();
		if ($config_ovip == 'I')
		{
			$query_csop_1 = 'SELECT * FROM '.$ovipjel.'term_csoportok WHERE id='.$row_1['csop_id'];
		}
		else
		{
			$query_csop_1 = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_1['csop_id'];
		}
		$res_csop_1 = $pdo->prepare($query_csop_1);
		$res_csop_1->execute();
		$row_csop_1  = $res_csop_1 -> fetch();
	}
	if (isset($_SESSION['osszahas_2']))
	{
		$termek_szam = $termek_szam + 1;
		if ($config_ovip == 'I')
		{
			$query_2 = 'SELECT * FROM '.$ovipjel.'termekek WHERE id='.$_SESSION['osszahas_2'];
		}
		else
		{
			$query_2 = 'SELECT * FROM '.$webjel.'termekek WHERE id='.$_SESSION['osszahas_2'];
		}
		$res_2 = $pdo->prepare($query_2);
		$res_2->execute();
		$row_2  = $res_2 -> fetch();
		if ($config_ovip == 'I')
		{
			$query_csop_2 = 'SELECT * FROM '.$ovipjel.'term_csoportok WHERE id='.$row_2['csop_id'];
		}
		else
		{
			$query_csop_2 = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_2['csop_id'];
		}
		$res_csop_2 = $pdo->prepare($query_csop_2);
		$res_csop_2->execute();
		$row_csop_2  = $res_csop_2 -> fetch();
	}
	if (isset($_SESSION['osszahas_3']))
	{
		$termek_szam = $termek_szam + 1;
		if ($config_ovip == 'I')
		{
			$query_3 = 'SELECT * FROM '.$ovipjel.'termekek WHERE id='.$_SESSION['osszahas_3'];
		}
		else
		{
			$query_3 = 'SELECT * FROM '.$webjel.'termekek WHERE id='.$_SESSION['osszahas_3'];
		}
		$res_3 = $pdo->prepare($query_3);
		$res_3->execute();
		$row_3  = $res_3 -> fetch();
		if ($config_ovip == 'I')
		{
			$query_csop_3 = 'SELECT * FROM '.$ovipjel.'term_csoportok WHERE id='.$row_3['csop_id'];
		}
		else
		{
			$query_csop_3 = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_3['csop_id'];
		}
		$res_csop_3 = $pdo->prepare($query_csop_3);
		$res_csop_3->execute();
		$row_csop_3  = $res_csop_3 -> fetch();
	}
	if (isset($_SESSION['osszahas_4']))
	{
		$termek_szam = $termek_szam + 1;
		if ($config_ovip == 'I')
		{
			$query_4 = 'SELECT * FROM '.$ovipjel.'termekek WHERE id='.$_SESSION['osszahas_4'];
		}
		else
		{
			$query_4 = 'SELECT * FROM '.$webjel.'termekek WHERE id='.$_SESSION['osszahas_4'];
		}
		$res_4 = $pdo->prepare($query_4);
		$res_4->execute();
		$row_4  = $res_4 -> fetch();
		if ($config_ovip == 'I')
		{
			$query_csop_4 = 'SELECT * FROM '.$ovipjel.'term_csoportok WHERE id='.$row_4['csop_id'];
		}
		else
		{
			$query_csop_4 = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_4['csop_id'];
		}
		$res_csop_4 = $pdo->prepare($query_csop_4);
		$res_csop_4->execute();
		$row_csop_4  = $res_csop_4 -> fetch();
	}
	if ($termek_szam == 4) { $szelesseg = 20; }
	else if ($termek_szam == 3) { $szelesseg = 25; }
	else if ($termek_szam == 2) { $szelesseg = 33; }
?>
<!--Fejléc-->
<div class="osszehas_sor">
	<div class="osszahas_oszlop" style="width:calc(<?php print $szelesseg; ?>% - 10px);">
		<b>&nbsp;</b>
	</div>
	<?php
	if (isset($_SESSION['osszahas_1']))
	{
		if ($row_1['kep'] != '')
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);">
					<a href="http://'.$domain.'/termekek/'.$row_csop_1['nev_url'].'/'.$row_1['nev_url'].'">
						<img src="http://'.$domain.'/images/termekek/'.$row_1['kep'].'" width="100%">
					</a>
				</div>';
		}
		else
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);">
					<a href="http://'.$domain.'/termekek/'.$row_csop_1['nev_url'].'/'.$row_1['nev_url'].'">
						<img src="http://'.$domain.'/webshop/images/noimage.png" width="100%">
					</a>
				</div>';
		}
	}
	if (isset($_SESSION['osszahas_2']))
	{
		if ($row_2['kep'] != '')
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);">
					<a href="http://'.$domain.'/termekek/'.$row_csop_2['nev_url'].'/'.$row_2['nev_url'].'">
						<img src="http://'.$domain.'/images/termekek/'.$row_2['kep'].'" width="100%">
					</a>
				</div>';
		}
		else
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);">
					<a href="http://'.$domain.'/termekek/'.$row_csop_2['nev_url'].'/'.$row_2['nev_url'].'">
						<img src="http://'.$domain.'/webshop/images/noimage.png" width="100%">
					</a>
				</div>';
		}
	}
	if (isset($_SESSION['osszahas_3']))
	{
		if ($row_3['kep'] != '')
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);">
					<a href="http://'.$domain.'/termekek/'.$row_csop_3['nev_url'].'/'.$row_3['nev_url'].'">
						<img src="http://'.$domain.'/images/termekek/'.$row_3['kep'].'" width="100%">
					</a>
				</div>';
		}
		else
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);">
					<a href="http://'.$domain.'/termekek/'.$row_csop_3['nev_url'].'/'.$row_3['nev_url'].'">
						<img src="http://'.$domain.'/webshop/images/noimage.png" width="100%">
					</a>
				</div>';
		}
	}
	if (isset($_SESSION['osszahas_4']))
	{
		if ($row_4['kep'] != '')
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);">
					<a href="http://'.$domain.'/termekek/'.$row_csop_4['nev_url'].'/'.$row_4['nev_url'].'">
						<img src="http://'.$domain.'/images/termekek/'.$row_4['kep'].'" width="100%">
					</a>
				</div>';
		}
		else
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);">
					<a href="http://'.$domain.'/termekek/'.$row_csop_4['nev_url'].'/'.$row_4['nev_url'].'">
						<img src="http://'.$domain.'/webshop/images/noimage.png" width="100%">
					</a>
				</div>';
		}
	}
	?>
</div>
<!--Név-->
<div class="osszehas_sor">
	<div class="osszahas_oszlop" style="width:calc(<?php print $szelesseg; ?>% - 10px);">
		<b>Név</b>
	</div>
	<?php
	if (isset($_SESSION['osszahas_1']))
	{
		print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);">
			<a href="http://'.$domain.'/termekek/'.$row_csop_1['nev_url'].'/'.$row_1['nev_url'].'">'.$row_1['nev'].'</a>
		</div>';
	}
	if (isset($_SESSION['osszahas_2']))
	{
		print '<div class="osszahas_oszlop"  style="width:calc('.$szelesseg.'% - 10px);">
			<a href="http://'.$domain.'/termekek/'.$row_csop_2['nev_url'].'/'.$row_2['nev_url'].'">'.$row_2['nev'].'</a>
		</div>';
	}
	if (isset($_SESSION['osszahas_3']))
	{
		print '<div class="osszahas_oszlop"  style="width:calc('.$szelesseg.'% - 10px);">
			<a href="http://'.$domain.'/termekek/'.$row_csop_3['nev_url'].'/'.$row_3['nev_url'].'">'.$row_3['nev'].'</a>
		</div>';
	}
	if (isset($_SESSION['osszahas_4']))
	{
		print '<div class="osszahas_oszlop"  style="width:calc('.$szelesseg.'% - 10px);">
			<a href="http://'.$domain.'/termekek/'.$row_csop_4['nev_url'].'/'.$row_4['nev_url'].'">'.$row_4['nev'].'</a>
		</div>';
	}
	?>
</div>
<!--Ár-->
<div class="osszehas_sor osszehas_szines">
	<div class="osszahas_oszlop" style="width:calc(<?php print $szelesseg; ?>% - 10px);">
		<b>Ár</b>
	</div>
	<?php
	$datum = date("Y-m-d");
	if (isset($_SESSION['osszahas_1']))
	{
		if ($row_1['akcio_ig'] >= $datum && $row_1['akcio_tol'] <= $datum) //Akciós
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);"><font style="text-decoration: line-through;">'.number_format($row_1['ar'], 0, ',', ' ').' Ft</font><br><span class="term_ar" style="color: #FB1F78; font-size: 16px;"> '.number_format($row_1['akciosar'], 0, ',', ' ').' Ft</span></div>';
		}
		else
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);">'.number_format($row_1['ar'], 0, ',', ' ').' Ft</div>';
		}
	}
	if (isset($_SESSION['osszahas_2']))
	{
		if ($row_2['akcio_ig'] >= $datum && $row_2['akcio_tol'] <= $datum) //Akciós
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);"><font style="text-decoration: line-through;">'.number_format($row_2['ar'], 0, ',', ' ').' Ft</font><br><span class="term_ar" style="color: #FB1F78; font-size: 16px;"> '.number_format($row_2['akciosar'], 0, ',', ' ').' Ft</span></div>';
		}
		else
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);">'.number_format($row_2['ar'], 0, ',', ' ').' Ft</div>';
		}
	}
	if (isset($_SESSION['osszahas_3']))
	{
		if ($row_3['akcio_ig'] >= $datum && $row_3['akcio_tol'] <= $datum) //Akciós
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);"><font style="text-decoration: line-through;">'.number_format($row_3['ar'], 0, ',', ' ').' Ft</font><br><span class="term_ar" style="color: #FB1F78; font-size: 16px;"> '.number_format($row_3['akciosar'], 0, ',', ' ').' Ft</span></div>';
		}
		else
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);">'.number_format($row_3['ar'], 0, ',', ' ').' Ft</div>';
		}
	}
	if (isset($_SESSION['osszahas_4']))
	{
		if ($row_4['akcio_ig'] >= $datum && $row_4['akcio_tol'] <= $datum) //Akciós
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);"><font style="text-decoration: line-through;">'.number_format($row_4['ar'], 0, ',', ' ').' Ft</font><br><span class="term_ar" style="color: #FB1F78; font-size: 16px;"> '.number_format($row_4['akciosar'], 0, ',', ' ').' Ft</span></div>';
		}
		else
		{
			print '<div class="osszahas_oszlop" style="width:calc('.$szelesseg.'% - 10px);">'.number_format($row_4['ar'], 0, ',', ' ').' Ft</div>';
		}
	}
	?>
</div>
