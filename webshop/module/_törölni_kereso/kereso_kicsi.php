<style>
	#kereso_kicsi {
		position: relative;
		padding: 9px 20px 0 0;
		float: right;
	}
	#kereso_kicsi .gomb_kereso {
		float: right;
		background: url('http://<?php print $domain; ?>/webshop/module/kereso/bg_gomb_kereso.png') repeat-x left top #354d6d;
		display: block;
		font-size: 16px;
		line-height: normal;
		color: #fcfcfc;
		font-weight: bold;
		font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		padding: 7px 13px 12px 14px;
		margin-left: 2px;
		width: 80px;
		height: 41px;
		cursor: pointer;
	}
	#kereso_kicsi .gomb_kereso:hover {
		background: #CAC9C9;
	}
	#kereso_kicsi input {
		float: left;
		border: 1px solid #cac9c9;
		color: #4a4a4a;
		background: #fdfdfd;
		padding: 8px 10px 8px 10px;
		width: 190px;
		height: 23px;
		line-height: 23px;
		outline: none;
	}
</style>
<div id="kereso_kicsi">
	<form action="http://<?php print $domain; ?>/kereso/" method="GET">
		<input class="gomb_kereso" type="submit" value="Kereső">
		<input type="text" name="keres_nev" value="">
	</form>
</div>