		<h4>Új jelszó</h4>	
	
		<p class="text-center">Ha elfelejtetted jelszavad, mindössze annyi a dolgod, hogy add meg a beregisztrált e-mail címed, és elküldjük új jelszavad.</p>

		<form class="rd-form" method="POST">
			<div class="form-wrap">
				<input class="form-input" id="email" type="email" name="email">
				<label class="form-label" for="email">E-mail</label>
			</div>
			<div class="form-button text-center">
				<button class="button button-gray-5" type="submit">Küldés</button>
			</div>
			<input type="hidden" name="command" value="jelszoEML">
		</form>

	<?php
		if (isset($_POST['command']) && $_POST['command'] == 'jelszoEML')
		{
			include $gyoker."/webshop/jelszoeml_check.php";
			if (jelszoeml_check($_POST) == "rendben")
			{
			//random jelszó generálása
				function createRandomPassword()
				{
					$chars = "abcdefghijkmnopqrstuvwxyz023456789";
					srand((double)microtime()*1000000);
					$i = 0;
					$pass = '' ;
					while ($i <= 7)
					{
						$num = rand() % 33;
						$tmp = substr($chars, $num, 1);
						$pass = $pass . $tmp;
						$i++;
					}
					return $pass;
				}
				$jelszo = createRandomPassword();
			//sql feltöltés
				$updatecommand = "UPDATE ".$webjel."users SET jelszo = '".password_hash($jelszo, PASSWORD_DEFAULT)."'  WHERE email = '".$_POST['email']."'";
				$result = $pdo->prepare($updatecommand);
				$result->execute();

				$mess =  "Kedves Látogató!". "<br><br>" ;
				$mess .=  "Az új jelszavad: ".$jelszo."<br>Jelszavad megváltoztathatod a Profil menüpontban.<br><br>" ;
				$mess .=  "<br><br>Üdvözlettel: <a href='".$domain."' style='color: blue;' target='_blank'>".$webnev."</a>" ;

				$htmlmsg = '<html>
					<body>
						'.$mess.'
					</body>
				</html>';

				require_once($gyoker.'/webshop/PHPMailer-master/PHPMailerAutoload.php');
				$mail = new PHPMailer();
				$mail->isHTML(true);
				$mail->CharSet = 'UTF-8';
				// $mail->addCustomHeader('MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n");
				if($conf_smtp == 1)
				{
					// SMTP
					$mail->IsSMTP(); // telling the class to use SMTP
					$mail->Host       = $smtp_host; // SMTP server
					$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
															   // 1 = errors and messages
															   // 2 = messages only
					$mail->SMTPAuth   = true;  
					$mail->SMTPSecure = $smtp_protokol;                 // enable SMTP authentication
					$mail->Host       = $smtp_host; // sets the SMTP server
					$mail->Port       = $smtp_port;                    // set the SMTP port for the GMAIL server
					$mail->Username   = $smtp_user; // SMTP account username
					$mail->Password   = $smtp_pass;        // SMTP account password
					$mail->SetFrom($smtp_email, $smtp_name);
					$mail->AddReplyTo($smtp_email, $smtp_name);
				}
				else
				{
					$mail->SetFrom($email, $webnev);
				}

				$mail->AddAddress($_POST['email'],'Látogató');
				$mail->Subject = "Új jelszó érkezett";
				$htmlmsg = $mess;
				//file_put_contents('filename.php', $htmlmsg);
				$mail->Body = $htmlmsg;
				if(!$mail->Send()) {
					echo "Mailer Error: " . $mail->ErrorInfo;
				}				

				print "<br><p style='color: #02876c;'>A jelszavad megváltozott!<br>Az új jelszavadat elküldtük az e-mail címedre.</p>";
			}
			else
			{
				print jelszoeml_check($_POST);
			}
		}
	?>
