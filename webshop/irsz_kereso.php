<?php

	if (!isset($_GET['script']) && !isset($_POST['irsz'])) {
		exit();
	}

	session_start();
	include '../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	$query = "SELECT varos FROM ".$webjel."varosok WHERE irsz = ? LIMIT 1";
	$res = $pdo->prepare($query);
	$res->execute(array($_POST['irsz']));
	$row = $res -> fetch();	

	if ($row) {
		echo $row['varos'];
	}