	<script>
	gtag('event', 'checkout_progress', {
	  "checkout_step": 1
	});
	</script>

	<?php
		if (isset($_SESSION['kosar_id']) && $_SESSION['kosar_id'] != 0)
		{
			$kupon = '';
			if(isset($_POST['kupon_kod']))
			{
				$query = "SELECT * FROM ".$webjel."kuponok WHERE kod='".$_POST['kupon_kod']."' AND rendeles_id=0 AND indul<='".date("Y-m-d")."' AND vege>='".date("Y-m-d")."'";
				$res = $pdo->prepare($query);
				$res->execute();
				$row = $res -> fetch();
				
				if(isset($row['id']))
				{
					if($row['min_kosar_ertek'] > $_POST['kosar_erteke'])
					{
						print '<div class="alert text-white bg-red-2 alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							A kosár értéke nem éri el a kuponban meghatározott minimum értéket ('.$row['min_kosar_ertek'].'Ft)!
						</div>';
					}
					else
					{
						$updatecommand = "UPDATE ".$webjel."kosar SET kupon_id='".$row['id']."' WHERE kosar_id=".$_SESSION['kosar_id'];
						$result = $pdo->prepare($updatecommand);
						$result->execute();
						$kupon = 'ok';
						if($row['egyszeri'] == 1) // Ha egyszeri, akkor érvényteleníteni kell
						{
							$updatecommand = "UPDATE ".$webjel."kuponok SET rendeles_id='".$_SESSION['kosar_id']."' WHERE id=".$row['id'];
							$result = $pdo->prepare($updatecommand);
							$result->execute();
						}
					}
				}
				else
				{
					print '<div class="alert text-white bg-red-2 alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							A megadott kuponkód hibás, vagy már nem érvényes!
					</div>';
				}
			}
	?>
		<script language="javascript">
			//kosár elemei darabszám beírásra
				function showTetelek(str)
				{
					if (window.XMLHttpRequest)
					  {// code for IE7+, Firefox, Chrome, Opera, Safari
					  xmlhttp=new XMLHttpRequest();
					  }
					else
					  {// code for IE6, IE5
					  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
					  }
					xmlhttp.onreadystatechange=function()
					  {
					  if (xmlhttp.readyState==4 && xmlhttp.status==200)
						{
							document.getElementById("kosar_elemei").innerHTML=xmlhttp.responseText;
							document.getElementById("kosar_erteke").value = document.getElementById("kosar_erteke_kuponhoz").value;
							$("input[type=number]").stepper({
								labels: {
									up: "",
									down: ""
								}
							});

							$.get(domain+'/webshop/kosar_darabszam.php?script=ok',function(response,status){ // Required Callback Function
						    		$('#kosar-darabszam').html(response);
							}); 							
						}
					  }
					xmlhttp.open("GET","<?php print $domain; ?>/webshop/module/mod_kosar_elemei.php?darab="+document.getElementById("darab_"+str).value+"&id="+str,true);
					xmlhttp.send();
				}
				function showTetelek_mobil(str)
				{
					if (window.XMLHttpRequest)
					  {// code for IE7+, Firefox, Chrome, Opera, Safari
					  xmlhttp=new XMLHttpRequest();
					  }
					else
					  {// code for IE6, IE5
					  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
					  }
					xmlhttp.onreadystatechange=function()
					  {
					  if (xmlhttp.readyState==4 && xmlhttp.status==200)
						{
							document.getElementById("kosar_elemei").innerHTML=xmlhttp.responseText;
							document.getElementById("kosar_erteke").value = document.getElementById("kosar_erteke_kuponhoz").value;
							$("input[type=number]").stepper({
								labels: {
									up: "",
									down: ""
								}
							});

							$.get(domain+'/webshop/kosar_darabszam.php?script=ok',function(response,status){ // Required Callback Function
						    		$('#kosar-darabszam').html(response);
							}); 							
						}
					  }
					xmlhttp.open("GET","<?php print $domain; ?>/webshop/module/mod_kosar_elemei.php?darab="+document.getElementById("darab2_"+str).value+"&id="+str,true);
					xmlhttp.send();
				}				
			//kosár elemei PLUSZ
				function showTetelekPlusz(str,term_id)
				{
					if(Number(document.getElementById("darab_"+str).value) < Number(document.getElementById("raktaron_"+str).value)){
						if (window.XMLHttpRequest)
						  {// code for IE7+, Firefox, Chrome, Opera, Safari
						  xmlhttp=new XMLHttpRequest();
						  }
						else
						  {// code for IE6, IE5
						  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
						  }
						xmlhttp.onreadystatechange=function()
						  {
						  if (xmlhttp.readyState==4 && xmlhttp.status==200)
							{
								document.getElementById("kosar_elemei").innerHTML=xmlhttp.responseText;
								document.getElementById("kosar_erteke").value = document.getElementById("kosar_erteke_kuponhoz").value;
							}
						  }
						xmlhttp.open("GET","<?php print $domain; ?>/webshop/module/mod_kosar_elemei.php?muvelet=plusz&regidarab="+document.getElementById("darab_"+str).value+"&id="+str+"&term_id="+term_id,true);
						xmlhttp.send();
					}
				}
			//kosár elemei MINUSZ
				function showTetelekMinusz(str,term_id)
				{
					if(document.getElementById("darab_"+str).value - 1 < 1)
						{
									return;
						}
					if (window.XMLHttpRequest)
					  {// code for IE7+, Firefox, Chrome, Opera, Safari
					  xmlhttp=new XMLHttpRequest();
					  }
					else
					  {// code for IE6, IE5
					  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
					  }
					xmlhttp.onreadystatechange=function()
					  {
					  if (xmlhttp.readyState==4 && xmlhttp.status==200)
						{
							document.getElementById("kosar_elemei").innerHTML=xmlhttp.responseText;
							document.getElementById("kosar_erteke").value = document.getElementById("kosar_erteke_kuponhoz").value;
						}
					  }
					xmlhttp.open("GET","<?php print $domain; ?>/webshop/module/mod_kosar_elemei.php?muvelet=minusz&regidarab="+document.getElementById("darab_"+str).value+"&id="+str+"&term_id="+term_id,true);
					xmlhttp.send();
				}

				function googleLista()
				{
					var lista = JSON.parse(document.getElementById("kosar_tartalma_google").value);

					var arr = new Array();

					for (var i = 0; i < lista.length; i++) {
						arr.push(lista[i]);
					}

					gtag('event', 'begin_checkout', { "items" : lista });
				}		

				function nezetValt(gomb)
				{	
					$('#kupontLenyito').addClass('d-none');
					$('#kupontBevaltDiv').removeClass('d-none');
				}		
		</script>
	<?php
			//termék törlése
			if (isset($_POST['command']) && $_POST['command'] == "TORLES")
			{
				$deletecommand = 'DELETE FROM '.$webjel.'kosar WHERE id = '.$_POST['id'];
				$stmt = $pdo->prepare($deletecommand);
				$stmt->bindParam(':id', $_POST['id'], PDO::PARAM_INT);	
				$stmt->execute();
				// Kupon törlése
				$updatecommand = "UPDATE ".$webjel."kosar SET kupon_id=0 WHERE kosar_id=".$_SESSION['kosar_id'];
				$result = $pdo->prepare($updatecommand);
				$result->execute();
			}
			// Van-e termék a kosárban
			$res = $pdo->prepare('SELECT * FROM '.$webjel.'kosar WHERE kosar_id='.$_SESSION['kosar_id']);
			$res->execute();
			$rownum = $res->fetchColumn();
			if ($rownum == 0)
			{
				$pdo->exec('DELETE FROM '.$webjel.'kosar WHERE kosar_id='.$_SESSION['kosar_id']);
				setcookie( "kosar_id", $_SESSION['uid'], 1, "/" );
				unset($_SESSION['kosar_id']);
				unset($_SESSION['uid']);
				
				header('Location: '.$domain.'/kosar/');
			}
			else
			{
					print '<div id="kosar_elemei">';
						include ('module/mod_kosar_elemei.php');
					print '</div>';

			}

			?>

		          <div class="row mt-4 row-20">
		          	<?php /*
		            <div class="col-md-6 text-center text-md-left">
		              <!-- RD Mailform-->
						<form id="kosar_uritese" action="" method="post">
							<button type="submit" class="btn btn-link btn-sm"><span class="fa fa-times"></span> Kosár ürítése</button>
							<input type="hidden" name="command" value="KOSAR_URIT"/>
						</form>                	
		            </div>
		            */ ?>
		          	<div class="col-md-12 text-center text-md-right">
						<a class="button button-default-kek termek_kosarba_gomb" href="<?=$domain?>/kassza" onclick="googleLista()">Adatok megadása</a>
					</div>
		          </div>

			<?php
		}
		else
		{
			print '<center><h3>A kosár tartalma üres.</h3>';
			print '<p><a class="button button-gradient button-lg" href="https://www.metroman.hu/termekek/">Tovább a termékekre</a></p>';
			print '<img src="'.$domain.'/webshop/images/ures_kosar.png" border="0"></center>';
		}
	?>
	