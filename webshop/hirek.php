<?php
	if (isset($_GET['nev_url']) && $_GET['nev_url'] != '') //  belépve
	{
		$res = $pdo->prepare("SELECT * FROM ".$webjel."hirek where nev_url='".$_GET['nev_url']."'");
		$res->execute();
		$row  = $res -> fetch();
		$time_input = $row['datum'];
		$date = DateTime::createFromFormat( 'Y-m-d', $time_input);
		$ujdatum = $date->format( 'Y.m.d.');
		print '<div class="col-12">';
		// if ($row['kep'] != '' )
		// {
			// print '<img src="'.$domain.'/images/termekek/'.$row['kep'].'" width="400" style="float:right; margin: 0 0 10px 20px;"/>';
		// }
		print $row['tartalom'];
		print '<p class="mt-5">'.$ujdatum.'</p>';
		print '<a href="'.$domain.'/hirek/" class="button button-xs button-primary-outline-2 mt-3" >Vissza</a>';
		print '</div>';
	
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirek_termekek WHERE hir_id=?");
		$res->execute(array($row['id']));
		$rownum = $res->fetchColumn();			

		if ($rownum > 0) {
			$query = "SELECT t.* FROM `".$webjel."termekek` t WHERE (t.csop_id IN ( SELECT ht.csop_id FROM ".$webjel."hirek_termekek ht WHERE ht.hir_id = ".$row['id']." ) OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok tcs WHERE tcs.termek_id= t.id AND tcs.termek_csoport_id IN ( SELECT ht.csop_id FROM ".$webjel."hirek_termekek ht WHERE ht.hir_id = ".$row['id']." ))) AND t.kifutott = 0 AND t.lathato = 1 AND t.ovip_torolt = 0 ORDER BY RAND() LIMIT 6";

			print '<div class="row row-30 kapcs_termekek pt-5">';

			print '<div class="col-12"><h4 class="heading-4 text-center pb-2">Kapcsolódó termékek</h4></div>';
			$szamlalo=1;
			foreach ($pdo->query($query) as $row)
			{
				include $gyoker.'/webshop/termek.php';	
			}
			print '</div>';			
		}
	}
	else // Hírek lista
	{
		//print '<h3>Hírek</h3>';
		$datum = date("Y-m-d");
		if ($oldal == "fooldal") {
			$query = "SELECT * FROM `".$webjel."hirek` Where `datum` <= '".$datum."' ORDER BY `datum` DESC, `id` DESC LIMIT 3";
		} else {
			$query = "SELECT * FROM `".$webjel."hirek` Where `datum` <= '".$datum."' ORDER BY `datum` DESC, `id` DESC";
		}
		
		$szamlali_hir = 0;
		foreach ($pdo->query($query) as $row)
		{
			$time_input = $row['datum'];
			$date = DateTime::createFromFormat( 'Y-m-d', $time_input);
			$datum_nap = $date->format( 'd');
			$datum_honap = $date->format( 'm');
			$datum_ev = $date->format( 'Y');
			if($datum_honap == '01'){$datum_honap='január';}
			else if($datum_honap == '02'){$datum_honap='február';}
			else if($datum_honap == '03'){$datum_honap='március';}
			else if($datum_honap == '04'){$datum_honap='április';}
			else if($datum_honap == '05'){$datum_honap='május';}
			else if($datum_honap == '06'){$datum_honap='június';}
			else if($datum_honap == '07'){$datum_honap='július';}
			else if($datum_honap == '08'){$datum_honap='augusztus';}
			else if($datum_honap == '09'){$datum_honap='szeptember';}
			else if($datum_honap == '10'){$datum_honap='október';}
			else if($datum_honap == '11'){$datum_honap='november';}
			else if($datum_honap == '12'){$datum_honap='december';}

			$datum_teljes = $datum_ev . '. ' .$datum_honap. ' ' .$datum_nap.'.';

			$query_kep = "SELECT * FROM ".$webjel."hir_kepek WHERE hir_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
			$res = $pdo->prepare($query_kep);
			$res->execute();
			$row_kep = $res -> fetch();
			
			if (isset($row_kep['kep'])) {
				$kep = $domain.'/images/termekek/'.$row_kep['kep'];
			} else {
				$kep = $domain.'/webshop/images/noimage.png';
			}	

			$link = $domain.'/hirek/'.$row['nev_url'];	
			
			?>

            <div class="col-sm-4">
              <!--post-->
              <article class="post block-xl"><img class="post-image" src="<?=$kep?>" alt="<?=$row['cim']?>"/>
                <div class="post-content">
                  <div><a class="post-tag post-tag-gradient" href="<?=$domain?>/hirek">Hírek</a>
                    <div class="heading-5 post-title"><a href="<?=$link?>"><?=$row['cim']?></a></div>
                  </div>
                  <div class="post-time">
                    <time datetime="<?=$row['datum']?>"><?=$datum_teljes?></time>
                  </div>
                </div>
                <div class="post-dummy"></div>
              </article>
            </div>

            <?php			
		}
	}
?>
