<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	if (isset($oldal) && $oldal == 'kassza' && isset($_POST['command']) && $_POST['command'] == 'KESZ') // Ha épp a kasszámál befejezi a fizetést, akkor a paypal miatt ne jelenjn meg
	{
			// Semmi!
	}
	else
	{
		?>
		<script src="<?php print $domain; ?>/webshop/scripts/jquery.tabSlideOut.js"></script>
		<?php
			if(isset($_SESSION['kosar_id']))
			{
				$contact_tab_kep = 'contact_tab_on.png';
			}
			else
			{
				$contact_tab_kep = 'contact_tab.png';
			}
		?>
		<script>
			 $(function(){
				 $('.slide-out-div').tabSlideOut({
					 tabHandle: '.handle',                              //class of the element that will be your tab
					 pathToTabImage: '<?php print $domain; ?>/webshop/images/<?php print $contact_tab_kep; ?>',   //path to the image for the tab (optionaly can be set using css)
					 imageHeight: '172px',                               //height of tab image
					 imageWidth: '48px',                               //width of tab image    
					 tabLocation: 'right',                               //side of screen where tab lives, top, right, bottom, or left
					 speed: 300,                                        //speed of animation
					 action: 'click',                                   //options: 'click' or 'hover', action to trigger animation
					 topPos: '248px',                                   //position from the top
					 fixedPosition: true                               //options: true makes it stick(fixed position) on scroll
				 });
			 });
		</script>
		<div class="slide-out-div" id="iderepul">
			<a class="handle" href="http://link-for-non-js-users"></a>
			<h4 style="padding-bottom:0;">Kosár tartalma</h4>
			<?php
				if (isset($_SESSION['kosar_id']))
				{
					?>
					<div style="font-size:12px; height:190px; overflow:auto; margin-bottom:10px;" >
						<table align="center" cellpadding="0" cellspacing="6" width="230" style="border-collapse: initial; width: 200px;">
							<?php
								$db = 0;
								$ar = 0;
								$query = "SELECT * FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];
								foreach ($pdo->query($query) as $row)
								{
									$db = $db + $row['term_db'];
									if($row['term_akcios_ar'] == 0) //ha nem akciós
									{
										$ar = $ar + ($row['term_ar'] * $row['term_db']);
									}
									else //ha akciós
									{
										$ar = $ar + ($row['term_akcios_ar'] * $row['term_db']);
									}
									$query_termlink = "SELECT * FROM ".$webjel."termekek where id=".$row['term_id'];
									$res = $pdo->prepare($query_termlink);
									$res->execute();
									$row_termlink = $res -> fetch();
									
									print '<tr valign="top">';
									$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_termlink['csop_id'];
									$res = $pdo->prepare($query_csop);
									$res->execute();
									$row_csop = $res -> fetch();
									print '<td align="center" style="padding-top: 6px;"><a href="'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row_termlink['nev_url'].'" title="'.$row['term_nev'].'" class="product-image">';
									// Ha OVIP termék
									if ($row_termlink['ovip_id'] !=0)
									{
										$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_termlink['id']." ORDER BY alap DESC LIMIT 1";
										$res = $pdo->prepare($query_kep);
										$res->execute();
										$row_kep = $res -> fetch();
										$alap_kep = $row_kep['kep'];
										if ($alap_kep == '') 
										{
											$kep_link = $domain.'/webshop/images/noimage.png';
										}
										else
										{
											if($row_kep['thumb'] != '')
											{
												$kep = $row_kep['thumb'];
											}
											else
											{
												$kep = $row_kep['kep'];
											}
											$kep_link = $kep;
										}
										echo '<img src="'.$kep_link.'" width="60" style=" margin-right:8px;" alt="'.$row['term_nev'].'">';
									}
									elseif ($row['term_kep'] == "")
									{
										print '<img src="'.$domain.'/webshop/images/noimage.png" width="60" style=" margin-right:8px;" alt="'.$row['term_nev'].'">';
									}
									else
									{
										print '<img src="'.$domain.'/images/termekek/'.$row['term_kep'].'" style="max-width:60px; max-height:60px; margin-right:8px;" alt="'.$row['term_nev'].'">';
									}
									print '</a></td>';
									print '<td style="padding-top: 6px;" align="left">
												'.$row['term_nev'].'
												<br>
												'.$row['term_db'].' db <br>';
												if($row['term_akcios_ar'] == 0) //ha nem akciós
												{
													print number_format(($row['term_ar'] * $row['term_db']), 0, ',', ' ').' Ft';
												}
												else
												{
													print number_format(($row['term_akcios_ar'] * $row['term_db']), 0, ',', ' ').' Ft';
												}
											print '</td>';
									print '</tr>';
								}
							?>
						</table>
					</div>
					<div style="font-size:12px;" >
						<table align="center" cellpadding="0" cellspacing="6" width="230" style="border-collapse: initial; width: 210px;">
							<tr>
								<td style="text-align: right;">Összesen:<b> <?php print number_format($ar, 0, ',', ' '); ?> Ft</b></td>
							</tr>
						</table>
					</div>
					<div style="font-size:12px; text-align:center; width:100%; padding-top: 10px;" >
						<table align="center" cellpadding="0" cellspacing="6" width="230" style="border-collapse: initial; width: 210px;">
							<tr>
								<td style="text-align: left; font-size: 18px;"><a href="<?php print $domain; ?>/kosar/">Kosár</a></td>
								<td style="text-align: right; font-size: 18px;"><a href="<?php print $domain; ?>/kassza/">FIZETEK</a></td>
							</tr>
						</table>
					</div>
					<?php
				}
				else
				{
					print '<p style="font-size:16px; padding-top: 30px;">A kosár tartalma üres.</p>';
				}
			?>
		</div>
	<?php
	}
	?>
