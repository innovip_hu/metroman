﻿<?php
	function jelszoeml_check($adatok)
	{
		include '../config.php';
		if($adatok['email'] == "" )
		{
			return "<p style='color: red;'>Minden mező kitöltése kötelező!</p>";
		}
		else
		{
			$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
			try
			{
				$pdo = new PDO(
				$dsn, $dbuser, $dbpass,
				Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
				);
			}
			catch (PDOException $e)
			{
				die("Nem lehet kapcsolódni az adatbázishoz!");
			}
			$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."users WHERE email = '".$adatok['email']."'");
			$res->execute();
			$rownum_email = $res->fetchColumn();
			if( $rownum_email == 0)
			{
				return "<p style='color: red;'>Nem regisztráltak ezzel az email címmel!</p>";
			}
			else
			{
				return "rendben";
			}
		}
	}
?>