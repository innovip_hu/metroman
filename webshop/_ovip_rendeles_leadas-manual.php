<?php
	if (!isset($_GET['rendeles_id'])) {
		exit();
	}

	$rendeles_id = $_GET['rendeles_id']; // ezt a változót meg kell adni az include előtt

	include '../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	// Rendelés adatai
	$query = "SELECT * FROM ".$webjel."rendeles WHERE id=".$rendeles_id;
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	// Felhasználó
	$query_user = "SELECT * FROM ".$webjel."rendeles_cimek where rendeles_id=".$row['id'];
	$res = $pdo->prepare($query_user);
	$res->execute();
	$row_user  = $res -> fetch();

	$ovip_szallitasi_nev = $row['vasarlo_nev'];
	$ovip_szallitasi_varos = $row['vasarlo_varos'];
	$ovip_szallitasi_cim = $row['vasarlo_cim'];
	$ovip_szallitasi_irsz = $row['vasarlo_irszam'];

	if ($row_user['cim_szall_nev'] != '')
	{
		$ovip_szallitasi_nev = $row_user['cim_szall_nev'];
		$ovip_szallitasi_varos = $row_user['cim_szall_varos'];
		$ovip_szallitasi_cim = $row_user['cim_szall_utca'].' '.$row_user['cim_szall_hszam'];
		$ovip_szallitasi_irsz = $row_user['cim_szall_irszam'];
	}

	$ovip_megjegyzes = $row['megjegyzes'];
	$delivery_point = '';

	if ($row['glscspont'] != '')
	{
		$ovip_megjegyzes .= ' GLS csomagpont: '.$row['glscspont'];
		$delivery_point = $row['glscspont_azonosito'];
	}

	if (trim($row['pickpackpont']) != '')
	{
		$ovip_megjegyzes .= ' PickPackPont: '.$row['pickpackpont'];
		$delivery_point = $row['ppp_azonosito'];
	}	

	$utanvet = 0;
	$utanvet_erteke = 0;
	$utanvet_afa = 0;	

	if ($row['fiz_mod'] == 'Utánvéttel fizetés')
	{
		$utanvet = 1;
		$utanvet_erteke = $row['utanvet'] > 0 ? $row['utanvet'] : 0; 
		$utanvet_afa = $row['utanvet'] > 0 ? $row['szallitasi_dij_afa']: 0;		
	}

	$rendeles = array('order_number' => $rendeles_id, 
					'name' => $row['vasarlo_nev'],
					'postal_code' => $row['vasarlo_irszam'],
					'city' => $row['vasarlo_varos'],
					'address' => $row['vasarlo_cim'],
					'tax_number' => $row_user['adoszam'],
					'email' => $row['vasarlo_email'],
					'phone' => $row_user['telefon'],
					'shipping_name' => $ovip_szallitasi_nev,
					'shipping_city' => $ovip_szallitasi_varos,
					'shipping_address' => $ovip_szallitasi_cim,
					'shipping_postal_code' => $ovip_szallitasi_irsz,
					'order_date' => $row['datum'],
					'payment_method' => $row['fiz_mod'],
					'shipping_method' => $row['szall_mod'],
					'shipping_cost' => $row['szallitasi_dij'],
					'shipping_cost_tax' => $row['szallitasi_dij_afa'],
					'cod_cost' => $utanvet_erteke,
					'cod_cost_tax' => $utanvet_afa,						
					'order_note' => $ovip_megjegyzes,
					'delivery_point_id' => $delivery_point,
					'cod' => $utanvet,
					'invoice_note' => $row['szamla_megjegyzes']);

	if ($row['fiz_mod'] == 'Bankkártyás fizetés') {
		$rendeles['payment_status'] = $row['fizetve_otp'] > 0 ? 1 : 0;
	}

	// Rendelés tételei
	$query_tetel = "SELECT * FROM ".$webjel."rendeles_tetelek WHERE rendeles_id=".$row['id'];
	foreach ($pdo->query($query_tetel) as $row_tetel)
	{
		$query_term = "SELECT * FROM ".$webjel."termekek WHERE id=".$row_tetel['term_id'];
		$res = $pdo->prepare($query_term);
		$res->execute();
		$row_term = $res -> fetch();

		if($row_tetel['term_akcios_ar'] > 0) // Ha akciós
		{
			$term_ar = $row_tetel['term_akcios_ar'];
		}
		else
		{
			$term_ar = $row_tetel['term_ar'];
		}
		// Paraméterek
		$query_jell = "SELECT * FROM ".$webjel."rendeles_tetelek_termek_parameterek WHERE rendeles_tetel_id = ".$row_tetel['id']." ORDER BY termek_parameter_nev ASC";
		foreach ($pdo->query($query_jell) as $row_tetelek_jell)
		{
			$row_tetel['term_nev'] .= ', '.$row_tetelek_jell['termek_parameter_nev'].': '.$row_tetelek_jell['termek_parameter_ertek_nev'];
		}												
		$rendeles['products'][] = array('product_name' => $row_tetel['term_nev'], 
								'gross_price' => $term_ar, 
								'quantity' => $row_tetel['term_db'],
								'ovip_product_id' => !empty($row_term['ovip_id']) ? $row_term['ovip_id'] : 0,
								'tax' => $row_tetel['afa']);
	}

	$request = 'sendOrder';

	$signature = hash('sha256', trim($ovip_ugyfel_id . $ovip_webshop_id . $ovip_authCode . $request . $ovip_ip_cim)); //hash generálása 

	$options = array(
	  	'location' => $ovip_soap_link,
	 	'uri' => $ovip_soap_link,
	  	'encoding' => 'UTF-8',
	  	'trace' => 1
	);

	$request = array(
		'extra_data' => $rendeles,
		'request' => $request,
		'user_id' => $ovip_ugyfel_id,
		'signature' => $signature,
		'webshop_id' => $ovip_webshop_id
	);

	try {

		$client = new SoapClient(NULL,$options);
		
		$tetelek = $client->getRequest($request);
		
		$rendeles['valasz'] = $tetelek;

		var_dump($tetelek);

		if (isset($tetelek['order_number']) && $tetelek['order_number'] > 0)
		{
			$eredmeny_menteshez = 'ok';
		}
		else
		{
			$eredmeny_menteshez = 'hiba';	

			$riasztas_hiba = 1;
		}

	} catch (Exception $e) {

		$rendeles['hiba'] = $e->getMessage();
		$eredmeny_menteshez = 'hiba';	
	}

	// echo $result;
	
	// Log fájl létrehozása

	$file = '/webshop/log_rendelesek/'.date('Ymd-His').'_'.$rendeles_id.'_'.$eredmeny_menteshez.'.log';
	$current =  print_r($rendeles, true);
	file_put_contents($gyoker.$file, $current);