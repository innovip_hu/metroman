﻿<?php

	function noreg_check($adatok){
		include '../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
		if($adatok['email'] == "" || $adatok['telefon'] == "" || $adatok['vezeteknev'] == "" ||  $adatok['cim_varos'] == "" || $adatok['cim_utca'] == "" || $adatok['cim_hszam'] == "" || $adatok['cim_irszam'] == "")
		{
		return "<p style='color: red;'>Minden mező kitöltése kötelező!</p>";
			break;
		}
		else
		{
		return "rendben";
		}
}
?>