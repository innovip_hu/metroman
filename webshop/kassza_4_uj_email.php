<?php
	// OTP visszatérés
	if(isset($_GET['tranzakcioAzonosito']))
	{
		if ($_GET['tranzakcioAzonosito'] == '')
		{
			print '<p style="text-align:center; color:#D27290;">Hiányzik a fizetési azonosító!</p>';
		}
		else
		{
			$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."rendeles WHERE kod_otp='".$_GET['tranzakcioAzonosito']."'");
			$res->execute();
			$rownum = $res->fetchColumn();
			if($rownum > 0)
			{
				print '<h3 style="text-align: center;">Rendelését sikeresen rögzítettük!</h3>';
				print '<h4 style="text-align: center;">Hamarosan e-mail-ben is megküldjük a vásárlása adatait.</h4>';
				print '<h4 style="text-align: center;">További szép napot kívánunk!</h4>
						<p style="text-align: center;">Banki engedélyszám: '.$_GET['authKod'].'</p>
									<p style="text-align: center;">Ha még nem tette, kövessen minket a Facebookon is, hogy ne maradjon le akcióinkról és újdonságainkról!
							<br><a href="https://www.facebook.com/MareDesignHu" target="_blank"><img src="'.$domain.'/images/facebook.png" style="width: 40px; margin-top: 10px;" alt="Facebook"></a></p>';
				
				// Fizetés visszaigazolása
				$updatecommand = "UPDATE ".$webjel."rendeles SET fizetve_otp=1 WHERE kod_otp='".$_GET['tranzakcioAzonosito']."'";
				$result = $pdo->prepare($updatecommand);
				$result->execute();
			}
			else
			{
				print '<p style="text-align:center; color:#D27290;">Érvénytelen fizetési azonosító!</p>';
			}
		}
	}
	// Rendelés leadása
	else if (isset($_POST['command']) && $_POST['command'] == 'rendeles_leadas' && (isset($_SESSION['kosar_id'])))
	{
		// Ha be van lépve
		if (isset($_SESSION['login_id'])) //REG
		{
			$query4 = "SELECT * FROM ".$webjel."users where id=".$_SESSION['login_id'];
			$user_id = $_SESSION['login_id'];
			$noreg = 0;
			$res = $pdo->prepare($query4);
			$res->execute();
			$row4 = $res -> fetch();
			
			$user_email = $row4['email'];
			$nev1 = $row4['vezeteknev'].' '.$row4['keresztnev'];
			$nev2 = $row4['vezeteknev'].' '.$row4['keresztnev'];
			$nev3 = $row4['vezeteknev'].' '.$row4['keresztnev'];
			$nev4 = $row4['szla_nev'];
			$telefon = $row4['telefon'];
			$cim1 = $row4['cim_irszam'].' '.$row4['cim_varos'];
			$cim2 = $row4['cim_utca'].' '.$row4['cim_hszam'];
			$szla_vasarlo_nev = $row4['vezeteknev'];
			$szla_vasarlo_email = $row4['email'];
			$szla_vasarlo_irszam = $row4['cim_irszam'];
			$szla_vasarlo_varos = $row4['cim_varos'];
			$szla_vasarlo_cim = $row4['cim_utca'].' '.$row4['cim_hszam'];
			$szla_vasarlo_utca = $row4['cim_utca'];
			$szla_vasarlo_hszam = $row4['cim_hszam'];

			$adoszam = $row4['adoszam'];

			$cim_szall_nev = $row4['cim_szall_nev'];
			$cim_szall_varos = $row4['cim_szall_varos'];
			$cim_szall_utca = $row4['cim_szall_utca'].' '.$row4['cim_szall_hszam'];
			$cim_szall_hszam = $row4['cim_szall_hszam'];
			$cim_szall_irszam = $row4['cim_szall_irszam'];

			if ($row4['cim_szall_varos'] == '')
			{
				$szallcim1 = $row4['cim_irszam'].' '.$row4['cim_varos'];
				$szallcim2 = $row4['cim_utca'].' '.$row4['cim_hszam'];
			}
			else
			{
				$nev3 = $row4['cim_szall_nev'];
				$szallcim1 = $row4['cim_szall_irszam'].' '.$row4['cim_szall_varos'];
				$szallcim2 = $row4['cim_szall_utca'].' '.$row4['cim_szall_hszam'];
			}
		}
		else // Nincs belépve
		{
			$_POST['nev'] = str_replace("'", "`", $_POST['nev']);
			$user_email = $_POST['email'];
			$nev1 = $_POST['nev'];
			$nev2 = $_POST['nev'];
			$nev3 = $_POST['nev'];
			$nev4 = $_POST['szla_nev'];
			$telefon = $_POST['telefon'];
			$cim1 = $_POST['cim_irszam'].' '.$_POST['cim_varos'];
			$cim2 = $_POST['cim_utca'].' '.$_POST['cim_hszam'];
			$szla_vasarlo_nev = $_POST['nev'];
			$szla_vasarlo_email = $_POST['email'];
			$szla_vasarlo_irszam = $_POST['cim_irszam'];
			$szla_vasarlo_varos = $_POST['cim_varos'];
			$szla_vasarlo_cim = $_POST['cim_utca'].' '.$_POST['cim_hszam'];
			$szla_vasarlo_utca = $_POST['cim_utca'];
			$szla_vasarlo_hszam = $_POST['cim_hszam'];	

			$adoszam = $_POST['adoszam'];		
			if ($_POST['cim_szall_varos'] == '')
			{
				$szallcim1 = $_POST['cim_irszam'].' '.$_POST['cim_varos'];
				$szallcim2 = $_POST['cim_utca'].' '.$_POST['cim_hszam'];
			}
			else
			{
				$nev3 = $_POST['cim_szall_nev'];
				$szallcim1 = $_POST['cim_szall_irszam'].' '.$_POST['cim_szall_varos'];
				$szallcim2 = $_POST['cim_szall_utca'].' '.$_POST['cim_szall_hszam'];
			}
			if(isset($_POST['regisztracio_checkbox']) && $_POST['regisztracio_checkbox'] == 'ok') // Regisztráció
			{
				$jelszo = $_POST['jelszo'];
				
				$cim_szall_nev = $_POST['cim_szall_nev'];
				$cim_szall_varos = $_POST['cim_szall_varos'];
				$cim_szall_utca = $_POST['cim_szall_utca'].' '.$_POST['cim_szall_utca'];
				$cim_szall_hszam = $_POST['cim_szall_hszam'];
				$cim_szall_irszam = $_POST['cim_szall_irszam'];
					
				$insertcommand = "INSERT INTO ".$webjel."users (email,telefon,vezeteknev,szla_nev,cim_varos,cim_utca,cim_hszam,cim_irszam,cim_szall_nev,cim_szall_varos,cim_szall_utca,cim_szall_hszam,cim_szall_irszam,tipus,jelszo,reg_datum,adoszam) VALUES ('".$_POST['email']."','".$_POST['telefon']."','".$_POST['nev']."','".$nev4."','".$_POST['cim_varos']."','".$_POST['cim_utca']."','".$_POST['cim_hszam']."','".$_POST['cim_irszam']."','".$cim_szall_nev."','".$cim_szall_varos."','".$cim_szall_utca."','".$cim_szall_hszam."','".$cim_szall_irszam."','user','".password_hash($jelszo, PASSWORD_DEFAULT)."',NOW(),'".$_POST['adoszam']."')";
				$result = $pdo->prepare($insertcommand);
				$result->execute();
				//id meghatározása
				$user_id = $pdo->lastInsertId();
				$noreg = 0;
			}
			else
			{
				$vezeteknev = $_POST['nev'];
				$keresztnev = '';
				$user_email = $_POST['email'];
				$telefon = $_POST['telefon'];
				$cim_varos = $_POST['cim_varos'];
				$cim_utca = $_POST['cim_utca'];
				$cim_hszam = $_POST['cim_hszam'];
				$cim_irszam = $_POST['cim_irszam'];
				$cim_szall_nev = $_POST['cim_szall_nev'];
				$cim_szall_varos = $_POST['cim_szall_varos'];
				$cim_szall_utca = $_POST['cim_szall_utca'].' '.$_POST['cim_szall_hszam'];
				$cim_szall_hszam = $_POST['cim_szall_hszam'];
				$cim_szall_irszam = $_POST['cim_szall_irszam'];
				$query = "INSERT INTO ".$webjel."users_noreg (vezeteknev,keresztnev,email,telefon,szla_nev,cim_varos,cim_utca,cim_hszam,cim_irszam,cim_szall_nev,cim_szall_varos,cim_szall_utca,cim_szall_hszam,cim_szall_irszam,adoszam) VALUES (:vezeteknev,:keresztnev,:email,:telefon,:szla_nev,:cim_varos,:cim_utca,:cim_hszam,:cim_irszam,:cim_szall_nev,:cim_szall_varos,:cim_szall_utca,:cim_szall_hszam,:cim_szall_irszam,:adoszam)";
				$result = $pdo->prepare($query);
				$result->execute(array(':vezeteknev'=>$vezeteknev,
									':keresztnev'=>$keresztnev,
									':email'=>$user_email,
									':telefon'=>$telefon,
									':szla_nev'=>$nev4,
									':cim_varos'=>$cim_varos,
									':cim_utca'=>$cim_utca,
									':cim_hszam'=>$cim_hszam,
									':cim_irszam'=>$cim_irszam,
									':cim_szall_nev'=>$cim_szall_nev,
									':cim_szall_varos'=>$cim_szall_varos,
									':cim_szall_utca'=>$cim_szall_utca,
									':cim_szall_hszam'=>$cim_szall_hszam,
									':cim_szall_irszam'=>$cim_szall_irszam,
									':adoszam'=>$_POST['adoszam']));
				
				$user_id = $pdo->lastInsertId();
				$noreg = 1;
			}
		}
		//szállítási költség meghatározása
		$query_f = "SELECT * FROM ".$webjel."kassza_fiz_mod WHERE id=".$_POST['fiz_mod'];
		$res = $pdo->prepare($query_f);
		$res->execute();
		$row_f = $res -> fetch();
		$fiz_mod = $row_f['nev'];

		$query_sz = "SELECT * FROM ".$webjel."kassza_szall_mod WHERE id=".$_POST['szall_mod'];
		$res = $pdo->prepare($query_sz);
		$res->execute();
		$row_sz = $res -> fetch();
		$szall_mod = $row_sz['nev'];
		// Rendelés
						
						// Weboldal beállításai
							$query_beall = 'SELECT * FROM '.$webjel.'beallitasok WHERE id=1';
							$res = $pdo->prepare($query_beall);
							$res->execute();
							$row_beall = $res -> fetch();
							
						// Kosár tartalmának összege, hogy meghatározzuk ingyenes-e a szállítás
							$ingy_szall = 0;
							$query = "SELECT * FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];
							$ar_ossz = 0;
							foreach ($pdo->query($query) as $row)
							{
								if($row['term_akcios_ar'] == 0) //ha nem akciós
								{
									$ar_ossz = $ar_ossz + ($row['term_ar'] * $row['term_db']);
								}
								else //ha akciós
								{
									$ar_ossz = $ar_ossz + ($row['term_akcios_ar'] * $row['term_db']);
								}
							}
							if ($row_beall['ingyenes_szallitas'] != 0 && $ar_ossz >= $row_beall['ingyenes_szallitas'])
							{
								$ingy_szall = 1;
							}
							// Kupon vizsgálat ingyen szállítás kedvezmény miatt
							$kupon_kedv = 0;
							$kupon_termek_id = 0;
							$kupon_minusz_tetel = 0;
							if($row['kupon_id'] > 0)
							{
								$query_kupon = "SELECT * FROM ".$webjel."kuponok where id=".$row['kupon_id'];
								$res = $pdo->prepare($query_kupon);
								$res->execute();
								$row_kupon = $res -> fetch();
								if($row_kupon['kupon_fajta'] == 'Ingyenes szállítás')
								{
									$ingy_szall = 1;
								}
								else if($row_kupon['kupon_fajta'] == 'Kedvezmény')
								{
									if ($row_kupon['kedv_tipus'] == 'szazalek')
									{
										$kupon_kedv = $row_kupon['kedvezmeny'];
										$kupon_minusz_tetel = round($ar_ossz * ($kupon_kedv / 100))*(-1);
									}
									else if ($row_kupon['kedv_tipus'] == 'osszeg')
									{
										$kupon_minusz_tetel = $row_kupon['kedvezmeny']*(-1);
									}
								}
								else if($row_kupon['kupon_fajta'] == 'Ajándék termék')
								{
									$kupon_termek_id = $row_kupon['termek_id'];
								}
							}

							if($row_f['bankkartya'] == 1 && isset($row4['id']))
							{
								$kod_otp = $row4['id'].'a'.time().rand(1, 99999);
							}
							else if($row_f['bankkartya'] == 1)
							{
								$kod_otp = '1a'.time().rand(1, 99999);
							}
							else
							{
								$kod_otp = '';
							}
							
						//rendelés létrehozása
							$insertcommand = "INSERT INTO ".$webjel."rendeles (user_id, datum, ido, rendeles_id, fiz_mod, szall_mod, noreg, ingy_szall, megjegyzes, ajandek_termek_id, postapont, glscspont, kod_otp, vasarlo_irszam, vasarlo_varos, vasarlo_cim, vasarlo_nev, vasarlo_email) 
							VALUES (:user_id, :datum, :ido, :rendeles_id, :fiz_mod, :szall_mod, :noreg, :ingy_szall, :megjegyzes, :ajandek_termek_id, :postapont, :glscspont, :kod_otp, :vasarlo_irszam, :vasarlo_varos, :vasarlo_cim, :vasarlo_nev, :vasarlo_email)";
							$result = $pdo->prepare($insertcommand);
							$result->execute(array(':user_id'=>$user_id,
					  							   ':datum'=>date('Y-m-d'),
					  							   ':ido'=>date('H:i:s'),
					  							   ':rendeles_id'=>$_SESSION['kosar_id'],
					  							   ':fiz_mod'=>$fiz_mod,
					  							   ':szall_mod'=>$szall_mod,
					  							   ':noreg'=>$noreg,
					  							   ':ingy_szall'=>$ingy_szall,
					  							   ':megjegyzes'=>$_POST['megjegyzes'],
					  							   ':ajandek_termek_id'=>$kupon_termek_id,
					  							   ':postapont'=>$_POST['valasztott_postapont'],
					  							   ':glscspont'=>$_POST['valasztott_gls_cspont'],
					  							   ':kod_otp'=>$kod_otp,
					  							   ':vasarlo_irszam'=>$szla_vasarlo_irszam,
					  							   ':vasarlo_varos'=>$szla_vasarlo_varos,
					  							   ':vasarlo_cim'=>$szla_vasarlo_cim,
					  							   ':vasarlo_nev'=>$szla_vasarlo_nev,
					  							   ':vasarlo_email'=>$szla_vasarlo_email));
							$last_rendeles_id = $pdo->lastInsertId();	

							$insertcommand = "INSERT INTO ".$webjel."rendeles_cimek (rendeles_id, vezeteknev, email, telefon, adoszam, szla_nev, cim_varos, cim_utca, cim_hszam, cim_irszam, cim_szall_nev, cim_szall_varos, cim_szall_utca, cim_szall_hszam, cim_szall_irszam) 
							VALUES (:rendeles_id, :vezeteknev, :email, :telefon, :adoszam, :szla_nev, :cim_varos, :cim_utca, :cim_hszam, :cim_irszam, :cim_szall_nev, :cim_szall_varos, :cim_szall_utca, :cim_szall_hszam, :cim_szall_irszam)";
							$result = $pdo->prepare($insertcommand);
							$result->execute(array(':rendeles_id'=>$last_rendeles_id,
					  							   ':vezeteknev'=>$szla_vasarlo_nev,
					  							   ':email'=>$szla_vasarlo_email,
					  							   ':telefon'=>$telefon,
					  							   ':adoszam'=>$adoszam,
					  							   ':szla_nev'=>$nev4,
					  							   ':cim_varos'=>$szla_vasarlo_varos,
					  							   ':cim_utca'=>$szla_vasarlo_cim,
					  							   ':cim_hszam'=>'',
					  							   ':cim_irszam'=>$szla_vasarlo_irszam,
					  							   ':cim_szall_nev'=>$cim_szall_nev,
					  							   ':cim_szall_varos'=>$cim_szall_varos,
					  							   ':cim_szall_utca'=>$cim_szall_utca,
					  							   ':cim_szall_hszam'=>'',
					  							   ':cim_szall_irszam'=>$cim_szall_irszam));

							
						//Tábla a rendelésről
							$tabla_tetelek = '<table align="left" cellpadding="6" cellspacing="0" style="background-color: #FFFFFF; font-family: calibri, sans-serif;" width="100%">
										<tr><td colspan="5"><b>Rendelés adatai</p></td></tr>
										<tr style="border: 0px solid #c9c9c9;">
											<td colspan="2" align="left" style="border: 0px solid #c9c9c9;">Megrendelés száma</td>
											<td colspan="3"><b style="background-color: #3E5878; color: #ffffff; padding: 0 5px;">'.$last_rendeles_id.'</b></td>
										</tr>
										<tr>
											<td colspan="2" align="left">
												Rendelés időpontja
											</td>
											<td colspan="3" align="left">
												'.date('Y.m.d').'&nbsp;'.date('H:i:s').'
											</td>
										</tr>
										<tr>
											<td colspan="2" align="left">
												Név
											</td>
											<td colspan="3" align="left">
												'.$nev1.'
											</td>
										</tr>										
										<tr>
											<td colspan="2" align="left">
												Számlázási cím
											</td>
											<td colspan="3" align="left">
												'.$cim1.','.$cim2.'
											</td>
										</tr>
										<tr>
											<td colspan="2" align="left">
												Szállítási Név
											</td>
											<td colspan="3" align="left">
												'.$nev3.'
											</td>
										</tr>										
										<tr>
											<td colspan="2" align="left">
												Szállítási cím
											</td>
											<td colspan="3" align="left">
												'.$szallcim1.','.$szallcim2.'
											</td>
										</tr>										
										<tr>
											<td colspan="2" align="left">
												Email
											</td>
											<td colspan="3" align="left">
												'.$user_email.'
											</td>
										</tr>
										<tr>
											<td colspan="2" align="left">
												Telefon
											</td>
											<td colspan="3" align="left">
												'.$telefon.'
											</td>
										</tr>																													
										<tr><td colspan="5">';

								/*		backup $tabla_tetelek = '<table align="left" cellpadding="6" cellspacing="0" style="background-color: #FFFFFF; font-family: calibri, sans-serif;" width="100%">
										<tr style="border: 0px solid #c9c9c9;">
											<td colspan="6" align="left" style="border: 0px solid #c9c9c9;"><b>Megrendelés száma:</b> '.$last_rendeles_id.'</td>
										</tr>
										<tr>
											<td colspan="2" align="left">
												<b>Vevő</b>
												<br>'.$nev1.'
												<br>'.$telefon.'
												<br>'.$user_email.'
											</td>
											<td colspan="2" align="left">
												<b>Számlázási cím</b>
												<br>'.$nev2.'
												<br>'.$cim1.'
												<br>'.$cim2.'
											</td>
											<td colspan="2" align="left">
												<b>Szállítási cím</b>
												<br>'.$nev3.'
												<br>'.$szallcim1.'
												<br>'.$szallcim2.'
											</td>
										</tr>
										<tr><td colspan="6">';*/
						
						//Kosár tartalma
							$query = "SELECT * FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];
							$ar = 0;
							$id = 0;
							$tabla_tetelek_2 = '';
							$tabla_tetelek.= '<table align="left" cellpadding="6" cellspacing="0" style="font-family: calibri, sans-serif;" width="100%"">';
							$tabla_tetelek.= '<tr align="left" style="font-weight:bold;" style="background-color: #3E5878; color: #FFFFFF;">
										<td width="175" colspan="2" style="background-color: #3E5878; color: #FFFFFF;">Név</td>
										<td style="background-color: #3E5878; color: #FFFFFF;">Mennyiség</td>
										<td style="background-color: #3E5878; color: #FFFFFF;">Egységár</td>
										<td align="right" style="background-color: #3E5878; color: #FFFFFF;">Összesen</td>
									</tr>'; //colspan ki, ha van kép
							foreach ($pdo->query($query) as $row)
							{
								if($row['term_akcios_ar'] == 0) //ha nem akciós
								{
									// $term_ar = $row['term_ar'] - (round($row['term_ar'] * ($kupon_kedv / 100)));
									$term_ar = $row['term_ar'];
								}
								else //ha akciós
								{
									// $term_ar = $row['term_akcios_ar'] - (round($row['term_akcios_ar'] * ($kupon_kedv / 100)));
									$term_ar = $row['term_akcios_ar'];
								}
								//termék adatai
								$query_term = "SELECT * FROM ".$webjel."termekek where id=".$row['term_id'];
								$res = $pdo->prepare($query_term);
								$res->execute();
								$row_term = $res -> fetch();					

								$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_term['id']." ORDER BY alap DESC LIMIT 1";
								$res = $pdo->prepare($query_kep);
								$res->execute();
								$row_kep = $res -> fetch();

								if ($row_kep['kep'] != '')
								{
									$kep = '<img src="'.$domain.'/images/termekek/'.$row_kep['kep'].'" width="30" border="0">';
									$kep_level = $domain.'/images/termekek/'.$row_kep['kep'];
								}
								else
								{
									$kep = '';
									$kep_level = $domain.'/webshop/images/noimage.png';
								}

								$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_term['csop_id'];
								$res = $pdo->prepare($query_csop);
								$res->execute();
								$row_csop = $res -> fetch();

								$termek_link = $domain.'/termekek/'.$row_csop['nev_url'].'/'.$row_term['nev_url'];
								
								//rendelés tétel létrehozása
								$insertcommand = "INSERT INTO ".$webjel."rendeles_tetelek (term_id,term_db,term_ar,term_akcios_ar,rendeles_id,term_nev,afa) VALUES ('".$row['term_id']."','".$row['term_db']."','".$row['term_ar']."','".$row['term_akcios_ar']."','".$last_rendeles_id."',:term_nev,'".$row['term_afa']."')";
								$result = $pdo->prepare($insertcommand);
								// $result->execute();
								$result->execute(array(':term_nev'=>$row['term_nev']));
								
								$last_id = $pdo->lastInsertId();
								
								// Készlet csökkentése
								if($config_keszlet_kezeles == 'I')
								{
									$pdo->exec("UPDATE ".$webjel."termekek SET raktaron=(raktaron - ".$row['term_db'].") WHERE id=".$row['term_id']);
								}

								$tabla_tetelek_2 .= '
								             <tr style="border-collapse:collapse;"> 
								              <td align="left" style="Margin:0;padding-top:5px;padding-bottom:10px;padding-left:20px;padding-right:20px;"> 
								               <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="178" valign="top"><![endif]--> 
								               <table class="es-left" cellspacing="0" cellpadding="0" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
								                 <tr style="border-collapse:collapse;"> 
								                  <td class="es-m-p0r es-m-p20b" width="178" valign="top" align="center" style="padding:0;Margin:0;"> 
								                   <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
								                     <tr style="border-collapse:collapse;"> 
								                      <td align="center" style="padding:0;Margin:0;"> <a href="'.$termek_link.'" target="_blank"><img src="'.$kep_level.'" alt="'.$row['term_nev'].'" class="adapt-img" title="'.$row['term_nev'].'" width="125" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a>
								                      </td> 
								                     </tr> 
								                   </table> </td> 
								                 </tr> 
								               </table> 
								               <!--[if mso]></td><td width="20"></td><td width="362" valign="top"><![endif]--> 
								               <table cellspacing="0" cellpadding="0" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
								                 <tr style="border-collapse:collapse;"> 
								                  <td width="362" align="left" style="padding:0;Margin:0;"> 
								                   <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
								                     <tr style="border-collapse:collapse;"> 
								                      <td align="left" style="padding:0;Margin:0;"> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, &quot;helvetica neue&quot;, helvetica, sans-serif;line-height:21px;color:#333333;"><br></p> 
								                       <table style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:100%;" class="cke_show_border" cellspacing="1" cellpadding="1" border="0"> 
								                         <tr style="border-collapse:collapse;"> 
								                          <td style="padding:0;Margin:0;"><a href="'.$termek_link.'" target="_blank">'.$row['term_nev'].'</a>';
												$query_jell = "SELECT * FROM ".$webjel."kosar_tetel_termek_parameter_ertek WHERE kosar_tetel_id = ".$row['id']." ORDER BY termek_parameter_nev ASC";
												foreach ($pdo->query($query_jell) as $row_jell)
												{
													$tabla_tetelek_2.= '<br/>Választott szín: '.$row_jell['termek_parameter_ertek_nev'];
													
												}
								                $tabla_tetelek_2 .= '</td> 
								                          <td style="padding:0;Margin:0;text-align:center;" width="60">'.$row['term_db'].'</td> 
								                          <td style="padding:0;Margin:0;text-align:center;" width="100">'.number_format($term_ar * $row['term_db'], 0, ',', ' ').' Ft</td> 
								                         </tr> 
								                       </table> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, &quot;helvetica neue&quot;, helvetica, sans-serif;line-height:21px;color:#333333;"><br></p> </td> 
								                     </tr> 
								                   </table> </td> 
								                 </tr> 
								               </table> 
								               <!--[if mso]></td></tr></table><![endif]--> </td> 
								             </tr>
								             <tr style="border-collapse:collapse;"> 
								              <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;"> 
								               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
								                 <tr style="border-collapse:collapse;"> 
								                  <td width="560" valign="top" align="center" style="padding:0;Margin:0;"> 
								                   <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
								                     <tr style="border-collapse:collapse;"> 
								                      <td align="center" style="padding:0;Margin:0;padding-bottom:10px;"> 
								                       <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
								                         <tr style="border-collapse:collapse;"> 
								                          <td style="padding:0;Margin:0px;border-bottom:1px solid #EFEFEF;background:rgba(0, 0, 0, 0) none repeat scroll 0% 0%;height:1px;width:100%;margin:0px;"></td> 
								                         </tr> 
								                       </table> </td> 
								                     </tr> 
								                   </table> </td> 
								                 </tr> 
								               </table> </td> 
								             </tr>               	
								             ';							
								
								$tabla_tetelek.= '<tr>
											<td><a href="'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row_term['nev_url'].'" style="-webkit-hyphens: none; word-break:keep-all;">
												<h5 style="-webkit-hyphens: none; word-break:keep-all;">'.$row['term_nev'].'';
											// Jellemzők
											$query_jell = "SELECT * FROM ".$webjel."kosar_tetel_termek_parameter_ertek WHERE kosar_tetel_id = ".$row['id']." ORDER BY termek_parameter_nev ASC";
											$elso = 1;
											foreach ($pdo->query($query_jell) as $row_jell)
											{
												if($elso == 1) { $tabla_tetelek.= ''; $elso = 0; } else { $tabla_tetelek.= '<br/>'; }
												$tabla_tetelek.= '<br/>Választott szín: '.$row_jell['termek_parameter_ertek_nev'];
												//$tabla_tetelek.= '<br/>'.$row_jell['termek_parameter_nev'].': '.$row_jell['termek_parameter_ertek_nev'];
												
												//rendelés tétel paraméterek
												$insertcommand = "INSERT INTO ".$webjel."rendeles_tetelek_termek_parameterek 
														(rendeles_tetel_id, termek_parameter_nev, termek_parameter_ertek_nev) 
														VALUES (".$last_id.", '".$row_jell['termek_parameter_nev']."', '".$row_jell['termek_parameter_ertek_nev']."')";
												$result = $pdo->prepare($insertcommand);
												$result->execute();
											}						
											//rendelés tétel paraméterek törlése
											$deletecommand = 'DELETE FROM '.$webjel.'kosar_tetel_termek_parameter_ertek WHERE kosar_tetel_id='.$row['id'];
											$result = $pdo->prepare($deletecommand);
											$result->execute();
											
											$tabla_tetelek.= '</h5></a></td>
											<td></td>
											<td>'.$row['term_db'].'</td>
											<td>'.number_format($term_ar, 0, ',', ' ').' Ft</td>
											<td align="right">'.number_format($term_ar * $row['term_db'], 0, ',', ' ').' Ft</td>
										</tr>';
								$ar = $ar + ($term_ar * $row['term_db']);
								$kupon_id = $row['kupon_id'];
							}
							if($kupon_minusz_tetel != 0) // Ha van kedvezményes összeg, akkor minuszos tétel
							{
								// <td>'.$kep.'</td> <td></td> helyett
 								$tabla_tetelek.= '<tr>
											<td>Kupon</td>
											<td></td>
											<td>1</td>
											<td>'.number_format($kupon_minusz_tetel, 0, ',', ' ').' Ft</td>
											<td align="right">'.number_format($kupon_minusz_tetel, 0, ',', ' ').' Ft</td>
										</tr>';
								$ar = $ar + ($kupon_minusz_tetel);
								// SQl
								$insertcommand = "INSERT INTO ".$webjel."rendeles_tetelek (term_id,term_db,term_ar,term_akcios_ar,rendeles_id,term_nev) VALUES ('0','1','".$kupon_minusz_tetel."','0','".$last_rendeles_id."','Kupon')";
								$result = $pdo->prepare($insertcommand);
								$result->execute();
							}
							//szállítási költség meghatározása
							/*
							if($row_f['utanvet'] == 1)
							{
								$szall_kolts = $row_sz['ar_utanvet'];
							}
							else
							{
								$szall_kolts = $row_sz['ar'];
							} */

							include $gyoker.'/webshop/suly_check.php';
							$szallitas = suly_check('minden_termek');

							if ($_POST['szall_mod'] == 5)
							{
								if($row_f['utanvet'] == 1)
								{
									$szall_kolts = $szallitas['szemelyes_ar_utanvet'];
								}
								else
								{
									$szall_kolts = $szallitas['szemelyes_ar'];
								}		
							}
							else
							{
								if($row_f['utanvet'] == 1)
								{
									$szall_kolts = $szallitas['ar_utanvet'];
								}
								else
								{
									$szall_kolts = $szallitas['ar'];
								}		
							}							
							// Ha van ingyenes szállítási költség
							if ($row_beall['ingyenes_szallitas'] != 0 && $ar >= $row_beall['ingyenes_szallitas'])
							{
								$szall_kolts = 0;
							}
							if($ingy_szall == 1)
							{
								$szall_kolts = 0;
							}
							
							// Rendelések táblába beírni a szállítási költséget + kupon 
							$updatecommand = "UPDATE ".$webjel."rendeles SET szallitasi_dij=?, szallitasi_dij_afa=?, kupon_id=? WHERE rendeles_id=?";
							$result = $pdo->prepare($updatecommand);
							$result->execute(array($szall_kolts,$row_beall['szallitas_afa'],$kupon_id,$_SESSION['kosar_id']));
							
							// Kupon kedvezmény
							$ar_kedvezmeny = 0;
							if($kupon_kedv > 0)
							{
								$ar_kedvezmeny = round($ar * ($row_kupon['kedvezmeny'] / 100));
							}
							
							if($kupon_termek_id > 0) // Ha kuponból ajándék
							{
								$query_ajandek = "SELECT * FROM ".$webjel."termekek WHERE id=".$kupon_termek_id;
								$res = $pdo->prepare($query_ajandek);
								$res->execute();
								$row_ajandek = $res -> fetch();
								$tabla_tetelek.= '<tr>
											<td>'.$row_ajandek['nev'].'</td>
											<td></td>
											<td>1</td>
											<td>0 Ft</td>
											<td align="right">0 Ft</td>
										</tr>';
							}
							
							$tabla_tetelek.= '<tr>
										<td colspan="3"></td>
										<td>Összesen</td>
										<td align="right">'.number_format($ar, 0, ',', ' ').' Ft</td>
									</tr>';

									
							$tabla_tetelek.= '<tr>
										<td colspan="3"></td>
										<td align="left">Szállítási költség</td>
										<td align="right">'.number_format($szall_kolts, 0, ',', ' ').' Ft</td>
									</tr>';
							
							$fizetendo = $ar + $szall_kolts;
							$tabla_tetelek.= '<tr style="font-weight:bold;">
										<td colspan="3"></td>
										<td>Fizetendő</td>
										<td align="right">'.number_format($fizetendo, 0, '.', ' ').' Ft</td>
									</tr>';
							$tabla_tetelek.= '</table>';
						
						$tabla_tetelek.= '</td></tr>';
						if ($_POST['megjegyzes'] != '')
						{
								$tabla_tetelek.= '<tr>
									<td colspan="4">
										<b>Megjegyzés:</b> '.$_POST['megjegyzes'].'
									</td>
									</tr>';
						}						
						if ($szall_mod == 'Egyedi szállítás')
						{
							$szall_mod = 'Házhozszállítás';
						}
						$tabla_tetelek.= '<tr>
							<td colspan="4">
								<b>Szállítás:</b> '.$szall_mod.'
								<br><b>Fizetési mód:</b> '.$fiz_mod;
								if ($fiz_mod == 'Banki átutalás')
								{
									$tabla_tetelek.= '<br>Az átutaláshoz szükséges adatok a következők:
												<br>Számlaszám: '.$bankszamla.'
												<br>Közlemény: Rendelésszám '.$last_rendeles_id;
								}
								else if ($szall_mod == 'PostaPont')
								{
									$tabla_tetelek.= '<br><b>A kiválasztott PostaPont:</b> '.$_POST['postapont'];
								}
								else if ($szall_mod == 'GLS CsomagPont')
								{
									$tabla_tetelek.= '<br><b>A kiválasztott GLS CsomagPont:</b> '.$_POST['glscspont'];
								}
							$tabla_tetelek.= '</td>
						</tr>
					</table>';
				//E-MAIL
					//üzenet
					$mess =  '<table align="center" cellpadding="12" cellspacing="0" border="0" style="background-color: #fff;">
								<tr>
									<td>
										<a href="'.$domain.'" style="color: blue;" target="_blank"><img src="'.$domain.'/images/logo.png" width="200" title="'.$webnev.'" /></a>
									</td>
								</tr>
								<tr>
									<td style="text-align:left;">
										<p><b>Kedves '.$nev1.'!</b></p>
										<p>Az alábbi rendelését adta le webáruházunkban:</p>
									</td>
								</tr>
									<td>
										'.$tabla_tetelek.'
									</td>
								<tr>
									<td style="text-align:left;">
										<p>Vásárlását ezúton is köszönjük!</p>
										<p>
											Üdvözlettel:
											<br>
											'.$webnev.'
										</p>
										<p>Ha még nem tette, kövessen minket a Facebookon is, hogy ne maradjon le akcióinkról és újdonságainkról!
										<br><a href="https://www.facebook.com/MareDesignHu" target="_blank"><img src="'.$domain.'/images/facebook.png" style="width: 40px; margin-top: 10px;" alt="Facebook"></a></p>
									</td>
								</tr>
							</table>';
					//phpMailer
					require_once($gyoker.'/webshop/PHPMailer-master/PHPMailerAutoload.php');
					include $gyoker.'/module/email.php';
					$mail = new PHPMailer();
					$mail->isHTML(true);
					$mail->CharSet = 'UTF-8';
					// $mail->addCustomHeader('MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n");
					if($conf_smtp == 1)
					{
						// SMTP
						$mail->IsSMTP(); // telling the class to use SMTP
						$mail->Host       = $smtp_host; // SMTP server
						$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
																   // 1 = errors and messages
																   // 2 = messages only
						$mail->SMTPAuth   = true;                  // enable SMTP authentication
						$mail->Host       = $smtp_host; // sets the SMTP server
						$mail->Port       = $smtp_port;                    // set the SMTP port for the GMAIL server
						$mail->Username   = $smtp_user; // SMTP account username
						$mail->Password   = $smtp_pass;        // SMTP account password
						$mail->SetFrom($smtp_email, $smtp_name);
						$mail->AddReplyTo($smtp_email, $smtp_name);
					}
					else
					{
						$mail->SetFrom($email, $webnev);
					}
					$mail->AddAddress($user_email, $nev1);
					$mail->Subject = "Rendelés értesítő - maredesign.hu";
					//$htmlmsg = '<html><body style="background-color: #d2d2d2">'.$mess.'</body></html>';
					$htmlmsg = $html;
					$mail->Body = $htmlmsg;
					if(!$mail->Send()) {
					  echo "Mailer Error: " . $mail->ErrorInfo;
					}
					
				//E-mail a tulajnak
					if ($config_email_vasar_tulaj == "I")
					{
						//phpMailer
						require_once($gyoker.'/webshop/PHPMailer-master/PHPMailerAutoload.php');
						$mail = new PHPMailer();
						$mail->isHTML(true);
						$mail->CharSet = 'UTF-8';
						// $mail->addCustomHeader('MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n");
						if($conf_smtp == 1)
						{
							// SMTP
							$mail->IsSMTP(); // telling the class to use SMTP
							$mail->Host       = $smtp_host; // SMTP server
							$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
																	   // 1 = errors and messages
																	   // 2 = messages only
							$mail->SMTPAuth   = true;                  // enable SMTP authentication
							$mail->Host       = $smtp_host; // sets the SMTP server
							$mail->Port       = $smtp_port;                    // set the SMTP port for the GMAIL server
							$mail->Username   = $smtp_user; // SMTP account username
							$mail->Password   = $smtp_pass;        // SMTP account password
							$mail->SetFrom($smtp_email, $smtp_name);
							$mail->AddReplyTo($smtp_email, $smtp_name);
						}
						else
						{
							$mail->SetFrom($email, $webnev);
						}
						$mail->AddAddress($email, 'WebShop');
						$mail->Subject = "Rendelés értesítő - maredesign.hu";
						$htmlmsg = '<html>
											<body>
												<table align="left" cellpadding="12" cellspacing="0" border="0">
													<tr>
														<td>
															<p>Tisztelt Üzemeltető</p>
														</td>
													</tr>
													<tr>
														<td>
															<p>A Webáruházban az alábbi vásárlás történt, melyet megtekinthet az <a href="'.$domain.'/adm/">adminisztrációs felületen</a>.</p>
														</td>
													</tr>
													<tr>
														<td>
															'.$tabla_tetelek.'
														</td>
													</tr>
													<tr>
														<td>
															<p>Üdvözlettel:<br><a href="'.$domain.'" style="color: blue;" target="_blank"><img src="'.$domain.'/images/logo.png" width="200" title="'.$webnev.'" /></a></p>
														</td>
													</tr>
												</table>
											</body>
										</html>';
						$mail->Body = $htmlmsg;
						if(!$mail->Send()) {
						  echo "Mailer Error: " . $mail->ErrorInfo;
						}
					}
		
					//Kosár ürítése
						$deletecommand = 'DELETE FROM '.$webjel.'kosar WHERE kosar_id='.$_SESSION['kosar_id'];
						$result = $pdo->prepare($deletecommand);
						$result->execute();
						$kosar_id_otphez = $_SESSION['kosar_id'];
						// $rendeles_id = $_SESSION['kosar_id'];
						$rendeles_id = $last_rendeles_id;
						unset($_SESSION['kosar_id']);
						unset($_SESSION['noreg_id']);
						
						if($ovip_authCode != '') // OVIP-pal összekapcsolt
						{
							include '_ovip_rendeles_leadas.php';
						}
					?>
					<h3 style="text-align: center;">Rendelését sikeresen rögzítettük!</h3>
					<h4 style="text-align: center;">Hamarosan e-mail-ben is megküldjük a vásárlása adatait.</h4>
					<h4 style="text-align: center;">További szép napot kívánunk!</h4>
					<p style="text-align: center;">Ha még nem tette, kövessen minket a Facebookon is, hogy ne maradjon le akcióinkról és újdonságainkról!
					<br><a href="https://www.facebook.com/MareDesignHu" target="_blank"><img src="<?php print $domain; ?>/images/facebook.png" style="width: 40px; margin-top: 10px;" alt="Facebook"></a></p>
					<?php		
		
		
		
		if($config_ecommerce == 1)
		{
			$rendeles_id_etracking = $last_rendeles_id;
			include $gyoker.'/webshop/ecommerce_tracking.php';
		}
		
		
		// Ha OTP, akkor tovább kell küldeni
		if($row_f['bankkartya'] == 1)
		{
			// Éles
			/* header('Location: '.$domain.'/otpwebshop/web_demo/fiz3.php?tranzakcioAzonosito='.$kod_otp.'&posId=02202502&osszeg='.round($fizetendo).'&devizanem=HUF&nyelvkod=hu&nevKell=true&shopMegjegyzes=Rendeles azonosito: '.$kosar_id_otphez.'&backURL='.$domain.'/otpwebshop/web_demo/fiz3.php?func=fiz3'); */
			// Teszt
			header('Location: '.$domain.'/otpwebshop/web_demo/fiz3.php?tranzakcioAzonosito='.$kod_otp.'&posId=%2302299991&osszeg='.round($fizetendo).'&devizanem=HUF&nyelvkod=hu&nevKell=true&shopMegjegyzes=Rendeles azonosito: '.$kosar_id_otphez.'&backURL='.$domain.'/otpwebshop/web_demo/fiz3.php?func=fiz3');
		}
	}
	// Kassza
	else if (isset($_SESSION['kosar_id']))
	{
		?>
		<script src="<?php print $domain; ?>/webshop/scripts/kassza_4.js" type="text/javascript"></script>
		<div class="kassza">
			<form method="POST" action="" id="rendeles_leadas_form">
				<a id="adatok_horgony"></a>
				<div class="row" style="margin-top: 0px;">
					<div class="col-md-12">
						<div class="kassza_title" attr_tart_id="adatok_tartalom">
							1. Adatok <span class="kassza_ceruza" ><img src="<?php print $domain; ?>/webshop/images/g_kassza_mod.png"/></span>
						</div>
					</div>
					<div class="col-md-12 margtop10" id="adatok_tartalom">
						<?php
							include $gyoker.'/webshop/kassza_4_adatok.php';
						?>
					</div>
				</div>
				<a id="szall_mod_horgony"></a>
				<div class="row" style="margin-top: 0px;">
					<div class="col-md-12">
						<div class="kassza_title" attr_tart_id="szall_mod_tartalom">
							2. Szállítási és fizetési módok <span class="kassza_ceruza" ><img src="<?php print $domain; ?>/webshop/images/g_kassza_mod.png"/></span>
						</div>
					</div>
					<div class="col-md-12 margtop10" id="szall_mod_tartalom" style="display:none;">
						<?php
							include $gyoker.'/webshop/kassza_4_fiz_szall_mod.php';
						?>
					</div>
				</div>
				<a id="osszes_horgony"></a>
				<div class="row" style="margin-top: 0px;">
					<div class="col-md-12">
						<div class="kassza_title" attr_tart_id="osszesites_tartalom">
							3. Rendelés összesítése
						</div>
					</div>
					<div class="col-md-12 margtop10" id="osszesites_tartalom" style="display:none;">
						<?php
							include $gyoker.'/webshop/kassza_4_osszesites.php';
						?>
					</div>
				</div>
				<input type="hidden" name="command" value="rendeles_leadas"/>
			</form>
		</div>
		<?php
	}
	else
	{
		print '<p>A kosár tartalma üres.</p>';
	}
?>
