<?php

	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	// Kereső
	if(isset($_GET['keres_nev']))
	{
		$_SESSION['keres_nev'] = $_GET['keres_nev'];
	}
	// Szűrés
	if(isset($_GET['command']) && $_GET['command'] == 'szures')
	{
		$oldal = 'termekek';
		include $gyoker.'/webshop/webshop.php';
	}
	// Nézet
	if(isset($_GET['nezet_uj']))
	{
		$_SESSION['nezet'] = $_GET['nezet_uj'];
	}
	// Kívánságlista
	if (isset($_GET['kivansaglista_termek']))
	{
		if(isset($_SESSION['login_id']))
		{
			$rownum = 0;
			$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."kivansag_lista WHERE termek_id = ".$_GET['kivansaglista_termek']." AND user_id = ".$_SESSION['login_id']);
			$res->execute();
			$rownum = $res->fetchColumn();
			if ($rownum == 0) // Ha még nincs a listában
			{
				$insertcommand = "INSERT INTO ".$webjel."kivansag_lista (termek_id,user_id) VALUES (".$_GET['kivansaglista_termek'].",".$_SESSION['login_id'].")";
				$result = $pdo->prepare($insertcommand);
				$result->execute();
			}
			print '<div class="alert alert-success alert-dismissable"><p>A terméket sikeresen a kívánságlistádra helyezted. Reméljük, hamarosan átkerül a kosaradba.</p></div>';
		}
		else
		{
			print '<div class="alert alert-danger alert-dismissable"><p>Helyezd az általad választott terméket egyszerűen a kívánságlistádra és add le rendelésed akkor, amikor már biztos vagy a dolgodban. A kívánságlistádon elhelyezett termékeid aktuális állapotát érdemes figyelni nehogy lemaradj a rendelésről, hiszen termékkínálatunkat folyamatosan változtatjuk. A kívánságlista használatához regisztráció szükséges. <a href="'.$domain.'/regisztracio/">Regisztrálj!</a></p></div>';
		}
	}
	// Összehasonlítás
	if (isset($_GET['osszeh_termek']))
	{
		if($_GET['osszeh_checked'] == 'no') // Bekapcsolás
		{
			if (!isset($_SESSION['osszahas_1']))
			{
				$_SESSION['osszahas_1'] = $_GET['osszeh_termek'];
			}
			else if (!isset($_SESSION['osszahas_2']))
			{
				$_SESSION['osszahas_2'] = $_GET['osszeh_termek'];
			}
			else if (!isset($_SESSION['osszahas_3']))
			{
				$_SESSION['osszahas_3'] = $_GET['osszeh_termek'];
			}
			else if (!isset($_SESSION['osszahas_4']))
			{
				$_SESSION['osszahas_4'] = $_GET['osszeh_termek'];
			}
		}
		else
		{
			if (isset($_SESSION['osszahas_1']) && $_SESSION['osszahas_1'] == $_GET['osszeh_termek'])
			{
				unset($_SESSION['osszahas_1']);
			}
			else if (isset($_SESSION['osszahas_2']) && $_SESSION['osszahas_2'] == $_GET['osszeh_termek'])
			{
				unset($_SESSION['osszahas_2']);
			}
			else if (isset($_SESSION['osszahas_3']) && $_SESSION['osszahas_3'] == $_GET['osszeh_termek'])
			{
				unset($_SESSION['osszahas_3']);
			}
			else if (isset($_SESSION['osszahas_4']) && $_SESSION['osszahas_4'] == $_GET['osszeh_termek'])
			{
				unset($_SESSION['osszahas_4']);
			}
		}
	}
	
		
	print '<input type="hidden" id="kat_urlnev" value="'.$_GET['kat_urlnev'].'" />';
	print '<input type="hidden" id="term_urlnev" value="" />';
	
	$csop_id = 0;
	$csop_leiras = '';
	if (isset($_GET['kat_urlnev'])) // Kategória leírása a felsorolás előtt
	{
		$res = $pdo->prepare("SELECT * FROM ".$webjel."term_csoportok where nev_url=?");
		$res->execute(array($_GET['kat_urlnev']));
		$row  = $res -> fetch();
		$csop_id = $row['id'];
		$csop_nev = $row['nev'];
		$csop_leiras = $row['leiras'];
		$csop_leiras_hosszu = $row['masodik_leiras'];
		$csop_kep = '';
		if ($row['kep'] != '')
		{
			$csop_kep = $domain.'/images/termekek/'.$row['kep'];
		}
	}
	

	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id = ".$csop_id." AND lathato=1 ORDER BY sorrend asc");
	$res->execute();
	$rownum = $res->fetchColumn();


// Termékek generálása
	$csak_termek = 1;
	if ($csak_termek == 1)
	{

	// Kategória leírása
		/*
		if ($csop_leiras != '')
		{
			print '<div class="row pb-4" style="margin-top: 0px;">';
				if ($csop_kep != '')
				{
					print '<div class="col-sm-2">
						<div class="image">
							<img src="'.$csop_kep.'" class="img-thumbnail">
						</div>
					</div>
					<div class="col-sm-10 text-left">';
				}
				else
				{
					print '<div class="col-sm-12">';
				}
					print '<div class="heading-6 kategoria-leiras"><b>';
					print $csop_leiras.'
				</b></div></div>
			</div>';
		}
		*/
	//oldalankénti darabszám
		if (isset($_GET['old_db']))
		{
			$_SESSION['old_db'] = $_GET['old_db'];
			unset($_SESSION['oldszam']);
		}
		if (isset($_SESSION['old_db']))
		{
			$db_per_oldal = $_SESSION['old_db'];
		}
		else
		{
			$db_per_oldal = 48; //alapból ennyi termék jelenik meg
		}
	// Szűrő
		$szuro_sql = '';
		if (isset($_SESSION['szuro_minimum_ar'])) // Ár szerinti szűrés
		{
			$szuro_sql = ' AND IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar) >= '.$_SESSION['szuro_minimum_ar'].' AND IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar) <= '.$_SESSION['szuro_maximum_ar'].' ';
		}
	// Kereső
		if (isset($_SESSION['keres_nev']))
		{
			$szuro_sql .=  " AND (nev LIKE '%".str_replace(" ", "%", $_SESSION['keres_nev'])."%' OR cikkszam LIKE '%".$_SESSION['keres_nev']."%' OR rovid_leiras LIKE '%".str_replace(" ", "%", $_SESSION['keres_nev'])."%' OR rovid_leiras LIKE '%".htmlentities($_SESSION['keres_nev'])."%') ";
		}
	
		// Szűrő
		if (isset($_SESSION['termek_parameter_ertek'])) {

			$szuro_termek_parameter_ertek = " AND (";
			$i = 0;
			$dinamikus_paramterek = [];
			foreach ($_SESSION['termek_parameter_ertek'] as $termek_parameter_id => $termek_parameter_ertekek)
			{
				$query = "SELECT nev FROM ".$webjel."term_csoportok_parameter WHERE id=?";
				$res = $pdo->prepare($query);
				$res->execute(array($termek_parameter_id));
				$row_din = $res -> fetch();

				if ($i > 0) {
					$szuro_termek_parameter_ertek .= " AND ";
				}
				$i++;
				$szuro_termek_parameter_ertek .= "EXISTS ("
						. "SELECT 1 FROM ".$webjel."term_csoportok_parameter_ertek ttpe WHERE (REPLACE(".$webjel."termekek.rovid_leiras,'&nbsp;',' ') LIKE CONCAT('%', ttpe.ertek2, '%') OR REPLACE(".$webjel."termekek.rovid_leiras,'&nbsp;',' ') LIKE CONCAT('%', ttpe.ertek, '%') OR ".$webjel."termekek.nev LIKE CONCAT('%', ttpe.ertek, '%')) AND (";
				$szuro_termek_parameter_ertek .= "ttpe.param_id=".$termek_parameter_id." AND (";
				foreach ($termek_parameter_ertekek as $termek_parameter_ertek) {
					if (reset($termek_parameter_ertekek) !== $termek_parameter_ertek) {
						$szuro_termek_parameter_ertek .= " OR ";
					}

					$dinamikus_paramterek[$row_din['nev']][] = $termek_parameter_ertek;

					$szuro_termek_parameter_ertek .= "ttpe.ertek='".$termek_parameter_ertek."'";

				}
				$szuro_termek_parameter_ertek .= ")))";
			}
			$szuro_termek_parameter_ertek .= ")";
		}
		else {
			$szuro_termek_parameter_ertek = "";
		}
		
	//rekordok száma

/*
		$res = $pdo->prepare("SELECT COUNT(*), IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar) AS ar_sorrend FROM ".$webjel."termekek WHERE (csop_id = ".$csop_id." OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=".$webjel."termekek.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id=".$csop_id.")
					OR csop_id IN (
							SELECT
							  id
							FROM
							  (
							  SELECT
							    *
							  FROM
							    metro_term_csoportok
							  ORDER BY
							    id,
							    csop_id
							) metro_term_csoportok,
							(
							SELECT
							  @pv := '".$csop_id."'
							) initialisation
							WHERE
							  FIND_IN_SET(csop_id, @pv) > 0 AND @pv := CONCAT(@pv, ',', id))	
		) AND lathato=1".$szuro_sql.$szuro_termek_parameter_ertek);
*/
		$osszes_csoport_check = array($csop_id);

		$query_cs = "SELECT id FROM ".$webjel."term_csoportok WHERE csop_id=".$csop_id;
		foreach ($pdo->query($query_cs) as $key => $value_cs)
		{
			$osszes_csoport_check[] = $value_cs['id'];

			$query_cs_1 = "SELECT id FROM ".$webjel."term_csoportok WHERE csop_id=".$value_cs['id'];
			foreach ($pdo->query($query_cs_1) as $key => $value_cs_1)
			{
				$osszes_csoport_check[] = $value_cs_1['id'];

				$query_cs_2 = "SELECT id FROM ".$webjel."term_csoportok WHERE csop_id=".$value_cs_1['id'];
				foreach ($pdo->query($query_cs_2) as $key => $value_cs_2)
				{
					$osszes_csoport_check[] = $value_cs_2['id'];
				}				
			}			
		}

		//var_dump($osszes_csoport_check);

		$res = $pdo->prepare("SELECT COUNT(*), IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar) AS ar_sorrend FROM ".$webjel."termekek WHERE (csop_id IN(".implode(',', $osszes_csoport_check).") OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=".$webjel."termekek.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id IN(".implode(',', $osszes_csoport_check)."))) AND lathato=1".$szuro_sql.$szuro_termek_parameter_ertek);
		$res->execute();
		$rownum = $res->fetchColumn();
	//kezdés meghatározása
		if (isset($_GET['oldszam']))
		{
			$_SESSION['oldszam'] = $_GET['oldszam'];
			$_SESSION['csop_id'] = $csop_id;
		}
		if (isset($_SESSION['oldszam']) && !isset($_POST['szuro_maximum_ar']))
		{
			$kezd = $_SESSION['oldszam'];
		}
		else
		{
			$kezd = 0;
		}
	//LAPOZÁS
		print '<div class="row row-10" style="margin-top: 0px;">';
			print	'<div class="col-lg-5 col-md-5 col-sm-12">';
				//Sorrend
					
					if (isset($_GET['sorrend_uj']))
					{
						if ($_GET['sorrend_uj'] == 'nev_nov')
						{
							$_SESSION['sorr_tip'] = 'nev';
							$_SESSION['sorrend'] = 'asc';
						}
						else if ($_GET['sorrend_uj'] == 'nev_csokk')
						{
							$_SESSION['sorr_tip'] = 'nev';
							$_SESSION['sorrend'] = 'desc';
						}
						else if ($_GET['sorrend_uj'] == 'ar_nov')
						{
							$_SESSION['sorr_tip'] = 'ar_sorrend';
							$_SESSION['sorrend'] = 'asc';
						}
						else if ($_GET['sorrend_uj'] == 'ar_csokk')
						{
							$_SESSION['sorr_tip'] = 'ar_sorrend';
							$_SESSION['sorrend'] = 'desc';
						}
						else if ($_GET['sorrend_uj'] == 'sorrend')
						{
							$_SESSION['sorr_tip'] = 'sorrend DESC, ar_sorrend';
							$_SESSION['sorrend'] = 'ASC';
						}												
					}
					if (isset($_SESSION['sorr_tip']))
					{
						$sorr_tip = $_SESSION['sorr_tip'];
					}					
					else
					{
						$sorr_tip = 'sorrend DESC, ar_sorrend'; // Alap rendezési feltétel
					}
					if (isset($_SESSION['sorrend']))
					{
						$sorrend = $_SESSION['sorrend'];
					}
					else
					{
						$sorrend = 'ASC'; // Alap rendezési feltétel
					}

					
					print '<div class="select-wrap">
						<select id="sorrend_uj" name="sorrend_uj">';
								if ($sorr_tip == 'sorrend DESC, ar_sorrend' && $sorrend == 'ASC')
								{
									print '<option value="sorrend" selected>Legjobb találat</option>';
								}							
								else
								{
									print '<option value="sorrend">Legjobb találat</option>';
								}								
								if ($sorr_tip == 'ar_sorrend' && $sorrend == 'asc')
								{
									print '<option value="ar_nov" selected>Ár szerint növekvő</option>';
								}
								else
								{
									print '<option value="ar_nov">Ár szerint növekvő</option>';
								}
								if ($sorr_tip == 'ar_sorrend' && $sorrend == 'desc')
								{
									print '<option value="ar_csokk" selected>Ár szerint csökkenő</option>';
								}
								else
								{
									print '<option value="ar_csokk">Ár szerint csökkenő</option>';
								}
								if ($sorr_tip == 'nev' && $sorrend == 'asc')
								{
									print '<option value="nev_nov" selected>Név szerint növekvő</option>';
								}
								else
								{
									print '<option value="nev_nov">Név szerint növekvő</option>';
								}
								if ($sorr_tip == 'nev' && $sorrend == 'desc')
								{
									print '<option value="nev_csokk" selected>Név szerint csökkenő</option>';
								}							
								else
								{
									print '<option value="nev_csokk">Név szerint csökkenő</option>';
								}							
							print '</select></div>';
							// Nézet
							if(isset($_SESSION['nezet']))
							{
								$nezet_fajl = $_SESSION['nezet'];
							}
							else
							{
								$nezet_fajl = 'termek'; // alap nézet
							}
						print '</div>';
					
			//oldalszám kiírása
					print '<div class="col-lg-7 col-md-7 col-sm-12 lapozo_sav">';
					if (isset($_SESSION['termek_parameter_ertek'])) {					
						echo '<a href="'.$domain.'/termekek/'.$_GET['kat_urlnev'].'/" class="button button-default-outline button-xs d-md-inline-block">Szűrő törlése</a>';
					}
					print '<div class="lapozo_ikonok_div">';
					if (isset($_SESSION['sorrend'])) { $old_sorrend = '&sorrend='.$_SESSION['sorrend'];}
					if (isset($_SESSION['sorr_tip'])) { $old_sorr_tip = '&sorr_tip='.$_SESSION['sorr_tip'];}
					if (isset($_SESSION['old_db'])) { $old_old_db = '&old_db='.$_SESSION['old_db'];}
					
					$aktual_oldszam = ($kezd + $db_per_oldal) / $db_per_oldal;
					$oldal_elso = '';
					$oldal_minusz_1 = '';
					$oldal_minusz_2 = '';
					$oldal_minusz_3 = '';
					$pontok_eleje = '';
					$oldal_utolso = '';
					$pontok_vege = '';
					$oldal_plusz_1 = '';
					$oldal_plusz_2 = '';
					$oldal_plusz_3 = '';
					$oldal_aktualis = '';
					
				//Lapozó ikonok
					if (isset($_SESSION['oldszam']) &&  $_SESSION['oldszam'] > 0)
					{
						print '<span>
							<input onClick="lapozas('.($kezd - $db_per_oldal).')" type="button" title="" value="<" class="lapozo_szamok"/>
						</span>';
					}
					
					if (($aktual_oldszam) > 1)
					{
						$oldal_elso = '<span><input onClick="lapozas(0)" type="button" title="" value="1" class="lapozo_szamok" /></span>';

					}
					if($rownum > $db_per_oldal) // Aktuális oldalszám, ha nem fér el egy oldalra az össz termék
					{
						$oldal_aktualis = '<span><font class="lapozo_szamok_aktualis">'.$aktual_oldszam.'</font></span>';
					}
					if (($aktual_oldszam - 1) > 1)
					{
						$oldal_minusz_1 = '<span>
							<input onClick="lapozas('.(($aktual_oldszam - 2)*$db_per_oldal).')" type="button" title="" value="'.($aktual_oldszam - 1).'" class="lapozo_szamok"/>
						</span>';
					}
					if (($aktual_oldszam - 2) > 1)
					{
						$oldal_minusz_2 = '<span>
							<input onClick="lapozas('.(($aktual_oldszam - 3)*$db_per_oldal).')" type="button" title="" value="'.($aktual_oldszam - 2).'" class="lapozo_szamok" />
						</span>';
					}
					if (($aktual_oldszam - 3) > 1)
					{
						$oldal_minusz_3 = '<span>
							<input onClick="lapozas('.(($aktual_oldszam - 4)*$db_per_oldal).')" type="button" title="" value="'.($aktual_oldszam - 3).'" class="lapozo_szamok" />
						</span>';
					}
					if (($aktual_oldszam) > 5)
					{
						$pontok_eleje = '<span>...</span>';
					}
					$utolso_oldszam = ceil($rownum / $db_per_oldal);
					if (($aktual_oldszam) < $utolso_oldszam)
					{
						$oldal_utolso = '<span>
							<input onClick="lapozas('.(($utolso_oldszam - 1)*$db_per_oldal).')" type="button" title="" value="'.$utolso_oldszam.'" class="lapozo_szamok" />
						</span>';
					}
					if (($aktual_oldszam) < ($utolso_oldszam - 4))
					{
						$pontok_vege = '<span>...</span>';
					}
					if (($aktual_oldszam + 1) < $utolso_oldszam)
					{
						$oldal_plusz_1 = '<span>
							<input onClick="lapozas('.(($aktual_oldszam + 0)*$db_per_oldal).')" type="button" title="" value="'.($aktual_oldszam + 1).'" class="lapozo_szamok" />
						</span>';
					}
					if (($aktual_oldszam + 2) < $utolso_oldszam)
					{
						$oldal_plusz_2 = '<span>
							<input onClick="lapozas('.(($aktual_oldszam + 1)*$db_per_oldal).')" type="button" title="" value="'.($aktual_oldszam + 2).'" class="lapozo_szamok" />
						</span>';
					}
					if (($aktual_oldszam + 3) < $utolso_oldszam)
					{
						$oldal_plusz_3 = '<span>
							<input onClick="lapozas('.(($aktual_oldszam + 2)*$db_per_oldal).')" type="button" title="" value="'.($aktual_oldszam + 3).'" class="lapozo_szamok" />
						</span>';
					}
					
					// print '<span>'.$oldal_elso.$pontok_eleje.$oldal_minusz_2.$oldal_minusz_1.$oldal_aktualis.$oldal_plusz_1.$oldal_plusz_2.$pontok_vege.$oldal_utolso.'</span><span></span>';
					print '<span align="right" colspan="4">'.$oldal_elso.$pontok_eleje.$oldal_minusz_3.$oldal_minusz_2.$oldal_minusz_1.$oldal_aktualis.$oldal_plusz_1.$oldal_plusz_2.$oldal_plusz_3.$pontok_vege.$oldal_utolso.'<span></span>';
					
				//Lapozó ikonok
					if (isset($_SESSION['oldszam']) &&  $_SESSION['oldszam'] > 0)
					{
						print '<span>';
						if (($kezd + $db_per_oldal) < $rownum)
						{
							print	'<input onClick="lapozas('.($kezd + $db_per_oldal).')" type="button" title="" value=">" class="lapozo_szamok" id="balrabutton" />';
						}
						print '</span>';
					}
					else
					{
						print '<span></span>';
						if (($kezd + $db_per_oldal) < $rownum)
						{
							print '<span width="24">
								<input onClick="lapozas('.($kezd + $db_per_oldal).')" type="button" title="" value=">" class="lapozo_szamok" id="balrabutton" />
							</span>';
						}
					}
				print	'</div></div></div>';
	//TERMÉKEK
		$datum = date('Y-m-d');

/*
		$query = "SELECT *, IF(akciosar > 0 AND akcio_ig >= '".$datum."', akciosar, ar) AS ar_sorrend FROM ".$webjel."termekek
			WHERE (csop_id=".$csop_id."
					OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=".$webjel."termekek.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id=".$csop_id.")
					OR csop_id IN (
							SELECT
							  id
							FROM
							  (
							  SELECT
							    *
							  FROM
							    metro_term_csoportok
							  ORDER BY
							    id,
							    csop_id
							) metro_term_csoportok,
							(
							SELECT
							  @pv := '".$csop_id."'
							) initialisation
							WHERE
							  FIND_IN_SET(csop_id, @pv) > 0 AND @pv := CONCAT(@pv, ',', id))					
					)
			AND lathato=1 ".$szuro_sql.$szuro_termek_parameter_ertek." ORDER BY ".$sorr_tip." ".$sorrend." LIMIT ".$kezd.",".$db_per_oldal;
*/


		if (isset($dinamikus_paramterek)) {
			echo '<div class="row mt-4">';
			echo '<div class="col-12">';
			echo '<div class="bg-gray-100 p-3 border">';
			echo '<p>Kiválasztott paraméter(ek):</p>';
			echo '<ul class="list-marked mt-1">';
			foreach ($dinamikus_paramterek as $key => $value) {
				echo '<li>'.$key.': <b>'.implode(', ', $value).'</b></li>';
			}
			echo '</ul>';
			echo '</div>';
			echo '</div>';
			echo '</div>';
		}
		$items = array();			

		$query = "SELECT *, IF(akciosar > 0 AND akcio_ig >= '".$datum."', akciosar, ar) AS ar_sorrend FROM ".$webjel."termekek WHERE (csop_id IN (".implode(',', $osszes_csoport_check).") OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=".$webjel."termekek.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id IN (".implode(',', $osszes_csoport_check)."))) AND lathato=1 ".$szuro_sql.$szuro_termek_parameter_ertek." ORDER BY ".$sorr_tip." ".$sorrend." LIMIT ".$kezd.",".$db_per_oldal;
		print '<div class="row row-lg row-40" id="termek_felsorolas" style="margin-top: 40px;">';
		foreach ($pdo->query($query) as $row)
		{
			include $gyoker.'/webshop/'.$nezet_fajl.'.php';
			// include $gyoker.'/webshop/termek_hosszu.php';

			$items[] = array('sku'=>$row['cikkszam'], 'name'=>$row['nev'], 'category'=>$csop_nev, 'price'=>$row['ar_sorrend'], 'id' => $row['id']);
		}

		if (empty($items) && $szuro_termek_parameter_ertek != '') {
			echo '<div class="col-12"><p>A választott szűrésnek nincs találata. Kérlek válassz más paramétereket.</p></div>';
		}
		print '</div>';
		
	//LAPOZÁS az alján is
		print '<div class="row lapozo_sav"><div class="col-lg-12"><div class="lapozo_ikonok_div">';
					
				//Lapozó ikonok
					if (isset($_SESSION['oldszam']) &&  $_SESSION['oldszam'] > 0)
					{
						print '<span>
							<input onClick="lapozas('.($kezd - $db_per_oldal).')" type="button" title="" value="<" class="lapozo_szamok" id="balrabutton" />
						</span>';
					}
					
				//oldalszám kiírása
					
					print '<span align="right" colspan="4">'.$oldal_elso.$pontok_eleje.$oldal_minusz_3.$oldal_minusz_2.$oldal_minusz_1.$oldal_aktualis.$oldal_plusz_1.$oldal_plusz_2.$oldal_plusz_3.$pontok_vege.$oldal_utolso.'<span></span>';
					
				//Lapozó ikonok
					if (isset($_SESSION['oldszam']) &&  $_SESSION['oldszam'] > 0)
					{
						print '<span>';
						if (($kezd + $db_per_oldal) < $rownum)
						{
							print	'<input onClick="lapozas('.($kezd + $db_per_oldal).')" type="button" title="" value=">" class="lapozo_szamok" id="balrabutton" />';
						}
						print '</span>';
					}
					else
					{
						if (($kezd + $db_per_oldal) < $rownum)
						{
							print '<span width="24">
								<input onClick="lapozas('.($kezd + $db_per_oldal).')" type="button" title="" value=">" class="lapozo_szamok" id="balrabutton" />
							</span>';
						}
					}
			print '</div></div></div>';

		if ($csop_leiras_hosszu != '')
		{
			print '<div class="row">';
					print '<div class="col-sm-12">';
					print $csop_leiras_hosszu.'
				</div></div>';
		}			
	}


function getItemJs(&$item, &$darab) {
  return <<<HTML
{
  "id": "{$item['id']}",
  "publicSku": "{$item['sku']}",
  'name': '{$item['name']}',
  "list_name": '{$item['category']}',
  "list_position": $darab,
  'price': '{$item['price']}'
},
HTML;
}	

?>

<script type="text/javascript">

		gtag('event', 'view_item_list', {
		  "items": [
		  	<?php
		  	$darab = 1;
			foreach ($items as &$item) {
			  echo getItemJs($item, $darab);
			  $darab++;
			}
			?>	  
			]
		});
</script>