
						<div class="media">
						  <a href="<?=$link?>"><img src="<?=$kep_link?>" class="mr-3 img-fluid" alt="<?=$value['nev']?>" style="max-width: 150px;"></a>
						  <div class="media-body align-self-center">
						  	<div class="row">
						  		<div class="col-md-5 align-self-center">
							    	<p class="mt-0 h6"><a href="<?=$link?>" class="text-body" style="white-space: normal;"><b><?=$value['nev']?></b></a></p>
							    	<p class="mt-0 small">Cikkszám: <?=$value['cikkszam']?></p>						  			
						  		</div>
						  		<div class="col-md-4 align-self-center">
							    	<p class="mt-0 small">
							    		<?php  
						          		if ($value['raktaron'] > 0)
						          		{
						          			echo 'Raktáron';
						          		}
						          		elseif ($value['rendelheto'] == 1)
						          		{
						          			echo 'Rendelhető';

											$query = "SELECT hatarido FROM ".$webjel."beszallito WHERE hatarido != '' AND id = ".$value['beszallito_id'];
											$res = $pdo->prepare($query);
											$res->execute();
											$row_beszall = $res -> fetch();

											if (isset($row_beszall['hatarido']))
											{
												echo '<br>Várható szállítási idő: '.$row_beszall['hatarido'];
											}						          								          			
						          		}		
						          		?>					    		

							    	</p>						  			
						  		</div>
						  		<div class="col-md-3 align-self-center">
							    	<p class="mt-0 text-left text-md-right"><b><?=$value['mennyiseg']?>x  <?php echo number_format($value['termek_ara'],0,'',' '); ?> Ft</b></p>						  			
						  		</div>						  		
						    </div>
						  </div>
						</div>

