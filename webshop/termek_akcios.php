<?php

	$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row['csop_id'];
	$res_csop = $pdo->prepare($query_csop);
	$res_csop->execute();
	$row_csop  = $res_csop -> fetch();
	$link = ''.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row['nev_url'];

	$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." AND spec=0 ORDER BY alap DESC LIMIT 1";
	$res = $pdo->prepare($query_kep);
	$res->execute();
	$row_kep = $res -> fetch();
	$alap_kep = $row_kep['kep'];
	if ($alap_kep == '') 
	{
		$kep_link = $domain.'/webshop/images/noimage.png';
	}
	elseif ($row_kep['ovip_termek_id'] !=0)
	{
		if($row_kep['thumb'] != '')
		{
			$kep = $row_kep['thumb'];
		}
		else
		{
			$kep = $row_kep['kep'];
		}
		$kep_link = $kep;
	}
	else
	{
		if($row_kep['thumb'] != '')
		{
			$kep = $row_kep['thumb'];
		}
		else
		{
			$kep = $row_kep['kep'];
		}
		$kep_link = $domain.'/images/termekek/'.$kep;
	}
	//ÁR
	$datum = date("Y-m-d");

	$van_arlista = 0;

	if (isset($_SESSION['arlista']))
	{
		$query_arlista = "SELECT 
					".$webjel."arlista_arak.ar, 
					".$webjel."arlista_arak.ar_akcios,
					".$webjel."arlista_arak.akcio_tol,
					".$webjel."arlista_arak.akcio_ig,
					".$webjel."termekek.ar as term_ar,
				FROM ".$webjel."arlista
				INNER JOIN ".$webjel."arlista_arak
				ON ".$webjel."arlista_arak.arlista_id = ".$webjel."arlista.id
				INNER JOIN ".$webjel."termekek
				ON ".$webjel."arlista_arak.termek_id = ".$webjel."termekek.id
				WHERE ".$webjel."arlista.id = ".$_SESSION['arlista']." AND ".$webjel."termekek.id=".$row['id'];
		$res = $pdo->prepare($query_arlista);
		$res->execute();
		$row_arlista = $res -> fetch();

		if (isset($row_arlista['ar']))
		{
			$van_arlista = 1;
		}
	}

	if ($van_arlista == 1)
	{
		if ($row_arlista['akcio_ig'] >= $datum && $row_arlista['akcio_tol'] <= $datum)
		{
			$term_ar = '<div class="product-price" style="color: #A20000;">'.number_format($row_arlista['ar_akcios'], 0, ',', ' ').' Ft</div> <div class="text-strike small">'.number_format($row_arlista['ar'], 0, ',', ' ').' Ft</div>';
		}
		else
		{
			$term_ar = '<div class="product-price">'.number_format($row_arlista['ar'], 0, ',', ' ').' Ft</div>';
		}			
	}
	elseif ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) //Akciós
	{
		$term_ar = '<div class="product-price" style="color: #A20000;">'.number_format($row['akciosar'], 0, ',', ' ').' Ft</div> <div class="text-strike small">'.number_format($row['ar'], 0, ',', ' ').' Ft</div>';
	}
	else //nem akciós
	{
		$term_ar = '<div class="product-price">'.number_format($row['ar'], 0, ',', ' ').' Ft</div>';
	}

?>


	<div class="col-sm-4">

        <article class="post-creative"><a class="post-creative-figure" href="<?=$link?>"><img src="<?=$kep_link?>" alt="<?=$row['nev']?>"/></a>
          <div class="post-creative-content">
            <div class="heading-6 post-creative-title"><a href="<?=$link?>"><?=$row['nev']?></a></div>
            <div class="post-creative-time">
              <time datetime="2019-03-12"><?=$term_ar?></time>
            </div>
          </div><a class="post-tag post-creative-tag post-tag-gradient" href="javascript:void(0);">Akció</a>
        </article>            

    </div>               
