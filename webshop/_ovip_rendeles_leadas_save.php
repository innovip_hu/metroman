<?php
	// $rendeles_id = 51; // ezt a változót meg kell adni az include előtt
	
	include '../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	// Rendelés adatai
	$query = "SELECT * FROM ".$webjel."rendeles WHERE id=".$rendeles_id;
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	// Felhasználó
	$query_user = "SELECT * FROM ".$webjel."rendeles_cimek where rendeles_id=".$row['id'];
	$res = $pdo->prepare($query_user);
	$res->execute();
	$row_user  = $res -> fetch();

	$ovip_szallitasi_nev = $row['vasarlo_nev'];
	$ovip_szallitasi_varos = $row['vasarlo_varos'];
	$ovip_szallitasi_cim = $row['vasarlo_cim'];
	$ovip_szallitasi_irsz = $row['vasarlo_irszam'];

	if ($row_user['cim_szall_nev'] != '')
	{
		$ovip_szallitasi_nev = $row_user['cim_szall_nev'];
		$ovip_szallitasi_varos = $row_user['cim_szall_varos'];
		$ovip_szallitasi_cim = $row_user['cim_szall_utca'].' '.$row_user['cim_szall_hszam'];
		$ovip_szallitasi_irsz = $row_user['cim_szall_irszam'];
	}

	$ovip_megjegyzes = $row['megjegyzes'];

	if ($row['glscspont'] != '')
	{
		$ovip_megjegyzes .= ' GLS csomagpont: '.$row['glscspont'];
	}

	if (trim($row['pickpackpont']) != '')
	{
		$ovip_megjegyzes .= ' PickPackPont: '.$row['pickpackpont'];
	}	

	$utanvet = 0;

	if ($row['fiz_mod'] == 'Utánvéttel fizetés')
	{
		$utanvet = 1;
	}

	$rendeles = array('order_number' => $rendeles_id, 
					'name' => $row['vasarlo_nev'],
					'postal_code' => $row['vasarlo_irszam'],
					'city' => $row['vasarlo_varos'],
					'address' => $row['vasarlo_cim'],
					'tax_number' => $row_user['adoszam'],
					'email' => $row['vasarlo_email'],
					'phone' => $row_user['telefon'],
					'shipping_name' => $ovip_szallitasi_nev,
					'shipping_city' => $ovip_szallitasi_varos,
					'shipping_address' => $ovip_szallitasi_cim,
					'shipping_postal_code' => $ovip_szallitasi_irsz,
					'order_date' => $row['datum'],
					'payment_method' => $row['fiz_mod'],
					'shipping_method' => $row['szall_mod'],
					'shipping_cost' => $row['szallitasi_dij'],
					'shipping_cost_tax' => $row['szallitasi_dij_afa'],
					'order_note' => $ovip_megjegyzes,
					'delivery_point_id' => $row['glscspont_azonosito'],
					'cod' => $utanvet,
					'invoice_note' => $row['szamla_megjegyzes']);
	// Rendelés tételei
	$query_tetel = "SELECT * FROM ".$webjel."rendeles_tetelek WHERE rendeles_id=".$row['id'];
	foreach ($pdo->query($query_tetel) as $row_tetel)
	{
		$query_term = "SELECT * FROM ".$webjel."termekek WHERE id=".$row_tetel['term_id'];
		$res = $pdo->prepare($query_term);
		$res->execute();
		$row_term = $res -> fetch();

		if($row_tetel['term_akcios_ar'] > 0) // Ha akciós
		{
			$term_ar = $row_tetel['term_akcios_ar'];
		}
		else
		{
			$term_ar = $row_tetel['term_ar'];
		}
		// Paraméterek
		$query_jell = "SELECT * FROM ".$webjel."rendeles_tetelek_termek_parameterek WHERE rendeles_tetel_id = ".$row_tetel['id']." ORDER BY termek_parameter_nev ASC";
		foreach ($pdo->query($query_jell) as $row_tetelek_jell)
		{
			$row_tetel['term_nev'] .= ', '.$row_tetelek_jell['termek_parameter_nev'].': '.$row_tetelek_jell['termek_parameter_ertek_nev'];
		}												
		$rendeles['products'][] = array('product_name' => $row_tetel['term_nev'], 
								'gross_price' => $term_ar, 
								'quantity' => $row_tetel['term_db'],
								'ovip_product_id' => !empty($row_term['ovip_id']) ? $row_term['ovip_id'] : 0,
								'tax' => $row_tetel['afa']);
	}

	$request = 'sendOrder';

	$signature = hash('sha256', trim($ovip_ugyfel_id . $ovip_webshop_id . $ovip_authCode . $request . $ovip_ip_cim)); //hash generálása 

	$options = array(
	  	'location' => $ovip_soap_link,
	 	'uri' => $ovip_soap_link,
	  	'encoding' => 'UTF-8',
	  	'trace' => 1
	);

	$request = array(
		'extra_data' => $rendeles,
		'request' => $request,
		'user_id' => $ovip_ugyfel_id,
		'signature' => $signature,
		'webshop_id' => $ovip_webshop_id
	);

	try {

		$client = new SoapClient(NULL,$options);
		
		$tetelek = $client->getRequest($request);
		
		$rendeles['valasz'] = $tetelek;

		if ($tetelek['order_number'] > 0)
		{
			$eredmeny_menteshez = 'ok';
		}
		else
		{
			$eredmeny_menteshez = 'hiba';	
		}

	} catch (Exception $e) {

		$rendeles['hiba'] = $e->getMessage();
		$eredmeny_menteshez = 'hiba';	

		$riasztas_hiba = 1;

	}

	// echo $result;
	
	// Log fájl létrehozása

	$file = '/webshop/log_rendelesek/'.date('Ymd-His').'_'.$rendeles_id.'_'.$eredmeny_menteshez.'.log';
	$current =  print_r($rendeles, true);
	file_put_contents($gyoker.$file, $current);

	if (isset($riasztas_hiba)) {
		//phpMailer
		require_once($gyoker.'/webshop/PHPMailer-master/PHPMailerAutoload.php');
		$mail = new PHPMailer();
		$mail->isHTML(true);
		$mail->CharSet = 'UTF-8';
		// $mail->addCustomHeader('MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n");
		if($conf_smtp == 1)
		{
			// SMTP
			$mail->IsSMTP(); // telling the class to use SMTP
			$mail->Host       = $smtp_host; // SMTP server
			$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
													   // 1 = errors and messages
													   // 2 = messages only
			$mail->SMTPAuth   = true;                  // enable SMTP authentication
			$mail->SMTPSecure = $smtp_protokol;
			$mail->Host       = $smtp_host; // sets the SMTP server
			$mail->Port       = $smtp_port;                    // set the SMTP port for the GMAIL server
			$mail->Username   = $smtp_user; // SMTP account username
			$mail->Password   = $smtp_pass;        // SMTP account password
			$mail->SetFrom($smtp_email, $smtp_name);
			$mail->AddReplyTo($smtp_email, $smtp_name);
		}
		else
		{
			$mail->SetFrom($email, $webnev);
		}
		$mail->AddAddress($email, 'WebShop');
		$mail->Subject = "Vásárlás - Riasztás!";
		$htmlmsg = '<html>
							<body>
								<table align="left" cellpadding="12" cellspacing="0" border="0">
									<tr>
										<td>
											<p>Tisztelt Üzemeltető</p>
										</td>
									</tr>
									<tr>
										<td>
											<p><span style="color: red;">A Webáruházban az a rendelés leadás nem sikerült az OVIP felé. Az alábbi linken megtekintheti a hibát:</span> <a href="'.$domain.$file'">Hiba log!</a>.</p>
										</td>
									</tr>
									<tr>
										<td>
											'.$tabla_tetelek.'
										</td>
									</tr>
									<tr>
										<td>
											<p>Üdvözlettel:<br><a href="'.$domain.'" style="color: blue;" target="_blank"><img src="'.$domain.'/images/logo.png" width="200" title="'.$webnev.'" /></a></p>
										</td>
									</tr>
								</table>
							</body>
						</html>';
		$mail->Body = $htmlmsg;
		if(!$mail->Send()) {
		  echo "Mailer Error: " . $mail->ErrorInfo;
		}		
	}
?>
