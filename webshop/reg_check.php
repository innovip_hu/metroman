<?php

	function reg_check($adatok){
		include '../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
		
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."users WHERE email='".$adatok['email']."'");
		$res->execute();
		$rownum_email = $res->fetchColumn();
		
		if($adatok['email'] == ""| $adatok['telefon'] == "" | $adatok['vezeteknev'] == "" |  $adatok['cim_varos'] == "" | $adatok['cim_utca'] == "" | $adatok['cim_hszam'] == "" | $adatok['cim_irszam'] == "" | $adatok['jszo1'] == "" | $adatok['jszo2'] == "" | $adatok['szla_nev'] == "")
		{
			return "Minden mező kitöltése kötelező!";
		}
		else if( $rownum_email > 0)
		{
			return "Már regisztráltak ezzel az e-mail címmel!";
		} 
		else if (strlen($_POST['jszo1']) < 8) {
			return "A jelszónak minimum 8 karakternek kell lennie!";
		}
		else if($_POST['jszo1'] != $_POST['jszo2'] )
		{
			return "A két jelszó nem egyezik!";
		}
		else if((isset($_POST['feltetel']) && $_POST['feltetel'] != "elfogad") || !isset($_POST['feltetel']))
		{
			return "A regisztrációs feltétel nincs elfogadva!";
		}
		else
		{
			return "rendben";
		}
	}
?>