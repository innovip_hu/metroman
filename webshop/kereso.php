	<?php
		if ($_GET['keres_nev'] == '')
		{
			print '<p>Nincs megadva keresési feltétel!</p>';
		}
		else
		{
			$keres_szoveg = str_replace(" ", "%", $_GET['keres_nev']);
		// Kategóriák
			
			
			$keres_szoveg_tomb = array();
			//$keres_szoveg_tomb = explode(" ",$_GET['keres_nev']);
			$keres_szoveg_tomb = preg_split('/[\s]+/', $_GET['keres_nev']);
			$feltetel = 'AND (';
			foreach($keres_szoveg_tomb as $key => $row_tomb)
			{
				if ($feltetel != 'AND (')
				{
					$feltetel .= ' AND ';
				}				
				$feltetel .= "(nev LIKE :feltetel".$key." OR cikkszam LIKE :feltetel".$key." OR rovid_leiras LIKE :feltetel".$key." OR rovid_leiras LIKE :feltetelh".$key.")";
			}
			$feltetel .= ')';

			$kategoriak_listak = [];

			$query = "SELECT DISTINCT csop_id FROM ".$webjel."termekek WHERE lathato = 1 ".$feltetel;
			$result = $pdo->prepare($query);
			foreach ($keres_szoveg_tomb as $key => $value)
			{
				$result->bindValue(':feltetel'.$key, '%'.$value.'%');
				$result->bindValue(':feltetelh'.$key, '%'.htmlentities($value).'%');
			}
			$result->execute();				
			foreach ($result as $row)
			{
				$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE lathato = 1 AND kifutott=0 ".$feltetel." AND csop_id=".$row['csop_id']);
				foreach ($keres_szoveg_tomb as $key => $value)
				{
					$res->bindValue(':feltetel'.$key, '%'.$value.'%');
					$res->bindValue(':feltetelh'.$key, '%'.htmlentities($value).'%');
				}				
				$res->execute();
				$rownum = $res->fetchColumn();
	
				$query_kat = "SELECT * FROM ".$webjel."term_csoportok WHERE id=".$row['csop_id'];
				$res = $pdo->prepare($query_kat);
				$res->execute();
				$row_kat = $res -> fetch();

				$kategoriak_listak[] = ['elem' => '<a href="'.$domain.'/termekek/'.$row_kat['nev_url'].'/?keres_nev='.$_GET['keres_nev'].'">'.$row_kat['nev'].' ('.$rownum.')</a>', 'darab' => $rownum];
			}
			uasort($kategoriak_listak, function($a, $b) {
			    return $a['darab'] - $b['darab'];
			});

			$kategoriak_listak = array_reverse($kategoriak_listak);
			print '<div class="kereso_talalat_kategoria">';
			foreach ($kategoriak_listak as $key => $value) {
				if ($key < 20) {
					echo $value['elem'];
				}
				
			}
			print '</div>';

		// Termékek
			if (isset($_GET['sorr_tip']))
			{
				$sorr_tip = $_GET['sorr_tip'];
			}
			else
			{
				$sorr_tip = 'ar_sorrend'; // Alap rendezési feltétel
			}
			if (isset($_GET['sorrend']))
			{
				$sorrend = $_GET['sorrend'];
			}
			else
			{
				$sorrend = 'ASC'; // Alap rendezési feltétel
			}
			print '<form action="" method="GET" id="kereso_sorrend_form"><select class="form-control" id="kereso_sorrend" name="sorrend">';
				if ($sorr_tip == 'ar_sorrend' && $sorrend == 'ASC')
				{
					print '<option attr_sorrend="ASC" attr_sorr_tip="ar_sorrend" value="ar_nov" selected>Ár szerint növekvő</option>';
				}
				else
				{
					print '<option attr_sorrend="ASC" attr_sorr_tip="ar_sorrend" value="ar_nov">Ár szerint növekvő</option>';
				}
				if ($sorr_tip == 'ar_sorrend' && $sorrend == 'DESC')
				{
					print '<option attr_sorrend="DESC" attr_sorr_tip="ar_sorrend" value="ar_csokk" selected>Ár szerint csökkenő</option>';
				}
				else
				{
					print '<option attr_sorrend="DESC" attr_sorr_tip="ar_sorrend" value="ar_csokk">Ár szerint csökkenő</option>';
				}
				if ($sorr_tip == 'nev' && $sorrend == 'ASC')
				{
					print '<option attr_sorrend="ASC" attr_sorr_tip="nev" value="nev_nov" selected>Név szerint növekvő</option>';
				}
				else
				{
					print '<option attr_sorrend="ASC" attr_sorr_tip="nev" value="nev_nov">Név szerint növekvő</option>';
				}
				if ($sorr_tip == 'nev' && $sorrend == 'DESC')
				{
					print '<option attr_sorrend="DESC" attr_sorr_tip="nev" value="nev_csokk" selected>Név szerint csökkenő</option>';
				}
				else
				{
					print '<option attr_sorrend="DESC" attr_sorr_tip="nev" value="nev_csokk">Név szerint csökkenő</option>';
				}
				if ($sorr_tip == 'id' && $sorrend == 'DESC')
				{
					print '<option attr_sorrend="DESC" attr_sorr_tip="id" value="id_csokk" selected>Legújabbak elöl</option>';
				}
				else
				{
					print '<option attr_sorrend="DESC" attr_sorr_tip="id" value="id_csokk">Legújabbak elöl</option>';
				}
			print '</select>
				<input type="hidden" name="keres_nev" value="'.$_GET['keres_nev'].'" />
				<input type="hidden" name="sorr_tip" id="sorr_tip" value="" />
				<input type="hidden" name="sorrend" id="sorrend" value="" />
			</form>';
			if (isset($_GET['sorr_tip']))
			{
				$sorr_tip = $_GET['sorr_tip'];
			}
			else
			{
				$sorr_tip = 'ar_sorrend'; // Alap rendezési feltétel
			}
			if (isset($_GET['sorrend']))
			{
				$sorrend = $_GET['sorrend'];
			}
			else
			{
				$sorrend = 'ASC'; // Alap rendezési feltétel
			}
			
			$i = 0;
			// Generálás
			$query = "SELECT id, csop_id, nev_url, akcio_ig, akcio_tol, ar, akciosar, rovid_leiras, raktaron, kifutott, ovip_torolt, rendelheto, nev, IF(akciosar > 0 AND akcio_ig >= '".date('Y-m-d')."', akciosar, ar) AS ar_sorrend FROM ".$webjel."termekek WHERE lathato = 1 AND kifutott = 0 ".$feltetel." ORDER BY sorrend DESC,".$sorr_tip." ".$sorrend." LIMIT 48";
			$result = $pdo->prepare($query);
			foreach ($keres_szoveg_tomb as $key => $value)
			{
				$result->bindValue(':feltetel'.$key, '%'.$value.'%');
				$result->bindValue(':feltetelh'.$key, '%'.htmlentities($value).'%');
			}
			$result->execute();			
			$a = 0;
			print '<div class="row row-30 kapcs_termekek">';
			foreach ($result as $row)
			{
				$i++;
				$a = 1;
				$kiemelt_termek = 1;
				include $gyoker.'/webshop/termek.php';
			}

			if ($i == 48) {
				print '<div class="col-md-12 text-center"><p><b>A keresés maximálisan 48 terméket tartalmaz. Szűkítsd tovább a találatokat további kulcsszavak megadásával.</b></p>';

				?>

					<form class="search_form pt-4" action="<?php print $domain; ?>/kereso/" method="GET">
				        <div class="input-group">
				          <input style="height: auto;" type="search" class="form-control" aria-label="Nem találod? Keresd itt." aria-describedby="button-addon2" name="keres_nev" value="<?=$_GET['keres_nev']?>" placeholder="Nem találod? Keresd itt." autocomplete="off">
				          <div class="input-group-append">
				            <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><span class="fa fa-search"></span></button>
				          </div>
				        </div> 			
					</form>


					</div>
				<?php				
			}
			print '</div>';
			if ($a == 0)
			{
				print '<p>A megadott keresési feltételre nincs találat! Próbáljon meg hasonló kifejezést keresni.</p>';

				?>

					<form class="search_form pt-4" action="<?php print $domain; ?>/kereso/" method="GET">
				        <div class="input-group">
				          <input style="height: auto;" type="search" class="form-control" aria-label="Nem találod? Keresd itt." aria-describedby="button-addon2" name="keres_nev" value="" placeholder="Nem találod? Keresd itt." autocomplete="off">
				          <div class="input-group-append">
				            <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><span class="fa fa-search"></span></button>
				          </div>
				        </div> 			
					</form>

				<?php
			}
		}