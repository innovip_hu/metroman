<?php
	// Link
	// if (isset($_GET['keres_nev']) || isset($_GET['keres_nev']) || isset($akcios_termek) || (isset($legujabb_termek) && $legujabb_termek == 'ok') || (isset($legnepszerubb_termekek) && $legnepszerubb_termekek == 'ok'))
	// {
		$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row['csop_id'];
		$res_csop = $pdo->prepare($query_csop);
		$res_csop->execute();
		$row_csop  = $res_csop -> fetch();
		$link = ''.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row['nev_url'];
	// }
	// else
	// {
		// $link = ''.$domain.'/termekek/'.$_GET['kat_urlnev'].'/'.$row['nev_url'];
	// }
	// Kép
	$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." AND spec=0 ORDER BY alap DESC LIMIT 1";
	$res = $pdo->prepare($query_kep);
	$res->execute();
	$row_kep = $res -> fetch();
	$alap_kep = $row_kep ? $row_kep['kep'] : "";
	if ($alap_kep == '') 
	{
		$kep_link = $domain.'/webshop/images/noimage.png';
	}
	elseif ($row_kep['ovip_termek_id'] !=0)
	{
		if($row_kep['thumb'] != '')
		{
			$kep = $row_kep['thumb'];
		}
		else
		{
			$kep = $row_kep['kep'];
		}
		$kep_link = $kep;
	}
	else
	{
		if($row_kep['thumb'] != '')
		{
			$kep = $row_kep['thumb'];
		}
		else
		{
			$kep = $row_kep['kep'];
		}
		$kep_link = $domain.'/images/termekek/'.$kep;
	}
	//ÁR
	$datum = date("Y-m-d");

	$van_arlista = 0;

	if (isset($_SESSION['arlista']))
	{
		$query_arlista = "SELECT 
					".$webjel."arlista_arak.ar, 
					".$webjel."arlista_arak.ar_akcios,
					".$webjel."arlista_arak.akcio_tol,
					".$webjel."arlista_arak.akcio_ig,
					".$webjel."termekek.ar as term_ar
				FROM ".$webjel."arlista
				INNER JOIN ".$webjel."arlista_arak
				ON ".$webjel."arlista_arak.arlista_id = ".$webjel."arlista.id
				INNER JOIN ".$webjel."termekek
				ON ".$webjel."arlista_arak.termek_id = ".$webjel."termekek.id
				WHERE ".$webjel."arlista.id = ".$_SESSION['arlista']." AND ".$webjel."termekek.id=".$row['id'];
		$res = $pdo->prepare($query_arlista);
		$res->execute();
		$row_arlista = $res -> fetch();

		if (isset($row_arlista['ar']))
		{
			$van_arlista = 1;
		}
	}

	if ($van_arlista == 1)
	{
		if ($row_arlista['akcio_ig'] >= $datum && $row_arlista['akcio_tol'] <= $datum)
		{
			$term_ar = '<div class="product-price akcios-ar">'.number_format($row_arlista['ar_akcios'], 0, ',', ' ').' Ft <span class="text-body fa fa-info-circle d-md-inline d-none" title="Az áthúzott ár az árcsökkentés alkalmazását megelőző 30 nap legalacsonyabb eladási ára."></span></div> <div class="text-strike akcios-regi-ar">'.number_format($row_arlista['ar'], 0, ',', ' ').' Ft</div>';
		}
		else
		{
			$term_ar = '<div class="product-price normal-ar">'.number_format($row_arlista['ar'], 0, ',', ' ').' Ft</div>';
		}			
	}
	elseif ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) //Akciós
	{
		$term_ar = '<div class="product-price akcios-ar"><div class="text-strike akcios-regi-ar text-muted font-weight-normal">'.number_format($row['ar'], 0, ',', ' ').' Ft <span class="text-body fa fa-info-circle d-md-inline d-none" title="Az áthúzott ár az árcsökkentés alkalmazását megelőző 30 nap legalacsonyabb eladási ára."></span></div>'.number_format($row['akciosar'], 0, ',', ' ').' Ft</div>';
		$akcios_blokk = 'akcios-blokk';
	}
	else //nem akciós
	{
		$term_ar = '<div class="product-price normal-ar">'.number_format($row['ar'], 0, ',', ' ').' Ft</div>';
		$akcios_blokk = '';
	}

?>

<?php if (isset($kiemelt_termek)): ?>
			<div class="col-sm-6 col-xl-3">
<?php else: ?>
			<div class="col-sm-6 col-xl-4">
<?php endif ?>



              <!-- Products-->
              <article class="product block-md <?=$akcios_blokk?>">
              	<?php 
              		/*
				<div class="termek_plecsnik" style="left: 0; top: 0;">
					<?php
						$query = "SELECT * 
						FROM ".$webjel."termek_uj_parameter_ertekek
						INNER JOIN ".$webjel."termek_uj_parameterek
						ON ".$webjel."termek_uj_parameter_ertekek.parameter_id = ".$webjel."termek_uj_parameterek.id
						WHERE termek_id=".$row['id']." AND ".$webjel."termek_uj_parameterek.plecsni = 1";
						foreach ($pdo->query($query) as $value) { ?>

							<img src="<?=$domain?>/images/termekek/<?=$value['ikon']?>" alt="<?=$value['ikon']?>" style="margin: -10px 0; max-width: 40px; height: auto;"><br>
							
					<?php	 }
					?>
				</div>
	               	*/

				$szoveg_javitott = preg_replace('/<h3>(.*?)<\/h3>/s', '', $row['rovid_leiras']);
				$szoveg_javitott = strip_tags(html_entity_decode($szoveg_javitott),'<span><strong><br><br />');
				//$szoveg_javitott = nl2br($szoveg_javitott);
              	?>
                <div class="product-body">            	
                  <div class="product-info">
                    <div class="heading-6 product-title"><b><a href="<?=$link?>" style="white-space: initial;"><?=$row['nev']?></a></b></div>
                    <div class="product-figure fels_termek_kep">
                    	<a href="<?=$link?>"><img src="<?=$kep_link?>" alt="<?=$row['nev']?>" style="max-height: 178px; width: auto;"/></a>
                    </div>
                  </div>
                  <div class="product-description pt-1">
                    <p class="lista-small">
                    	<?=$szoveg_javitott?>	
                	</p>
                    <a class="button button-primary button-sm" href="<?=$link?>" style="margin-top: 20px;">Megnézem</a>
                  </div>
                </div>
                <div class="product-panel">
                  <?=$term_ar?>
                  <div class="product-share">
                  	<?php 
                  		if ($row['raktaron'] > 0)
                  		{
                  			$keszleten = 'Raktáron';
                  			$class_r = 'raktaron';
                  		}
                  		elseif ($row['rendelheto'] == 1)
                  		{
                  			$keszleten = 'Rendelhető';
                  			$class_r = 'raktaron-rendelheto';
                  		}
                  		elseif ($row['ovip_torolt'] == 1)
                  		{
                  			$keszleten = 'Kifutott';
                  			$class_r = 'nemrendelheto-kifutott';
                  		}
                  		elseif ($row['kifutott'] == 1)
                  		{
                  			$keszleten = 'Nem rendelhető';
                  			$class_r = 'nemrendelheto-kifutott';
                  		}                  		
                  		else
                  		{
                  			$keszleten = 'Nem rendelhető';	
                  			$class_r = 'nemrendelheto-kifutott';
                  		}
                  	?>
											<div class="<?=$class_r?>"><?=$keszleten?></div>
                  </div>
                </div>
              </article>
            </div>               
