<?php 
  session_start();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
      <?php 
        $oldal = 'fooldal';
        include 'config.php';
        include $gyoker.'/module/mod_head.php';
      ?>

      <title><?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">      
  </head>
  <body>
    <?php include $gyoker.'/module/mod_body_start.php'; ?>
    <div class="page">
      <?php include $gyoker.'/module/mod_header.php'; ?>
      <!--Swiper-->
      <section class="section swiper-container swiper-slider swiper-slider-6 bg-default bg-image-1 overlay-creative" data-autoplay="5000" data-simulate-touch="false" data-slide-effect="fade" style="background-image: url(images/hatter.png?v=01); background-position: center; background-repeat: repeat;">
        <div class="swiper-wrapper">
        <?php
          $query = "SELECT * FROM ".$webjel."slider WHERE sorrend != 100 ORDER BY sorrend ASC";
          foreach ($pdo->query($query) as $row)
          { ?>  

          <div class="swiper-slide">
            <div class="swiper-slide-caption">
              <div class="container">
                <div class="row row-30 justify-content-center flex-column-reverse flex-lg-row">
                  <div class="col-lg-6">
                    <div class="swiper-media-box"><img src="images/termekek/<?=$row['kep']?>" alt="<?=$row['nev']?>"/>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="swiper-caption-box">
                      <div class="heading-1"><span class="d-inline-block d-lg-block"><?=$row['nev']?></span></div>
                      <div class="swiper-caption-box__subtitle"><span><?=$row['szoveg']?></span></div>
                      <?php if ($row['link'] != ''): ?>
                        <a class="button button-gradient button-lg" href="<?=$row['link']?>"><?=$row['gomb_felirat']?></a>
                      <?php endif ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <?php
          }
        ?>


        </div>
        <!--Swiper Pagination-->
        <div class="swiper-pagination"></div>
        <!--Swiper Navigation-->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
      </section>
      <section class="section">
        <!-- Owl Carousel-->
        <div class="owl-carousel owl-creative" data-loop="true" data-items="1" data-sm-items="2" data-xl-items="2"  data-xxl-items="4" data-dots="false" data-mouse-drag="false" data-autoplay="true">
          <!-- Box abby-->
          <article class="box-abby bg-primary" style="cursor: pointer;" onclick="window.location.href='termekek/Auto-Motor-Hajo/'">
            <div class="box-abby__media"><img src="images/banner01__auto_motor_hajo.jpg" alt="Autó, Motor & Hajó" width="298" height="447"/>
            </div>
            <div class="heading-6 box-abby-subtitle text-white">Autó, Motor & Hajó</div>
            <p class="box-abby__caption">Autó Hifi, 12-24 Voltos autós elektronikai, kényelmi kiegészítők és felszerelések nagy választékban.</p>
          </article>
          <!-- Box abby-->
          <article class="box-abby bg-red-2" style="cursor: pointer;" onclick="window.location.href='termekek/Muszaki-cikkek/'">
            <div class="box-abby__media"><img src="images/banner03__muszaki cikk.jpg" alt="Műszaki cikk" width="298" height="447"/>
            </div>
            <div class="heading-6 box-abby-subtitle text-white">Műszaki cikk</div>
            <p class="box-abby__caption">Karaoke, Telefon, Kamera, Műszer, Tápegység, Napenergia, Akkumulátor, Számítástechnikai és Elektronikai cikkek  és rengeteg érdekesség egy helyen.</p>
          </article>
          <!-- Box abby-->
          <article class="box-abby bg-primary" style="cursor: pointer;" onclick="window.location.href='termekek/Biztonsagtechnika/'">
            <div class="box-abby__media">
                <img src="images/banner02__biztonsagtechnika.jpg" alt="Biztonságtechnika" width="298" height="447"/>
            </div>
            <div class="heading-6 box-abby-subtitle text-white">Biztonságtechnika</div>
            <p class="box-abby__caption">Komplett riasztók, Megfigyelő rendszerek, Kaputechnika, Kaputelefon, Beléptető rendszerek  kedvező áron.</p>
          </article>
          <article class="box-abby bg-accent-red" style="cursor: pointer;" onclick="window.location.href='termekek/Otthon-kert-hobbi/'">
            <div class="box-abby__media"><img src="images/banner04__otthn_kert_hobbi.jpg" alt="Otthon, kert, hobbi" width="298" height="447"/>
            </div>
            <div class="heading-6 box-abby-subtitle text-white">Otthon, kert, hobbi</div>
            <p class="box-abby__caption">Háztartási gép, Világítás, Dekoráció, Fűtés-Hűtés, Villamosság, Kisállat Riasztók, Hobbi és Kerti kiegészítők minden korosztály számára.</p>
          </article>          
        </div>
      </section>

      <section class="section section-xl bg-default text-center">
        <div class="container-fluid position-relative">
          <h1 class="font-weight-bold wow fadeInUp heading-2"><?php echo $pdo->query("SELECT kiemelt_szekcio FROM ".$webjel."beallitasok WHERE id=1")->fetchColumn(); ?></h1>
          <div class="row row-xl row-30 row-md-50 kapcs_termekek">
              <?php 
                  $query = "SELECT * FROM ".$webjel."termekek WHERE kiemelt = 1 AND lathato = 1 ORDER BY RAND() LIMIT 8";
                  foreach ($pdo->query($query) as $row)
                  {
                    $kiemelt_termek = 1;
                    include $gyoker.'/webshop/termek.php';
                  }
              ?>
            <div class="col-12"><a class="button button-gradient button-md" href="<?=$domain?>/termekek">További termékeink</a></div>
          </div>
        </div>
      </section>

      <?php 
        $res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirek WHERE `datum` <= CURDATE()");
        $res->execute();
        $rownum = $res->fetchColumn();
      ?>

      <?php if ($rownum > 0): ?>

      <section class="section section-lg bg-gray-100">
        <div class="container-fluid">
          <h2 class="font-weight-bold wow fadeInUp text-center">Hírek</h2>
          <div class="row row-xl row-30">

            <?php include $gyoker.'/webshop/hirek.php'; ?>

            <div class="col-12 text-center"><a class="button button-gradient button-md" href="<?=$domain?>/hirek">További híreink</a></div>
          </div>
        </div>
      </section>      

      <?php endif ?>

      <!--Icon List-->
      <section class="section bg-gray-13">
        <div class="row no-gutters bordered-1">
          <?php $query = "SELECT * FROM ".$webjel."hirek2 ORDER BY sorrend ASC"; ?>
          <?php foreach ($pdo->query($query) as $key => $value): ?>
            <div class="col-sm-6 col-xl-3">
              <!-- Box classic-->
              <article class="box-classic box-advantages box-classic-1 wow fadeInUp h-100" style="cursor: pointer;" onclick="window.location.href='<?=$value['elozetes']?>'">
                <div class="box-classic-icon mdi <?=$value['ikon']?>"></div>
                <div class="heading-4 box-classic-title"><?=$value['cim']?></div>
                <div class="big box-classic-text"><?=$value['tartalom']?></div>
              </article>
            </div>            
          <?php endforeach ?>

        </div>
      </section>

       <!--Our blog-->
      <section class="section section-lg bg-default text-center">
        <div class="container">
          <h2 class="font-weight-bold wow fadeInUp">Akcióink a készlet erejéig</h2>
          <!-- Owl Carousel-->
          <div class="owl-carousel owl-style-4 owl-nav-1" data-items="1" data-nav="true" data-dots="true" data-autoplay="true" data-margin="30" data-xl-margin="170" data-smart-speed="400">


            <?php
                unset($kiemelt_termek);
                $data_darab = 0; 

                $query = "SELECT * FROM ".$webjel."termekek WHERE akcio_ig >= CURDATE() AND akcio_tol <= CURDATE() AND lathato = 1 ORDER BY RAND() LIMIT 8";
                foreach ($pdo->query($query) as $row)
                { 
                  $data_darab++;
                  if ($data_darab == 1)
                  {
                    echo '<div class="row row-30 kapcs_termekek">';          
                  }
                  
                  include $gyoker.'/webshop/termek.php';

                  if ($data_darab == 3)
                  {
                    echo '</div>';          
                    $data_darab = 0;
                  }                  
                }
            ?>            

          </div>
        </div>
      </section>

      <!--About us-->
      <section class="section bg-gray-100 section-xxl position-relative overflow-hidden">
        <div class="floating-text__left">Metroman</div>
        <div class="container">
          <h3 class="font-weight-bold wow fadeInUp text-center heading-2">Megbízhatóság felsőfokon</h3>
          <div class="row justify-content-center flex-lg-row-reverse row-40">
            <div class="col-md-11 col-lg-6 wow fadeInRight align-self-center">
              <p>Közel 25 éve foglalkozunk elektronikai termékekkel, mára TÖBB EZER terméket érhetsz el nálunk, ráadásul a  webshopok közül kiemelkedünk hatalmas raktárkészletünkkel, így akár már másnap megkaphatod rendelésedet. A szállítási és fizetési módokat is úgy alakítottuk ki, hogy a legkényelmesebben legyen vásárlóinknak. Már 60.000Ft-os vásárlástól megkaphatod rendelésedet INGYENES SZÁLLÍTÁSSAL.</p>
              <p>Jelenlegi profilunk az autós és motoros felszerelés, elektronikai eszközök és kiegészítők, szórakoztató elektronikai, villamossági valamint biztonságtechnikai és sok egyéb otthon, a ház körül hasznos  termékek forgalmazása.</p>
            </div>
            <div class="col-sm-10 col-md-6 col-lg-6 wow fadeIn">
              <!-- Video modern-->
              <article class="video-modern">
                <img class="img-responsive" src="images/Metroman_uzlet.jpg" alt="Üdvözöljük weboldalunkon"/>
              </article>
            </div>
          </div>
        </div>
      </section>

      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>
    <script type="text/javascript" src="<?php print $domain; ?>/scripts/sajat.js"></script>

    

    <script>
        $(document).ready(function(){

            $('.owl-creative').each(function(){  

                var highestBox = 0;
                $('.box-abby__caption', this).each(function(){

                    if($(this).height() > highestBox) 
                    highestBox = $(this).height(); 
                });  

                $('.box-abby__caption',this).height(highestBox);

            });  
        });

    </script>    
  </body>
</html>