<?php 
  session_start();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
      <?php 
        $oldal = 'aszf';
        include '../config.php';
        include $gyoker.'/module/mod_head.php';
      ?>

      <title>ÁSZF | <?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">    

      <style>
        #gendoc_aszf * {
          font-size: unset;
        }

        #gendoc_aszf h1, #gendoc_aszf h1 {
            padding: unset;
            margin: unset;
        }        

        h3 + p, h2 + p {
            margin-top: 0px;
        }   

        h2,h3 {
          margin-top: 30px;
        }     
      </style>        
  </head>
  <body>
    <?php include $gyoker.'/module/mod_body_start.php'; ?>
    <div class="page">
      <?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(../images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                <h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5">Általános szerződési feltételek</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">

          </div>
        </div>
      </section>

      <section class="section section-lg bg-default">
        <div class="container">
            <ul class="breadcrumbs-custom-path">
              <li><a href="<?=$domain?>">Főoldal</a></li>
              <li class="active">ÁSZF</li>
            </ul>          
          <div class="row row-30 justify-content-center">
            <div class="col-md-12">
                <script id="barat_script">var st = document.createElement("script");st.src = "//admin.fogyasztobarat.hu/e-api.js";st.type = "text/javascript";st.setAttribute("data-id", "ZRFI4WU7");st.setAttribute("id", "fbarat-embed");st.setAttribute("data-type", "aszf");var s = document.getElementById("barat_script");s.parentNode.insertBefore(st, s);</script>
                <?php /*<div id="gendoc_aszf" style="width: 100%; overflow-y: auto;"><p><span class="fa fa-spinner fa-spin"></span> Betöltés</p></div>*/ ?>
            </div>
          </div>
        </div>
      </section>      

      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>
<?php /* 
    <script type="text/javascript">jQuery.get("https://admin.fogyasztobarat.hu/api.php?aszf=ZRFI4WU7&nonformatted", function(ret){jQuery("div#gendoc_aszf").html(ret).promise().done(function(){$('table').each(function(e){$(this).addClass('table table-bordered table-striped table-condensed');
          });});var sl=window.location.href.split("#");if(jQuery("#"+sl[1]).size()){jQuery("html, body").animate({scrollTop:jQuery("#"+sl[1]).offset().top},1000); }}); </script>
      */ ?>


  </body>
</html>