<?php
  	session_start();
 	ob_start();

    include 'config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}


	$filenev = "webshop/gls/gls".date('Ymd').".csv";
	$link ="http://datarequester.gls-hungary.com/glsconnect/getDropoffPoints_csv.php?ctrcode=hu";
		file_put_contents($filenev,fopen($link, 'r'));

			if (file_exists($filenev)) { //Csak akkor fut, ha létezik a generált fájl

					$pdo-> exec("TRUNCATE ".$webjel."gls_csomagpontok"); //Tábla ürítése

						$handle = fopen($filenev,"r");
						$flag = true;
						while (($data = fgetcsv($handle,0,";")) != FALSE)
						{	
							$data = array_map("utf8_encode", $data); //UTF-8 konvertálás FONTOS
							if($flag) { $flag = false; continue; } // első sor skip
							$id = $data[0];
							$irszm = $data[2];
							$varos = $data[3];
							$varos = str_replace("õ", "ő", $varos);
							$varos = str_replace("Õ", "Ő", $varos);
							$cim = $data[4];
							$nev = $data[5];
							$automata = $data[9];

							//var_dump($data);

							$insertcommand = "INSERT INTO ".$webjel."gls_csomagpontok (nev,irszam,varos,cim,azonosito,automata) VALUES (:nev,:irszam,:varos,:cim,:azonosito,:automata)";
							$result = $pdo->prepare($insertcommand);
							$result->execute(array(':nev'=>$nev,
											  ':irszam'=>$irszm,
											  ':varos'=>$varos,
											  ':cim'=>$cim,
											  ':automata'=>$automata,
											  ':azonosito'=>$id));

							//$pdo->exec("INSERT INTO ".$webjel."gls_csomagpontok (nev,irszam,varos,cim,azonosito) VALUES ('".$nev."','".$irszm."','".$varos."','".$cim."','".$id.")");
						}		    	
			} else
			{
				echo "A szinkronizálás sikertelen!";
			}
?>
