<?php

	include 'config.php';
  	// PDO
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolodni az adatbózishoz!");
	}

	function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
	    header('Content-Type: application/csv;charset=UTF-8');
	    header('Content-Disposition: attachment; filename="'.$filename.'";');

	    // open the "output" stream
	    // see http://www.php.net/manual/en/wrappers.php.php#refsect2-wrappers.php-unknown-unknown-unknown-descriptioq
	    ob_clean();
	    $f = fopen('php://output', 'w');

	    foreach ($array as $line) {
	        fputcsv($f, $line, $delimiter);
	    }
	}   

	$keplink = $domain . '/images/termekek/';
	$csv_array = [];
	$csv_array[] = ['kep','termek_cikkszam'];

	$query = "SELECT tk.kep,t.cikkszam,tk.id FROM ".$webjel."termek_kepek tk
			INNER JOIN ".$webjel."termekek t
			ON t.id = tk.termek_id
			WHERE tk.spec = 0 AND lekerdezve = 0
			LIMIT 1000";
	foreach ($pdo->query($query) as $key => $value) {
		$size = getimagesize($keplink . rawurlencode($value['kep']));

		if ($size && $size[1] > $size[0]) {
			$csv_array[] = [$value['kep'],$value['cikkszam']];
		}

		$pdo->exec("UPDATE ".$webjel."termek_kepek SET lekerdezve=1 WHERE id=".$value['id']);
	}

	array_to_csv_download($csv_array,'termek_kepek.csv');

?>