<?php 
    session_start();
    $protocol = 'HTTP/1.0';

    if ( $_SERVER['SERVER_PROTOCOL'] === 'HTTP/1.1' ) {
        $protocol = 'HTTP/1.1';
    }

    header( $protocol . ' 503 Service Unavailable', true, 503 );
    header( 'Retry-After: 3600' );        
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
      <?php 
        $oldal = '503';
        include 'config.php';
        include $gyoker.'/module/mod_head.php';
      ?>

      <title>Karbantartás - <?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">      
  </head>
  <body>
    <?php include $gyoker.'/module/mod_body_start.php'; ?>
    <div class="page">
      <?php include $gyoker.'/module/mod_header.php'; ?>
 
      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(../images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                <h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5 fejresz-title">Karbantartás</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">
            <ul class="breadcrumbs-custom-path">
              <li><a href="<?=$domain?>">Főoldal</a></li>
              <li class="active">503</li>
            </ul>
          </div>
        </div>
      </section>

      <section class="section section-lg bg-default text-center">
        <div class="container">

          <h5 class="title-4">A szolgáltatás jelenleg nem elérhető!</h5>

          <p>Kedves látogató! Az oldal jelenleg karbantartás miatt nem üzemel!<br>Kérjük térjen vissza néhány órával később!</p>

        </div>
      </section>


      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>
    <script type="text/javascript" src="<?php print $domain; ?>/scripts/sajat.js"></script>
  </body>
</html>