/*-----------------------------------------------------------------------------------*/
/*	Kateg�ria lenyit�
/*-----------------------------------------------------------------------------------*/
 $(window).on("load", function() {
	$( "#mobil_kat_lenyito" ).click(function() {
		$( "#kategoria_menu" ).animate({
			height: [ "toggle", "swing" ]
		}, 300);
	});
});

/*-----------------------------------------------------------------------------------*/
/*	fekv� term�kek divje ugyanolyan magas
/*-----------------------------------------------------------------------------------*/
// Term�kek felsorol�sa a kateg�ri�ban
 $(window).on("load", function() {
    $('.kapcs_termekek').each(function(){  
        var highestBox = 0;
        $('.fels_termek_adatok', this).each(function(){

            if($(this).height() > highestBox) 
               highestBox = $(this).height(); 
        });  
        $('.fels_termek_adatok',this).height(highestBox);
	});  
});

 $(window).on("load", function() {
    $('.kapcs_termekek').each(function(){  
        var highestBox = 0;
        $('.product-title', this).each(function(){

            if($(this).height() > highestBox) 
               highestBox = $(this).height(); 
        });  
        $('.product-title',this).height(highestBox);
	});  
});

 $(window).on("load", function() {
    $('.kapcs_termekek').each(function(){  
        var highestBox = 0;
        $('.product-panel', this).each(function(){

            if($(this).height() > highestBox) 
               highestBox = $(this).height(); 
        });  
        $('.product-panel',this).height(highestBox);
	});  
}); 
 
// Hasonl� term�kek k�pei
 $(window).on("load", function() {
    $('.kapcs_termekek').each(function(){  
        var highestBox = 0;
        $('.fels_termek_kep', this).each(function(){

            if($(this).height() > highestBox) 
               highestBox = $(this).height(); 
        });  
        $('.fels_termek_kep',this).height(highestBox);
	});  
});
// Hasonl� term�kek nevei
 $(window).on("load", function() {
    $('.kapcs_termekek').each(function(){  
        var highestBox = 0;
        $('.fels_termek_nev', this).each(function(){

            if($(this).height() > highestBox) 
               highestBox = $(this).height(); 
        });  
        $('.fels_termek_nev',this).height(highestBox);
	});  
});
// Hasonl� term�kek div
 $(window).on("load", function() {
    $('.kapcs_termekek').each(function(){  
        var highestBox = 0;
        $('.fels_termek_div', this).each(function(){

            if($(this).height() > highestBox) 
               highestBox = $(this).height(); 
        });  
        $('.fels_termek_div',this).height(highestBox);
	});  
});
 $(window).on("load", function() {
	termekekIgazitasa()
});
// Term�k k�pek �s sz�vegek igaz�t�sa
	function termekekIgazitasa() {
		// felsorol�sban n�gyzetes termek k�pek 
		if ($('#termekek_ajax_div').length > 0)
		{
			$('#termekek_ajax_div').waitForImages(function() {
				
				$('#termek_felsorolas').each(function(){  
					var highestBox = 0;
					$('.fels_termek_kep', this).each(function(){

						if($(this).height() > highestBox) 
						   highestBox = $(this).height(); 
					});  
					$('.fels_termek_kep',this).height(highestBox);
				});  
				// felsorol�sban n�gyzetes termek adatok 
				$('#termek_felsorolas').each(function(){  
					var highestBox = 0;
					$('.fels_termek_adatok', this).each(function(){

						if($(this).height() > highestBox) 
						   highestBox = $(this).height(); 
					});  
					$('.fels_termek_adatok',this).height(highestBox);
				});


				$('#termek_felsorolas').each(function(){  
					var highestBox = 0;
					$('.product-title', this).each(function(){

						if($(this).height() > highestBox) 
						   highestBox = $(this).height(); 
					});  
					$('.product-title',this).height(highestBox);
				}); 

				$('#termek_felsorolas').each(function(){  
					var highestBox = 0;
					$('.product-panel', this).each(function(){

						if($(this).height() > highestBox) 
						   highestBox = $(this).height(); 
					});  
					$('.product-panel',this).height(highestBox);
				});				

			});	
		}	 
	}
