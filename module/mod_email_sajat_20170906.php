<?php

$mess = '<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta content="telephone=no" name="format-detection" />
	<title>Üzenet a weboldalról</title>
	

	<style type="text/css" media="screen">
		body {
			padding:0 !important;
			margin:0 !important;
			display:block !important;
			background:#fff; -webkit-text-size-adjust:none;
			font-family: sans-serif;
		}
		a {
			color:#00b8e4;
			text-decoration:underline
		}
		h3 a {
			color:#1f1f1f;
			text-decoration:none
		}
		h3 {
			color:#901200;
		}
		.text2 a {
			color:#ea4261;
			text-decoration:none
		}
		p {
			padding:0 !important;
			margin:0 !important
			color:#a38b68;
		} 
	</style>
</head>
<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; background:#fff; -webkit-text-size-adjust:none">

<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff">
	<tr>
		<td align="center" valign="top">
			<table width="800" border="0" cellspacing="0" cellpadding="0">
				<!-- Header -->
				<tr>
					<td align="center" bgcolor="#fff" style="border-bottom:5px solid #F3F3F3;">
						<table width="620" border="0" cellspacing="0" cellpadding="0">
							<tr height="80">
								<td class="img" style="font-size:0pt; line-height:0pt; text-align:left;";><a href="'.$domain.'" target="_blank"><img src="'.$domain.'/images/logo.png" alt="" border="0" height="50" /></a></td>
							</tr>
						</table>
					</td>
				</tr>
				<!-- END Header -->
				<!-- Tárgy -->
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="20" bgcolor="#F9F9F9">
							<tr valign="top">
								<td align="left" style="font-size:12px; line-height:20px; text-align:justify;";>
									<h3>Kedves Adminisztrátor!</h3>'.$uzenet.'<h3 style="font-weight: normal">Üdvözlettel:<br>'.$webnev.'</h3>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<!-- END Tárgy -->
				<!-- Footer -->
				<tr>
					<td align="center" bgcolor="#3B3B3B" style="border-top:1px solid #F3F3F3;">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" >
							<tr>
								<td height="40">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<!-- END Footer -->
			</table>
		</td>
	</tr>
</table>

</body>
</html>';
?>