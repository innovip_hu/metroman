    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="<?php print $domain; ?>/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900">
    <link rel="stylesheet" href="<?php print $domain; ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php print $domain; ?>/css/fonts.css?v=1">
    <link rel="stylesheet" href="<?php print $domain; ?>/css/style.css?v=5">
    <link href="<?php print $domain; ?>/webshop/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php print $domain; ?>/css/update.css?v=36">
    <style>.ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3);clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}</style>

    <script type="text/javascript" src="<?php print $domain; ?>/config.js"></script>

    <link rel="stylesheet" href="<?=$domain?>/css/kategoria-oldal.css?v=3">
    <link rel="stylesheet" href="<?=$domain?>/css/Header-modositas.css?v=2">

<?php
  	// PDO
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	}
	catch (PDOException $e)
	{
	    $protocol = 'HTTP/1.0';

	    if ( $_SERVER['SERVER_PROTOCOL'] === 'HTTP/1.1' ) {
	        $protocol = 'HTTP/1.1';
	    }

	    header( $protocol . ' 503 Service Unavailable', true, 503 );
	    header( 'Retry-After: 3600' );

		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	if (isset($_COOKIE['kosar_id']) && !isset($_SESSION['kosar_id']))
	{
		$query = "SELECT kosar_id FROM ".$webjel."kosar WHERE uid = ? LIMIT 1";
		$res = $pdo->prepare($query);
		$res->execute(array($_COOKIE['kosar_id']));
		$row_kosar_suti = $res -> fetch();		

		if (isset($row_kosar_suti['kosar_id']))
		{
			$_SESSION['uid'] = $_COOKIE['kosar_id'];
			$_SESSION['kosar_id'] = $row_kosar_suti['kosar_id'];
		}
		else
		{
			setcookie( "kosar_id", $_COOKIE['kosar_id'], 1, "/" ); //Ha nem érvényes a süti, akkor törölni
		}
	}

	$google_termek_id = 0;

	if ($oldal == 'kosar') {

		if (!empty($_SESSION['kosar_id'])) {
			$query = "SELECT term_id FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];
			foreach ($pdo->query($query) as $key => $row_yusp) {
				if ($key > 0) {
					print "\n";
				}
				print '<meta name="yuspItemInCart" content="'.$row_yusp['term_id'].'">';
			}			
		} else {
			print '<meta name="yuspItemInCart" content="" >';
		}
		
	}				
	elseif($oldal=='termekek')
	{
		$description = '';
		$keywords = '';
		$title = 'Termékek';
		// Dinamikus meta adatok
		if (isset($_GET['term_urlnev']) && $_GET['term_urlnev'] == '' && (isset($_GET['kat_urlnev']) || $_GET['kat_urlnev'] != '')) // Ha nincs termék kiválasztva
		{
			$res = $pdo->prepare("SELECT * FROM ".$webjel."term_csoportok where nev_url='".$_GET['kat_urlnev']."'");
			$res->execute();
			$row  = $res -> fetch();
			
			if (isset($row['robotnoindex']) && $row['robotnoindex'] == 1) {
				echo '<meta name="robots" content="noindex" />';
			}

			// Szűrő törlése
			if ((isset($_SESSION['csop_id']) && $_SESSION['csop_id'] != $row['id']) || isset($_SESSION['szuro_fooldal']))
			{
				unset($_SESSION['szuro_minimum_ar']);
				unset($_SESSION['szuro_maximum_ar']);
				unset($_SESSION['szuro_nem_ffi']);
				unset($_SESSION['szuro_nem_no']);
				unset($_SESSION['szuro_eletkor_fiatal']);
				unset($_SESSION['szuro_eletkor_felnott']);
				unset($_SESSION['szuro_eletkor_idos']);
				unset($_SESSION['termek_parameter_ertek']);
				unset($_SESSION['oldszam']);
				unset($_SESSION['keres_nev']);
			}
			//unset($_SESSION['szuro_fooldal']); // Ha belépett egy kategóriába, akkor a főoldali szűrőt töröljük
			$_SESSION['csop_id'] = $row['id'];
			
			// $description = mb_substr($row['leiras'],0,156,'UTF-8');
			// $keywords = $row['nev'];
			// $title = $row['nev'];
			$description = $row['seo_description'] ? $row['seo_description'] : mb_substr(strip_tags($row['leiras']),0,156,'UTF-8');
			$keywords = $row['nev'];
			$title = $row['seo_title'] ? $row['seo_title'] : $row['nev'];
		}
		else if (isset($_GET['term_urlnev']) && $_GET['term_urlnev'] != '')// Termék is van
		{
			echo '<link rel="stylesheet" href="'.$domain.'/termekek/termek.css?v=1">';
			$res = $pdo->prepare("SELECT * FROM ".$webjel."termekek where nev_url='".$_GET['term_urlnev']."'");
			$res->execute();
			$row  = $res -> fetch();
			if(isset($row['id']))
			{
				$google_termek_id = $row['id'];
				$google_termek_sku = $row['cikkszam'];
				$google_termek_nev = $row['nev'];
				// $description = mb_substr($row['rovid_leiras'],0,156,'UTF-8');
				$description = $row['seo_description'] ? $row['seo_description'] : mb_substr(strip_tags($row['rovid_leiras']),0,156,'UTF-8');
				$keywords = $row['nev'];
				// $title = $row['nev'];
				$title = $row['seo_title'] ? $row['seo_title'] : $row['nev'];
				
				// Facebook megosztáshoz kép
				$row_kep = $pdo->query(""
						. "SELECT kep "
						. "FROM ".$webjel."termek_kepek "
						. "WHERE termek_id=".$row['id']." "
						. "ORDER BY alap DESC")
				->fetchColumn();
				if (!$row_kep)
				{
					$kep_link = ''.$domain.'/webshop/images/noimage.png';
				}
				else
				{
					$kep_link = ''.$domain.'/images/termekek/'.$row_kep;
				}
				print '<meta property="og:title" content="'.$title.'" />';
				print '<meta property="og:type" content="website"/>';
				print '<meta property="og:url" content="'.$domain.'/termekek/'.$_GET['kat_urlnev'].'/'.$_GET['term_urlnev'].'" />';
				print '<meta property="og:image" content="'.$kep_link.'" />';
				print '<meta property="og:description" content="'.$description.'" />';
				print '<meta name="yuspItemId" content="'.$row['id'].'">';
			}
		}
		// Dublin core
		?>
		<meta name = "DC.Title" content = "<?php print $title; ?>">
		<meta name = "DC.Subject" content = "<?php echo $description; ?>">
		<meta name = "DC.Publisher" content = "<?php print $webnev; ?>">
		<meta name = "DC.Date" content = "<?php print date('Y-m-d'); ?>T<?php print date('h:i:s'); ?>">
		<meta name = "DC.Type" content = "website">
		<meta name = "DC.Format" content = "text/html">
		<?php if (isset($_GET['term_urlnev'])): ?>
			<meta name = "DC.Identifier" content = "<?php echo $domain.'/termekek/'.$_GET['kat_urlnev'].'/'.$_GET['term_urlnev']; ?>">
		<?php elseif (isset($_GET['kat_urlnev'])): ?>
			<meta name = "DC.Identifier" content = "<?php echo $domain.'/termekek/'.$_GET['kat_urlnev'].'/'; ?>">	
		<?php else: ?>
			<meta name = "DC.Identifier" content = "<?php echo $domain.'/termekek/'; ?>">	
		<?php endif ?>
		
		<?php
		
		print '<meta name="description" content="'.$description.'" />
		<title>'.$title.'</title>';
	}
	else if($oldal=='hirek')
	{
		// Facebook megosztáshoz kép
		if (isset($_GET['nev_url']) && $_GET['nev_url'] != '')
		{
			$res = $pdo->prepare("SELECT * FROM ".$webjel."hirek WHERE nev_url='".$_GET['nev_url']."'");
			$res->execute();
			$row  = $res -> fetch();

			if (!$row) {
				header('Location: '.$domain.'/404.php');
				exit();
			}
			// Kép
			$query_kep = "SELECT * FROM ".$webjel."hir_kepek WHERE hir_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
			$res = $pdo->prepare($query_kep);
			$res->execute();
			$row_kep = $res -> fetch();
			$alap_kep = $row_kep['kep'];
			if ($alap_kep == '') 
			{
				// $kep_link = ''.$domain.'/webshop/images/noimage.png';
				$kep_link = '';
			}
			else
			{
				$kep_link = ''.$domain.'/images/termekek/'.$row_kep['kep'];
			}
			print '<meta property="og:locale" content="hu_HU" />';
			print '<meta property="og:title" content="'.$row['cim'].'" />';
			print '<meta property="og:type" content="website"/>';
			print '<meta property="og:url" content="'.$domain.'/hirek/'.$_GET['nev_url'].'" />';
			print '<meta property="og:image" content="'.$kep_link.'" />';
			print '<meta property="og:description" content="'.$row['elozetes'].'" />';
			print '<meta property="og:site_name" content="'.$webnev.'" />';
			// Dublin core
			?>
			<meta name = "DC.Title" content = "<?php print $row['cim']; ?>">
			<meta name = "DC.Subject" content = "<?php echo $row['elozetes']; ?>">
			<meta name = "DC.Publisher" content = "<?php print $webnev; ?>">
			<meta name = "DC.Date" content = "<?php print date('Y-m-d'); ?>T<?php print date('h:i:s'); ?>">
			<meta name = "DC.Type" content = "website">
			<meta name = "DC.Format" content = "text/html">
			<meta name = "DC.Identifier" content = "<?php echo $domain.'/hirek/'.$_GET['nev_url']; ?>">
			<?php
			
			print '<meta name="description" content="'.$row['elozetes'].'" />
			<title>'.$row['cim'].'</title>';
		} else {
			print '<meta name="description" content="'.$description.'" />
			<title>'.$title.'</title>';			
		}
	}	
	else
	{
		?>
		<meta property='og:locale' content='hu_HU'>
		<meta property='og:type' content='<?php echo $og_type; ?>'/>
		<meta property='og:title' content='<?php print $title; ?>'>
		<meta property='og:description' content='<?php print $description; ?>'>
		<meta property='og:url' content='<?php echo "{$domain}/"; ?>'>
		<meta property='og:site_name' content='<?php print $title; ?>'>
		<?php
		// Dublin core
		?>
		<meta name = "DC.Title" content = "<?php print $title; ?>">
		<meta name = "DC.Subject" content = "<?php print $description; ?>">
		<meta name = "DC.Publisher" content = "<?php print $webnev; ?>">
		<meta name = "DC.Date" content = "<?php print date('Y-m-d'); ?>T<?php print date('h:i:s'); ?>">
		<meta name = "DC.Type" content = "website">
		<meta name = "DC.Format" content = "text/html">
		<meta name = "DC.Identifier" content = "<?php echo "{$domain}/"; ?>">
		<?php
	}


	include $gyoker.'/webshop/webshop.php';
?>


<script src='https://www.google.com/recaptcha/api.js'></script>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-8556426-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
 
  var google_consent=1;
	gtag('consent', 'default', {
		'ad_storage': 'denied',
		'ad_user_data': 'denied',
		'ad_personalization': 'denied',
		'analytics_storage': 'denied',
		'functionality_storage': 'denied',
		'personalization_storage': 'denied',
		'security_storage': 'granted'
	});

	<?php if (isset($_COOKIE['meres_ok'])) { ?>

	gtag('consent', 'update', {
		'ad_storage': 'granted',
		'ad_user_data': 'granted',
		'ad_personalization': 'granted',
		'analytics_storage': 'granted',
		'functionality_storage': 'granted',
		'personalization_storage': 'granted',
		'security_storage': 'granted'
	});	

	<?php } ?>	

  gtag('config', 'UA-8556426-1');

  <?php if ($google_termek_id > 0) { ?>
	gtag('event', 'view_item', {  
	   "items": [  
	     {  
	       "id": "<?=$google_termek_id?>",  
	       "publicSku": "<?=$google_termek_sku?>",  
	       "name": "<?=$google_termek_nev?>",  
		   "google_business_vertical" : "custom"
	    }  
	  ]  
	}); 
  <?php } ?>   
</script>

<!-- Hotjar Tracking Code for www.metroman.hu -->

<script>

    (function(h,o,t,j,a,r){

        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};

        h._hjSettings={hjid:1990183,hjsv:6};

        a=o.getElementsByTagName('head')[0];

        r=o.createElement('script');r.async=1;

        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;

        a.appendChild(r);

    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');

</script>

<script>
(function(g,r,a,v,i,t,y){g[a]=g[a]||[];g[a].push({type:'config',partnerId:t,targetServer:i,dynamic:true});y=r.createElement(v), g=r.getElementsByTagName(v)[0];y.async=1;y.src='//'+i+'/js/'+t+'/gr_reco7.min.js';g.parentNode.insertBefore(y,g);})(window, document, '_gravity','script', 'metroman.engine.yusp.com', 'metroman');</script>

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '819944485728452');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=819944485728452&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->


<script>
  (function(w,d,t,r,u)
  {
    var f,n,i;
    w[u]=w[u]||[],f=function()
    {
      var o={ti:"97037215", enableAutoSpaTracking: true};
      o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")
    },
    n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function()
    {
      var s=this.readyState;
      s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)
    },
    i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)
  })
  (window,document,"script","//bat.bing.com/bat.js","uetq");
</script>