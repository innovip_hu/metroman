      <footer class="section footer-modern bg-gray-13">
        <div class="footer-modern-body section-lg">
          <div class="container-fluid container-style-2 wow fadeInUp">
            <div class="row row-30 row-md-50 justify-content-md-between">
              <div class="col-lg-3 col-md-6">
                <div class="footer-modern-brand">
                  <!--Brand--><a class="brand" href="<?=$domain?>"><img class="brand-logo-dark" src="<?=$domain?>/images/logo.png" alt="logo"/><img class="brand-logo-light" src="<?=$domain?>/images/logo_w.png" alt="logo"/></a>
                </div>
                <a data-toggle="modal" data-target="#hirlevel_feliratkozas" href="javascript:void(0);">
                  <img src="<?=$domain?>/images/hirlevel-feliratkozas-metroman-3.png" alt="Hírlevél feliratkozás" class="img-fluid">
                </a>       
                <?php /*
                <p class="footer-caption">www.metroman.hu Autós és Műszaki webáruház</p>
                <div class="footer-modern-list-social">
                  <ul class="list-social list-social-3 list-inline list-inline-xl">
                    <li><a class="icon mdi mdi-facebook" href="https://www.facebook.com/MetromanHungariaKft/"></a></li>
                  </ul>
                </div>


                <p class="footer-caption">Értesülj újdonságainkról és ne maradj le akcióinkról:</p>
                <a data-toggle="modal" data-target="#hirlevel_feliratkozas" href="javascript:void(0);" class="mt-2 button button-primary button-xs"><span class="fa fa-pencil pr-1"></span>Hírlevél feliratkozás</a>
                */ ?> 
              </div>
              <div class="col-lg-3 col-md-6">
                <div class="heading-5 footer-modern-title">Információk</div>
                <ul class="list-terms">
                  <li><span class="fa fa-chevron-right text-primary pr-1"></span><a href="<?=$domain?>/kapcsolat">Cégadatok és kapcsolatfelvétel</a></li>
                  <li><span class="fa fa-chevron-right text-primary pr-1"></span><a href="<?=$domain?>/szallitasi-feltetelek/">Szállítási feltételek</a></li>
                  <li><span class="fa fa-chevron-right text-primary pr-1"></span><a href="<?=$domain?>/aszf">Általános szerződési feltételek</a></li>
                  <li><span class="fa fa-chevron-right text-primary pr-1"></span><a href="<?=$domain?>/beszallitoknak">Beszállítóknak</a></li>
                  <li><span class="fa fa-chevron-right text-primary pr-1"></span><a href="https://simplepay.hu/vasarlo-aff" target="_blank">Bankkártyás fizetési tájékoztató</a></li>
                  <?php /*
                  <li><span class="fa fa-chevron-right text-primary pr-1"></span><a href="<?=$domain?>/fogyaszto-barat">Fogyasztó Barát tájékoztató</a></li>
                  */  ?>
                  <li><span class="fa fa-chevron-right text-primary pr-1"></span><a href="<?=$domain?>/adatkezelesi-tajekoztato">Adatvédelmi tájékoztató</a></li>
                  <li><span class="fa fa-chevron-right text-primary pr-1"></span><a href="<?=$domain?>/hasznalati-utmutato">Használati útmutatók keresése</a></li>
                </ul>
              </div>               
              <div class="col-lg-3 col-md-6">
                <div class="heading-5 footer-modern-title">Elérhetőségek</div>
                <ul class="list-contacts">
                  <li><span class="icon mdi mdi-cellphone-android"></span><a class="big" href="tel:+36 30 348 7254">+36 30 348 7254</a></li>
                  <li><span class="icon mdi mdi-phone"></span><a class="big" href="tel:+36 93 900 837">+36 93 900 837</a></li>
                  <li><span class="icon mdi mdi-map-marker"></span><a class="big" href="https://goo.gl/maps/jG9JovfeywrVy3mA9" target="_blank">8800 Nagykanizsa, Hevesi Sándor u. 8</a></li>
                  <li><span class="icon mdi mdi-email-outline"></span><a class="big" href="mailto:info@metroman.hu">info@metroman.hu</a></li>
                </ul>
              </div>              
              <div class="col-lg-3 col-md-6">
                <div class="heading-5 footer-modern-title">Nyitvatartás</div>
                <ul class="footer-modern-schedule">
                  <li><span>Hétfőtől - Péntekig</span><span>de. 9-12h du. 13-17h</span></li>
                  <li><span>Szombat</span><span>de. 9-12h</span></li>
                  <li><span>Vasárnap</span><span>zárva</span></li>
                </ul>
              </div>             
            </div>
          </div>
        </div>
        <div class="footer-modern-panel bg-gray-7">
          <div class="container wow fadeInUp">
            <div class="row">
              <div class="col-xl-12">
                    <p class="rights">Metroman Hungária Kft.<span>&nbsp;&copy;&nbsp;</span><span class="copyright-year"></span><span>.&nbsp;</span><span>| Készítette:</span><span>.&nbsp;</span><a href="https://www.innovip.hu/" target="_blank" rel="nofollow">Innovip.hu Kft.</a></p>
              </div>
            </div>
          </div>
        </div>
      </footer>

<?php
// Cookie figyelmeztetés
	if(!isset($_COOKIE['oldal_ell_uj']))
	{
		// setcookie('oldal_ell', 1, time() + (86400 * 150), "/");
		?>
		<style>
			#cookie_div {
				background-color: rgba(51,51,51,0.8);
				border-radius: 0;
				box-shadow: 0 0 5px rgba(0,0,0,0.5);
				padding: 20px 0;
				position: fixed;
				left: 0;
				right: 0;
				bottom: 0;
				z-index: 9999;
				color: white;
				text-align: center;
			}
			.cookie_btn {
				background-color: #ff6543;
				padding: 3px 9px;
				cursor: pointer;
			}

      .cookie_btn_2 {
        background-color: #000;
        color: #fff;
        padding: 3px 9px;
        cursor: pointer;
      }       
		</style>
		<script>
			function cookieRendben() {
				// document.cookie="oldal_ell=1";
				document.getElementById('cookie_div').style.display = 'none';
				var expires;
				var days = 100;
				var date = new Date();
				date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
				expires = "; expires=" + date.toGMTString();
        document.cookie = "oldal_ell_uj=1" + expires + "; path=/";
				document.cookie = "meres_ok=1" + expires + "; path=/";
			}

      function cookieNem() {
                // document.cookie="oldal_ell=1";
                document.getElementById('cookie_div').style.display = 'none';
                var expires;
                var days = 100;
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
                document.cookie = "oldal_ell_uj=1" + expires + "; path=/";
            }      
		</script>
		<div id="cookie_div" style="">A weboldalon sütiket (cookie-kat) használunk, hogy a biztonságos böngészés mellett a legjobb felhasználói élményt nyújthassuk látogatóinknak. <a href="<?=$domain?>/adatkezelesi-tajekoztato" style="color: white; text-decoration:underline;">További információk.</a>  <span class="cookie_btn" onClick="cookieRendben()">Elfogadom</span> <span class="cookie_btn_2" onClick="cookieNem()">Elutasítom</span></div>
		<?php
	}