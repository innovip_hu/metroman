    <div class="snackbars" id="form-output-global"></div>
    <script src="<?=$domain?>/js/core.min.js"></script>
    <script src="<?=$domain?>/js/script.js?v=2"></script>


    <script type="text/javascript" src="<?php print $domain; ?>/webshop/scripts/kereso_ac.js?v=3"></script>
    
    <script>
    	$(document).ready(function(){
  			$.get(domain+'/webshop/kosar_darabszam.php?script=ok',function(response,status){ // Required Callback Function
  		    		$('#kosar-darabszam').html(response);
  			});    		
    	});
    </script> 


    <script>
      function hirlevel_ellenorzes_mindenhol()
      {
        var note_box = '<div class="alert alert-danger alert-dismissable bg-gradient-1" style="margin-bottom: 10px;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>A név és email cím kitöltése kötelező!</div>';
        var note_box2 = '<div class="alert alert-danger alert-dismissable bg-gradient-1" style="margin-bottom: 10px;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Az adatkezelési tájékoztató nem lett elfogadva!</div>';      
        var hirl_oke = 0;
        if(document.getElementById("hirl_email").value == '' || document.getElementById("hirl_nev").value == '')
        {
          var hirl_oke = 1;
          document.getElementById('hirlevel_riaszt').innerHTML = note_box;
        }
        else
        {
          document.getElementById('hirlevel_riaszt').innerHTML = '';
        }

        if (document.getElementById('hirlevel_gdpr').checked !== true)
        {
          var hirl_oke = 1; 
          document.getElementById('hirlevel_riaszt2').innerHTML = note_box2;
        }
        else
        {
          document.getElementById('hirlevel_riaszt2').innerHTML = '';
        }

        if(grecaptcha.getResponse().length == 0)
        {
          var hirl_oke = 1;
        }      
    
        if(hirl_oke == 1)
        {
          return false;
        }
        else
        {
          gtag('event', 'click', {  
             'event_category': 'newsletter',  
             'event_label': 'popup_newsletter_btn'
          });           

          return true;
        }
      }
    </script> 

      <div class="modal fade" id="hirlevel_feliratkozas" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header bg-sajatv2">
              <h5 class="modal-title text-white" id="exampleModalLongTitle">Hírlevél feliratkozás</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>              
            </div>
            <div class="modal-body">
              <p class="text-left"><b>Iratkozzon fel hirlevelünkre!</b></p>
              <div id="hirlevel_riaszt"></div>
              <form method="post" action="<?=$domain?>/hirlevel/" onsubmit="return hirlevel_ellenorzes_mindenhol()">
                <div class="form-group mt-2">
                  <input type="text" name="nev" id="hirl_nev" placeholder="Az Ön neve" class="form-control">
                </div>
                <div class="form-group">
                  <input type="text" name="email" id="hirl_email" placeholder="Az Ön e-mail címe" class="form-control">
                </div>
                <div id="hirlevel_riaszt2"></div>
                <div class="form-wrap">
                  <label class="checkbox-inline">
                    <input name="hirlevel_gdpr" value="1" id="hirlevel_gdpr" type="checkbox" class="checkbox-custom">
                    <span class='checkbox-custom-dummy'></span>
                    Az <a href="<?=$domain?>/adatkezelesi-tajekoztato" style="text-decoration: underline;" target="_blank">adatkezelési tájékoztatót</a> elolvastam és elfogadom.
                  </label>
                </div>
                <div class="text-center">
                <div class="g-recaptcha mt-1" data-sitekey="6LfB6skZAAAAAAJmdpm2NzsRHAaPAMaMlMxnafZX" style="display: inline-block;"></div>  
                <input type="hidden" name="command" value="hirl_felir">       
                <div class="mt-2">
                  <button type="submit" class="btn btn-primary">Feliratkozom a hírlevélre</button>
                </div>            
                </div> 
              </form>
            </div>
          </div>
          
        </div>
      </div>     