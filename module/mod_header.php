      <header class="section page-header">

        <div class="header-top-panel">
          <div class="container wow fadeInUp">
            <div class="row justify-content-center">
              <div class="col-xl-12">
                <ul class="footer-modern-nav list-inline align-items-baseline justify-content-center justify-content-lg-between">
                  <li style="position: relative;">
                    <?php include $gyoker.'/webshop/kereso_form.php'; ?>                   
                  </li>
                  <li><a data-toggle="modal" data-target="#hirlevel_feliratkozas" href="javascript:void(0);"><span class="fa fa-pencil pr-1"></span>Hírlevél feliratkozás</a></li>
                  <li><a href="<?=$domain?>/szallitasi-feltetelek"><span class="fa fa-truck pr-1"></span>Szállítás</a></li>
                  <?php if (isset($_SESSION['login_id'])): ?>
                    <li><a href="<?=$domain?>/profil/?command=kivansaglista"><span class="fa fa-heart pr-1"></span>Kivánságlista</a></li>                  
                    <li><a href="<?=$domain?>/profil"><span class="fa fa-user pr-1"></span>Profil</a></li>                  
                  <?php else: ?>
                    <li><a href="<?=$domain?>/belepes"><span class="fa fa-sign-in pr-1"></span>Belépés</a></li>
                    <li><a href="<?=$domain?>/kapcsolat"><span class="fa fa-info-circle pr-1"></span>Információk</a></li>
                  <?php endif ?>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <!--RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-modern rd-navbar-modern-1" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="61px" data-xl-stick-up-offset="61px" data-xxl-stick-up-offset="61px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true" data-lg-auto-height="true" data-xl-auto-height="true" data-xxl-auto-height="true">
            <div class="rd-navbar-main-outer">
              <div class="rd-navbar-main">
                <!--RD Navbar Panel-->
                <div class="rd-navbar-panel">
                  <!--RD Navbar Toggle-->
                  <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                  <!--RD Navbar Brand-->
                  <div class="rd-navbar-brand">
                    <!--Brand--><a class="brand" href="<?=$domain?>"><img class="brand-logo-dark" src="<?=$domain?>/images/logo.png" alt="Logo" /><img class="brand-logo-light" src="<?=$domain?>/images/logo.png" alt="Logo"/></a>
                  </div>
                </div>
                <div class="rd-navbar-nav-wrap w-100 text-right">
                  <ul class="rd-navbar-nav">
                    <?php
                      $query_csop = "SELECT nev_url,nev,id FROM ".$webjel."term_csoportok WHERE csop_id = 0 AND lathato=1 ORDER BY sorrend ASC"; 
                      foreach ($pdo->query($query_csop) as $key => $value_fo)
                      { ?>
                        
                        <li class="rd-nav-item"><a class="rd-nav-link" href="<?=$domain?>/termekek/<?=$value_fo['nev_url']?>/"><?=$value_fo['nev']?></a><span class="pl-1 fa fa-caret-right text-primary d-none d-xl-inline"></span>
                          <ul class="rd-menu rd-navbar-dropdown">
                            <?php 
                                $query_alcsop = "SELECT nev_url,nev,id FROM ".$webjel."term_csoportok WHERE csop_id = ".$value_fo['id']." AND lathato = 1 ORDER BY sorrend ASC"; 
                                foreach ($pdo->query($query_alcsop) as $key => $value_al)
                                { $query_alalcsop = "SELECT nev_url,nev,id FROM ".$webjel."term_csoportok WHERE csop_id = ".$value_al['id']." AND lathato = 1 ORDER BY sorrend ASC";
                                      $res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id=?");
                                      $res->execute(array($value_al['id']));
                                      $rownum = $res->fetchColumn();                              
                                 ?>   
                                  <li class="rd-dropdown-item">
                                      <a class="rd-dropdown-link" href="<?=$domain?>/termekek/<?=$value_al['nev_url']?>/"><?=$value_al['nev']?>
                                        <?php if ($rownum > 0): ?>
                                            <span class="d-none d-lg-inline fa fa-chevron-right pull-right"></span>  
                                        <?php endif ?>
                                      </a>
                                        <?php $elso = 0; ?>
                                        <?php foreach ($pdo->query($query_alalcsop) as $key => $value_al_al): ?>
                                        <?php if ($elso == 0): ?>
                                          <ul class="rd-menu rd-navbar-dropdown">
                                          <?php $elso = 1; ?>
                                        <?php endif ?>
                                          <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="<?=$domain?>/termekek/<?=$value_al_al['nev_url']?>/"><?=$value_al_al['nev']?></a>
                                        <?php endforeach ?>    
                                        <?php if ($elso == 1): ?>
                                          </ul>
                                        <?php endif ?>                                    
                                  </li>                     
                                  <?php
                                }
                            ?> 


                          </ul>
                        </li> 

                        <?php
                      }
                    ?>

                      <li class="rd-nav-item d-lg-none"><a class="rd-nav-link" href="<?=$domain?>/akcios-termekek/">Akciós termékek</a></li>
                      <li class="rd-nav-item d-lg-none"><a class="rd-nav-link" href="<?=$domain?>/legujabb-termekek/">Legújabb termékek</a></li>
                      <?php /*
                      <li class="rd-nav-item d-lg-none"><a class="rd-nav-link" href="<?=$domain?>/kiemelt-termekek/">Kiemelt termékek</a></li>
                      */ ?>
                      <li class="rd-nav-item d-lg-none"><a class="rd-nav-link" href="<?=$domain?>/legnepszerubb-termekek/">Népszerű termékek</a></li>

<?php 
/*
                          <li class="rd-nav-item"><a class="rd-nav-link" href="<?=$domain?>/termekek/<?=$value_fo['nev_url']?>/"><?=$value_fo['nev']?></a>
                            <div class="rd-menu rd-navbar-megamenu rd-navbar-sajatmega small text-body">
                              <div class="card-columns">

                                        <?php 
                                            $query_alcsop = "SELECT nev_url,nev,id FROM ".$webjel."term_csoportok WHERE csop_id = ".$value_fo['id']." ORDER BY sorrend ASC"; 
                                            foreach ($pdo->query($query_alcsop) as $key => $value_al)
                                            { $query_alalcsop = "SELECT nev_url,nev,id FROM ".$webjel."term_csoportok WHERE csop_id = ".$value_al['id']." ORDER BY sorrend ASC";
                                             ?>
                                              <div class="card border-0">
                                                  <ul>                                              
                                                    <li><a href="<?=$domain?>/termekek/<?=$value_al['nev_url']?>/"><b><?=$value_al['nev']?></b></a></li>
                                                    <?php foreach ($pdo->query($query_alalcsop) as $key => $value_al_al): ?>
                                                    <li><a href="<?=$domain?>/termekek/<?=$value_al_al['nev_url']?>/"><?=$value_al_al['nev']?></a></li>
                                                    <?php endforeach ?>
                                                  </ul>                              
                                              </div>                                                    
                                              <?php
                                            }
                                        ?>

                              </div>
                            </div>                      
                          </li>

*/
 ?>

                  </ul>
                </div>
                <div class="rd-navbar-elements">
                  <a class="rd-navbar-basket fa-shopping-cart rd-navbar-fixed-element-2" href="<?=$domain?>/kosar"><span id="kosar-darabszam"></span></a>
                  <button class="sidebar-toggle sidebar-toggle-1 rd-navbar-fixed-element-1" data-multitoggle=".sidebar-wrap" data-multitoggle-blur=".rd-navbar-wrap" data-multitoggle-isolate="" title="Információk"><span></span></button>
                </div>
              </div>
            </div>
            <button class="sidebar-toggle sidebar-toggle-2" data-multitoggle=".sidebar-wrap" data-multitoggle-blur=".rd-navbar-wrap" data-multitoggle-isolate=""><span></span></button>
            <div class="sidebar-wrap">
              <div class="sidebar">
                <h4 class="sidebar-title heading-5">Kapcsolat</h4><img src="<?=$domain?>/images/sidebox.jpg" alt="Metroman üzlet" width="333" height="262"/>
                <ul class="contacts-creative">
                  <li>
                    <div class="unit unit-spacing-md">
                      <div class="unit-left"><span class="icon fa-phone"></span></div>
                      <div class="unit-body"><a href="tel: +36 30 348 7254">+36 30 348 7254</a><br><a href="tel:+36 93 900 837">+36 93 900 837</a>
                      </div>
                    </div>                    
                  </li>
                  <li>
                    <div class="unit unit-spacing-md">
                      <div class="unit-left"><span class="icon fa-envelope-o"></span></div>
                      <div class="unit-body">E-mail:<br><a href="mailto:info@metroman.hu">info@metroman.hu</a>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="unit unit-spacing-md">
                      <div class="unit-left"><span class="icon fa-clock-o"></span></div>
                      <div class="unit-body">Nyitvatartás<br>Hétfőtől péntekig:<br>de. 9-12h; du. 13-17h<br>Szombat: de. 9-12h</div>
                    </div>
                  </li>
                  <li>
                    <div class="unit unit-spacing-md">
                      <div class="unit-left"><span class="icon fa-map-o"></span></div>
                      <div class="unit-body"><a href="https://goo.gl/maps/jG9JovfeywrVy3mA9" target="_blank">8800 Nagykanizsa, Hevesi Sándor u. 8</a></div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </header>