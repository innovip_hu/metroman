<?php
	include 'config.php';

	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsol�dni az adatb�zishoz!");
	}

	$datum = date("Y-m-d");

	$xml = new DOMDocument('1.0', 'UTF-8');
	$root = $xml->createElement("urlset");
	$xml->appendChild($root);
	$root->setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
	$root->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
	$root->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
	
	// PDF generálása
	$query = "SELECT tk.kep
	FROM ".$webjel."termek_kepek tk
	INNER JOIN ".$webjel."termekek t
	ON tk.termek_id=t.id 
	WHERE tk.spec=1 AND tk.extension = 'pdf'";
	foreach ($pdo->query($query) as $row)
	{
		$loc = $xml->createElement("loc");
		$locText = $xml->createTextNode($domain."/dokumentumok/termekek/".$row['kep']);
		$loc->appendChild($locText);

		$changefreq = $xml->createElement("changefreq");
		$changefreqText = $xml->createTextNode("daily");
		$changefreq->appendChild($changefreqText);

		$priority = $xml->createElement("priority");
		$priorityText = $xml->createTextNode("0.75");
		$priority->appendChild($priorityText);
		
		$url = $xml->createElement("url");
		$url->appendChild($loc);
		$url->appendChild($changefreq);
		$url->appendChild($priority);
		
		$root->appendChild($url);
	}

	$xml->save("sitemap_pdf.xml");