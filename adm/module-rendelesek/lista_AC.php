<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	// Lezárás
	if(isset($_GET['lezar_id']))
	{
		$updatecommand = "UPDATE ".$webjel."rendeles SET teljesitve=1 WHERE id=".$_GET['lezar_id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
	}
	else if(isset($_GET['felnyit_id']))
	{
		$updatecommand = "UPDATE ".$webjel."rendeles SET teljesitve=0 WHERE id=".$_GET['felnyit_id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
	}
	else if(isset($_GET['torol_id']))
	{
		$deletecommand = "DELETE FROM ".$webjel."rendeles WHERE id =".$_GET['torol_id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();
	}
	
	
// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 50; //ALAPÁLLAPOT
	}
// Rekordok száma
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."rendeles ");
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'id'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'DESC'; // Alap rendezési feltétel
	}
?>
<div class="content-wrapper bg_admin">

	<section class="content-header">
	  <h1>Rendelések</h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Rendelések</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body no-padding with-border">
						<div class="mailbox-controls">
							<div class="btn-group">
								<div class="has-feedback">
									<input id="tabla_kereso" type="text" class="form-control input-sm" placeholder="Kereső" onkeyup="tablaAutoComplete()" autocomplete="off" >
									<span class="glyphicon glyphicon-search form-control-feedback"></span>
								</div>
							</div>
							<span class="menu_gomb"><a onClick="autoCompleteTorles('module-rendelesek/lista.php')"><i class="fa fa-close"></i></span></a>
							<div class="pull-right">
								<?php
									if($oldalszam > $rownum) $oldalszam_mod = $rownum;
									else $oldalszam_mod = $oldalszam;
									$vege = $kezd+$oldalszam_mod;
									if($vege > $rownum) $vege = $rownum;
									print ($kezd+1).'-'.$vege.'/'.$rownum;
								?>
								<div class="btn-group">
								<?php
									if ($kezd != 0) // Lapozás vissza (balra)
									{
										?>
										<button onclick="lapozasVissza('module-rendelesek/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
										<?php
									}
									if (($utolso_oldal / $aktualis_oldal) != 1) // Lapozás előre (jobbra)
									{
										?>
										<button onclick="lapozasTovabb('module-rendelesek/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
										<?php
									}
								?>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table table-hover table-striped margbot0">
								<tbody>
									<tr>
										<th>Azonosító</th>
										<th>Megrendelő</th>
										<th>Dátum</th>
										<th>Fizetés</th>
										<th>Szállítás</th>
										<th></th><!--ikon-->
										<th style="text-align:right;">Összeg</th>
									</tr>
									<?php
									// Dátum miatti karakterek szűrése
									$datumkeres = "";
									if (strpos($_GET['tabla_kereso'], 'ő') == false && strpos($_GET['tabla_kereso'], 'Ő') == false && strpos($_GET['tabla_kereso'], 'ű') == false && strpos($_GET['tabla_kereso'], 'Ű') == false) {
										$datumkeres = " OR ".$webjel."rendeles.datum LIKE '%".$_GET['tabla_kereso']."%' ";
									}
									
									$query = "SELECT *, ".$webjel."rendeles.id as id
													, ".$webjel."rendeles.rendeles_id as rendeles_id
													, SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar > 0, ".$webjel."rendeles_tetelek.term_akcios_ar * ".$webjel."rendeles_tetelek.term_db, ".$webjel."rendeles_tetelek.term_ar * ".$webjel."rendeles_tetelek.term_db)) as rendelt_osszeg 
													, IF(".$webjel."rendeles.teljesitve = 1, 1, ".$webjel."rendeles.szallitva) as szall_sorrend
									FROM ".$webjel."rendeles 
									LEFT JOIN ".$webjel."rendeles_tetelek 
									ON ".$webjel."rendeles.id = ".$webjel."rendeles_tetelek.rendeles_id 
									WHERE ".$webjel."rendeles.rendeles_id LIKE '%".$_GET['tabla_kereso']."%' 
										OR ".$webjel."rendeles.vasarlo_nev LIKE '%".$_GET['tabla_kereso']."%' 
										OR ".$webjel."rendeles.fiz_mod LIKE '%".$_GET['tabla_kereso']."%' 
										OR ".$webjel."rendeles.szall_mod LIKE '%".$_GET['tabla_kereso']."%' 
										".$datumkeres."
									GROUP BY ".$webjel."rendeles.id
									ORDER BY ".$webjel."rendeles.teljesitve ASC, szall_sorrend ASC, ".$webjel."rendeles.id DESC LIMIT ".$kezd.",".$oldalszam;
									foreach ($pdo->query($query) as $row)
									{
										?>
										<tr class="kattintos_sor" onClick="Belepes('module-rendelesek/rendeles.php', 'module-rendelesek/lista.php', <?php print $row['id']; ?>)">
										<?php
										print '<td>'.$row['id'].'</td>
											<td>'.$row['vasarlo_nev'].'</td>
											<td>'.$row['datum'].'</td>
											<td>'.$row['fiz_mod'];
												if($row['fiz_mod'] == 'Bankkártyás fizetés') // Bankkártyás fizetés
												{
													if($row['fizetve_otp'] == 1) // Fizetve
													{
														print ' <span style="color: #00a65a;"><i class="fa fa-check"></i></span>';
													}	
													else
													{
														print ' <span style="color: #dd4b39;"><i class="fa fa-times"></i></span>';
													}
												}
											print '</td>
											<td>'.$row['szall_mod'].'</td>';
											// Ikon
											print '<td>';
											if($row['teljesitve'] == 1) // Teljesítve
											{
												print '<span style="color:#3C8DBC;"><i class="fa fa-check"></i></span>';
											}
											else if($row['szallitva'] == 1) // Szállítás alatt
											{
												print '<span style="color:#3C8DBC;"><i class="fa fa-truck"></i></span>';
											}
											print '</td>';
											print '<td style="text-align:right;">';
												if($row['rendelt_osszeg'] == '')
												{
													print '0';
												}
												else
												{
													print number_format($row['rendelt_osszeg'], 0, ',', ' ');
												}
											print ' Ft</td>
										</tr>';
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer no-padding">
						<div class="mailbox-controls">
							<div class="btn-group" style="height:30px">
								&nbsp;
							</div>
							<div class="pull-right">
								<?php
									print ($kezd+1).'-'.$vege.'/'.$rownum;
								?>
								<div class="btn-group">
								<?php
									if ($kezd != 0) // Lapozás vissza (balra)
									{
										?>
										<button onclick="lapozasVissza('module-rendelesek/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
										<?php
									}
									if (($utolso_oldal / $aktualis_oldal) != 1) // Lapozás előre (jobbra)
									{
										?>
										<button onclick="lapozasTovabb('module-rendelesek/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
										<?php
									}
								?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<input type="hidden" name="oldalszam" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<?php print $kezd;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<?php print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<?php print $sorrend;?>"/>
