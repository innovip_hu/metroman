<?php
	include '../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	

// Excel
	//Include PHPExcel
	require_once($gyoker.'/adm/PHPExcel.php');

	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();

	// Set document properties
	$objPHPExcel->getProperties()->setCreator("Gumiabroncs ’96 Kft.")
								 ->setLastModifiedBy("Gumiabroncs ’96 Kft.")
								 ->setTitle("Office 2007 XLSX Termekek")
								 ->setSubject("Office 2007 XLSX Termekek")
								 ->setDescription("GDPR export")
								 ->setKeywords("GDPR")
								 ->setCategory("export");

	// Fejléc

	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Az adatkezelő neve: '.$webnev);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'Az adatkezelő elérhetősége: '.$email);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', 'Adatvédelmi tisztviselője neve: nincsen');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A4', 'Az adatkezelés célja: ügyféllel való kapcsolattartás, tájékoztatás');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A5', 'Az adatkezelés jogalapja: szerződés teljesítése, jogos érdek (reklamáció)');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A6', 'Az érintettek kategóriái: az adatkezelővel kapcsolatban álló természetes személyek');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A7', 'A személyes adatok kategóriái: az ügyféllel kötött szerződésben megadott, szerződése teljesítéséhez szükséges személyes adatok');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A8', 'A címzettek kategóriái: ');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A9', 'Harmadik országba történő továbbítás: nincs');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A10', 'Adattörlésre előirányzott határidő: az érintett törlési kéréséig, regisztráció törlésével azonnal, továbbá ügyfélkapcsolat fennállása,  polgári jogi igények érvényesítésének határideje');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A11', 'Technikai és szervezési intézkedések: lásd Adatkezelési Szabályzat és Tájékoztató');

	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C13:E13');
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F13:K13');
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L13:P13');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C13', 'Személyes adatok');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F13', 'Számlázási cím');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L13', 'Eltérő szállítási cím');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A14', 'Sorszám');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B14', 'Vásárlás/regisztráció ideje');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C14', 'Vásárló neve');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D14', 'E-mail cím');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E14', 'Telefonszám');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F14', 'Számlázási név');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G14', 'Város');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H14', 'Utca');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I14', 'Házszám');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J14', 'Irányítószám');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K14', 'Adószám');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L14', 'Szállítási név');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M14', 'Város');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N14', 'Utca');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O14', 'Házszám');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P14', 'Irányítószám');

	$oszlop = PHPExcel_Cell::stringFromColumnIndex(13);
	
	// Fejléc kék
	$objPHPExcel->getActiveSheet()->getStyle('A13:P13')->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('rgb' => '2C82C9')
							),
		  'borders' => array(
								'bottom'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							),
		  'font' => array(
								// 'bold'	=> true,
								'color'	=> array('rgb' => 'FFFFFF')
							)
		 )
	);
	$objPHPExcel->getActiveSheet()->getStyle('A14:P14')->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('rgb' => '2C82C9')
							),
		  'borders' => array(
								'bottom'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							),
		  'font' => array(
								// 'bold'	=> true,
								'color'	=> array('rgb' => 'FFFFFF')
							)
		 )
	);


	$j = 15;
	$kezd = $j;
	$query = "SELECT * FROM ".$webjel."users";
	foreach ($pdo->query($query) as $row)
	{


		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(0, $j, $row['id']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(1, $j, $row['reg_datum']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(2, $j, $row['vezeteknev']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(3, $j, $row['email']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(4, $j, $row['telefon']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, $j, $row['szla_nev']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(6, $j, $row['cim_varos']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(7, $j, $row['cim_utca']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(8, $j, $row['cim_hszam']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(9, $j, $row['cim_irszam']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(10, $j, $row['adoszam']);
		if ($row['cim_szall_varos'] != '')
		{
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(11, $j, $row['cim_szall_nev']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(12, $j, $row['cim_szall_varos']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(13, $j, $row['cim_szall_utca']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(14, $j, $row['cim_szall_hszam']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(15, $j, $row['cim_szall_irszam']);
		}
		$j++;
	}

	$query = "SELECT * FROM ".$webjel."users_noreg
		INNER JOIN ".$webjel."rendeles
		ON ".$webjel."users_noreg.id = ".$webjel."rendeles.user_id
		WHERE ".$webjel."rendeles.noreg = 1";
	foreach ($pdo->query($query) as $row)
	{


		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(0, $j, 'nr');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(1, $j, $row['datum']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(2, $j, $row['vezeteknev']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(3, $j, $row['email']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(4, $j, $row['telefon']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, $j, $row['szla_nev']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(6, $j, $row['cim_varos']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(7, $j, $row['cim_utca']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(8, $j, $row['cim_hszam']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(9, $j, $row['cim_irszam']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(10, $j, $row['adoszam']);
		if ($row['cim_szall_varos'] != '')
		{
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(11, $j, $row['cim_szall_nev']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(12, $j, $row['cim_szall_varos']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(13, $j, $row['cim_szall_utca']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(14, $j, $row['cim_szall_hszam']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(15, $j, $row['cim_szall_irszam']);
		}
		$j++;
	}

	 $styleArray = array(
	      'borders' => array(
	          'allborders' => array(
	              'style' => PHPExcel_Style_Border::BORDER_THIN
	          )
	      )
	  );


	$objPHPExcel->getActiveSheet()->getStyle('A'.$kezd.':P'.$j)->applyFromArray($styleArray);


		// $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, $j, 'Összesen');
		// $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(6, $j, number_format($szumma,0,',',' '));
	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('Keresési eredmény');

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);

	// Save Excel 2007 file
	// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	// $objWriter->save($gyoker.'/temszoft/exportok/kereses_'.$_SESSION['ugyfel_id'].'.xlsx');

	// Excel letöltés
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="gdpr_export.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');


?>  