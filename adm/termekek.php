<?php
	session_start();
	ob_start();
	
	include '../config.php';
	
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	$oldal = 'termekek';
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Termékek | Admin</title>
		<?php
			include 'module/head.php';
		?>
		<link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
	</head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
		<?php
			include 'module/header.php';
			include 'module/menu.php';
		?>
		<div id="munkaablak">
			<?php
				if(isset($_GET['command']) && $_GET['command']=='uj_termek')
				{
					include 'module-termekek/uj_termek.php';
				}
				else if(isset($_GET['command']) && $_GET['command']=='tomeges_akcio')
				{
					include 'module-termekek/tomeges_akcio_termek.php';
				}
				else if(isset($_GET['id']))
				{
					include 'module-termekek/termek.php';
				}
				else
				{
					include 'module-termekek/lista.php';
				}
			?>
		</div>
		<?php
			include 'module/footer.php';
		?>
    </div>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- Datepicker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <!-- CK Editor -->
    <script src="plugins/ckeditor/ckeditor.js"></script>
    <!-- Dropzone -->
	<script src="scripts/dropzone.js"></script>
    <!-- Saját -->
	<script src="scripts/termekek.js?v=04"></script>
    <!-- Page Script -->
    <script>
      $(function () {
		$('.checkbox input').iCheck({
		  checkboxClass: 'icheckbox_flat-blue',
		  radioClass: 'iradio_flat-blue'
		});
        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function () {
          var clicks = $(this).data('clicks');
          if (clicks) {
            //Uncheck all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
          } else {
            //Check all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
          }
          $(this).data("clicks", !clicks);
        });

        //Handle starring for glyphicon and font awesome
        $(".mailbox-star").click(function (e) {
          e.preventDefault();
          //detect type
          var $this = $(this).find("a > i");
          var glyph = $this.hasClass("glyphicon");
          var fa = $this.hasClass("fa");

          //Switch states
          if (glyph) {
            $this.toggleClass("glyphicon-star");
            $this.toggleClass("glyphicon-star-empty");
          }

          if (fa) {
            $this.toggleClass("fa-star");
            $this.toggleClass("fa-star-o");
          }
        });
		// Datepicker
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			startDate: false,
			isRTL: false,
			autoclose:true,
			todayBtn: true,
			todayHighlight: true,
		});
		// CKEDitor
		$(function () {
			var config = {
				"extraPlugins" : 'imagebrowser,youtube',
				"imageBrowser_listUrl": "../../../../../mediatar.json",
				htmlEncodeOutput: false,
				entities: false
			};
			CKEDITOR.replace( 'editor1', config );
			CKEDITOR.config.toolbar = [
			   ['Format','FontSize','Maximize','Source', 'Table'],
			   ['Find','Replace','-','SelectAll','-'],
			   ['Bold','Italic','Underline','Strike','-','Outdent','Indent','-','RemoveFormat'],
			   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
			   ['Link','Smiley','TextColor','BGColor','-','Undo','Redo'],
			   ['Image', 'Youtube']
			] ;
		});
		// Több kategóriához rendelés
		$('.muliselectes.csoportok').multipleSelect({
		});
		// Dropzone
		$("#kepfeltoltes").dropzone({
			dictDefaultMessage: "Húzd ide a feltöltendő képeket, vagy kattints a mezőbe",
			autoProcessQueue: true,
			// acceptedFiles: "image/jpeg",
			url: 'module-termekek/kep_feltoltes.php?id='+<?php if(isset($_GET['id'])) { echo $_GET['id']; } else { echo 0; } ?>,
			// maxFiles: 20, // Number of files at a time
			maxFilesize: 20, //in MB
			maxfilesexceeded: function(file)
			{
				alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
			},
			init: function () {
				this.on("complete", function (file) {
					setTimeout( function() {
						$('.dz-complete').remove();
					}, 2000); // feltöltés után az ikon törlése
					if (window.XMLHttpRequest)
					  {// code for IE7+, Firefox, Chrome, Opera, Safari
					  xmlhttp=new XMLHttpRequest();
					  }
					else
					  {// code for IE6, IE5
					  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
					  }
					xmlhttp.onreadystatechange=function()
					  {
					  if (xmlhttp.readyState==4 && xmlhttp.status==200)
						{
							document.getElementById("termek_kepek_div").innerHTML=xmlhttp.responseText;
						}
					  }
					xmlhttp.open("GET","module-termekek/termek_kepek.php?script=ok&id="+<?php if(isset($_GET['id'])) { echo $_GET['id']; } else { echo 0; } ?>,true);
					xmlhttp.send();
				});
			}
		});
     });
    </script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
	<?php
		include 'module/body_end.php';
	?>
  </body>
</html>
