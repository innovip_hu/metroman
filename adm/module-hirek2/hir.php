<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 50; //ALAPÁLLAPOT
	}
// Rekordok száma
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirek2");
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'id'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'ASC'; // Alap rendezési feltétel
	}
	
	// Új termék mentése
	if(isset($_POST['command']) && $_POST['command'] == 'uj_hir')
	{
		// URL név meghatározása
		include $gyoker.'/adm/module/mod_urlcim.php';
		if ($nev_url == '')
		{
			$nev_url = rand(1,99999);
		}
		// Egyezőség vizsgálata
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirek2 WHERE nev_url = '".$nev_url."'");
		$res->execute();
		$rownum2 = $res->fetchColumn();
		if ($rownum2 > 0) // Ha van már ilyen nevű
		{
			$query = "SELECT * FROM ".$webjel."hirek2 WHERE nev_url = '".$nev_url."'";
			$res = $pdo->prepare($query);
			$res->execute();
			$row = $res -> fetch();
			if($rownum2 > 1 || $row['id'] != $_GET['id']) // Ha nem saját maga
			{
				$nev_url = $nev_url.'-'.time().rand(1, 999);
			}
		}
		// Mentés
		$updatecommand = "UPDATE ".$webjel."hirek2 SET cim=?, elozetes=?, sorrend=?, nev_url=? WHERE id=?";
		$result = $pdo->prepare($updatecommand);
		$result->execute(array($_POST['cim'],$_POST['elozetes'],$_POST['sorrend'],$nev_url,$_GET['id']));
	}
	
	// Adatok mentése
	if(isset($_POST['command']) && $_POST['command'] == 'adatok_mentese')
	{
		// URL név meghatározása
		include $gyoker.'/adm/module/mod_urlcim.php';
		if ($nev_url == '')
		{
			$nev_url = rand(1,99999);
		}
		// Egyezőség vizsgálata
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirek2 WHERE nev_url = '".$nev_url."'");
		$res->execute();
		$rownum2 = $res->fetchColumn();
		if ($rownum2 > 0) // Ha van már ilyen nevű
		{
			$query = "SELECT * FROM ".$webjel."hirek2 WHERE nev_url = '".$nev_url."'";
			$res = $pdo->prepare($query);
			$res->execute();
			$row = $res -> fetch();
			if($rownum2 > 1 || $row['id'] != $_GET['id']) // Ha nem saját maga
			{
				$nev_url = $nev_url.'-'.$_GET['id'];
			}
		}
		// Mentés
		$updatecommand = "UPDATE ".$webjel."hirek2 SET cim=?, elozetes=?, tartalom=?, sorrend=?, nev_url=?, ikon=? WHERE id=?";
		$result = $pdo->prepare($updatecommand);
		$result->execute(array($_POST['cim'],$_POST['elozetes'],$_POST['tartalom'],$_POST['sorrend'],$nev_url,$_POST['ikon'],$_GET['id']));
	}
	
	// Adatok
	$query = "SELECT * FROM ".$webjel."hirek2 WHERE id=".$_GET['id'];
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
?>
<!--Modal-->
<div class="example-modal">
<div id="rakerdez_torles" class="modal modal-danger">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<h4 class="modal-title">Törlés</h4>
	  </div>
	  <div class="modal-body">
		<p>Biztos törölni szeretnéd a boxot?</p>
	  </div>
	  <div class="modal-footer">
		<button onClick="torol('<?php print $_GET['id']; ?>', '<?php print $_GET['fajl']; ?>')" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Igen</button>
		<button onClick="megsem('rakerdez_torles')" type="button" class="btn btn-outline">Mégsem</button>
	  </div>
	</div>
  </div>
</div>
</div>
		  
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1>Főoldali boxok<small></small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li><a onClick="vissza('<?php print $_GET['fajl']; ?>')"><i class="fa fa-newspaper-o"></i>Főoldali boxok</a></li>
		<li class="active"><?php print $row['cim']; ?></li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- Személyes adatok -->
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title">Box: <?php print $row['cim']; ?></h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Cím</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
										<input type="text" class="form-control" id="cim" placeholder="Cím" value="<?php print $row['cim']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label>Link</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-link"></i></span>
										<input type="text" class="form-control" id="elozetes" placeholder="Link" value="<?php print $row['elozetes']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label>Sorrend</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-sort-numeric-asc"></i></span>
										<input type="text" class="form-control" id="sorrend" placeholder="Sorrend" value="<?php print $row['sorrend']; ?>">
									</div>
								</div>	
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label>Tartalom</label>
									<textarea class="form-control" id="tartalom" rows="3" placeholder="Tartalom"><?php print $row['tartalom']; ?></textarea>
								</div>

								<?php
								/**
								* Available Font Awesome icons
								*
								* Get all icons from a font-awesome.css file and list in json mode
								*
								* @author Alessandro Gubitosi <gubi.ale@iod.io>
								* @license http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License, version 3
								*/


								/**
								 * Remove items from an array
								 * @param  array                $array                  The array to manage
								 * @param  void                 $element                An array or a string of the item to remove
								 * @return array                                        The cleaned array with resetted keys
								 */
								function array_delete($array, $element) {
								        return (is_array($element)) ? array_values(array_diff($array, $element)) : array_values(array_diff($array, array($element)));
								}

								$icons_file = "../../css/fonts.css";
								$parsed_file = file_get_contents($icons_file);
								preg_match_all("/mdi\-([a-zA-z0-9\-]+[^\:\.\,\s])/", $parsed_file, $matches);
								$exclude_icons = array("fa-lg", "fa-2x", "fa-3x", "fa-4x", "fa-5x", "fa-ul", "fa-li", "fa-fw", "fa-border", "fa-pulse", "fa-rotate-90", "fa-rotate-180", "fa-rotate-270", "fa-spin", "fa-flip-horizontal", "fa-flip-vertical", "fa-stack", "fa-stack-1x", "fa-stack-2x", "fa-inverse");
								$icons = (object) array("icons" => array_delete($matches[0], $exclude_icons));

								//var_dump($icons);
								?>	

								<div class="form-group">
									<label>Ikon <a href="../ikonok.php" target="_blank"><span class="fa fa-info-circle"></span></a></label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-info"></i></span>
										<select class="form-control" id="ikon">
											<option value="">- ikon választása -</option>
											<?php
												foreach ($icons->icons as $row2)
												{
													if($row['ikon'] == $row2)
													{
														print '<option value="'.$row2.'" selected>'.$row2.'</option>';
													}
													else
													{
														print '<option value="'.$row2.'">'.$row2.'</option>';
													}
												}
											?>
										 </select>
									</div>
								</div>															
							</div>							
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" onClick="mentes('<?php print $_GET['fajl']; ?>', <?php print $row['id']; ?>)" class="btn btn-primary">Mentés</button>
						<div class="box-tools pull-right">
							<button class="btn btn-danger" onclick="rakerdez('rakerdez_torles')" >Törlés</button>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>
</div>

<input type="hidden" name="oldalszam" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<?php print $kezd;?>"/>
<input type="hidden" name="kezd" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<?php print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<?php print $sorrend;?>"/>
