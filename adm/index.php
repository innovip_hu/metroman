<?php
	session_start();
	ob_start();
	
	$oldal = 'admi_fooldal';
	include '../config.php';
	
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	if (isset($_POST['jelszo'])) // Belépés
	{
		
		$username = addslashes($_POST['email']);
		$query = "SELECT * FROM ".$webjel."users WHERE email = '".$username."'";
		$res = $pdo->prepare($query);
		$res->execute();
		$row  = $res -> fetch();
		$jelszo_ok = password_verify($_POST['jelszo'], $row['jelszo']);
		if ($row['tipus'] == 'admin' && $jelszo_ok == true) //Ha minden adat stimmel
		{
			$_SESSION['login_id'] = $row['id'];
			$_SESSION['login_nev'] = $row['email'];
			$_SESSION['login_tipus'] = 'admin';
		}
		else
		{
			$uzenet = '<div id="note_c" class="alert alert-danger alert-dismissable">Hibás azonosító!</div>';
		}
	}
	else if (isset($_POST['command']) && $_POST['command'] == 'logout') // Kilépés
	{
		unset($_SESSION['login_id']);
		unset($_SESSION['login_nev']);
		unset($_SESSION['login_tipus']);
	}
	
	if(isset($_SESSION['login_tipus']) && $_SESSION['login_tipus'] == 'admin')
	{
		include 'nyitooldal.php';
	}
	else
	{
		include 'login.php';
	}
?>
