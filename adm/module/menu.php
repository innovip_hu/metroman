      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
              <a href="http://www.innovip.hu" target="_blank"><img src="images/innovip_logo_feher.png" style="width: 100%;"></a>
          </div>
		  
 		<?php if($conf_webshop == 1) { ?>
          <!-- Termék kereső -->
          <form action="#" id="kereso_form" method="get" class="sidebar-form">
            <div class="input-group">
              <input onKeyUp="termekKeresoAC()" type="text" id="termek_kereso_input_AC" value="" class="form-control" placeholder="Termék Kereső...">
              <span class="input-group-btn">
                <span onClick="termekKeresoAC()" type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></span>
              </span>
            </div>
          </form>
 		<?php } ?>
          <!-- Menü -->
          <ul class="sidebar-menu">
			<li <?php if($oldal=='nyitooldal'){print 'class="active"';} ?>><a href="index.php"><i class="fa fa-home"></i> <span>Nyitóoldal</span></a></li>
			<li><a href="<?= $domain ?>" target="_blank"><i class="fa fa-desktop"></i> <span>Weboldal</span></a></li>
			
			<!-- Webshop -->
			<?php if($conf_webshop == 1) { ?>
				<li class="header">Webáruház</li>
				<?php if($conf_rendelesek == 1) { ?>
					<li <?php if($oldal=='rendelesek'){print 'class="active"';} ?>>
					  <a href="rendelesek.php">
						<i class="fa fa-shopping-cart"></i>
						<span>Rendelések</span>
						<?php
							$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."rendeles WHERE teljesitve=0");
							$res->execute();
							$rownum_rendelesek = $res->fetchColumn();
							if($rownum_rendelesek > 0)
							{
								print '<span class="label label-primary pull-right">'.$rownum_rendelesek.'</span>';
							}
						?>
						
					  </a>
					</li>
				<?php } ?>
				<li class="treeview <?php if (isset($_GET['csop_id'])) { echo 'active'; } ?>">
					<a href="#">
						<i class="fa fa-dropbox"></i> <span>Termékek</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu <?/*active*/?>">
						<li>
							<a class="menu_term_kat <?php if (isset($_GET['csop_id']) && $_GET['csop_id'] == -6) { echo 'menu_term_kat_aktiv'; } ?>" href="termekek.php?csop_id=-6" >
								<i class="fa <?php if (isset($_GET['csop_id']) && $_GET['csop_id'] == -6) { echo 'fa-circle'; } else { echo 'fa-circle-o'; } ?> text-maroon"></i> Ovip törölt termékek
							</a>							
						</li>						
						<li>
							<a class="menu_term_kat <?php if (isset($_GET['csop_id']) && $_GET['csop_id'] == -3) { echo 'menu_term_kat_aktiv'; } ?>" href="termekek.php?csop_id=-3" >
								<i class="fa <?php if (isset($_GET['csop_id']) && $_GET['csop_id'] == -3) { echo 'fa-circle'; } else { echo 'fa-circle-o'; } ?> text-danger"></i> 0-ás készlet
							</a>							
						</li>
						<li>
							<a class="menu_term_kat <?php if (isset($_GET['csop_id']) && $_GET['csop_id'] == -4) { echo 'menu_term_kat_aktiv'; } ?>" href="termekek.php?csop_id=-4" >
								<i class="fa <?php if (isset($_GET['csop_id']) && $_GET['csop_id'] == -4) { echo 'fa-circle'; } else { echo 'fa-circle-o'; } ?> text-green"></i> Rendelhető státusz
							</a>							
						</li>						
						<li>
							<a class="menu_term_kat <?php if (isset($_GET['csop_id']) && $_GET['csop_id'] == -5) { echo 'menu_term_kat_aktiv'; } ?>" href="termekek.php?csop_id=-5" >
								<i class="fa <?php if (isset($_GET['csop_id']) && $_GET['csop_id'] == -5) { echo 'fa-circle'; } else { echo 'fa-circle-o'; } ?> text-info"></i> Nem rendelhető státusz
							</a>							
						</li>						
						<li>
							<a class="menu_term_kat <?php if (isset($_GET['csop_id']) && $_GET['csop_id'] == -1) { echo 'menu_term_kat_aktiv'; } ?>" href="termekek.php?csop_id=-1" >
								<i class="fa <?php if (isset($_GET['csop_id']) && $_GET['csop_id'] == -1) { echo 'fa-circle'; } else { echo 'fa-circle-o'; } ?> text-yellow"></i> Akciós termékek
							</a>
						</li>
						<?php
						if($conf_kiemelt_term > 0)
						{
							if (isset($_GET['csop_id']) && $_GET['csop_id'] == -2) { $potty = 'fa-circle'; $aktivmenu = 'menu_term_kat_aktiv'; }
							else { $potty = 'fa-circle-o'; $aktivmenu = ''; }
							print '<li><a class="menu_term_kat '.$aktivmenu.'" href="termekek.php?csop_id=-2" ><i class="fa '.$potty.' text-aqua"></i> Kiemelt termékek</a></li>';
						}
						$query_csop1 = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id=0 ORDER BY sorrend ASC";
						foreach ($pdo->query($query_csop1) as $row_csop1)
						{
							$rownum = 0;
							$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id=".$row_csop1['id']);
							$res->execute();
							$rownum = $res->fetchColumn();
							if($rownum > 0) // Ha van alkategória
							{
								?>
										<script>
										function openLINK(id) {
										  location.href = "termekek.php?csop_id="+id;
										}
										</script>
								<?php
								if (isset($_GET['csop_id']) && $_GET['csop_id'] == $row_csop1['id']) { $potty = 'fa-circle'; $aktivmenu = 'menu_term_kat_aktiv'; $menu_open = ' menu-open" style="display:block;"'; } 
								else { $potty = 'fa-circle-o'; $aktivmenu = ''; $menu_open = '"';}								
								print '<li><a class="menu_term_kat '.$aktivmenu.'" onclick="openLINK('.$row_csop1['id'].')"><i class="fa '.$potty.'"></i> '.$row_csop1['nev'].' <i class="fa fa-angle-left pull-right"></i></a>
									<ul class="treeview-menu'.$menu_open.'>';
									$query_csop2 = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id=".$row_csop1['id']." ORDER BY sorrend ASC";
									foreach ($pdo->query($query_csop2) as $row_csop2)
									{
										$rownum = 0;
										$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id=".$row_csop2['id']);
										$res->execute();
										$rownum = $res->fetchColumn();
										if($rownum > 0) // Ha van alkategória
										{
											if (isset($_GET['csop_id']) && $_GET['csop_id'] == $row_csop2['id']) { $potty = 'fa-circle'; $aktivmenu = 'menu_term_kat_aktiv'; $menu_open = ' menu-open" style="display:block;"'; } 
											else { $potty = 'fa-circle-o'; $aktivmenu = ''; $menu_open = '"';}												
											print '<li><a class="menu_term_kat '.$aktivmenu.'" onclick="openLINK('.$row_csop2['id'].')"><i class="fa '.$potty.'"></i> '.$row_csop2['nev'].' <i class="fa fa-angle-left pull-right"></i></a>
												<ul class="treeview-menu'.$menu_open.'>';
												$query_csop3 = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id=".$row_csop2['id']." ORDER BY sorrend ASC";
												foreach ($pdo->query($query_csop3) as $row_csop3)
												{
													if (isset($_GET['csop_id']) && $_GET['csop_id'] == $row_csop3['id']) { $potty = 'fa-circle'; $aktivmenu = 'menu_term_kat_aktiv'; } 
													else { $potty = 'fa-circle-o'; $aktivmenu = ''; }
													print '<li><a class="menu_term_kat '.$aktivmenu.'" href="termekek.php?csop_id='.$row_csop3['id'].'" ><i class="fa '.$potty.'"></i> '.$row_csop3['nev'].'</a></li>';
												}
											print '</ul></li>';
										}
										else // Ha nincs alkategória
										{
											if (isset($_GET['csop_id']) && $_GET['csop_id'] == $row_csop2['id']) { $potty = 'fa-circle'; $aktivmenu = 'menu_term_kat_aktiv'; } 
											else { $potty = 'fa-circle-o'; $aktivmenu = ''; }
											print '<li><a class="menu_term_kat '.$aktivmenu.'" href="termekek.php?csop_id='.$row_csop2['id'].'" ><i class="fa '.$potty.'"></i> '.$row_csop2['nev'].'</a></li>';
										}
									}
								print '</ul></li>';
							}
							else // Ha nincs alkategória
							{
								if (isset($_GET['csop_id']) && $_GET['csop_id'] == $row_csop1['id']) { $potty = 'fa-circle'; $aktivmenu = 'menu_term_kat_aktiv'; } 
								else { $potty = 'fa-circle-o'; $aktivmenu = ''; }
								print '<li><a class="menu_term_kat '.$aktivmenu.'" href="termekek.php?csop_id='.$row_csop1['id'].'" ><i class="fa '.$potty.'"></i> '.$row_csop1['nev'].'</a></li>';
							}
						}
						?>
					</ul>
				</li>
				
				<!-- Termék export/import -->
				<?php if($conf_excel_imp_exp == 1) { ?>
				<li <?php if($oldal=='termek_export_import'){print 'class="active"';} ?>><a href="termek_export_import.php"><i class="fa fa-refresh"></i> <span>Termék export/import</span></a></li>
				<?php } ?>
				
				<!-- Kategóriák -->
				<li <?php if($oldal=='kategoriak'){print 'class="active"';} ?>><a href="kategoriak.php"><i class="fa fa-folder-open"></i> <span>Kategóriák</span></a></li>
				
				<!-- Paraméterek -->
				<?php if($conf_parameterek == 1) { ?>
				<li <?php if($oldal=='parameterek'){print 'class="active"';} ?>><a href="parameterek.php"><i class="fa fa-puzzle-piece"></i> <span>Termék paraméterek</span></a></li>
				<?php } ?>
				
				<!-- Készlet -->
				<?php if($conf_keszlet == 1) { ?>
				<li <?php if($oldal=='keszlet'){print 'class="active"';} ?>><a href="keszlet.php"><i class="fa fa-cubes"></i> <span>Készlet</span></a></li>
				<?php } ?>
				
				<li <?php if($oldal=='statisztikak'){print 'class="active"';} ?>><a href="statisztikak.php"><i class="fa fa-bar-chart"></i> <span>Statisztikák</span></a></li>
				
				<!-- Kupon -->
				<?php if($conf_kupon == 1) { ?>
				<li <?php if($oldal=='kuponok'){print 'class="active"';} ?>><a href="kuponok.php"><i class="fa fa-tag"></i> <span>Kuponok</span></a></li>
				<li <?php if($oldal=='kupon_statisztikak'){print 'class="active"';} ?>><a href="kupon_statisztikak.php"><i class="fa fa-tag"></i> <span>Kupon statisztikák</span></a></li>
				<?php } ?>
				
				<!-- Kívánságlista -->
				<?php if($conf_kivansaglista == 1) { ?>
				<li <?php if($oldal=='kivansaglista'){print 'class="active"';} ?>><a href="kivansaglista.php"><i class="fa fa-heart"></i> <span>Kívánságlista</span></a></li>
				<?php } ?>
				
				<!-- Beállítások -->
				<li <?php if($oldal=='beallitasok'){print 'class="active"';} ?>><a href="beallitasok.php"><i class="fa fa-cog"></i> <span>Beállítások</span></a></li>
				
				<!-- Árak megosztása -->
				<?php if($conf_arak_megosztasa == 1) { ?>
				<li <?php if($oldal=='arak_megosztasa'){print 'class="active"';} ?>><a href="arak_megosztasa.php"><i class="fa fa-share-alt"></i> <span>Árak megosztása</span></a></li>
				<?php } ?>
			<?php } ?>
			
			<li <?php if($oldal=='felhasznalok'){print 'class="active"';} ?>><a href="felhasznalok.php"><i class="fa fa-user"></i> <span>Felhasználók</span></a></li>
			
			<li class="header">Weboldal</li>
			
 			<!-- Médiatár -->
			<?php if($conf_mediatar == 1) { ?>
			<li <?php if($oldal=='mediatar'){print 'class="active"';} ?>><a href="mediatar.php"><i class="fa fa-film"></i> <span>Médiatár</span></a></li>
			<?php } ?>
			
 			<!-- Hírek -->
			<?php if($conf_hirek == 1 && $conf_hirek_kat == 0) { ?>
			<li <?php if($oldal=='hirek'){print 'class="active"';} ?>><a href="hirek.php"><i class="fa fa-newspaper-o"></i> <span>Hírek</span></a></li>
			<?php } ?>
			
			<!-- Hírek kategória -->
			<?php if ($conf_hirek_kat == 1): ?>
			<li class="treeview <?php if (isset($_GET['hir_csop_id'])) { echo 'active'; } ?>">
				<a href="#">
					<i class="fa fa-newspaper-o"></i> <span>Hírek</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu <?/*active*/?>">
					<?php
					$query_csop1 = "SELECT * FROM ".$webjel."hirek_kategoriak ORDER BY nev ASC";
					foreach ($pdo->query($query_csop1) as $row_csop1)
					{
						if (isset($_GET['hir_csop_id']) && $_GET['hir_csop_id'] == $row_csop1['id']) { $potty = 'fa-circle'; $aktivmenu = 'menu_term_kat_aktiv'; } 
						else { $potty = 'fa-circle-o'; $aktivmenu = ''; }
						print '<li><a class="menu_term_kat '.$aktivmenu.'" href="hirek-spec.php?hir_csop_id='.$row_csop1['id'].'" ><i class="fa '.$potty.'"></i> '.$row_csop1['nev'].'</a></li>';
					}
					?>
				</ul>	
			</li>

			<li <?php if($oldal=='kategoriak-hirek'){print 'class="active"';} ?>><a href="hirek-kategoriak.php"><i class="fa fa-newspaper-o"></i> <span>Hírek kategóriák</span></a></li>				
			<?php endif ?>


			<!-- Hírek2 -->
			<?php if($conf_hirek2 == 1) { ?>
			<li <?php if($oldal==$conf_hirek2_nev){print 'class="active"';} ?>><a href="hirek2.php"><i class="fa fa-newspaper-o"></i> <span><?=$conf_hirek2_nev?></span></a></li>
			<?php } ?>			
			
			<!-- Hírlevél -->
			<?php if($conf_hirlevel == 1) { ?>
			<li <?php if($oldal=='hirlevel'){print 'class="active"';} ?>>
				<a href="hirlevel.php">
					<i class="fa fa-envelope"></i>
					<span>Hírlevél</span>
					<?php
						$rownum_hirl = 0;
						$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirlevel WHERE lekerdezve=0");
						$res->execute();
						$rownum_hirl = $res->fetchColumn();
						if($rownum_hirl > 0)
						{
							print '<span class="label pull-right bg-red">'.$rownum_hirl.'</span>';
						}
					?>
				</a>
            </li>
			<?php } ?>
			
			<!-- Pop-up -->
			<?php if($conf_popup == 1) { ?>
			<li <?php if($oldal=='popup'){print 'class="active"';} ?>><a href="popup.php"><i class="fa fa-commenting"></i> <span>Pop-up</span></a></li>
			<?php } ?>
			
			<!-- Banner -->
			<?php if($conf_banner == 1) { ?>
			<li <?php if($oldal=='bannerek'){print 'class="active"';} ?>><a href="bannerek.php"><i class="fa fa-picture-o"></i> <span>Bannerek</span></a></li>
			<?php } ?>
			
			<!-- Slider -->
			<?php if($conf_slider == 1) { ?>
				<li <?php if($oldal=='sliderek'){print 'class="active"';} ?>><a href="sliderek.php"><i class="fa fa-desktop"></i> <span>Sliderek</span></a></li>
			<?php } ?>
			
			<!-- Video Slider -->
			<?php if($conf_video_slider == 1) { ?>
				<li <?php if($oldal=='video_sliderek'){print 'class="active"';} ?>><a href="video_sliderek.php"><i class="fa fa-video-camera"></i> <span>Video Sliderek</span></a></li>
			<?php } ?>
			
			<!-- Galéria -->
			<?php if($conf_galeria == 1) { ?>
			<li <?php if($oldal=='galeria'){print 'class="active"';} ?>><a href="galeria.php"><i class="fa fa-camera"></i> <span>Galéria</span></a></li>
			<?php } ?>
			
			<!-- Youtube Galéria -->
			<?php if($conf_yt_galeria == 1) { ?>
			<li <?php if($oldal=='yt_galeria'){print 'class="active"';} ?>><a href="yt_galeria.php"><i class="fa fa-youtube-play"></i> <span>Youtube galéria</span></a></li>
			<?php } ?>
			
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
