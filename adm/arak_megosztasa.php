<?php
	session_start();
	ob_start();
	
	include '../config.php';
	
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	$oldal = 'arak_megosztasa';
	if(isset($_GET['megoszto']))
	{
		include 'xml_arak_megosztasa.php';
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Árak megosztása | Admin</title>
		<?php
			include 'module/head.php';
		?>
		<script src="scripts/hirlevel.js"></script>
	</head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
		<?php
			include 'module/header.php';
			include 'module/menu.php';
		?>
		
		<div id="munkaablak">
			<div class="content-wrapper bg_admin">
				<section class="content-header">
				  <h1 id="myModal">Árak megosztása</h1>
				  <ol class="breadcrumb">
					<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
					<li class="active">Árak megosztása</li>
				  </ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">XML fájl generálás</h3>
									<div class="box-tools pull-right">
										<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="box-body" id="szallitas_div">
									<a href="?megoszto=arukereso" class="btn btn-app"><i class="fa fa-share-alt"></i> Megosztás</a>
									<?php
										if(isset($_GET['megoszto']))
										{
											// print '<a href="'.$domain.'/adm/xml_'.$_GET['megoszto'].'.xml" target="_blank">'.$domain.'/adm/xml_'.$_GET['megoszto'].'.xml</a>';
											?>
											<ul class="products-list product-list-in-box">
												<li class="item">
													<div class="product-img">
														<img src="images/arukereso.png">
													</div>
													<div class="product-info">
														Árukereső
														<span class="product-description">
															<a href="<?php print $domain; ?>/adm/xml_arukereso.xml" target="_blank"><?php print $domain; ?>/adm/xml_arukereso.xml</a>
														</span>
													</div>
												</li>
												<li class="item">
													<div class="product-img">
														<img src="images/argep.png">
													</div>
													<div class="product-info">
														Árgép
														<span class="product-description">
															<a href="<?php print $domain; ?>/adm/xml_argep.xml" target="_blank"><?php print $domain; ?>/adm/xml_argep.xml</a>
														</span>
													</div>
												</li>
												<li class="item">
													<div class="product-img">
														<img src="images/olcsobbat.png">
													</div>
													<div class="product-info">
														Olcsóbbat.hu
														<span class="product-description">
															<a href="<?php print $domain; ?>/adm/xml_olcsobbat.xml" target="_blank"><?php print $domain; ?>/adm/xml_olcsobbat.xml</a>
														</span>
													</div>
												</li>
																								
												<li class="item">
													<div class="product-img">
														<img src="images/facebook-logo.png">
													</div>
													<div class="product-info">
														Facebook Catalog
														<span class="product-description">
															<a href="<?php print $domain; ?>/adm/xml_facebook.xml" target="_blank"><?php print $domain; ?>/adm/xml_facebook.xml</a>
														</span>
													</div>
												</li>	

												<li class="item">
													<div class="product-img">
														<img src="images/new-google-favicon-512.png">
													</div>
													<div class="product-info">
														Google Merchant
														<span class="product-description">
															<a href="<?php print $domain; ?>/adm/xml_google_merchant.xml" target="_blank"><?php print $domain; ?>/adm/xml_google_merchant.xml</a>
														</span>
													</div>
												</li>												
											</ul>
											<?php
										}
									?>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<?php
			include 'module/footer.php';
		?>
    </div>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- Datepicker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <!-- CK Editor -->
    <script src="plugins/ckeditor/ckeditor.js"></script>
    <!-- Dropzone -->
	<script src="scripts/dropzone.js"></script>
    <!-- Page Script -->
    <script>
      $(function () {

        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function () {
          var clicks = $(this).data('clicks');
          if (clicks) {
            //Uncheck all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
          } else {
            //Check all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
          }
          $(this).data("clicks", !clicks);
        });

        //Handle starring for glyphicon and font awesome
        $(".mailbox-star").click(function (e) {
          e.preventDefault();
          //detect type
          var $this = $(this).find("a > i");
          var glyph = $this.hasClass("glyphicon");
          var fa = $this.hasClass("fa");

          //Switch states
          if (glyph) {
            $this.toggleClass("glyphicon-star");
            $this.toggleClass("glyphicon-star-empty");
          }

          if (fa) {
            $this.toggleClass("fa-star");
            $this.toggleClass("fa-star-o");
          }
        });
      });
    </script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
	<?php
		include 'module/body_end.php';
	?>
  </body>
</html>
