<?php
 	$oldal = 'nyitooldal';
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Admin</title>
		<?php
			include 'module/head.php';
		?>
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			<?php
				include 'module/header.php';
				include 'module/menu.php';
			?>
			<div id="munkaablak">
				<div class="content-wrapper <?php /* bg_admin */ ?>">
					<section class="content-header">
					  <h1>
						Nyitóoldal<small></small>
					  </h1>
					  <ol class="breadcrumb">
						<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
					  </ol>
					</section>

					<section class="content">
					  <!-- Menü dobozok -->
					  <div class="row">
						<!--Webshop-->
						<?php if($conf_rendelesek == 1) { ?>
							<?php if($conf_excel_imp_exp == 1) { ?>
								<div class="col-lg-3 col-xs-6">
								  <div class="small-box bg-blue">
									<div class="inner">
									  <h3><?php print $rownum_rendelesek; ?><sup style="font-size: 20px">Új</sup></h3>
									  <p>Rendelések</p>
									</div>
									<div class="icon">
									  <i class="ion ion-ios-cart"></i>
									</div>
									<a href="rendelesek.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
								  </div>
								</div>
							<?php } ?>
							
							<!-- Termék export/import -->
							<?php if($conf_excel_imp_exp == 1) { ?>
								<div class="col-lg-3 col-xs-6">
								  <div class="small-box bg-red">
									<div class="inner">
										<h3>&nbsp;</h3>
										<p>Termék export/import</p>
									</div>
									<div class="icon">
									  <i class="fa fa-refresh"></i>
									</div>
									<a href="termek_export_import.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
								  </div>
								</div>
							<?php } ?>
							
							<!-- Termék kategóriák -->
							<div class="col-lg-3 col-xs-6">
							  <div class="small-box bg-gray-active">
								<div class="inner">
									<h3>
										<?php
										$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok");
										$res->execute();
										$rownum = $res->fetchColumn();
										print $rownum;
										?>
									</h3>
									<p>Termék kategóriák</p>
								</div>
								<div class="icon">
								  <i class="ion ion-ios-folder"></i>
								</div>
								<a href="kategoriak.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
							  </div>
							</div>
							
							<!-- Paraméterek -->
							<?php if($conf_parameterek == 1) { ?>
								<div class="col-lg-3 col-xs-6">
								  <div class="small-box bg-aqua">
									<div class="inner">
										<h3>
											<?php
											$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termek_parameterek");
											$res->execute();
											$rownum = $res->fetchColumn();
											print $rownum;
											?>
										</h3>
										<p>Termék paraméterek</p>
									</div>
									<div class="icon">
									  <i class="fa fa-puzzle-piece"></i>
									</div>
									<a href="parameterek.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
								  </div>
								</div>
							<?php } ?>
							
							<!-- Készlet -->
							<?php if($conf_keszlet == 1) { ?>
								<div class="col-lg-3 col-xs-6">
								  <div class="small-box bg-maroon">
									<div class="inner">
									  <h3>&nbsp;</h3>
									  <p>Készlet</p>
									</div>
									<div class="icon">
									  <i class="ion ion-ios-keypad"></i>
									</div>
									<a href="#" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
								  </div>
								</div>
							<?php } ?>
							
							<div class="col-lg-3 col-xs-6">
							  <div class="small-box bg-olive">
								<div class="inner">
								  <h3>&nbsp;</h3>
								  <p>Statisztikák</p>
								</div>
								<div class="icon">
								  <i class="ion ion-podium"></i>
								</div>
								<a href="statisztikak.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
							  </div>
							</div>
							
							<!-- Kupon -->
							<?php if($conf_kupon == 1) { ?>
							<div class="col-lg-3 col-xs-6">
							  <div class="small-box bg-orange">
								<div class="inner">
									<h3>
										<?php
										$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."kuponok");
										$res->execute();
										$rownum = $res->fetchColumn();
										print $rownum;
										?>
									</h3>
									<p>Kuponok</p>
								</div>
								<div class="icon">
								  <i class="ion ion-ios-pricetag"></i>
								</div>
								<a href="kuponok.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
							  </div>
							</div>
							<?php } ?>
							
							<!-- Kívánságlista -->
							<?php if($conf_kivansaglista == 1) { ?>
							<div class="col-lg-3 col-xs-6">
							  <div class="small-box bg-light-blue">
								<div class="inner">
									<h3>
										<?php
										$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."kivansag_lista");
										$res->execute();
										$rownum = $res->fetchColumn();
										print $rownum;
										?>
									</h3>
									<p>Kívánságlista</p>
								</div>
								<div class="icon">
								  <i class="ion ion-heart"></i>
								</div>
								<a href="kivansaglista.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
							  </div>
							</div>
							<?php } ?>
							
							<div class="col-lg-3 col-xs-6">
							  <div class="small-box bg-teal">
								<div class="inner">
								  <h3>&nbsp;</h3>
								  <p>Beállítások</p>
								</div>
								<div class="icon">
								  <i class="ion ion-ios-gear"></i>
								</div>
								<a href="beallitasok.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
							  </div>
							</div>

							<!-- Árak megosztása -->
							<?php if($conf_arak_megosztasa == 1) { ?>
							<div class="col-lg-3 col-xs-6">
							  <div class="small-box bg-blue">
								<div class="inner">
									<h3>&nbsp;</h3>
									<p>Árak megosztása</p>
								</div>
								<div class="icon">
								  <i class="ion ion-android-share-alt"></i>
								</div>
								<a href="arak_megosztasa.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
							  </div>
							</div>
							<?php } ?>
						<?php } ?>
						
						<div class="col-lg-3 col-xs-6">
						  <div class="small-box bg-aqua">
							<div class="inner">
							  <h3>
								<?php
								$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."users");
								$res->execute();
								$rownum = $res->fetchColumn();
								print $rownum;
								?>
							  </h3>
							  <p>Felhasználók</p>
							</div>
							<div class="icon">
							  <i class="ion ion-ios-people"></i>
							</div>
							<a href="<?php print $domain; ?>/adm/felhasznalok.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
						  </div>
						</div>
						
						<!-- Médiatár -->
						<?php if($conf_mediatar == 1) { ?>
						<div class="col-lg-3 col-xs-6">
						  <div class="small-box bg-lime">
							<div class="inner">
								<h3>&nbsp;</h3>
								<p>Médiatár</p>
							</div>
							<div class="icon">
							  <i class="fa fa-film"></i>
							</div>
							<a href="mediatar.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
						  </div>
						</div>
						<?php } ?>
						
						<!-- Hírek -->
						<?php if($conf_hirek == 1 && $conf_hirek_kat == 0) { ?>
						<div class="col-lg-3 col-xs-6">
						  <div class="small-box bg-red">
							<div class="inner">
							  <h3>
								<?php
								$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirek");
								$res->execute();
								$rownum = $res->fetchColumn();
								print $rownum;
								?>
							  </h3>
							  <p>Hírek</p>
							</div>
							<div class="icon">
							  <i class="ion ion-ios-paper"></i>
							</div>
							<a href="hirek.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
						  </div>
						</div>
						<?php } ?>

						<!-- Hírek kategória -->
						<?php if ($conf_hirek_kat == 1): ?>
						<div class="col-lg-3 col-xs-6">
						  <div class="small-box bg-yellow-gradient">
							<div class="inner">
							  <h3>
								<?php
								$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirek_kategoriak");
								$res->execute();
								$rownum = $res->fetchColumn();
								print $rownum;
								?>
							  </h3>
							  <p>Hírek kategóriák</p>
							</div>
							<div class="icon">
							  <i class="ion ion-ios-folder"></i>
							</div>
							<a href="hirek-kategoriak.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
						  </div>
						</div>				
						<?php endif ?>							

						<?php if($conf_hirek2 == 1) { ?>
						<div class="col-lg-3 col-xs-6">
						  <div class="small-box bg-teal">
							<div class="inner">
							  <h3>
								<?php
								$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirek2");
								$res->execute();
								$rownum = $res->fetchColumn();
								print $rownum;
								?>
							  </h3>
							  <p><?=$conf_hirek2_nev?></p>
							</div>
							<div class="icon">
							  <i class="ion ion-ios-paper"></i>
							</div>
							<a href="hirek2.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
						  </div>
						</div>
						<?php } ?>						

						<!-- Hírlevél -->
						<?php if($conf_hirlevel == 1) { ?>
						<div class="col-lg-3 col-xs-6">
						  <div class="small-box bg-gray">
							<div class="inner">
							  <h3><?php print $rownum_hirl; ?><sup style="font-size: 20px">Új</sup></h3>
							  <p>Hírlevél</p>
							</div>
							<div class="icon">
							  <i class="ion ion-ios-email"></i>
							</div>
							<a href="hirlevel.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
						  </div>
						</div>
						<?php } ?>
						
						<!-- Pop-up -->
						<?php if($conf_popup == 1) { ?>
						<div class="col-lg-3 col-xs-6">
						  <div class="small-box bg-gray-active">
							<div class="inner">
								<h3>
									<?php
									$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."popup");
									$res->execute();
									$rownum = $res->fetchColumn();
									print $rownum;
									?>
								</h3>
								<p>Pop-up</p>
							</div>
							<div class="icon">
							  <i class="fa fa-commenting"></i>
							</div>
							<a href="popup.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
						  </div>
						</div>
						<?php } ?>
						
						<!-- Banner -->
						<?php if($conf_banner == 1) { ?>
						<div class="col-lg-3 col-xs-6">
						  <div class="small-box bg-green">
							<div class="inner">
								<h3>
									<?php
									$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."banner");
									$res->execute();
									$rownum = $res->fetchColumn();
									print $rownum;
									?>
								</h3>
								<p>Bannerek</p>
							</div>
							<div class="icon">
							  <i class="ion ion-ios-photos"></i>
							</div>
							<a href="bannerek.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
						  </div>
						</div>
						<?php } ?>
						
						<!-- Slider -->
						<?php if($conf_slider == 1) { ?>
							<div class="col-lg-3 col-xs-6">
							  <div class="small-box bg-teal">
								<div class="inner">
									<h3>
										<?php
										$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."slider");
										$res->execute();
										$rownum = $res->fetchColumn();
										print $rownum;
										?>
									</h3>
								  <p>Sliderek</p>
								</div>
								<div class="icon">
								  <i class="ion ion-ios-monitor"></i>
								</div>
								<a href="sliderek.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
							  </div>
							</div>
						<?php } ?>
						
						<!-- Video Slider -->
						<?php if($conf_video_slider == 1) { ?>
							<div class="col-lg-3 col-xs-6">
							  <div class="small-box bg-yellow">
								<div class="inner">
									<h3>
										<?php
										$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."video_slider");
										$res->execute();
										$rownum = $res->fetchColumn();
										print $rownum;
										?>
									</h3>
								  <p>Video Sliderek</p>
								</div>
								<div class="icon">
								  <i class="ion ion-ios-videocam"></i>
								</div>
								<a href="video_sliderek.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
							  </div>
							</div>
						<?php } ?>
						
						<!-- Galéria -->
						<?php if($conf_galeria == 1) { ?>
							<div class="col-lg-3 col-xs-6">
							  <div class="small-box bg-purple">
								<div class="inner">
									<h3>
										<?php
										$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."galeria");
										$res->execute();
										$rownum = $res->fetchColumn();
										print $rownum;
										?>
									</h3>
									<p>Galéria</p>
								</div>
								<div class="icon">
								  <i class="ion ion-ios-camera"></i>
								</div>
								<a href="galeria.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
							  </div>
							</div>
						<?php } ?>
						
						<!-- Youtube Galéria -->
						<?php if($conf_yt_galeria == 1) { ?>
							<div class="col-lg-3 col-xs-6">
							  <div class="small-box bg-aqua">
								<div class="inner">
									<h3>
										<?php
										$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."yt_galeria");
										$res->execute();
										$rownum = $res->fetchColumn();
										print $rownum;
										?>
									</h3>
									<p>Youtube galéria</p>
								</div>
								<div class="icon">
								  <i class="fa fa-youtube"></i>
								</div>
								<a href="yt_galeria.php" class="small-box-footer">Tovább <i class="fa fa-arrow-circle-right"></i></a>
							  </div>
							</div>
						<?php } ?>
					  </div>
					  <!-- Menü dobozok VÉGE -->
			 
					<!--Webshop-->
					<?php if($conf_webshop == 1) { ?>
					  <!-- Statisztika -->
					  <div class="row">
						<div class="col-md-12">
						  <div class="box">
							<div class="box-header with-border">
								<h3 class="box-title">Vásárlási statisztika <small>
									<?php
										$honapok = array("nulla", "Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December");
										$datum_kezd = date("Y-m-d", strtotime("-6 months"));
										$datum_vege = date('Y-m-d');
										print $datum_kezd.' és '.$datum_vege.' között';
										
										$szinek = array("#00c0ef", "#3c8dbc", "#d2d6de","#f56954","#f39c12",  "#605CA8", "#00A65A", "#D81B60", "#39CCCC");
										$szinek_2 = array("#f56954", "#f39c12", "#605CA8", "#39CCCC", "#00c0ef", "#3c8dbc", "#d2d6de", "#00A65A", "#D81B60");
										shuffle($szinek);
										shuffle($szinek_2);
									?>
								</small></h3>
							</div><!-- /.box-header -->
							<div class="box-body">
							  <div class="row">
									<div class="col-md-8">
										<div class="chart">
											<canvas id="areaChart" style="height: 180px;"></canvas>
										</div>
									</div>
								<div class="col-md-4">
								  <!--<p class="text-center">
									<strong>Fizetés és szállítás</strong>
								  </p>-->
									<?php
										$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."rendeles WHERE datum >= '".$datum_kezd."' AND datum <= '".$datum_vege."' AND teljesitve = 1");
										$res->execute();
										$fizmod_ossz = $res->fetchColumn();
	
										$query = "SELECT *, COUNT(fiz_mod) as db FROM ".$webjel."rendeles 
											WHERE datum >= '".$datum_kezd."' AND datum <= '".$datum_vege."' AND teljesitve = 1 
											GROUP BY fiz_mod
											ORDER BY fiz_mod ASC";
										$i = 0;
										foreach ($pdo->query($query) as $row)
										{
											$szazalek = round(100 * $row['db'] / $fizmod_ossz);
											// print '<div><i class="fa fa-square" style="color:'.$szinek_2[$i].'"></i> '.$row['fiz_mod'].': '.$row['db'].' db</div>';
											print '<div class="progress-group">
												<span class="progress-text">'.$row['fiz_mod'].'</span>
												<span class="progress-number"><b>'.$row['db'].'</b>/'.$fizmod_ossz.'</span>
												<div class="progress sm">
													<div class="progress-bar" style="width: '.$szazalek.'%; background-color: '.$szinek_2[$i].';"></div>
												</div>
											</div>';
											$i++;
										}
									?>
								</div>
							  </div>
							</div>
						  </div>
						</div>
					  </div>
					<?php } ?>
					  

					</section>
				</div>
			</div>
			<?php
				include 'module/footer.php';
			?>
		</div>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <!-- CK Editor -->
    <script src="plugins/ckeditor/ckeditor.js"></script>
    <!-- Bootstrap WYSIHTML5
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script> -->
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="plugins/chartjs/Chart.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard3.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- Dropzone -->
	<script src="scripts/dropzone.js"></script>
	<?php
		include 'module/body_end.php';
	?>
	<script>
      $(function () {
        /* ChartJS
         * -------
         * Here we will create a few charts using ChartJS
         */

        //--------------
        //- AREA CHART -
        //--------------

        // Get context with jQuery - using jQuery's .get() method.
        var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var areaChart = new Chart(areaChartCanvas);
			<?php
				$chart_honapok = array();
				$chart_ertekek = array();
				$query = "SELECT *, MONTH(".$webjel."rendeles.datum) as honap, SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar=0, ".$webjel."rendeles_tetelek.term_ar * ".$webjel."rendeles_tetelek.term_db,".$webjel."rendeles_tetelek.term_akcios_ar * ".$webjel."rendeles_tetelek.term_db)) as sum_ar  
							FROM ".$webjel."rendeles_tetelek 
							INNER JOIN ".$webjel."rendeles 
							ON ".$webjel."rendeles.id = ".$webjel."rendeles_tetelek.rendeles_id 
							WHERE ".$webjel."rendeles.datum >= '".$datum_kezd."' AND ".$webjel."rendeles.datum <= '".$datum_vege."' AND ".$webjel."rendeles.teljesitve = 1 
							GROUP BY YEAR(".$webjel."rendeles.datum), MONTH(".$webjel."rendeles.datum)";
				foreach ($pdo->query($query) as $row)
				{
					// print $honapok[$row['honap']].' : '.$row['sum_ar'].'<br/>';
					$chart_honapok[] = $honapok[$row['honap']];
					$chart_ertekek[] = $row['sum_ar'];
				}
			?>
        var areaChartData = {
          labels: [
				<?php
					foreach($chart_honapok as $x)
					{
						print '"'.$x.'", ';
					}
				?>
		  ],
          datasets: [
            {
              label: "Digital Goods",
              fillColor: "rgba(60,141,188,0.9)",
              strokeColor: "rgba(60,141,188,0.8)",
              pointColor: "#3b8bba",
              pointStrokeColor: "rgba(60,141,188,1)",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(60,141,188,1)",
              data: [
			  	<?php
					foreach($chart_ertekek as $x)
					{
						print '"'.$x.'", ';
					}
				?>
			  ]
            }
          ]
        };

        var areaChartOptions = {
          //Boolean - If we should show the scale at all
          showScale: true,
          //Boolean - Whether grid lines are shown across the chart
          scaleShowGridLines: false,
          //String - Colour of the grid lines
          scaleGridLineColor: "rgba(0,0,0,.05)",
          //Number - Width of the grid lines
          scaleGridLineWidth: 1,
          //Boolean - Whether to show horizontal lines (except X axis)
          scaleShowHorizontalLines: true,
          //Boolean - Whether to show vertical lines (except Y axis)
          scaleShowVerticalLines: true,
          //Boolean - Whether the line is curved between points
          bezierCurve: true,
          //Number - Tension of the bezier curve between points
          bezierCurveTension: 0.3,
          //Boolean - Whether to show a dot for each point
          pointDot: false,
          //Number - Radius of each point dot in pixels
          pointDotRadius: 4,
          //Number - Pixel width of point dot stroke
          pointDotStrokeWidth: 1,
          //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
          pointHitDetectionRadius: 20,
          //Boolean - Whether to show a stroke for datasets
          datasetStroke: true,
          //Number - Pixel width of dataset stroke
          datasetStrokeWidth: 2,
          //Boolean - Whether to fill the dataset with a color
          datasetFill: true,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
          //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio: true,
          //Boolean - whether to make the chart responsive to window resizing
          responsive: true
        };

        //Create the line chart
        areaChart.Line(areaChartData, areaChartOptions);


      });
	</script>
  </body>
</html>
