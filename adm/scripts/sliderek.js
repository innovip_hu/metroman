	// Mentés megjelenítése
	function mentesNezet(id, input) {
		document.getElementById("slider_footer_"+id).style.display = 'block';
		document.getElementById(input+id).style.color = '#DD4B39';
	}
	// Mentés
	function sliderMentes(id) {
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("slider_footer_"+id).style.display = 'none';
				document.getElementById('nev_'+id).style.color = '#555';
				document.getElementById('link_'+id).style.color = '#555';
				document.getElementById('linkgomb_'+id).style.color = '#555';
				document.getElementById('szoveg_'+id).style.color = '#555';
				document.getElementById('sorrend_'+id).style.color = '#555';
				// Hover message
				$('<span>Adatok elmentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		}
 		xmlhttp.open("POST","module-sliderek/slider_mentes.php",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("id="+id+"&nev="+encodeURIComponent(document.getElementById("nev_"+id).value)+"&link="+encodeURIComponent(document.getElementById("link_"+id).value)+"&linkgomb="+encodeURIComponent(document.getElementById("linkgomb_"+id).value)+"&szoveg="+encodeURIComponent(document.getElementById("szoveg_"+id).value)+"&sorrend="+encodeURIComponent(document.getElementById("sorrend_"+id).value));
	}
	// Modal nyitás
	function rakerdez_slider(modal_id, id, nev) {
		document.getElementById("modal_torles_id").value=id;
		document.getElementById("modal_torles_nev").innerHTML=nev;
		document.getElementById(modal_id).style.display = 'block';
	};
	function megsem_slider(modal_id) {
		document.getElementById("modal_torles_id").value=0;
		document.getElementById("modal_torles_nev").innerHTML='';
		document.getElementById(modal_id).style.display = 'none';
	};
	// slider törlése
	function sliderTorles(modal_id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById(modal_id).style.display = 'none';
				document.getElementById("sliderek").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Slider törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-sliderek/sliderek.php?script=ok&torlendo_slider_id="+document.getElementById("modal_torles_id").value,true);
		xmlhttp.send();
	};

	