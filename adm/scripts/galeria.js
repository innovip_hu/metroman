	// lapozás Tovább gomb
	function lapozasTovabb(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+(Number(document.getElementById("kezd").value)+Number(document.getElementById("oldalszam").value)),true);
		xmlhttp.send();
	};
	// lapozás Vissza gomb
	function lapozasVissza(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+(Number(document.getElementById("kezd").value)-Number(document.getElementById("oldalszam").value)),true);
		xmlhttp.send();
	};
	
 	// Belépés
	function Belepes(belep_fajl,fajl,id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				// Dropzone
				$("#kepfeltoltes").dropzone({
					dictDefaultMessage: "Húzd ide a feltöltendő képeket, vagy kattints a mezőbe",
					autoProcessQueue: true,
					// acceptedFiles: "image/jpeg",
					url: 'upload_galeria.php?id='+id,
					// maxFiles: 20, // Number of files at a time
					maxFilesize: 10, //in MB
					maxfilesexceeded: function(file)
					{
					alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
					},
					init: function () {
						this.on("complete", function (file) {
							setTimeout( function() {
								$('.dz-complete').remove();
							}, 2000); // feltöltés után az ikon törlése
							if (window.XMLHttpRequest)
							  {// code for IE7+, Firefox, Chrome, Opera, Safari
							  xmlhttp=new XMLHttpRequest();
							  }
							else
							  {// code for IE6, IE5
							  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
							  }
							xmlhttp.onreadystatechange=function()
							  {
							  if (xmlhttp.readyState==4 && xmlhttp.status==200)
								{
									document.getElementById("galeria_kepek_div").innerHTML=xmlhttp.responseText;
								}
							  }
							xmlhttp.open("GET","module-galeria/galeria_kepek.php?script=ok&id="+id,true);
							xmlhttp.send();
						});
					}
				});
			}
		  }
		xmlhttp.open("GET",belep_fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&id="+id+"&fajl="+fajl,true);
		xmlhttp.send();
	};

	// Visszalépés a listába
	function vissza(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+encodeURIComponent(document.getElementById("oldalszam").value)+"&sorr_tip="+encodeURIComponent(document.getElementById("sorr_tip").value)+"&sorrend="+encodeURIComponent(document.getElementById("sorrend").value)+"&kezd="+encodeURIComponent(document.getElementById("kezd").value),true);
		xmlhttp.send();
	};

 	// Új galéria
	function ujGaleria(fajl, belep_fajl)
	{
		if(document.getElementById("uj_galeria").value == '') {
			document.getElementById("uj_galeria").focus();
			document.getElementById("riaszt_galeria").innerHTML = '<div class="alert alert-danger margbot0">Add meg a galéria nevét!</div>';
		}
		else {
			// ÚJ termék létrehozása ID miatt
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var id = parseFloat(xmlhttp.responseText); // Az új termék ID-je
					// alert(id);
					if (window.XMLHttpRequest)
					  {// code for IE7+, Firefox, Chrome, Opera, Safari
					  xmlhttp=new XMLHttpRequest();
					  }
					else
					  {// code for IE6, IE5
					  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
					  }
					xmlhttp.onreadystatechange=function()
					  {
					  if (xmlhttp.readyState==4 && xmlhttp.status==200)
						{
							document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
							// Hover message
							$('<span>Új galéria elmentve.</span>').hovermessage({
								autoclose : 3000,
								position : 'top-right',
							});
							// Dropzone
							$("#kepfeltoltes").dropzone({
								dictDefaultMessage: "Húzd ide a feltöltendő képeket, vagy kattints a mezőbe",
								autoProcessQueue: true,
								// acceptedFiles: "image/jpeg",
								url: 'upload_galeria.php?id='+id,
								// maxFiles: 20, // Number of files at a time
								maxFilesize: 10, //in MB
								maxfilesexceeded: function(file)
								{
								alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
								},
								init: function () {
									this.on("complete", function (file) {
										setTimeout( function() {
											$('.dz-complete').remove();
										}, 2000); // feltöltés után az ikon törlése
										if (window.XMLHttpRequest)
										  {// code for IE7+, Firefox, Chrome, Opera, Safari
										  xmlhttp=new XMLHttpRequest();
										  }
										else
										  {// code for IE6, IE5
										  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
										  }
										xmlhttp.onreadystatechange=function()
										  {
										  if (xmlhttp.readyState==4 && xmlhttp.status==200)
											{
												document.getElementById("galeria_kepek_div").innerHTML=xmlhttp.responseText;
											}
										  }
										xmlhttp.open("GET","module-galeria/galeria_kepek.php?script=ok&id="+id,true);
										xmlhttp.send();
									});
								}
							});
						}
					  }
					xmlhttp.open("POST",belep_fajl+"?script=ok&command=uj_galeria&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&fajl="+fajl+"&id="+id,true);
					xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
					xmlhttp.send("nev="+encodeURIComponent(document.getElementById("uj_galeria").value));
			}
		  }
		xmlhttp.open("POST","module-galeria/galeria_letrehozas.php",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("nev="+encodeURIComponent(document.getElementById("uj_galeria").value));
		}
	};
	// Képek alapértelmezése
	function galeriaKepAlap(id, kep_id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("galeria_kepek_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Alapértelmezés beállítva</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-galeria/galeria_kepek.php?script=ok&id="+id+"&alap_kep_id="+kep_id,true);
		xmlhttp.send();
	};
	
	// Képek törlése
	function termekKepTorles(id, kep_id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("galeria_kepek_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Kép törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-galeria/galeria_kepek.php?script=ok&id="+id+"&torlendo_kep_id="+kep_id,true);
		xmlhttp.send();
	};

	function megnevezesMentes(id, kep_id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("galeria_kepek_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Adatok mentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-galeria/galeria_kepek.php?script=ok&id="+id+"&megnevezes_mentes="+kep_id+"&megnevezes="+encodeURIComponent($('#megnevezes_'+kep_id).val())+"&sorrend="+encodeURIComponent($('#sorrend_'+kep_id).val()),true);
		xmlhttp.send();
	};	
	// Kategória módosításának mentése
	function mentesGaleria(id, fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				// Hover message
				$('<span>Adatok elmentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
				// Dropzone
				$("#kepfeltoltes").dropzone({
					dictDefaultMessage: "Húzd ide a feltöltendő képeket, vagy kattints a mezőbe",
					autoProcessQueue: true,
					// acceptedFiles: "image/jpeg",
					url: 'upload_galeria.php?id='+id,
					// maxFiles: 20, // Number of files at a time
					maxFilesize: 10, //in MB
					maxfilesexceeded: function(file)
					{
					alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
					},
					init: function () {
						this.on("complete", function (file) {
							setTimeout( function() {
								$('.dz-complete').remove();
							}, 2000); // feltöltés után az ikon törlése
							if (window.XMLHttpRequest)
							  {// code for IE7+, Firefox, Chrome, Opera, Safari
							  xmlhttp=new XMLHttpRequest();
							  }
							else
							  {// code for IE6, IE5
							  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
							  }
							xmlhttp.onreadystatechange=function()
							  {
							  if (xmlhttp.readyState==4 && xmlhttp.status==200)
								{
									document.getElementById("galeria_kepek_div").innerHTML=xmlhttp.responseText;
								}
							  }
							xmlhttp.open("GET","module-galeria/galeria_kepek.php?script=ok&id="+id,true);
							xmlhttp.send();
						});
					}
				});
			}
		  }
		xmlhttp.open("POST","module-galeria/galeria.php?script=ok&command=mentes&id="+id+"&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&fajl="+fajl,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("nev="+encodeURIComponent(document.getElementById("nev").value)
			+"&sorrend="+encodeURIComponent(document.getElementById("sorrend").value)
			+"&leiras="+encodeURIComponent(document.getElementById("leiras").value)
			);
	};
	// Modal nyitás
	function rakerdez(modal_id) {
		document.getElementById(modal_id).style.display = 'block';
	};
	function megsem(modal_id) {
		document.getElementById(modal_id).style.display = 'none';
	};
	// Galéria törlése
	function torol(id, fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				// Hover message
				$('<span>Galéria törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&command=galeria_torles&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd")+"&id="+id,true);
		xmlhttp.send();
	};
