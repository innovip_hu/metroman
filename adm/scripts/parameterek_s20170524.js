	// Modal nyitás
	function rakerdez_kupon(modal_id, id, nev) {
		document.getElementById("modal_torles_id").value=id;
		document.getElementById("modal_torles_nev").innerHTML=nev;
		document.getElementById(modal_id).style.display = 'block';
	};
	function rakerdez_param_ertek(modal_id, id, nev, ertek_id) {
		document.getElementById("modal_torles_id_ertek").value=id;
		document.getElementById("modal_torles_nev_ertek").innerHTML=nev;
		document.getElementById("modal_torles_id_ertek_ertek").value=ertek_id;
		document.getElementById(modal_id).style.display = 'block';
	};
	function megsem(modal_id) {
		document.getElementById("modal_torles_id").value=0;
		document.getElementById("modal_torles_nev").innerHTML='';
		document.getElementById(modal_id).style.display = 'none';
	};
	// Kupon törlése
	function kuponTorles(modal_id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById(modal_id).style.display = 'none';
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Paraméter törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-parameterek/lista.php?script=ok&torlendo_parameter_id="+document.getElementById("modal_torles_id").value,true);
		xmlhttp.send();
	};
	// Paraméter érték törlése (lenyílók)
	function parameterErtekTorles(modal_id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById(modal_id).style.display = 'none';
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Paraméter érték törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-parameterek/lista.php?script=ok&parameter_id="+document.getElementById("modal_torles_id_ertek").value+"&torlendo_parameter_ertek_id="+document.getElementById("modal_torles_id_ertek_ertek").value,true);
		xmlhttp.send();
	};
	// Paraméter típusai
	function parameterTipusai() {
		if (document.getElementById('tipus').value == 'egyedi') {
			$("#mertekegyseg_div").slideDown(300);
			$("#elso_ertek_div").slideUp(300);
			document.getElementById('mertekegyseg').value = '';
			document.getElementById('elso_ertek').value = '';
		}
		else if (document.getElementById('tipus').value == 'igen_nem') {
			$("#mertekegyseg_div").slideUp(300);
			$("#elso_ertek_div").slideUp(300);
			document.getElementById('mertekegyseg').value = '';
			document.getElementById('elso_ertek').value = '';
		}
		else {
			$("#mertekegyseg_div").slideUp(300);
			$("#elso_ertek_div").slideDown(300);
			document.getElementById('mertekegyseg').value = '';
			document.getElementById('elso_ertek').value = '';
		}
	};
	// Új kupon lenyitása
	function ujKuponFrom() {
		$( "#uj_kupon" ).animate({
			height: [ "toggle", "swing" ]
		}, 300);
		$("#uj_parameter_ertek_div").slideUp(300);
	};
	// Új paraméter érték lenyitása lenyitása
	function ujParameterErtekForm() {
		$( "#uj_parameter_ertek_div" ).animate({
			height: [ "toggle", "swing" ]
		}, 300);
		$("#uj_kupon").slideUp(300);
	};
	// Paraméter típusai
	function ujParameterMentes() {
		$( "#uj_kupon" ).slideUp( 300, function() {
			// Összecsukás után mentés
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
					// Hover message
					$('<span>Új paraméter létrehozva.</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("POST","module-parameterek/lista.php?script=ok",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("command=uj_parameter"
				+"&nev="+encodeURIComponent(document.getElementById("nev").value)
				+"&mertekegyseg="+encodeURIComponent(document.getElementById("mertekegyseg").value)
				+"&tipus="+encodeURIComponent(document.getElementById("tipus").value)
				+"&elso_ertek="+encodeURIComponent(document.getElementById("elso_ertek").value)
				);
		});
	};
	// Új paraméter érték m,entése
	function ujParameterErtekMentes() {
		$( "#uj_parameter_ertek_div" ).slideUp( 300, function() {
			// Összecsukás után mentés
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
					// Hover message
					$('<span>Új érték létrehozva.</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("POST","module-parameterek/lista.php?script=ok",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("command=uj_parameter_ertek"
				+"&nev="+encodeURIComponent(document.getElementById("ertek_nev").value)
				+"&parameter_id="+encodeURIComponent(document.getElementById("ertek_parametere").value)
				);
		});
	};

	