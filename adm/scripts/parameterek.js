// Listában save gomb megjelenés
	function gombokMegjelenitese() {
		$('.sgomb_ok').keyup(function(){
			var id = $(this).attr('attr_termid');
			$('#mod-mentes'+id).css('display', 'block');
		});
		// iCheck
		$('.sgomb_cb').on('ifChecked', function(event){
			var id = $(this).attr('attr_termid');
			$('#mod-mentes'+id).css('display', 'block');
		});
		$('.sgomb_cb').on('ifUnchecked', function(event){
			var id = $(this).attr('attr_termid');
			$('#mod-mentes'+id).css('display', 'block');
		});
	
	};
	$( document ).ready(function() {
		gombokMegjelenitese();
	});
// Paraméter törlées
	function parameterTorles(id, nev) {
		$.confirm({
			icon: 'fa fa-warning',
			title: 'Paraméter törlése',
			theme: 'supervan', // 'material', 'bootstrap', 'light', 'dark'
			content: 'Biztos törölni szeretnéd a(az) "'+nev+'" paramétert?',
			buttons: {
				igen: {
					text: 'Igen',
					action: function(){
						$.post('module-parameterek/lista.php?script=ok',{
								torles_id : id
							},function(response,status){ // Required Callback Function
								$('#munkaablak').html(response);
								$('<span>Adatok elmentve.</span>').hovermessage({
										autoclose : 3000,
										position : 'top-right',
									});
								//iCheck for checkbox and radio inputs
								$('.checkbox').iCheck({
								  checkboxClass: 'icheckbox_flat-blue',
								  radioClass: 'iradio_flat-blue'
								});
								gombokMegjelenitese();
						});
					}
				},
				nem: {
					text: 'Nem',
					action: function(){
						// bezár
					}
				}
			}
		});
	};
// Paraméter mentése
	function mentesParameter(id){
		$.post('module-parameterek/ajax_parameter_mentese.php',{
			nev : $('#nev_'+id).val(),
			szuro : $('#szuro_cbox_'+id).prop( "checked" ),
			szuro_sorrend : $('#szuro_sorrend_'+id).val(),
			valaszthato : $('#valaszthato_cbox_'+id).prop( "checked" ),
			plecsni : $('#plecsni_cbox_'+id).prop( "checked" ),
			mertekegyseg : $('#me_'+id).val(),
			id : id 

		},function(response,status){ // Required Callback Function
			// alert(response);
			$('<span>Paraméter törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			$('#mod-mentes'+id).css('display', 'none');
		});
	}
// Paraméter ikon feltöltés
	function ikonFeltoltes(id) {
		$.confirm({
			icon: 'fa fa-cloud-upload',
			title: 'Ikon feltöltése',
			type: 'blue',
			theme: 'light', // 'material', 'bootstrap', 'light', 'dark'
			closeIcon: true,
			content: '<p>Tallózd be a feltöltendő ikont.</p>'+
					'<form id="ikon_form" action="" method="POST" enctype="multipart/form-data">'+
						'<div class="form-group">'+
							'<input type="file" name="file" >'+
							'<input type="hidden" name="ikon_param_id" value="'+id+'">'+
						'</div>'+
					'</form>',
			buttons: {
				mentes: {
					text: 'Mentés',
					action: function(){
						$('#ikon_form').submit();
					}
				},
			}
		});
	};
// Ikon törlés
	function ikonTorles(id) {
		$.confirm({
			icon: 'fa fa-warning',
			title: 'Ikon törlése',
			theme: 'supervan', // 'material', 'bootstrap', 'light', 'dark'
			content: 'Biztos törölni szeretnéd az ikont?',
			buttons: {
				igen: {
					text: 'Igen',
					action: function(){
						$.post('module-parameterek/lista.php?script=ok',{
								torles_ikon_id : id
							},function(response,status){ // Required Callback Function
								$('#munkaablak').html(response);
								$('<span>Ikon törölve.</span>').hovermessage({
										autoclose : 3000,
										position : 'top-right',
									});
								//iCheck for checkbox and radio inputs
								$('.checkbox').iCheck({
								  checkboxClass: 'icheckbox_flat-blue',
								  radioClass: 'iradio_flat-blue'
								});
								gombokMegjelenitese();
						});
					}
				},
				nem: {
					text: 'Nem',
					action: function(){
						// bezár
					}
				}
			}
		});
	};

	