	// lapozás Tovább gomb
	function lapozasTovabb(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+(Number(document.getElementById("kezd").value)+Number(document.getElementById("oldalszam").value)),true);
		xmlhttp.send();
	};
	// lapozás Vissza gomb
	function lapozasVissza(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+(Number(document.getElementById("kezd").value)-Number(document.getElementById("oldalszam").value)),true);
		xmlhttp.send();
	};
	
 	// Belépés
	function Belepes(belep_fajl,fajl,id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
			}
		  }
		xmlhttp.open("GET",belep_fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&id="+id+"&fajl="+fajl,true);
		xmlhttp.send();
	};

	// Visszalépés a listába
	function vissza(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+encodeURIComponent(document.getElementById("oldalszam").value)+"&sorr_tip="+encodeURIComponent(document.getElementById("sorr_tip").value)+"&sorrend="+encodeURIComponent(document.getElementById("sorrend").value)+"&kezd="+encodeURIComponent(document.getElementById("kezd").value),true);
		xmlhttp.send();
	};
	// Alapadatok mentése
	function mentes(fajl, id) {
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;

				// Hover message
				$('<span>Adatok elmentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});

			}
		  }
		xmlhttp.open("POST","module-hirek2/hir.php?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&id="+id+"&fajl="+fajl,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("command=adatok_mentese&cim="+encodeURIComponent(document.getElementById("cim").value)
				+"&elozetes="+encodeURIComponent(document.getElementById("elozetes").value)
				+"&tartalom="+encodeURIComponent(document.getElementById("tartalom").value)
				+"&ikon="+encodeURIComponent(document.getElementById("ikon").value)
				+"&sorrend="+encodeURIComponent(document.getElementById("sorrend").value)
				);
	};
	// Modal nyitás
	function rakerdez(modal_id) {
		document.getElementById(modal_id).style.display = 'block';
	};
	function megsem(modal_id) {
		document.getElementById(modal_id).style.display = 'none';
	};
	// Hír törlése
	function torol(id, fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&command=hir_torles&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+(Number(document.getElementById("kezd").value)-Number(document.getElementById("oldalszam").value))+"&id="+id,true);
		xmlhttp.send();
	};
	// Új hír
	function ujHir(fajl) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;

			}
		  }
		xmlhttp.open("GET","module-hirek2/hir_uj.php?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&fajl="+fajl,true);
		xmlhttp.send();
	};
	// Képek törlése
	function hirKepTorles(id, kep_id) {
		var leiras = encodeURIComponent(CKEDITOR.instances.editor1.getData());
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("hir_kepek_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Kép törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-hirek2/hir_kepek.php?script=ok&id="+id+"&torlendo_kep_id="+kep_id,true);
		xmlhttp.send();
	};
	// Képek alapértelmezése
	function hirKepAlap(id, kep_id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("hir_kepek_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Alapértelmezés beállítva</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-hirek2/hir_kepek.php?script=ok&id="+id+"&alap_kep_id="+kep_id,true);
		xmlhttp.send();
	};
	// Új hír
	function mentesUjHir(fajl) {		
		// ÚJ termék létrehozása ID miatt
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var id = parseFloat(xmlhttp.responseText); // Az új termék ID-je
				
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
						$('html,body').scrollTop(0);
						// Datepicker
					}
				  }
				xmlhttp.open("POST","module-hirek2/hir.php?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&id="+id+"&fajl="+fajl,true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send("command=uj_hir&cim="+encodeURIComponent(document.getElementById("cim").value)
						+"&elozetes="+encodeURIComponent(document.getElementById("elozetes").value)
						+"&sorrend="+encodeURIComponent(document.getElementById("sorrend").value)
					);
			}
		  }
		xmlhttp.open("GET","module-hirek2/hir_letrehozas.php",true);
		xmlhttp.send();
	};