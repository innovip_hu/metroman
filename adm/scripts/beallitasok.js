	// Szállítás mentése
	function mentesSzallitas() {
		var data = $('#szallitas_form').serializeArray();
		data.push({name: "command", value: 'szallitasi_arak'});

		$.post('module-beallitasok/szallitas.php?script=ok', $.param(data), function(response,status){ 
			$('#szallitas_div').html(response);
			// Hover message
			$('<span>Szállítási adatok módosítva.</span>').hovermessage({
				autoclose : 3000,
				position : 'top-right',
			});
		});
		
	};

	function ujSav(id) {
		var data = $('#szallitas_form').serializeArray();
		data.push({name: "command", value: 'uj_sav_rogzites'});
		data.push({name: "szall_id", value: id});

		$.post('module-beallitasok/szallitas.php?script=ok', $.param(data),function(response,status){ 
			$('#szallitas_div').html(response);
			// Hover message
			$('<span>Szállítási sáv rögzítve.</span>').hovermessage({
				autoclose : 3000,
				position : 'top-right',
			});
		});		
	}


	function mentesSav(id) {
		var data = $('#szallitas_form').serializeArray();
		data.push({name: "command", value: 'sav_mentese'});
		data.push({name: "sav_id", value: id});

		$.post('module-beallitasok/szallitas.php?script=ok', $.param(data),function(response,status){ 
			$('#szallitas_div').html(response);
			// Hover message
			$('<span>Szállítási sáv módosítva.</span>').hovermessage({
				autoclose : 3000,
				position : 'top-right',
			});
		});		
	}	

	function torlesSav(id) {
		var data = $('#szallitas_form').serializeArray();
		data.push({name: "command", value: 'sav_torles'});
		data.push({name: "sav_id", value: id});

		$.post('module-beallitasok/szallitas.php?script=ok', $.param(data),function(response,status){ 
			$('#szallitas_div').html(response);
			// Hover message
			$('<span>Szállítási sáv törölve.</span>').hovermessage({
				autoclose : 3000,
				position : 'top-right',
			});
		});		
	}		

	function mentesKiemelt() {
		$.post('module-beallitasok/beallitas.php?script=ok', {
			command: 'uj_cim',
			kiemelt_szekcio: $('#kiemelt_adat').val()
		} , function(response,status){ 
			$('#kiemelt_div').html(response);
			// Hover message
			$('<span>Név adatok módosítva.</span>').hovermessage({
				autoclose : 3000,
				position : 'top-right',
			});
		});
	};

	function mentesKapcsolat() {
		$.post('module-beallitasok/beallitas-kapcsolat.php?script=ok', {
			command: 'uj_kapcsolat',
			kapcsolat: $('#kapcsolat').val()
		} , function(response,status){ 
			$('#kapcsolat_div').html(response);
			// Hover message
			$('<span>Kapcsolat adatok módosítva.</span>').hovermessage({
				autoclose : 3000,
				position : 'top-right',
			});
		});
	};	

	// Új ÁFA mentése
	function ujAfa() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("afa_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Új ÁFA elmentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-beallitasok/afa.php?script=ok&command=uj_afa&afa="+document.getElementById("afa_uj").value,true);
		xmlhttp.send();
	};
	// ÁFA módosításának aktiválása
	function afaMentesAktiv(id) {
		$("#afa_mentes_gomb_"+id).addClass("aktiv_gomb");
		$("#afa_"+id).css("color","#DD4B39");
	};
	// ÁFA módosítása
	function afaMentes(id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("afa_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>ÁFA módosítva.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-beallitasok/afa.php?script=ok&command=afa_modositas&id="+id+"&afa="+document.getElementById("afa_"+id).value,true);
		xmlhttp.send();
	};
	// ÁFA módosítása
	function afaTorles(id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("afa_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>ÁFA törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-beallitasok/afa.php?script=ok&command=afa_torles&id="+id,true);
		xmlhttp.send();
	};


	function mentesBeszallito() {
		var data = $('#beszallito_form').serializeArray();
		data.push({name: "command", value: 'beszallito_mentes'});

		$.post('module-beallitasok/beszallito.php?script=ok', $.param(data), function(response,status){ 
			$('#beszallito_div').html(response);
			// Hover message
			$('<span>Szállítási határidők módosítva.</span>').hovermessage({
				autoclose : 3000,
				position : 'top-right',
			});
		});
		
	};

	function cssFeltoltes() {
		var formKatalogus = new FormData($('#css_form')[0]);


			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("css_div").innerHTML=xmlhttp.responseText;
					// Hover message
					$('<span>Fájl feltöltve.</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("POST","module-beallitasok/css.php?script=ok",true);
			xmlhttp.send(formKatalogus);

	}	