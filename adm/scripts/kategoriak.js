	// Modal nyitás
	function rakerdez(id, nev, kep) {
		document.getElementById('rakerdez_torles').style.display = 'block';
		document.getElementById('modal_torles_id').value = id;
		document.getElementById('modal_torles_nev').innerHTML = nev;
		document.getElementById('modal_torles_kep').innerHTML = kep;
	}
	function megsem(modal_id) {
		document.getElementById(modal_id).style.display = 'none';
		document.getElementById('modal_torles_id').value = '';
		document.getElementById('modal_torles_nev').innerHTML = '';
		document.getElementById('modal_torles_kep').innerHTML = '';
	}
	// Kategória törlése
	function torol()
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Kategória törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-kategoriak/lista.php?script=ok&torol_id="+document.getElementById("modal_torles_id").value+"&kep="+document.getElementById("modal_torles_kep").value,true);
		xmlhttp.send();
	};
	// Kategória sorrend FEL
	function egySzinttelFeljebb(id, csop_id, sorrend)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Kategória módosítva.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-kategoriak/lista.php?script=ok&sorrend_fel_id="+id+"&csop_id="+csop_id+"&sorrend="+sorrend,true);
		xmlhttp.send();
	};
	// Kategória sorrend LE
	function egySzinttelLejjebb(id, csop_id, sorrend)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Kategória módosítva.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-kategoriak/lista.php?script=ok&sorrend_le_id="+id+"&csop_id="+csop_id+"&sorrend="+sorrend,true);
		xmlhttp.send();
	};
	// Új kategória létrehozása
	function ujKategoria(csop_id, nev)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				// iCheck for checkbox and radio inputs
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
			}
		  }
		xmlhttp.open("GET","module-kategoriak/uj_kategoria.php?script=ok&csop_id="+csop_id+"&nev="+nev,true);
		xmlhttp.send();
	};
	// Új kategória mentése
	function mentesUjKategoria(csop_id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Új kategória létrehozva.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("POST","module-kategoriak/lista.php?script=ok&command=uj_kategoria&csop_id="+csop_id,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("nev="+encodeURIComponent(document.getElementById("nev").value)
			+"&leiras="+encodeURIComponent(document.getElementById("leiras").value)
			+"&seo_title="+encodeURIComponent(document.getElementById("seo_title").value)
			+"&seo_description="+encodeURIComponent(document.getElementById("seo_description").value)
			+"&lathato="+encodeURIComponent(document.getElementById("lathato").checked)
			);
	};
	// Kategória módosítása
	function kategoriaMOD(id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				// iCheck for checkbox and radio inputs
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
				// Dropzone
				$("#kepfeltoltes").dropzone({
					dictDefaultMessage: "Húzd ide a feltöltendő képet, vagy kattints a mezőbe",
					autoProcessQueue: true,
					// acceptedFiles: "image/jpeg",
					url: 'module-kategoriak/upload.php?id='+id,
					// maxFiles: 20, // Number of files at a time
					maxFilesize: 10, //in MB
					maxfilesexceeded: function(file)
					{
					alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
					},
					init: function () {
						this.on("complete", function (file) {
							setTimeout( function() {
								$('.dz-complete').remove();
							}, 2000); // feltöltés után az ikon törlése
							if (window.XMLHttpRequest)
							  {// code for IE7+, Firefox, Chrome, Opera, Safari
							  xmlhttp=new XMLHttpRequest();
							  }
							else
							  {// code for IE6, IE5
							  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
							  }
							xmlhttp.onreadystatechange=function()
							  {
							  if (xmlhttp.readyState==4 && xmlhttp.status==200)
								{
									document.getElementById("kategoria_kepek_div").innerHTML=xmlhttp.responseText;
									document.getElementById("kepfetolto_form").style.display = "none";
								}
							  }
							xmlhttp.open("GET","module-kategoriak/kategoria_kepek.php?script=ok&id="+id,true);
							xmlhttp.send();
						});
					}
				});

				$(function () {
					var config = {
						"extraPlugins" : 'imagebrowser,youtube',
						"imageBrowser_listUrl": "../../../../../mediatar.json",
						htmlEncodeOutput: false,
						entities: false
					};
					CKEDITOR.replace( 'editor1', config );
					CKEDITOR.config.toolbar = [
					   ['Format','FontSize','Maximize','Source', 'Table'],
					   ['Find','Replace','-','SelectAll','-'],
					   ['Bold','Italic','Underline','Strike','-','Outdent','Indent','-','RemoveFormat'],
					   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
					   ['Link','Smiley','TextColor','BGColor','-','Undo','Redo'],
					   ['Image', 'Youtube']
					] ;
				});				
			}
		  }
		xmlhttp.open("GET","module-kategoriak/kategoria_mod.php?script=ok&id="+id,true);
		xmlhttp.send();
	};
	// Kategória módosításának mentése
	function mentesKategoria(id)
	{
		var leiras = encodeURIComponent(CKEDITOR.instances.editor1.getData());
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				// Hover message
				$('<span>Adatok elmentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
				// Dropzone
				$("#kepfeltoltes").dropzone({
					dictDefaultMessage: "Húzd ide a feltöltendő képet, vagy kattints a mezőbe",
					autoProcessQueue: true,
					// acceptedFiles: "image/jpeg",
					url: 'module-kategoriak/upload.php?id='+id,
					// maxFiles: 20, // Number of files at a time
					maxFilesize: 10, //in MB
					maxfilesexceeded: function(file)
					{
					alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
					},
					init: function () {
						this.on("complete", function (file) {
							setTimeout( function() {
								$('.dz-complete').remove();
							}, 2000); // feltöltés után az ikon törlése
							if (window.XMLHttpRequest)
							  {// code for IE7+, Firefox, Chrome, Opera, Safari
							  xmlhttp=new XMLHttpRequest();
							  }
							else
							  {// code for IE6, IE5
							  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
							  }
							xmlhttp.onreadystatechange=function()
							  {
							  if (xmlhttp.readyState==4 && xmlhttp.status==200)
								{
									document.getElementById("kategoria_kepek_div").innerHTML=xmlhttp.responseText;
									document.getElementById("kepfetolto_form").style.display = "none";
								}
							  }
							xmlhttp.open("GET","module-kategoriak/kategoria_kepek.php?script=ok&id="+id,true);
							xmlhttp.send();
						});
					}
				});
				// iCheck for checkbox and radio inputs
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});

				$(function () {
					var config = {
						"extraPlugins" : 'imagebrowser,youtube',
						"imageBrowser_listUrl": "../../../../../mediatar.json",
						htmlEncodeOutput: false,
						entities: false
					};
					CKEDITOR.replace( 'editor1', config );
					CKEDITOR.config.toolbar = [
					   ['Format','FontSize','Maximize','Source', 'Table'],
					   ['Find','Replace','-','SelectAll','-'],
					   ['Bold','Italic','Underline','Strike','-','Outdent','Indent','-','RemoveFormat'],
					   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
					   ['Link','Smiley','TextColor','BGColor','-','Undo','Redo'],
					   ['Image', 'Youtube']
					] ;
				});					
			}
		  }
		xmlhttp.open("POST","module-kategoriak/kategoria_mod.php?script=ok&command=mentes&id="+id,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("nev="+encodeURIComponent(document.getElementById("nev").value)
			+"&nev_url="+encodeURIComponent(document.getElementById("nev_url").value)
			+"&leiras="+encodeURIComponent(document.getElementById("leiras").value)
			+"&masodik_leiras="+leiras
			+"&seo_title="+encodeURIComponent(document.getElementById("seo_title").value)
			+"&seo_description="+encodeURIComponent(document.getElementById("seo_description").value)
			+"&csop_id="+encodeURIComponent(document.getElementById("csop_id").value)
			+"&lathato="+encodeURIComponent(document.getElementById("lathato").checked)
			);
	};
	// Új kategória mentése
	function mentesUjKategoria2()
	{
		document.getElementById('uj_kategoria_form').submit();
	};
	// Képek törlése
	function kategoriaKepTorles(id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("kategoria_kepek_div").innerHTML=xmlhttp.responseText;
				document.getElementById("kepfetolto_form").style.display = "block";
				// Hover message
				$('<span>Kép törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-kategoriak/kategoria_kepek.php?command=kep_torles&script=ok&id="+id,true);
		xmlhttp.send();
	};

	function uj_parameter_hozzaadasa_mentese(id) {
		if($('#parmeter_hozzaadas').val() == 0 && $('#uj_parameter').val() == '')
		{
			$.confirm({
				icon: 'fa fa-warning',
				title: 'Üres mező',
				type: 'red',
				theme: 'light', // 'material', 'bootstrap', 'light', 'dark'
				content: 'Válassz egy paramétert, vagy add meg az újnak a nevét!',
				buttons: {
					ok: {
						action: function(){
							// $('#akcio_merteke').focus()
						}
					}
				}
			});
		}
		else if($('#uj_parameter_ertek').val() == '')
		{
			$.confirm({
				icon: 'fa fa-warning',
				title: 'Üres mező',
				type: 'red',
				theme: 'light', // 'material', 'bootstrap', 'light', 'dark'
				content: 'Add meg a paraméter értékét!',
				buttons: {
					ok: {
						action: function(){
							// $('#akcio_merteke').focus()
						}
					}
				}
			});
		}
		else
		{

			$.post('module-kategoriak/parameterek.php?script2=ok&id='+id,{
					parmeter_hozzaadas : $('#parmeter_hozzaadas').val(),
					uj_parameter : $('#uj_parameter').val(),
					uj_parameter_ertek : $('#uj_parameter_ertek').val(),
					command : 'uj_parameter'
				},function(response,status){ // Required Callback Function
					$('#parameterek_uj_div').html(response);
					$('<span>Új paraméter hozzáadva!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
			});

		}
	};


	function param_ertek_torles2(id, param_id) {
		$.confirm({
			icon: 'fa fa-warning',
			title: 'Paraméter törlése',
			theme: 'supervan', 
			content: 'Biztos törölni szeretnéd a paramétert?',
			buttons: {
				igen: {
					text: 'Igen',
					action: function(){
						$.post('module-kategoriak/parameterek.php?script2=ok&id='+id,{
								param_id : param_id,
								command : 'parameter_ertek_torlese'
							},function(response,status){ 
								$('#parameterek_uj_div').html(response);
								$('<span>Paraméter érték törölve!</span>').hovermessage({
									autoclose : 3000,
									position : 'top-right',
								});
						});
					}
				},
				nem: {
					text: 'Nem',
					action: function(){
						// bezár
					}
				}
			}
		});
	};