function termekExport() {
	$('#export_overlay').css('display', 'block');
	setTimeout(function(){ 
		$.post('module-termek_export_import/termek_export.php',{
				csop_id : $('#csop_id').val()
			},function(response,status){ 
				$('#export_overlay').css('display', 'none');
				$('#letolt_gomb').css('display', 'inline-block');
				$('<span>Az fájl elkészült.</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				// alert(response);
		});
	}, 1000);
};
