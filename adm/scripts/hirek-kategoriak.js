	// Modal nyitás
	function rakerdez(id, nev) {
		document.getElementById('rakerdez_torles').style.display = 'block';
		document.getElementById('modal_torles_id').value = id;
		document.getElementById('modal_torles_nev').innerHTML = nev;
	}
	function megsem(modal_id) {
		document.getElementById(modal_id).style.display = 'none';
		document.getElementById('modal_torles_id').value = '';
		document.getElementById('modal_torles_nev').innerHTML = '';
	}
	// Kategória törlése
	function torol()
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Kategória törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-hirek-kategoriak/lista.php?script=ok&torol_id="+document.getElementById("modal_torles_id").value+"&kep="+document.getElementById("modal_torles_kep").value,true);
		xmlhttp.send();
	};

	// Új kategória létrehozása
	function ujKategoria(csop_id, nev)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				// iCheck for checkbox and radio inputs
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
			}
		  }
		xmlhttp.open("GET","module-hirek-kategoriak/uj_kategoria.php?script=ok&csop_id="+csop_id+"&nev="+nev,true);
		xmlhttp.send();
	};
	// Új kategória mentése
	function mentesUjKategoria(csop_id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Új kategória létrehozva.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("POST","module-hirek-kategoriak/lista.php?script=ok&command=uj_kategoria&csop_id="+csop_id,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("nev="+encodeURIComponent(document.getElementById("nev").value)
			+"&leiras="+encodeURIComponent(document.getElementById("leiras").value)
			+"&seo_title="+encodeURIComponent(document.getElementById("seo_title").value)
			+"&seo_description="+encodeURIComponent(document.getElementById("seo_description").value)
			+"&lathato="+encodeURIComponent(document.getElementById("lathato").checked)
			);
	};
	// Kategória módosítása
	function kategoriaMOD(id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				// iCheck for checkbox and radio inputs
			}
		  }
		xmlhttp.open("GET","module-hirek-kategoriak/kategoria_mod.php?script=ok&id="+id,true);
		xmlhttp.send();
	};
	// Kategória módosításának mentése
	function mentesKategoria(id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				// Hover message
				$('<span>Adatok elmentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("POST","module-hirek-kategoriak/kategoria_mod.php?script=ok&command=mentes&id="+id,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("nev="+encodeURIComponent(document.getElementById("nev").value)
			+"&leiras="+encodeURIComponent(document.getElementById("leiras").value)
			+"&seo_title="+encodeURIComponent(document.getElementById("seo_title").value)
			+"&seo_description="+encodeURIComponent(document.getElementById("seo_description").value)
			);
	};
	// Új kategória mentése
	function mentesUjKategoria2()
	{
		document.getElementById('uj_kategoria_form').submit();
	};