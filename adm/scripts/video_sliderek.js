	// Mentés megjelenítése
	function mentesNezet(id, input) {
		document.getElementById("video_slider_footer_"+id).style.display = 'block';
		document.getElementById(input+id).style.color = '#DD4B39';
	}
	// Mentés
	function video_sliderMentes(id) {
		// Preloader
		$('#video_slider_mentes_gomb_'+id).css('display', 'none');
		$('#video_slider_preloader_'+id).css('display', 'inline-block');
		
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("video_slider_footer_"+id).style.display = 'none';
				document.getElementById('nev_'+id).style.color = '#555';
				document.getElementById('link_'+id).style.color = '#555';
				document.getElementById('szoveg_'+id).style.color = '#555';
				document.getElementById('sorrend_'+id).style.color = '#555';
				if ($("#"+"video_"+id).length > 0) {
					document.getElementById('video_'+id).style.color = '#555';
				}
				// Preloader
				$('#video_slider_mentes_gomb_'+id).css('display', 'inline-block');
				$('#video_slider_preloader_'+id).css('display', 'none');
				// Hover message
				$('<span>Adatok elmentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		}
		
		var formData = new FormData();
		if ($("#"+"video_"+id).length > 0) {
			var file = document.getElementById('video_'+id);
			var _file = file.files[0];
			formData.append("video", _file);
		}
		formData.append("id", id);
		formData.append("nev", (document.getElementById("nev_"+id).value));
		formData.append("link", (document.getElementById("link_"+id).value));
		formData.append("szoveg", (document.getElementById("szoveg_"+id).value));
		formData.append("sorrend", (document.getElementById("sorrend_"+id).value));
		
 		xmlhttp.open("POST","module-video_sliderek/slider_mentes.php",true);
		//xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		//xmlhttp.send("id="+id+"&nev="+encodeURIComponent(document.getElementById("nev_"+id).value)+"&link="+encodeURIComponent(document.getElementById("link_"+id).value)+"&szoveg="+encodeURIComponent(document.getElementById("szoveg_"+id).value)+"&sorrend="+encodeURIComponent(document.getElementById("sorrend_"+id).value));
		xmlhttp.send(formData);
	}
	// Modal nyitás
	function rakerdez_video_slider(modal_id, id, nev) {
		document.getElementById("modal_torles_id").value=id;
		document.getElementById("modal_torles_nev").innerHTML=nev;
		document.getElementById(modal_id).style.display = 'block';
	};
	function megsem_video_slider(modal_id) {
		document.getElementById("modal_torles_id").value=0;
		document.getElementById("modal_torles_nev").innerHTML='';
		document.getElementById(modal_id).style.display = 'none';
	};
	// video slider törlése
	function video_sliderTorles(modal_id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById(modal_id).style.display = 'none';
				document.getElementById("video_sliderek").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Video Slider törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-video_sliderek/sliderek.php?script=ok&torlendo_slider_id="+document.getElementById("modal_torles_id").value,true);
		xmlhttp.send();
	};

	