	// lapozás Tovább gomb
	function lapozasTovabb(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+(Number(document.getElementById("kezd").value)+Number(document.getElementById("oldalszam").value)),true);
		xmlhttp.send();
	};
	// lapozás Vissza gomb
	function lapozasVissza(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+(Number(document.getElementById("kezd").value)-Number(document.getElementById("oldalszam").value)),true);
		xmlhttp.send();
	};
	
 	// Belépés
	function Belepes(belep_fajl,fajl,id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
				// Datepicker
				$('.datepicker').datepicker({
					format: 'yyyy-mm-dd',
					startDate: false,
					isRTL: false,
					autoclose:true,
					todayBtn: true,
					todayHighlight: true,
				});

				$('.muliselectes.csoportok').multipleSelect();
				// CKEDitor
				$(function () {
					var config = {
						"extraPlugins" : 'imagebrowser,youtube',
						"imageBrowser_listUrl": "../../../../../mediatar.json",
						htmlEncodeOutput: false,
						entities: false,
						allowedContent: true
					};
					CKEDITOR.replace( 'editor1', config );
					CKEDITOR.config.toolbar = [
					   ['Format','FontSize','Maximize','Source', 'Table'],
					   ['Bold','Italic','Underline','Strike','-','Outdent','Indent','-','RemoveFormat'],
					   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
					   ['Link','Smiley','TextColor','BGColor','-','Undo','Redo'],
					   ['Image', 'Youtube']
					] ;
				});
				// Dropzone
				$("#kepfeltoltes").dropzone({
					dictDefaultMessage: "Húzd ide a feltöltendő képeket, vagy kattints a mezőbe",
					autoProcessQueue: true,
					// acceptedFiles: "image/jpeg",
					url: 'module-hirek/upload.php?id='+id,
					// maxFiles: 20, // Number of files at a time
					maxFilesize: 10, //in MB
					maxfilesexceeded: function(file)
					{
					alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
					},
					init: function () {
						this.on("complete", function (file) {
							setTimeout( function() {
								$('.dz-complete').remove();
							}, 2000); // feltöltés után az ikon törlése
							if (window.XMLHttpRequest)
							  {// code for IE7+, Firefox, Chrome, Opera, Safari
							  xmlhttp=new XMLHttpRequest();
							  }
							else
							  {// code for IE6, IE5
							  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
							  }
							xmlhttp.onreadystatechange=function()
							  {
							  if (xmlhttp.readyState==4 && xmlhttp.status==200)
								{
									document.getElementById("hir_kepek_div").innerHTML=xmlhttp.responseText;
								}
							  }
							xmlhttp.open("GET","module-hirek/hir_kepek.php?script=ok&id="+id,true);
							xmlhttp.send();
						});
					}
				});
			}
		  }
		xmlhttp.open("GET",belep_fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&id="+id+"&fajl="+fajl,true);
		xmlhttp.send();
	};

	// Visszalépés a listába
	function vissza(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+encodeURIComponent(document.getElementById("oldalszam").value)+"&sorr_tip="+encodeURIComponent(document.getElementById("sorr_tip").value)+"&sorrend="+encodeURIComponent(document.getElementById("sorrend").value)+"&kezd="+encodeURIComponent(document.getElementById("kezd").value),true);
		xmlhttp.send();
	};
	// Alapadatok mentése
	function mentes(fajl, id) {
		var leiras = encodeURIComponent(CKEDITOR.instances.editor1.getData());
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// Datepicker
				$('.datepicker').datepicker({
					format: 'yyyy-mm-dd',
					startDate: false,
					isRTL: false,
					autoclose:true,
					todayBtn: true,
					todayHighlight: true,
				});

				$('.muliselectes.csoportok').multipleSelect();
				
				// CKEDitor
				$(function () {
					var config = {
						"extraPlugins" : 'imagebrowser,youtube',
						"imageBrowser_listUrl": "../../../../../mediatar.json",
						htmlEncodeOutput: false,
						entities: false,
						allowedContent: true
					};
					CKEDITOR.replace( 'editor1', config );
					CKEDITOR.config.toolbar = [
					   ['Format','FontSize','Maximize','Source', 'Table'],
					   ['Bold','Italic','Underline','Strike','-','Outdent','Indent','-','RemoveFormat'],
					   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
					   ['Link','Smiley','TextColor','BGColor','-','Undo','Redo'],
					   ['Image', 'Youtube']
					] ;
				});
				// Hover message
				$('<span>Adatok elmentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
				// Dropzone
				$("#kepfeltoltes").dropzone({
					dictDefaultMessage: "Húzd ide a feltöltendő képeket, vagy kattints a mezőbe",
					autoProcessQueue: true,
					// acceptedFiles: "image/jpeg",
					url: 'module-hirek/upload.php?id='+id,
					// maxFiles: 20, // Number of files at a time
					maxFilesize: 10, //in MB
					maxfilesexceeded: function(file)
					{
					alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
					},
					init: function () {
						this.on("complete", function (file) {
							setTimeout( function() {
								$('.dz-complete').remove();
							}, 2000); // feltöltés után az ikon törlése
							if (window.XMLHttpRequest)
							  {// code for IE7+, Firefox, Chrome, Opera, Safari
							  xmlhttp=new XMLHttpRequest();
							  }
							else
							  {// code for IE6, IE5
							  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
							  }
							xmlhttp.onreadystatechange=function()
							  {
							  if (xmlhttp.readyState==4 && xmlhttp.status==200)
								{
									document.getElementById("hir_kepek_div").innerHTML=xmlhttp.responseText;
								}
							  }
							xmlhttp.open("GET","module-hirek/hir_kepek.php?script=ok&id="+id,true);
							xmlhttp.send();
						});
					}
				});
			}
		  }
		xmlhttp.open("POST","module-hirek/hir.php?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&id="+id+"&fajl="+fajl,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("command=adatok_mentese&cim="+encodeURIComponent(document.getElementById("cim").value)
				+"&datum="+encodeURIComponent(document.getElementById("datum").value)
				+"&elozetes="+encodeURIComponent(document.getElementById("elozetes").value)
				+"&hir_termekek="+encodeURIComponent(getSelectValues(document.getElementById("hir_termekek")))
				+"&tartalom="+leiras
				);
	};
	
	function getSelectValues(select) {
		var result = [];
		var options = select && select.options;
		var opt;
		for (var i=0, iLen=options.length; i<iLen; i++) {
			opt = options[i];
			if (opt.selected) {
				result.push(opt.value || opt.text);
			}
		}
		return result;
	}

	// Modal nyitás
	function rakerdez(modal_id) {
		document.getElementById(modal_id).style.display = 'block';
	};
	function megsem(modal_id) {
		document.getElementById(modal_id).style.display = 'none';
	};
	// Hír törlése
	function torol(id, fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&command=hir_torles&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+(Number(document.getElementById("kezd").value)-Number(document.getElementById("oldalszam").value))+"&id="+id,true);
		xmlhttp.send();
	};
	// Új hír
	function ujHir(fajl) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// Datepicker
				$('.datepicker').datepicker({
					format: 'yyyy-mm-dd',
					startDate: false,
					isRTL: false,
					autoclose:true,
					todayBtn: true,
					todayHighlight: true,
				});
				// CKEDitor
				$(function () {
					var config = {
						"extraPlugins" : 'imagebrowser,youtube',
						"imageBrowser_listUrl": "../../../../../mediatar.json",
						htmlEncodeOutput: false,
						entities: false,
						allowedContent: true
					};
					CKEDITOR.replace( 'editor1', config );
					CKEDITOR.config.toolbar = [
					   ['Format','FontSize','Maximize','Source', 'Table'],
					   ['Bold','Italic','Underline','Strike','-','Outdent','Indent','-','RemoveFormat'],
					   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
					   ['Link','Smiley','TextColor','BGColor','-','Undo','Redo'],
					   ['Image', 'Youtube']
					] ;
				});
			}
		  }
		xmlhttp.open("GET","module-hirek/hir_uj.php?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&fajl="+fajl,true);
		xmlhttp.send();
	};
	// Képek törlése
	function hirKepTorles(id, kep_id) {
		var leiras = encodeURIComponent(CKEDITOR.instances.editor1.getData());
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("hir_kepek_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Kép törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-hirek/hir_kepek.php?script=ok&id="+id+"&torlendo_kep_id="+kep_id,true);
		xmlhttp.send();
	};
	// Képek alapértelmezése
	function hirKepAlap(id, kep_id) {
		var leiras = encodeURIComponent(CKEDITOR.instances.editor1.getData());
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("hir_kepek_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Alapértelmezés beállítva</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-hirek/hir_kepek.php?script=ok&id="+id+"&alap_kep_id="+kep_id,true);
		xmlhttp.send();
	};
	// Új hír
	function mentesUjHir(fajl) {
		var tartalom = encodeURIComponent(CKEDITOR.instances.editor1.getData());
		
		// ÚJ termék létrehozása ID miatt
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var id = parseFloat(xmlhttp.responseText); // Az új termék ID-je
				
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
						$('html,body').scrollTop(0);
						// Datepicker
						$('.datepicker').datepicker({
							format: 'yyyy-mm-dd',
							startDate: false,
							isRTL: false,
							autoclose:true,
							todayBtn: true,
							todayHighlight: true,
						});
						// CKEDitor
						$(function () {
							var config = {
								"extraPlugins" : 'imagebrowser,youtube',
								"imageBrowser_listUrl": "../../../../../mediatar.json",
								htmlEncodeOutput: false,
								entities: false,
								allowedContent: true
							};
							CKEDITOR.replace( 'editor1', config );
							CKEDITOR.config.toolbar = [
							   ['Format','FontSize','Maximize','Source', 'Table'],
							   ['Bold','Italic','Underline','Strike','-','Outdent','Indent','-','RemoveFormat'],
							   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
							   ['Link','Smiley','TextColor','BGColor','-','Undo','Redo'],
							   ['Image', 'Youtube']
							] ;
						});
						// Dropzone
						$("#kepfeltoltes").dropzone({
							dictDefaultMessage: "Húzd ide a feltöltendő képeket, vagy kattints a mezőbe",
							autoProcessQueue: true,
							// acceptedFiles: "image/jpeg",
							url: 'module-hirek/upload.php?id='+id,
							// maxFiles: 20, // Number of files at a time
							maxFilesize: 10, //in MB
							maxfilesexceeded: function(file)
							{
							alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
							},
							init: function () {
								this.on("complete", function (file) {
									setTimeout( function() {
										$('.dz-complete').remove();
									}, 2000); // feltöltés után az ikon törlése
									if (window.XMLHttpRequest)
									  {// code for IE7+, Firefox, Chrome, Opera, Safari
									  xmlhttp=new XMLHttpRequest();
									  }
									else
									  {// code for IE6, IE5
									  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
									  }
									xmlhttp.onreadystatechange=function()
									  {
									  if (xmlhttp.readyState==4 && xmlhttp.status==200)
										{
											document.getElementById("hir_kepek_div").innerHTML=xmlhttp.responseText;
										}
									  }
									xmlhttp.open("GET","module-hirek/hir_kepek.php?script=ok&id="+id,true);
									xmlhttp.send();
								});
							}
						});
					}
				  }
				xmlhttp.open("POST","module-hirek/hir.php?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&id="+id+"&fajl="+fajl,true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send("command=uj_hir&cim="+encodeURIComponent(document.getElementById("cim").value)
						+"&datum="+encodeURIComponent(document.getElementById("datum").value)
						+"&elozetes="+encodeURIComponent(document.getElementById("elozetes").value)
						+"&tartalom="+tartalom
					);
			}
		  }
		xmlhttp.open("GET","module-hirek/hir_letrehozas.php",true);
		xmlhttp.send();
	};


	function vagolapramasol(value) {
	    var tempInput = document.createElement("input");
	    tempInput.style = "position: absolute; left: -1000px; top: -1000px";
	    tempInput.value = value;
	    document.body.appendChild(tempInput);
	    tempInput.select();
	    document.execCommand("copy");
	    document.body.removeChild(tempInput);

		$('<span>A link a vágólapra másolva!</span>').hovermessage({
			autoclose : 3000,
			position : 'top-right',
		});	    
	}