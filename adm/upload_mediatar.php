<?php
	header("Access-Control-Allow-Origin: *"); 
	header('Content-type: application/json');
	
	include '../config.php';
	include $gyoker.'/adm/config_adm.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	// require_once('simpleImage_class.php');
	$ds = DIRECTORY_SEPARATOR;
	$dir = $gyoker.'/images/mediatar/';
	
	// Kép másolása mappába
	if (!empty($_FILES)) {
		$tempFile = $_FILES['file']['tmp_name'];                    
		$targetPath = $dir . $ds;
		$file = $_FILES['file']['name'];
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		$fn = 'img-'.time().rand(0,999999999999).'.'.$ext; // ez a jó
		$targetFile =  $targetPath. $fn; 
		move_uploaded_file($tempFile,$targetFile);
		
		// Bélyegkép készítése 2
		/* require_once($gyoker.'/adm/simpleImage_class2.php');
		$image = new SimpleImage();
		$image->load($targetPath. $fn);
		if($conf_galeria_csak_szelesseg == 1)
		{
			$image->resizeToWidth($conf_galeria_thumb_max_szeklesseg); // csak szélesség szerint
		}
		else
		{
			$data = getimagesize($targetPath. $fn);
			$imgW = $data[0];
			$imgH = $data[1];
			$max_width = $conf_galeria_thumb_max_szeklesseg; // set a max width
			$max_height = $conf_galeria_thumb_max_magassag; // set a max height
			if($imgW > $imgH){ // width is greater
			  if($imgW > $max_width) $image->resizeToWidth($max_width);
			}
			else { // height is greater
			  if($imgH > $max_height) $image->resizeToHeight($max_height);
			}
		}
		$thumb = 'thumb-'.$fn;
		$image->save($targetPath.$thumb);		 */
	}
?>  