<?php
	// Hírek
	$conf_hirek = 1;
	$conf_hirek_kat = 0;
	// Hírek2
	$conf_hirek2 = 1;

	//Hírek 2 modul neve
	$conf_hirek2_nev = "Főoldali boxok";

	// Hírlevél
	$conf_hirlevel = 1;
	// Banner
	$conf_banner = 0;
	$conf_banner_meret = ' (Méret: 300 x 250 pixel)';
	// Slider
	$conf_slider = 1;
	$conf_slider_meret = ' (A kép méretaránya fekvő legyen, minimum 1300px, maximum 1920px széles. A fájl mérete ne legyen nagyobb 500KB-nál)';
	// Video Slider
	$conf_video_slider = 0;
	$conf_video_slider_kikapcsolt_mezok = array(/*'nev', 'link', 'szoveg', */'sorrend');
	// Galéria
	$conf_galeria = 0;
	// YouTube galéria
	$conf_yt_galeria = 0;
	// Pop-up
	$conf_popup = 0;
	
	// Médiatár
	$conf_mediatar = 1;
	
	// Webshop
	$conf_webshop = 1;
	// Rendelések
	$conf_rendelesek = 1;
	// Excel import/export
	$conf_excel_imp_exp = 0;
	// Kívánságlista
	$conf_kivansaglista = 1;
	// Árak megosztása
	$conf_arak_megosztasa = 1;
	// Kiemelt termék
	$conf_kiemelt_term = 1;
	// Kupon
	$conf_kupon = 1;
	// Több kategóriához való rendelés
	$conf_tobb_kategoria = 1;
	// Készlet
	$conf_keszlet = 0;
	// Paraméterek
	$conf_parameterek = 0;
	// Szállításkor email
	$conf_szall_email = 1;
	/* // Szállítás
		// Egyedi szállítás
		$conf_szall_egyedi = 1;
		// PostaPont
		$conf_szall_postapont = 1;
		// GLS CsomagPont
		$conf_szall_gls_cspont = 1; */
		
	// Kép méretezése
	$conf_kepmeretezes = 1000; // sima kép max szélessége		
	$conf_csak_szelesseg = 1; // Ha 0, akkor a magasságot is nézi
	$conf_thumb_max_szeklesseg = 340;
	$conf_thumb_max_magassag = 255;
		
	// Galéria méretezése
	$conf_galeria_csak_szelesseg = 0; // Ha 0, akkor a magasságot is nézi
	$conf_galeria_thumb_max_szeklesseg = 340;
	$conf_galeria_thumb_max_magassag = 340;
	
		
	// Mező nevek  a termék export/import-hoz
	$conf_mezok = array('id', 'csop_id', 'nev', 'leiras', 'rovid_leiras', 'ar', 'akciosar', 'akcio_tol', 'akcio_ig', 'lathato', 'kiemelt');

?>  