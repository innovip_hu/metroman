<?php
	session_start();
	ob_start();
	
	include '../config.php';
	
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	$oldal = 'hirlevel';
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Hírlevél | Admin</title>
		<?php
			include 'module/head.php';
		?>
		<script src="scripts/hirlevel.js"></script>
	</head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
		<?php
			include 'module/header.php';
			include 'module/menu.php';
		?>
		<!--Modal-->
		<div class="example-modal">
			<div id="rakerdez_torles" class="modal modal-danger">
			  <div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Törlés</h4>
					</div>
					<div class="modal-body">
						<p>Biztos ki szeretnéd üríteni a tárolót?</p>
					</div>
					<div class="modal-footer">
						<button onClick="hirlevelTorles('rakerdez_torles')" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Igen</button>
						<button onClick="megsem_hirl('rakerdez_torles')" type="button" class="btn btn-outline">Mégsem</button>
					</div>
				</div>
			  </div>
			</div>
		</div>
		
		<div id="munkaablak">
			<div class="content-wrapper bg_admin">
				<section class="content-header">
				  <h1 id="myModal">Hírlevél</h1>
				  <ol class="breadcrumb">
					<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
					<li class="active">Hírlevél</li>
				  </ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Felíratkozottak lekérdezése</h3>
									<div class="box-tools pull-right">
										<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="box-body" id="szallitas_div">
									<a href="xml_generator_hirlevel_uj.php" class="btn btn-app"><i class="fa fa-cloud-download"></i> Újak</a>
									<a href="xml_generator_hirlevel_ossz.php" class="btn btn-app"><i class="fa fa-cloud-download"></i> Összes</a>
									<a onclick="rakerdez_hirl('rakerdez_torles')" class="btn btn-app"><i class="fa fa-times"></i> Ürítés</a>
								</div>
								<div class="box-footer">
									FIGYELEM! Csak azok felhasználhatók, ahol a regisztráció dátuma nem "0000-00-00"!
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<?php
			include 'module/footer.php';
		?>
    </div>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- Datepicker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <!-- CK Editor -->
    <script src="plugins/ckeditor/ckeditor.js"></script>
    <!-- Dropzone -->
	<script src="scripts/dropzone.js"></script>
    <!-- Page Script -->
    <script>
      $(function () {

        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function () {
          var clicks = $(this).data('clicks');
          if (clicks) {
            //Uncheck all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
          } else {
            //Check all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
          }
          $(this).data("clicks", !clicks);
        });

        //Handle starring for glyphicon and font awesome
        $(".mailbox-star").click(function (e) {
          e.preventDefault();
          //detect type
          var $this = $(this).find("a > i");
          var glyph = $this.hasClass("glyphicon");
          var fa = $this.hasClass("fa");

          //Switch states
          if (glyph) {
            $this.toggleClass("glyphicon-star");
            $this.toggleClass("glyphicon-star-empty");
          }

          if (fa) {
            $this.toggleClass("fa-star");
            $this.toggleClass("fa-star-o");
          }
        });
      });
    </script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
	<?php
		include 'module/body_end.php';
	?>
  </body>
</html>
