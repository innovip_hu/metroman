<?php
	include '../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	$csv_mentes = array();

	$csv_mentes[] = array('Sorszám','Vásárlás/regisztráció ideje','Vásárló neve','E-mail cím','Telefonszám','Számlázási név','Város','Utca','Házszám','Irányítószám','Adószám','Szállítási név','Város','Utca','Házszám','Irányítószám');

	$query = "SELECT * FROM ".$webjel."users";
	foreach ($pdo->query($query) as $row)
	{
		$csv_mentes[] = array($row['id'],$row['reg_datum'],$row['vezeteknev'],$row['email'],$row['telefon'],$row['szla_nev'],$row['cim_varos'],$row['cim_utca'],$row['cim_hszam'],$row['cim_irszam'],$row['adoszam'],$row['cim_szall_nev'],$row['cim_szall_varos'],$row['cim_szall_utca'], $row['cim_szall_hszam'],$row['cim_szall_irszam']);	
	}

	$query = "SELECT * FROM ".$webjel."users_noreg
		INNER JOIN ".$webjel."rendeles
		ON ".$webjel."users_noreg.id = ".$webjel."rendeles.user_id
		WHERE ".$webjel."rendeles.noreg = 1";
	foreach ($pdo->query($query) as $row)
	{
		$csv_mentes[] = array('nr',$row['reg_datum'],$row['vezeteknev'],$row['email'],$row['telefon'],$row['szla_nev'],$row['cim_varos'],$row['cim_utca'],$row['cim_hszam'],$row['cim_irszam'],$row['adoszam'],$row['cim_szall_nev'],$row['cim_szall_varos'],$row['cim_szall_utca'], $row['cim_szall_hszam'],$row['cim_szall_irszam']);	
	}


function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="'.$filename.'";');

    // open the "output" stream
    // see http://www.php.net/manual/en/wrappers.php.php#refsect2-wrappers.php-unknown-unknown-unknown-descriptioq
    $f = fopen('php://output', 'w');

    foreach ($array as $line) {
        fputcsv($f, $line, $delimiter);
    }
}   

array_to_csv_download(
	$csv_mentes,
  "gdpr_export.csv"
);


?>  