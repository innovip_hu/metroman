	<script src="scripts/termekek.js"></script>
<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	// Termék törlése
	if(isset($_GET['command']) && $_GET['command'] == 'termek_torles')
	{
		// Képek törlése
		$query = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id =".$_GET['id'];
		foreach ($pdo->query($query) as $row)
		{
			$dir = $gyoker."/images/termekek/";
			unlink($dir.$row['kep']);
		}
		$deletecommand = "DELETE FROM ".$webjel."termek_kepek WHERE termek_id =".$_GET['id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();
		// Termék törlés
		$pdo->exec("DELETE FROM ".$webjel."termekek WHERE id =".$_GET['id']);
	}
	// Tömeges akció
	if(isset($_POST['command']) && $_POST['command'] == 'uj_tomeges_akcio')
	{
		if($_POST['akcio_merteke'] >= 0 && $_POST['akcio_merteke'] < 100)
		{
			$updatecommand = "UPDATE ".$webjel."termekek SET akciosar=ROUND(ar -(ar * ".$_POST['akcio_merteke']." / 100)), akcio_tol='".$_POST['akcio_tol']."', akcio_ig='".$_POST['akcio_ig']."' WHERE csop_id=".$_GET['csop_id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();
		}
	}
	
	// Tömeges akció megszűntetése
	if(isset($_POST['command']) && $_POST['command'] == 'tomeges_akcio_megszuntetese')
	{
		$updatecommand = "UPDATE ".$webjel."termekek SET akciosar=0, akcio_tol='0000-00-00', akcio_ig='0000-00-00' WHERE csop_id=".$_GET['csop_id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
	}
	
// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 50; //ALAPÁLLAPOT
	}
// Rekordok száma
	if($_GET['csop_id'] == '-1') // Akciós termékek
	{
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE ".$webjel."termekek.akciosar > 0 AND  ".$webjel."termekek.akcio_ig >= NOW() AND ".$webjel."termekek.akcio_tol <= NOW()");
	}
	else if($_GET['csop_id'] == '-2') // Kiemelt termékek
	{
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE ".$webjel."termekek.kiemelt = 1");
	}
	else
	{
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE csop_id=".$_GET['csop_id']." OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=".$webjel."termekek.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id=".$_GET['csop_id'].")");
	}
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
	if($kezd < 0) { $kezd = 0; }
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'id'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'DESC'; // Alap rendezési feltétel
	}
	
	$query = "SELECT * FROM ".$webjel."term_csoportok WHERE id=".$_GET['csop_id'];
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	$kategoria_nev = $row['nev'];
	
	// Termék darabszámok meghatározása
	if($_GET['csop_id'] == '-1') // Akciós termékek
	{
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE ".$webjel."termekek.akciosar > 0 AND  ".$webjel."termekek.akcio_ig >= NOW() AND ".$webjel."termekek.akcio_tol <= NOW()");
		$kategoria_nev = 'Akciós termékek';
	}
	else if($_GET['csop_id'] == '-2') // Kiemelt termékek
	{
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE ".$webjel."termekek.kiemelt = 1");
		$kategoria_nev = 'Kiemelt termékek';
	}
	else
	{
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE csop_id=".$_GET['csop_id']." OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=".$webjel."termekek.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id=".$_GET['csop_id'].")");
	}
	$res->execute();
	$rownum = $res->fetchColumn();
	// Össz
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek");
	$res->execute();
	$rownum_ossz = $res->fetchColumn();
?>
<div class="content-wrapper">
	<section class="content-header">
	  <h1 id="myModal">Termékek<small><?php print $kategoria_nev.' ('.$rownum_ossz.'/'.$rownum.' termék)'; ?></small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Termékek</li>
	  </ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body no-padding with-border">
						<div class="mailbox-controls">
							<?php
							if($_GET['csop_id'] > 0)
							{
								// Nézet
								if(isset($_COOKIE['admin_termek_lista_nezet']) && $_COOKIE['admin_termek_lista_nezet'] == 'kepes')
								{
									?><a class="menu_gomb" onClick="nezetValtas('module-termekek/lista.php', <?php print $_GET['csop_id']; ?>, 'alap')" title="Nézet"><span class="fa fa-navicon menu_gomb"></span> Nézet</a><?php
								}
								else
								{
									?><a class="menu_gomb" onClick="nezetValtas('module-termekek/lista.php', <?php print $_GET['csop_id']; ?>, 'kepes')" title="Nézet"><span class="fa fa-th-large menu_gomb"></span> Nézet</a><?php
								}
								
								?>
								<a onClick="BelepesUjTermekbe('module-termekek/uj_termek.php', 'module-termekek/lista.php', <?php print $_GET['csop_id']; ?>)"><span class="menu_gomb"><i class="fa fa-plus"></i> Új termék</span></a>
								<a onClick="BelepesTomegesAkcio('module-termekek/tomeges_akcio_termek.php', 'module-termekek/lista.php', <?php print $_GET['csop_id']; ?>)"><span class="menu_gomb"><i class="fa fa-star"></i> Tömeges akció</span></a>
								<?php
							}
							?>
							<div class="pull-right">
								<?php
									if($oldalszam > $rownum) $oldalszam_mod = $rownum;
									else $oldalszam_mod = $oldalszam;
									$vege = $kezd+$oldalszam_mod;
									if($vege > $rownum) $vege = $rownum;
									if($rownum > 0)
									{
										print ($kezd+1).'-'.$vege.'/'.$rownum;
									}
								?>
								<div class="btn-group">
								<?php
									if ($kezd != 0) // Lapozás vissza (balra)
									{
										?>
										<button onclick="lapozasVisszaTermekek('module-termekek/lista.php', <?php print $_GET['csop_id']; ?>)" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
										<?php
									}
									if (($utolso_oldal / $aktualis_oldal) != 1 && $rownum>0) // Lapozás előre (jobbra)
									{
										?>
										<button onclick="lapozasTovabbTermekek('module-termekek/lista.php', <?php print $_GET['csop_id']; ?>)" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
										<?php
									}
								?>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table table-hover table-striped margbot0">
								<tbody>
									<tr>
										<?php
											if(isset($_COOKIE['admin_termek_lista_nezet']) && $_COOKIE['admin_termek_lista_nezet'] == 'kepes')
											{
												print '<th></th>';
												print '<th>ID</th>';
												print '<th>ÁFA</th>';
												if($config_keszlet_kezeles == 'I')
												{
													print '<th style="text-align:right;">Készlet</th>';
												}
												print '<th style="text-align:right;">Akció</th>
												<th style="text-align:right;">Ár</th>';
											}
											else
											{
												print '<th>ID</th>
												<th>Megnevezés</th>
												<th>ÁFA</th>';
												if($config_keszlet_kezeles == 'I')
												{
													print '<th style="text-align:right;">Készlet</th>';
												}
												print '
												<th style="text-align:right;">Akció Kezdete</th>
												<th style="text-align:right;">Akció Vége</th>
												<th style="text-align:right;">Akció</th>
												<th style="text-align:right;">Ár</th>
												<th style="text-align:right;"></th>';
											}
										?>
									</tr>
									<?php
									if($_GET['csop_id'] == '-1') // Akciós termékek
									{
										$query = "SELECT *, ".$webjel."termekek.id as id, ".$webjel."afa.afa as afa
										FROM ".$webjel."termekek 
										INNER JOIN ".$webjel."afa 
										ON ".$webjel."termekek.afa = ".$webjel."afa.id 
										WHERE ".$webjel."termekek.akciosar > 0 AND  ".$webjel."termekek.akcio_ig >= NOW() AND ".$webjel."termekek.akcio_tol <= NOW() 
										ORDER BY ".$webjel."termekek.nev ASC LIMIT ".$kezd.",".$oldalszam;
									}
									else if($_GET['csop_id'] == '-2') // Kiemelt termékek
									{
										$query = "SELECT *, ".$webjel."termekek.id as id, ".$webjel."afa.afa as afa
										FROM ".$webjel."termekek 
										INNER JOIN ".$webjel."afa 
										ON ".$webjel."termekek.afa = ".$webjel."afa.id 
										WHERE ".$webjel."termekek.kiemelt = 1  
										ORDER BY ".$webjel."termekek.nev ASC LIMIT ".$kezd.",".$oldalszam;
									}
									else
									{
										$query = "SELECT *, ".$webjel."termekek.id as id, ".$webjel."afa.afa as afa
										FROM ".$webjel."termekek 
										INNER JOIN ".$webjel."afa 
										ON ".$webjel."termekek.afa = ".$webjel."afa.id 
										WHERE csop_id=".$_GET['csop_id']." OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=".$webjel."termekek.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id=".$_GET['csop_id'].") 
										ORDER BY ".$webjel."termekek.nev ASC LIMIT ".$kezd.",".$oldalszam;
									}
									foreach ($pdo->query($query) as $row)
									{
										if($row['lathato'] == 0) { $nem_lathato_sor = 'nem_lathato_sor';} else { $nem_lathato_sor = ''; }
										if(isset($_COOKIE['admin_termek_lista_nezet']) && $_COOKIE['admin_termek_lista_nezet'] == 'kepes')
										{
											?>
											<tr id="horgony_<?php print $row['id']; ?>" class="kattintos_sor <?php print $nem_lathato_sor; ?>" >
											<?php
												print '<td rowspan="2" style="width:140px; text-align: center;" >';
													$row_kep = '';
													$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." ORDER BY alap DESC, id ASC LIMIT 1";
													$res = $pdo->prepare($query_kep);
													$res->execute();
													$row_kep = $res -> fetch();
													if($row_kep['kep'] != '')
													{
														print '<img style="max-height: 100px; max-width: 140px;" src="'.$domain.'/images/termekek/'.$row_kep['kep'].'" />';
													}
												print '</td>';
												if($config_keszlet_kezeles == 'I')
												{
													$colspan = 5;
												}
												else
												{
													$colspan = 4;
												}
												print '<th colspan="'.$colspan.'">'.$row['nev'].'</th>';
												print '</tr>';
											?>
											<tr class="kattintos_sor <?php print $nem_lathato_sor; ?>">
											<?php
												if($row['akciosar'] > 0)
												{
													if($row['akcio_ig'] >= date("Y-m-d") && $row['akcio_tol'] <= date("Y-m-d"))
													{
														$akciosar = $row['akciosar'];
													}
													else
													{
														$akciosar = 0;
													}
												}
												else { $akciosar = $row['akciosar']; }
													print '<td>'.$row['id'].'</td>
													<td>'.$row['afa'].' %</td>';
														if($config_keszlet_kezeles == 'I')
														{
															print '<td style="text-align:right;">'.$row['raktaron'].' db</td>';
														}
													print '<td style="text-align:right;">'.number_format($akciosar, 0, ',', ' ').' Ft</td>
													<td style="text-align:right;">'.number_format($row['ar'], 0, ',', ' ').' Ft</td>
												</tr>';
											print '</tr>';
										}
										else
										{
											?>
											<tr class="kattintos_sor <?php print $nem_lathato_sor; ?>">
											<?php
											if($row['akciosar'] > 0)
											{
												if($row['akcio_ig'] >= date("Y-m-d") && $row['akcio_tol'] <= date("Y-m-d"))
												{
													$akciosar = $row['akciosar'];
												}
												else
												{
													$akciosar = 0;
												}
											}
											else { $akciosar = $row['akciosar']; }
												print '<td class="lista-id">'.$row['id'].'</td>
												<td'?> 
												onClick="BelepesTermekbe('module-termekek/termek.php', 'module-termekek/lista.php', <?php print $row['id']; ?>, <?php print $_GET['csop_id']; ?>)" 
												<?php print '>'.$row['nev'].'</td>';

												print '<td>'.$row['afa'].' %</td>';
												if($config_keszlet_kezeles == 'I')
												{
													print '<td style="text-align:right;">'.$row['raktaron'].' db</td>';
												}
												print '
												<td><input type="text" class="form-control datepicker" '?>  id="mod-akcio-tol<?= $row['id']?>" <?php print' placeholder="Akció kezdete" value=" '. $row['akcio_tol'].'" ></td>
												<td><input type="text" class="form-control datepicker"'?>  id="mod-akcio-ig<?= $row['id']?>" <?php print' placeholder="Akció vége" value="'.$row['akcio_ig'].'" ></td>
												<td  style="text-align:right;"><input class="form-control" '?> id="mod-akcios-ar<?= $row['id'] ?>" <?php print'  value ='.$akciosar.' ></td>
												
												<td style="text-align:right;"><input class="form-control" '?> id ="mod-ar<?= $row['id'] ?>" <?php print 'value ='.$row['ar'].' ></td>
												<td><button type="submit" 
												onclick = mentesListaAr("module-termekek/lista-mod.php","'.$row['id'].'","'.$_GET['csop_id'].'") 

												class="btn btn-primary " '?> id="mod-mentes<?= $row['id'] ?>"><?php print 'Mentés</button></td>
											</tr>';
										}
									}
									?>

									
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer no-padding">
						<div class="mailbox-controls">
							<div class="pull-right">
								<?php
									if($rownum > 0)
									{
										print ($kezd+1).'-'.$vege.'/'.$rownum;
									}
								?>
								<div class="btn-group">
								<?php
									if ($kezd != 0) // Lapozás vissza (balra)
									{
										?>
										<button onclick="lapozasVisszaTermekek('module-termekek/lista.php', <?php print $_GET['csop_id']; ?>)" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
										<?php
									}
									if (($utolso_oldal / $aktualis_oldal) != 1 && $rownum>0) // Lapozás előre (jobbra)
									{
										?>
										<button onclick="lapozasTovabbTermekek('module-termekek/lista.php', <?php print $_GET['csop_id']; ?>)" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
										<?php
									}
								?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>



<input type="hidden" name="oldalszam" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<?php print $kezd;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<?php print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<?php print $sorrend;?>"/>
