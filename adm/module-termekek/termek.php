<?php
	// Adatok
	$query = "SELECT * FROM ".$webjel."termekek WHERE id=".$_GET['id'];
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	if($_GET['csop_id'] == 'kereso') { $_GET['csop_id'] = $row['csop_id'];}
	$query_csop = "SELECT * FROM ".$webjel."term_csoportok WHERE id=".$_GET['csop_id'];
	$res = $pdo->prepare($query_csop);
	$res->execute();
	$row_csop = $res -> fetch();
	$kat_nev = $row_csop['nev'];
	if($_GET['csop_id'] == '-1') // Akciós termékek
	{
		$kat_nev = 'Akciós termékek';
	}
	else if($_GET['csop_id'] == '-2') // Kiemelt termékek
	{
		$kat_nev = 'Kiemelt termékek';
	}
	// Akciós-e
	if ($row['akcio_ig'] >= date("Y-m-d") && $row['akcio_tol'] <= date("Y-m-d")) // Ha akciós
	{
		$akcios = 1;
	}
	else
	{
		$akcios = 0;
	}
	
	if(!isset($_GET['kezd'])) { $_GET['kezd'] = 0; }
?>
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1>Termék <small>(ID = <?php print $row['id']; ?>)</small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li><a href="?csop_id=<?= $_GET['csop_id'] ?>&kezd=<?= $_GET['kezd'] ?>#horgony_<?php print $row['id']; ?>"><i class="fa fa-folder-open"></i> <?php print $kat_nev; ?></a></li>
		<li class="active"><?php print $row['nev']; ?></li>
	  </ol>
	</section>

	<section class="content">
		<!-- Termék adatok -->
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Termék adatok</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Megnevezés</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
										<input type="text" class="form-control" id="nev" placeholder="Megnevezés" value="<?php print $row['nev']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label>Név url</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-globe"></i></span>
										<input type="text" class="form-control" id="nev_url" placeholder="Név url" value="<?php print $row['nev_url']; ?>" readonly>
										<span class="input-group-addon" onClick="nevUrlMentes(<?=$row['id']?>)" style="cursor: pointer;"><i class="fa fa-floppy-o"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label>Átirányítás (honnan)</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-arrow-left"></i></span>
										<input type="text" class="form-control" id="honnan" placeholder="Átirányítás (honnan)" value="<?php print $row['honnan']; ?>">
									</div>
								</div>		
								<div class="form-group">
									<label>Átirányítás (hova)</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-arrow-right"></i></span>
										<input type="text" class="form-control" id="hova" placeholder="Átirányítás (hova)" value="<?php print $row['hova']; ?>">
									</div>
								</div>	
								<div class="form-group">
									<label>Átirányítás típusa</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-globe"></i></span>
										<select class="form-control" id="tipus">
											<option value="0">Nincs átirányítás</option>											
											<option value="301" <?php if ($row['tipus'] == 301) { echo 'selected'; } ?>>301</option>											
											<option value="302" <?php if ($row['tipus'] == 302) { echo 'selected'; } ?>>302</option>											
										 </select>
									</div>
								</div>																	
								<div class="form-group">
									<label>Ár</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<input type="text" class="form-control" id="ar" placeholder="Ár" value="<?php print $row['ar']; ?>" >
										<span class="input-group-addon">Ft</span>
									</div>
								</div>																					
								<div class="form-group">
									<label>ÁFA</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<select class="form-control" id="afa">
											<?php
												$query2 = "SELECT * FROM ".$webjel."afa ORDER BY id asc";
												foreach ($pdo->query($query2) as $row2)
												{
													if($row['afa'] == $row2['id'])
													{
														print '<option value="'.$row2['id'].'" selected>'.$row2['afa'].'</option>';
													}
													else
													{
														print '<option value="'.$row2['id'].'">'.$row2['afa'].'</option>';
													}
												}
											?>
										 </select>
										<span class="input-group-addon">%</span>
									</div>
								</div>
								<div class="form-group">
									<label>Akciós ár</label>
									<div class="input-group">
										<span class="input-group-addon <?php if($akcios==1){print 'input_jelolo_kek';} ?>"><i class="fa fa-star-o"></i></span>
										<input type="text" class="form-control" id="akciosar" placeholder="Akciós ár" value="<?php print $row['akciosar']; ?>" >
										<span class="input-group-addon">Ft</span>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>Akció kezdete</label>
											<div class="input-group">
												<span class="input-group-addon <?php if($akcios==1){print 'input_jelolo_kek';} ?>"><i class="fa fa-calendar"></i></span>
												<input type="text" class="form-control datepicker" id="akcio_tol" placeholder="Akció kezdete" value="<?php print $row['akcio_tol']; ?>" >
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Akció vége</label>
											<div class="input-group">
												<span class="input-group-addon <?php if($akcios==1){print 'input_jelolo_kek';} ?>"><i class="fa fa-calendar"></i></span>
												<input type="text" class="form-control datepicker" id="akcio_ig" placeholder="Akció vége" value="<?php print $row['akcio_ig']; ?>" >
											</div>
										</div>
									</div>
								</div>
								<div class="form-group" <?php if($config_keszlet_kezeles != 'I') { print 'style="display:none;"'; } ?>>
									<label>Készlet</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-archive"></i></span>
										<input type="text" class="form-control" id="raktaron" placeholder="Készlet" value="<?php print $row['raktaron']; ?>" >
										<span class="input-group-addon">db</span>
									</div>
								</div>
								<div class="form-group">
									<label>Kategória</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-folder-open"></i></span>
										<select class="form-control" id="csop_id">
											<?php
												$query2 = "SELECT * FROM ".$webjel."term_csoportok ORDER BY nev ASC";
												// $query2 = "SELECT * FROM `term_csoportok` as t1 WHERE NOT EXISTS (SELECT 1 FROM `term_csoportok` as t2 WHERE t1.`csop_id` = t2.`id`)";
												foreach ($pdo->query($query2) as $row2)
												{
													$rownum = 0;
													$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id=".$row2['id']);
													$res->execute();
													$rownum = $res->fetchColumn();
													if($rownum == 0) // nincs alkategóriája
													{
														if($row2['csop_id'] > 0)
														{
															$query_szul = "SELECT * FROM ".$webjel."term_csoportok WHERE id=".$row2['csop_id'];
															$res = $pdo->prepare($query_szul);
															$res->execute();
															$row_szul = $res -> fetch();
															$nev = $row2['nev'].' ('.$row_szul['nev'].')';
														}
														else
														{
															$nev = $row2['nev'];
														}
														
														if($row['csop_id'] == $row2['id'])
														{
															print '<option value="'.$row2['id'].'" selected>'.$nev.'</option>';
														}
														else
														{
															print '<option value="'.$row2['id'].'">'.$nev.'</option>';
														}
													}
												}
											?>
										 </select>
									</div>
								</div>
								<div class="form-group" <?php if($conf_tobb_kategoria == 0) { print 'style="display:none;"'; } ?>>
									<label>Több kategóriához való rendelés</label>
									<div class="input-group">
										<?php
											include('termek_urlap_csoportok.php');
										?>
									</div>
								</div>
								<div class="form-group">
									<label>Rövid leírás</label>
									<textarea class="form-control" id="rovid_leiras" rows="3" placeholder="Rövid leírás"><?php print $row['rovid_leiras']; ?></textarea>
								</div>
								<div class="form-group">
									<label>Meta title</label>
									<div class="input-group">
										<input type="text" class="form-control" id="seo_title" placeholder="Meta title" value="<?php print $row['seo_title']; ?>" >
									</div>
								</div>
								<div class="form-group">
									<label>Meta description</label>
									<textarea class="form-control" id="seo_description" rows="3" placeholder="Meta description"><?php print $row['seo_description']; ?></textarea>
								</div>
								<div class="form-group">
									<label>Kifutott eddig</label>
									<div class="input-group">
										<span class="input-group-addon <?php if($row['ovip_torolt']==1){print 'input_jelolo_kek';} ?>"><i class="fa fa-calendar"></i></span>
										<input type="text" class="form-control datepicker" id="kifutott_eddig" placeholder="Akció kezdete" value="<?php print $row['kifutott_eddig']; ?>" >
									</div>
								</div>								
								<div class="row margtop10">
									<div class="col-sm-6">
										<div class="checkbox ">
											<label>
												<input type="checkbox" class="minimal" id="lathato" <?php if($row['lathato'] == 1)print 'checked'; ?>> Látható
											</label>
										</div>
									</div>
									<div class="col-sm-6" <?php if($conf_kiemelt_term == 0){ print 'style="display:none;"'; } ?>>
										<div class="checkbox">
											<label>
												<input type="checkbox" class="minimal" id="kiemelt" <?php if($row['kiemelt'] == 1)print 'checked'; ?>> Kiemelt
											</label>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="checkbox">
											<label>
												<input type="checkbox" class="minimal" id="ppp_tiltas" <?php if($row['ppp_tiltas'] == 1)print 'checked'; ?>> Pick Pack Pont tiltás
											</label>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="checkbox">
											<label>
												<input type="checkbox" class="minimal" id="rendelheto" <?php if($row['rendelheto'] == 1)print 'checked'; ?>> Rendelhető (0-ás készlet esetén is)
											</label>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="checkbox">
											<label>
												<input type="checkbox" class="minimal" id="kifutott" <?php if($row['kifutott'] == 1)print 'checked'; ?>> Kifutott termék
											</label>
										</div>
									</div>									
									<div class="col-sm-12">
										<div class="checkbox">
											<label>
												<input type="radio" class="minimal" name="sorrend" value="1" <?php if($row['sorrend'] == 1)print 'checked'; ?>> Legjobb találat
											</label>
										</div>
										<div class="checkbox">
											<label>
												<input type="radio" class="minimal" name="sorrend" value="0" <?php if($row['sorrend'] == 0)print 'checked'; ?>> Normál találat
											</label>
										</div>
										<div class="checkbox">
											<label>
												<input type="radio" class="minimal" name="sorrend" value="-1" <?php if($row['sorrend'] == -1)print 'checked'; ?>> Hátrasorolt találat
											</label>
										</div>	
										<div class="checkbox">
											<label>
												<input type="radio" class="minimal" name="sorrend" value="-2" <?php if($row['sorrend'] == -2)print 'checked'; ?>> Automatikusan hátrasorolt
											</label>
										</div>											
										<div class="checkbox">
											<label>
												<input type="radio" class="minimal" name="sorrend" value="-3" <?php if($row['sorrend'] == -3)print 'checked'; ?>> Hátrasorolt kifutott
											</label>
										</div>																													
									</div>																																						
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Leírás</label>
									<textarea id="editor1" name="editor1" rows="10" cols="80">
										<?php print $row['leiras']; ?>
									</textarea>
								</div>
							</div>
						</div>
					</div>

					<div class="box-footer">
						<button type="submit" onClick="mentesTermek(<?php print $row['id']; ?>)" class="btn btn-primary">Mentés</button>
					</div>
				</div>
			</div>
			<!-- Képek -->
			<div class="col-md-6">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Képek feltöltése</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<form id="kepfeltoltes" action="/upload-target" class="dropzone"></form>
					</div>
					<div class="box-body" id="termek_kepek_div">
						<?php
							include('termek_kepek.php');
						?>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<!-- Gombok -->
				<div class="box box-danger">
					<div class="box-header">
						<h3 class="box-title">Vezérlőpult</h3>
					</div>
					<div class="box-body">
						<a onclick="torolTermek(<?php print $_GET['csop_id']; ?>, <?php print $_GET['id']; ?>)" class="btn btn-app"><i class="fa fa-times"></i> Törlés</a>
						<a onClick="klonozas(<?php print $_GET['csop_id']; ?>, <?php print $_GET['id']; ?>, <?php print $_GET['kezd']; ?>)" class="btn btn-app"><i class="fa fa-clone"></i> Klónozás</a>
					</div>
				</div>
				<!-- Új Paraméterek -->
				<div class="box box-lila" <?php if($conf_parameterek == 0) { print 'style="display:none;"'; } ?>>
					<div class="box-header">
						<h3 class="box-title">Termék paraméterek</h3>
					</div>
					<div class="box-body" id="parameterek_uj_div">
						<?php
							include('termek_uj_parameterek.php');
						?>
					</div>
				</div>
				<!--RENDELÉSEK-->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Rendelések</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-hover table-striped margbot0">
							<tbody>
								<tr>
									<th>Azonosító</th>
									<th>Dátum</th>
									<th>Szállítás</th>
									<th>Fizetés</th>
									<th style="text-align:right;">Mennyiség</th>
									<th style="text-align:right;">Érték</th>
								</tr>
								<?php
									$query_rend = "SELECT *, ".$webjel."rendeles.id as id, 
												".$webjel."rendeles.rendeles_id as rendeles_id, 
												SUM(".$webjel."rendeles_tetelek.term_db) as db, 
												SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar > 0, ".$webjel."rendeles_tetelek.term_akcios_ar * ".$webjel."rendeles_tetelek.term_db, ".$webjel."rendeles_tetelek.term_ar * ".$webjel."rendeles_tetelek.term_db)) as rendelt_osszeg 
											FROM ".$webjel."rendeles 
											INNER JOIN ".$webjel."rendeles_tetelek 
											ON ".$webjel."rendeles.id = ".$webjel."rendeles_tetelek.rendeles_id 
											WHERE ".$webjel."rendeles_tetelek.term_id=".$_GET['id']." 
											GROUP BY ".$webjel."rendeles.id
											ORDER BY ".$webjel."rendeles.id DESC";
									foreach ($pdo->query($query_rend) as $row_rend)
									{
										?>
											<tr class="kattintos_sor" onClick="window.location.href = '<?php print $domain; ?>/adm/rendelesek.php?id=<?php print $row_rend['id']; ?>&fajl=module-rendelesek/lista.php';">
										<?php
											print '<td>'.$row_rend['id'].'</td>
											<td>'.$row_rend['datum'].'</td>
											<td>'.$row_rend['szall_mod'].'</td>
											<td>'.$row_rend['fiz_mod'].'</td>
											<td style="text-align:right;">'.$row_rend['db'].' db</td>
											<td style="text-align:right;">'.number_format($row_rend['rendelt_osszeg'], 0, ',', ' ').' Ft</td>
										</tr>';
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
