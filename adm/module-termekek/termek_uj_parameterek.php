<?php
	if (isset($_GET['script2']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	if(isset($_POST['command']) && $_POST['command']=='uj_parameter')
	{
		// Új paraméter
		if(isset($_POST['uj_parameter']) && $_POST['uj_parameter'] != '')
		{
			$insertcommand = "INSERT INTO ".$webjel."termek_uj_parameterek (nev, mertekegyseg) VALUES (:nev, :mertekegyseg)";
			$result = $pdo->prepare($insertcommand);
			$result->execute(array(':nev'=>$_POST['uj_parameter'], ':mertekegyseg'=>$_POST['uj_parameter_me']));
			$parameter_id = $pdo->lastInsertId();
		}
		// Paraméter hozzáadás
		else 
		{
			$parameter_id = $_POST['parmeter_hozzaadas'];
		}
		
		// Érték
		$insertcommand = "INSERT INTO ".$webjel."termek_uj_parameter_ertekek (parameter_id, termek_id, ertek) VALUES (".$parameter_id.", ".$_GET['id'].", :nev)";
		$result = $pdo->prepare($insertcommand);
		$result->execute(array(':nev'=>$_POST['uj_parameter_ertek']));
	}
	else if(isset($_POST['command']) && $_POST['command']=='parameter_ertek_torlese')
	{
		$pdo->exec("DELETE FROM ".$webjel."termek_uj_parameter_ertekek WHERE id =".$_POST['param_id']);
	}
	
	// Új paraméter hozzáadása select
	print '<div class="row">
		<div class="col-md-5"><div class="form-group">
			<label>Hozzáadás</label>
			<div class="input-group">
				<span class="input-group-addon input_jelolo_nsarga"><i class="fa fa-folder-open"></i></span>
				<select onChange="parameterOpcioSelect()" class="form-control" id="parmeter_hozzaadas" attr_termek_id="'.$_GET['id'].'">
					<option value="0"> - Válassz - </option>';
					$query_param_valaszto = "SELECT * FROM ".$webjel."termek_uj_parameterek ORDER BY nev ASC";
					foreach ($pdo->query($query_param_valaszto) as $row_param_valaszto)
					{
						$megys = '';
						if($row_param_valaszto['mertekegyseg'] != '') { $megys = ' ('.$row_param_valaszto['mertekegyseg'].')'; }
						print '<option value="'.$row_param_valaszto['id'].'" >'.$row_param_valaszto['nev'].$megys.'</option>';
					}
				print '</select>
			</div>
		</div></div>';
		// Új paraméter létrehozása
		print '<div class="col-md-4"><div class="form-group">
			<label>Új paraméter</label>
			<div class="input-group">
				<span class="input-group-addon input_jelolo_kek"><i class="fa fa-plus"></i></span>
				<input class="form-control" type="text" id="uj_parameter" name="uj_parameter" placeholder="Új paraméter">
			</div>
		</div></div>
		<div class="col-md-3"><div class="form-group">
			<label>Mértékegység</label>
			<div class="input-group">
				<input class="form-control" type="text" id="uj_parameter_me" name="uj_parameter_me" placeholder="">
			</div>
		</div></div>
	</div>';
	print '<div class="row">
		<div class="col-md-6"><div class="form-group">
			<div class="input-group">
				<input class="form-control" type="text" id="uj_parameter_ertek" name="uj_parameter_ertek" placeholder="Paraméter értéke">
			</div>
		</div></div>';
		// Új paraméter létrehozása
		print '<div class="col-md-6"><div class="form-group">
			<div id="parameter_ertek_opcio" class="input-group"></div>
		</div></div>
	</div>';
	print '<div class="form-group">
		<div class="input-group">
			<a onClick="uj_parameter_hozzaadasa_mentese('.$_GET['id'].')" class="btn btn-primary" style="width: 100%;">Új paraméter hozzáadása/mentése</a>
		</div>
	</div>';
?>
	<div class="table-responsive">
		<table class="table table-hover table-bordered table-striped margbot0">
			<tbody>
				<tr>
					<th colspan="2">Rögzített paraméterek</th>
					<th class="text-right">felár</th>
					<th></th>
				</tr>
				<?php
					$query_param = "SELECT *, ".$webjel."termek_uj_parameter_ertekek.id AS id 
						FROM ".$webjel."termek_uj_parameter_ertekek 
						INNER JOIN ".$webjel."termek_uj_parameterek
						ON ".$webjel."termek_uj_parameterek.id = ".$webjel."termek_uj_parameter_ertekek.parameter_id 
						WHERE termek_id=".$_GET['id']." 
						ORDER BY ".$webjel."termek_uj_parameterek.nev ASC, ".$webjel."termek_uj_parameter_ertekek.ertek ASC";
					$pm = '';
					foreach ($pdo->query($query_param) as $row_param)
					{
						echo '<tr>
							<td>';
								if($row_param['nev'] != $pm) { echo $row_param['nev']; $pm = $row_param['nev']; }
							echo '</td>
							<td>'.$row_param['ertek'].' '.$row_param['mertekegyseg'].'</td>
							<td class="text-right">';
								if($row_param['valaszthato'] == 1)
								{
									echo '<a onClick="param_felar_mentes('.$row_param['id'].')" id="param_felar_gomb_'.$row_param['id'].'" class="btn fa fa-floppy-o" style="padding: 2px 4px; display:none;" ></a>
									<input type="text" onKeyUp="param_felar_mentes_elok('.$row_param['id'].')" id="param_felar_input_'.$row_param['id'].'" value="'.$row_param['felar'].'" style="padding: 0 6px; width: 80px; text-align: right;" class="param_felar_input">';
								}
							echo '</td>
							<td style="width: 38px;"><a onClick="param_ertek_torles2('.$_GET['id'].', '.$row_param['id'].')" class="btn fa fa-times" style="padding: 2px 4px;" ></a></td>
						</tr>';
					}
				?>
			</tbody>
		</table>
	</div>


