<?php
	// Adatok
	$query = "SELECT * FROM ".$webjel."term_csoportok WHERE id=".$_GET['csop_id'];
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	$kat_nev = $row['nev'];
?>
		  
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1>Új termék </small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li><a href="?csop_id=<?= $_GET['csop_id'] ?>"><i class="fa fa-folder-open"></i> <?php print $kat_nev; ?></a></li>
		<li class="active">Új termék</li>
	  </ol>
	</section>

	<section class="content">
		<!-- Termék adatok -->
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Termék adatok</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Megnevezés</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
										<input type="text" class="form-control" id="nev" placeholder="Megnevezés" value="">
									</div>
								</div>
								<div class="form-group">
									<label>Ár</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<input type="text" class="form-control" id="ar" placeholder="Ár" value="" >
										<span class="input-group-addon">Ft</span>
									</div>
								</div>
								<div class="form-group">
									<label>ÁFA</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<select class="form-control" id="afa">
											<?php
												$query2 = "SELECT * FROM ".$webjel."afa ORDER BY id asc";
												foreach ($pdo->query($query2) as $row2)
												{
													print '<option value="'.$row2['id'].'">'.$row2['afa'].'</option>';
												}
											?>
										 </select>
										<span class="input-group-addon">%</span>
									</div>
								</div>
								<div class="form-group">
									<label>Akciós ár</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-star-o"></i></span>
										<input type="text" class="form-control" id="akciosar" placeholder="Akciós ár" value="" >
										<span class="input-group-addon">Ft</span>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>Akció kezdete</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												<input type="text" class="form-control datepicker" id="akcio_tol" placeholder="Akció kezdete" value="" >
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Akció vége</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												<input type="text" class="form-control datepicker" id="akcio_ig" placeholder="Akció vége" value="" >
											</div>
										</div>
									</div>
								</div>
								<div class="form-group" <?php if($config_keszlet_kezeles != 'I') { print 'style="display:none;"'; } ?>>
									<label>Készlet</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-archive"></i></span>
										<input type="text" class="form-control" id="raktaron" placeholder="Készlet" value="" >
										<span class="input-group-addon">db</span>
									</div>
								</div>
								<div class="form-group">
									<label>Rövid leírás</label>
									<textarea class="form-control" id="rovid_leiras" rows="3" placeholder="Rövid leírás"></textarea>
								</div>
								<div class="form-group">
									<label>Meta title</label>
									<div class="input-group">
										<input type="text" class="form-control" id="seo_title" placeholder="Meta title" value="" >
									</div>
								</div>
								<div class="form-group">
									<label>Meta description</label>
									<textarea class="form-control" id="seo_description" rows="3" placeholder="Meta description"></textarea>
								</div>
								<div class="row margtop10">
									<div class="col-sm-6">
										<div class="checkbox ">
											<label>
												<input type="checkbox" class="minimal" id="lathato" checked/> Látható
											</label>
										</div>
									</div>
									<div class="col-sm-6" <?php if($conf_kiemelt_term == 0){ print 'style="display:none;"'; } ?>>
										<div class="checkbox">
											<label>
												<input type="checkbox" class="minimal" id="kiemelt" /> Kiemelt
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Leírás</label>
									<textarea id="editor1" name="editor1" rows="10" cols="80"></textarea>
								</div>
							</div>
						</div>
					</div>

					<div class="box-footer">
						<button type="submit" onClick="mentesUjTermek('<?php print $_GET['csop_id']; ?>')" class="btn btn-primary">Mentés</button>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
