<?php

	session_start();
	ob_start();
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	$update = "UPDATE ".$webjel."termekek SET rendelheto = ".$_POST['rendelheto']." WHERE id = ".$_POST["term_id"];
	$res = $pdo->prepare($update);
	$res -> execute();

?>