<?php
	header("Access-Control-Allow-Origin: *"); 
	header('Content-type: application/json');
	
	include '../../config.php';
	include '../../adm/config_adm.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	$ds = DIRECTORY_SEPARATOR;
	$dir = $gyoker.'/images/termekek/';
	$dir_pdf = $gyoker.'/dokumentumok/termekek/';
	
	// Termék neve
	$query = "SELECT * FROM ".$webjel."termekek WHERE id=".$_GET['id'];
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	$urlnev = $row['nev_url'];
	
	// Kép másolása mappába
	if (!empty($_FILES) && $_FILES['file']['error'] == 0) {

		$tempFile = $_FILES['file']['tmp_name'];
		// $kep_eredeti_szelesseg = imagesx($tempFile);
		// $kep_eredeti_magassag = imagesy($tempFile);
		require_once($gyoker.'/adm/simpleImage_class3.php');
		$image = new SimpleImage();		
		$targetPath = $dir . $ds;
		$file = $_FILES['file']['name'];
		$fajlnev = pathinfo($file, PATHINFO_FILENAME);
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		$ext = strtolower($ext);
		// $fn = 'img-'.time().rand(0,999999999999).'.'.$ext;
		
		if ($ext == 'pdf' || $ext == 'zip')
		{
			$fn = $urlnev.'-'.$fajlnev.'.'.$ext;
			$targetPath = $dir_pdf . $ds;
			$targetFile =  $targetPath. $fn;
			move_uploaded_file($tempFile,$targetFile);

			$insertcommand = "INSERT INTO ".$webjel."termek_kepek (termek_id,kep,spec,extension) VALUES (:termek_id,:kep,:spec,:extension)";
			$result = $pdo->prepare($insertcommand);
			$result->execute(array(':termek_id'=>$_GET['id'],
							  ':kep'=>$fn,
							  ':spec'=>1,
							  ':extension'=>$ext));
		}
		else
		{
			$query = "SELECT sorszam FROM ".$webjel."termek_kepek WHERE spec=0 AND termek_id=".$_GET['id']." ORDER BY sorszam DESC LIMIT 1";
			$res = $pdo->prepare($query);
			$res->execute();
			$row_sorszam = $res -> fetch();

			if (isset($row_sorszam['sorszam'])) {
				$sorszam = $row_sorszam['sorszam'] + 1;
			}
			else
			{
				$sorszam = 1;
			}

			$fn = $urlnev.'-'.$sorszam.'.'.$ext;
			$fn2 = $urlnev.'-'.$sorszam.'-thumb.'.$ext;
			$targetFile =  $targetPath. $fn;
			$image -> load($tempFile);
			$image -> resizeToWidth($conf_kepmeretezes);
			//move_uploaded_file($tempFile,$targetFile);
			$image -> save($targetFile);
			
			// Bélyegkép készítése 2
			$image->load($tempFile);
			if($conf_csak_szelesseg == 1)
			{
				$image->resizeToWidth($conf_thumb_max_szeklesseg); // csak szélesség szerint
			}
			else
			{
				$image->resize_and_cut($conf_thumb_max_szeklesseg,$conf_thumb_max_magassag);
			}
			$thumb = $fn2;
			$image->save($targetPath.$thumb);

			
			// Adatbázidba töltés
			$insertcommand = "INSERT INTO ".$webjel."termek_kepek (termek_id,kep,thumb) VALUES (:termek_id,:kep,:thumb)";
			$result = $pdo->prepare($insertcommand);
			$result->execute(array(':termek_id'=>$_GET['id'],
							  ':kep'=>$fn,
							  ':thumb'=>$thumb));

			$updatecommand = "UPDATE ".$webjel."termek_kepek SET sorszam=? WHERE spec=0 AND termek_id=?";
			$result = $pdo->prepare($updatecommand);
			$result->execute(array($sorszam,$_GET['id']));			
		}
	}
?>  