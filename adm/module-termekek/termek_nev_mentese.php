<?php
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	$nev_url = $_POST['nev'];
	$patterns = array();
	$patterns[0] = '/á/';
	$patterns[1] = '/é/';
	$patterns[2] = '/í/';
	$patterns[3] = '/ó/';
	$patterns[4] = '/ö/';
	$patterns[5] = '/ő/';
	$patterns[6] = '/ú/';
	$patterns[7] = '/ü/';
	$patterns[8] = '/ű/';
	$patterns[9] = '/Á/';
	$patterns[10] = '/É/';
	$patterns[11] = '/Í/';
	$patterns[12] = '/Ó/';
	$patterns[13] = '/Ö/';
	$patterns[14] = '/Ő/';
	$patterns[15] = '/Ú/';
	$patterns[16] = '/Ü/';
	$patterns[17] = '/Ű/';
	$patterns[18] = '/ /';
	$patterns[19] = '/\//';
	$patterns[20] = '/"/';
	$replacements = array();
	$replacements[0] = 'a';
	$replacements[1] = 'e';
	$replacements[2] = 'i';
	$replacements[3] = 'o';
	$replacements[4] = 'o';
	$replacements[5] = 'o';
	$replacements[6] = 'u';
	$replacements[7] = 'u';
	$replacements[8] = 'u';
	$replacements[9] = 'A';
	$replacements[10] = 'E';
	$replacements[11] = 'I';
	$replacements[12] = 'O';
	$replacements[13] = 'O';
	$replacements[14] = 'O';
	$replacements[15] = 'U';
	$replacements[16] = 'U';
	$replacements[17] = 'U';
	$replacements[18] = '-';
	$replacements[19] = '-';
	$replacements[20] = 'col';
	$nev_url = preg_replace($patterns, $replacements, $nev_url);
	$nev_url = preg_replace('/[^a-zA-Z0-9_-]/', '', $nev_url);
	$nev_url = preg_replace('/-+/', '-', $nev_url);

	// Egyezőség vizsgálata
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE nev_url = '".$nev_url."'");
	$res->execute();
	$rownum2 = $res->fetchColumn();
	if ($rownum2 > 0) // Ha van már ilyen nevű
	{
		$query = "SELECT * FROM ".$webjel."termekek WHERE nev_url = '".$nev_url."'";
		$res = $pdo->prepare($query);
		$res->execute();
		$row = $res -> fetch();
		if($rownum2 > 1 || $row['id'] != $_GET['id']) // Ha nem saját maga
		{
			$nev_url = $nev_url.'-'.$_GET['id'];
		}
	}

	$updatecommand = "UPDATE ".$webjel."termekek SET nev_url=? WHERE id=?";
	$result = $pdo->prepare($updatecommand);
	$result->execute(array($nev_url, $_GET['id']));

	echo $nev_url;
?>