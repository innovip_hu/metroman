<?php
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	// Képek törlése
	$query = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id =".$_GET['id'];
	foreach ($pdo->query($query) as $row)
	{
		$dir = $gyoker."/images/termekek/";
		unlink($dir.$row['thumb']);
		unlink($dir.$row['kep']);
	}
	$deletecommand = "DELETE FROM ".$webjel."termek_kepek WHERE termek_id =".$_GET['id'];
	$result = $pdo->prepare($deletecommand);
	$result->execute();
	// Termék törlés
	$pdo->exec("DELETE FROM ".$webjel."termekek WHERE id =".$_GET['id']);

	$pdo->exec("DELETE FROM ".$webjel."termek_uj_parameter_ertekek WHERE termek_id =".$_GET['id']);
?>