<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	/* 
	if(isset($_GET['command']) && $_GET['command'] == 'uj_galeria')
	{
		// URL név meghatározása
		include $gyoker.'/adm/module/mod_urlnev.php';
		if ($nev_url == '')
		{
			$nev_url = rand(1,99999);
		}
		// Egyezőség vizsgálata
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."galeria WHERE nev_url = '".$nev_url."'");
		$res->execute();
		$rownum2 = $res->fetchColumn();
		if ($rownum2 > 0) // Ha van már ilyen nevű
		{
			$nev_url = $nev_url.'-'.rand(1,99999);
		}
		
		$insertcommand = "INSERT INTO ".$webjel."galeria (nev,nev_url) VALUES (:nev,:nev_url)";
		$result = $pdo->prepare($insertcommand);
		$result->execute(array(':nev'=>$_POST['nev'],
						  ':nev_url'=>$nev_url));
	}
	 */
	// Galéria törlése
	if(isset($_GET['command']) && $_GET['command'] == 'galeria_torles')
	{
		// SQL
		$deletecommand = "DELETE FROM ".$webjel."yt_galeria WHERE id =".$_GET['id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();
		// Linkek törlése
		$deletecommand = "DELETE FROM ".$webjel."yt_galeria_linkek WHERE galeria_id =".$_GET['id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();
	}

// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 50; //ALAPÁLLAPOT
	}
// Rekordok száma
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."yt_galeria ");
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'id'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'DESC'; // Alap rendezési feltétel
	}
?>
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1 id="myModal">Youtube galéria</h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Youtube galéria</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-6">
				<div class="box box-primary">
					<div class="box-body no-padding with-border">
						<div class="mailbox-controls">
							<div class="btn-group">
								<div class="has-feedback">
									<input id="uj_galeria" type="text" class="form-control input-sm" placeholder="Új galéria neve" /><div id="riaszt_galeria"></div>
								</div>
							</div>
							<span class="menu_gomb"><a onClick="ujGaleria('module-yt_galeria/lista.php', 'module-yt_galeria/galeria.php')"><i class="fa fa-plus"></i> Új youtube galéria mentése</span></a>
							
							<div class="pull-right">
								<?php
									if($oldalszam > $rownum) $oldalszam_mod = $rownum;
									else $oldalszam_mod = $oldalszam;
									$vege = $kezd+$oldalszam_mod;
									if($vege > $rownum) $vege = $rownum;
									if($rownum == 0) {$kezd_lapozo = 0;} else {$kezd_lapozo = $kezd+1;}
									print $kezd_lapozo.'-'.$vege.'/'.$rownum;
								?>
								<div class="btn-group">
								<?php
									if ($kezd != 0) // Lapozás vissza (balra)
									{
										?>
										<button onclick="lapozasVissza('module-yt_galeria/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
										<?php
									}
									if (($utolso_oldal / $aktualis_oldal) > 1) // Lapozás előre (jobbra)
									{
										?>
										<button onclick="lapozasTovabb('module-yt_galeria/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
										<?php
									}
								?>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table table-hover table-striped margbot0">
								<tbody>
									<tr>
										<th>ID</th>
										<th>Név</th>
										<th>Sorrend</th>
									</tr>
									<?php
									$query = "SELECT * FROM ".$webjel."yt_galeria ORDER BY sorrend=0,sorrend ASC, id ASC LIMIT ".$kezd.",".$oldalszam;
									foreach ($pdo->query($query) as $row)
									{
										?>
										<tr class="kattintos_sor" onClick="Belepes('module-yt_galeria/galeria.php', 'module-yt_galeria/lista.php', <?php print $row['id']; ?>)">
										<?php
										print '<td>'.$row['id'].'</td>
											<td>'.$row['nev'].'</td>
											<td>'.$row['sorrend'].'</td>
										</tr>';
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer no-padding">
						<div class="mailbox-controls">
							<div class="btn-group" style="height:30px">
								&nbsp;
							</div>
							<div class="pull-right">
								<?php
									print $kezd_lapozo.'-'.$vege.'/'.$rownum;
								?>
								<div class="btn-group">
								<?php
									if ($kezd != 0) // Lapozás vissza (balra)
									{
										?>
										<button onclick="lapozasVissza('module-yt_galeria/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
										<?php
									}
									if (($utolso_oldal / $aktualis_oldal) > 1) // Lapozás előre (jobbra)
									{
										?>
										<button onclick="lapozasTovabb('module-yt_galeria/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
										<?php
									}
								?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<input type="hidden" name="oldalszam" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<?php print $kezd;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<?php print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<?php print $sorrend;?>"/>
