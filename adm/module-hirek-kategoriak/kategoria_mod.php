<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	if(isset($_GET['command']) && $_GET['command'] == 'mentes')
	{
		// URL név meghatározása
		include $gyoker.'/adm/module/mod_urlnev.php';
		if ($nev_url == '')
		{
			$nev_url = rand(1,99999);
		}
		// Egyezőség vizsgálata
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirek_kategoriak WHERE nev_url = '".$nev_url."'");
		$res->execute();
		$rownum2 = $res->fetchColumn();
		if ($rownum2 > 0) // Ha van már ilyen nevű
		{
			$query = "SELECT * FROM ".$webjel."hirek_kategoriak WHERE nev_url = '".$nev_url."'";
			$res = $pdo->prepare($query);
			$res->execute();
			$row = $res -> fetch();
			if($rownum2 > 1 || $row['id'] != $_GET['id']) // Ha nem saját maga
			{
				$nev_url = $nev_url.'-'.$_GET['id'];
			}
		}
		
		$updatecommand = "UPDATE ".$webjel."hirek_kategoriak SET nev=?, leiras=?, nev_url=?, seo_title=?, seo_description=? WHERE id=?";
		$result = $pdo->prepare($updatecommand);
		$result->execute(array($_POST['nev'],$_POST['leiras'],$nev_url,$_POST['seo_title'],$_POST['seo_description'],$_GET['id']));
	}
	
	$query = "SELECT * FROM ".$webjel."hirek_kategoriak WHERE id=".$_GET['id'];
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	
?>
		  
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1>Kategória módosítása<small>(<?php print $row['nev']; ?>)</small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li><a href="hirek-kategoriak.php"><i class="fa fa-folder-open"></i> Kategóriák</a></li>
		<li class="active">Kategória módosítása</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-6">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Adatok</h3>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label>Megnevezés</label>
							<div class="input-group">
								<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
								<input type="text" class="form-control" id="nev" placeholder="Megnevezés" value="<?php print $row['nev']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label>Leírás</label>
							<textarea class="form-control" id="leiras" rows="3" placeholder="Leírás"><?php print $row['leiras']; ?></textarea>
						</div>
						<div class="form-group">
							<label>SEO title</label>
							<div class="input-group">
								<span class="input-group-addon input_jelolo_kek"><i class=""><i class="fa fa-link"></i></i></span>
								<input type="text" class="form-control" id="seo_title" placeholder="SEO title" value="<?php print $row['seo_title']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label>SEO description</label>
							<div class="input-group">
								<span class="input-group-addon input_jelolo_kek"><i class=""><i class="fa fa-link"></i></i></span>
								<input type="text" class="form-control" id="seo_description" placeholder="SEO description" value="<?php print $row['seo_description']; ?>">
							</div>
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" onClick="mentesKategoria('<?php print $row['id']; ?>')" class="btn btn-primary">Mentés</button>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
