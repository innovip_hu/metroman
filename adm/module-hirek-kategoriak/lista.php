<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	// Kategória törlése
	if(isset($_GET['torol_id']))
	{
		// Termékek
		$query = "SELECT * FROM ".$webjel."hirek WHERE csop_id=".$_GET['torol_id'];
		foreach ($pdo->query($query) as $row)
		{
			// Képek törlése
			$query_kep = "SELECT * FROM ".$webjel."hir_kepek WHERE hir_id =".$row['id'];
			foreach ($pdo->query($query_kep) as $row_kep)
			{
				$dir = $gyoker."/images/termekek/";
				unlink($dir.$row_kep['kep']);
			}
			$deletecommand = "DELETE FROM ".$webjel."hir_kepek WHERE hir_id =".$row['id'];
			$result = $pdo->prepare($deletecommand);
			$result->execute();
			// Termék
			$deletecommand = 'DELETE FROM '.$webjel.'hirek WHERE id = '.$row['id'];
			$result = $pdo->prepare($deletecommand);
			$result->execute();
			// Jellemzők
		}
		
		$deletecommand = "DELETE FROM ".$webjel."hirek_kategoriak WHERE id =".$_GET['torol_id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();
	}

	// Új kategória 2
	if(isset($_POST['command']) && $_POST['command'] == 'uj_kategoria_mentese')
	{	
		// URL név meghatározása
		include $gyoker.'/adm/module/mod_urlnev.php';
		if ($nev_url == '')
		{
			$nev_url = rand(1,99999);
		}
		// Egyezőség vizsgálata
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirek_kategoriak WHERE nev_url = '".$nev_url."'");
		$res->execute();
		$rownum2 = $res->fetchColumn();
		if ($rownum2 > 0) // Ha van már ilyen nevű
		{
			$nev_url = $nev_url.'-'.rand(1,99999);
		}
		
		$insertcommand = "INSERT INTO ".$webjel."hirek_kategoriak (nev, leiras, nev_url, seo_title, seo_description) VALUES (:nev, :leiras, :nev_url, :seo_title, :seo_description)";
		$result = $pdo->prepare($insertcommand);
		$result->execute(array(
						  ':nev'=>$_POST['nev'],
						  ':leiras'=>$_POST['leiras'],
						  ':nev_url'=>$nev_url,
						  ':seo_title'=>$_POST['seo_title'],
						  ':seo_description'=>$_POST['seo_description']));

	}
?>
<!--Modal-->
<div class="example-modal">
	<div id="rakerdez_torles" class="modal modal-danger">
	  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Törlés</h4>
			</div>
			<div class="modal-body">
				<p>Biztos törölni szeretnéd a(az) <span id="modal_torles_nev"></span> kategóriát?</p>
				<p>A kategória minden híre és képe törölve lesz!</p>
			</div>
			<div class="modal-footer">
				<input type="hidden" id="modal_torles_id" value="" />
				<input type="hidden" id="modal_torles_kep" value="" />
				<button onClick="torol()" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Igen</button>
				<button onClick="megsem('rakerdez_torles')" type="button" class="btn btn-outline">Mégsem</button>
			</div>
		</div>
	  </div>
	</div>
</div>

<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1 id="myModal">Hírek kategóriák</h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Hírek kategóriák</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<span class="menu_gomb"><a onClick="ujKategoria(0, 'Főkategória')"><i class="fa fa-plus"></i> Új Kategória</span></a>
					</div>
					<div class="box-body kategroia_kezelo">
						<ul class="tree">
							<?php
								$query = "SELECT * FROM ".$webjel."hirek_kategoriak ORDER BY nev ASC";
								foreach ($pdo->query($query) as $row)
								{
										print '<li>'.$row['nev'];
										?>
										<div class="btn-group">
											<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
												<span class="caret"></span>
											</button>
											<ul class="dropdown-menu">
												<li><a onClick="kategoriaMOD(<?php print $row['id']; ?>)"><i class="fa fa-pencil"></i>Módosítás</a></li>
												<li><a onClick="rakerdez(<?php print $row['id']; ?>, '<?php print $row['nev']; ?>')"><i class="fa fa-trash"></i>Törlés</a></li>
											</ul>
										</div>
										<?php
									print '</li>';
								}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

