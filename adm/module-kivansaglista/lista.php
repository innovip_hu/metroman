<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
?>
<!--Modal
<div class="example-modal">
	<div id="rakerdez_torles" class="modal modal-danger">
	  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Törlés</h4>
			</div>
			<div class="modal-body">
				<p>Biztos törölni szeretnéd a(az) <span id="modal_torles_nev"></span> kupont?</p>
			</div>
			<div class="modal-footer">
				<input type="hidden" id="modal_torles_id" value="" />
				<button onClick="kuponTorles('rakerdez_torles')" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Igen</button>
				<button onClick="megsem('rakerdez_torles')" type="button" class="btn btn-outline">Mégsem</button>
			</div>
		</div>
	  </div>
	</div>
</div>-->

<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1 id="myModal">Kívánságlista</h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Kívánságlista</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-6">
				<div class="box box-primary">
					<div class="table-responsive">
						<table class="table table-hover table-striped margbot0">
							<tbody>
								<tr>
									<th>Terméknév</th>
									<th>Listában szerepel</th>
								</tr>
								<?php
									$query = "SELECT COUNT(".$webjel."kivansag_lista.termek_id) as db, 
												".$webjel."termekek.nev,
												".$webjel."termekek.id 
										FROM ".$webjel."kivansag_lista 
										INNER JOIN ".$webjel."termekek
										ON ".$webjel."kivansag_lista.termek_id = ".$webjel."termekek.id
										GROUP BY ".$webjel."kivansag_lista.termek_id
										ORDER BY db DESC";
									foreach ($pdo->query($query) as $row)
									{
										// $res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."kivansag_lista WHERE termek_id=".$row['termek_id']);
										// $res->execute();
										// $rownum = $res->fetchColumn();

										print '<tr>
											<td>'.$row['nev'].'</td>
											<td>x '.$row['db'].'</td>
										</tr>';
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

