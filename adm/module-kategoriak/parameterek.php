<?php
	if (isset($_GET['script2']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}

	if(isset($_POST['command']) && $_POST['command']=='uj_parameter')
	{
		// Új paraméter
		if(isset($_POST['uj_parameter']) && $_POST['uj_parameter'] != '')
		{
			$insertcommand = "INSERT INTO ".$webjel."term_csoportok_parameter (nev,csop_id) VALUES (:nev, :csop_id)";
			$result = $pdo->prepare($insertcommand);
			$result->execute(array(':nev'=>$_POST['uj_parameter'], ':csop_id'=>$_GET['id']));
			$parameter_id = $pdo->lastInsertId();
		}
		// Paraméter hozzáadás
		else 
		{
			$query = "SELECT id FROM ".$webjel."term_csoportok_parameter WHERE csop_id=? AND nev=?";
			$res = $pdo->prepare($query);
			$res->execute(array($_GET['id'],$_POST['parmeter_hozzaadas']));
			$row = $res -> fetch();	

			if (isset($row['id'])) 
			{
				$parameter_id = $row['id'];
			}		
			else
			{
				$insertcommand = "INSERT INTO ".$webjel."term_csoportok_parameter (nev,csop_id) VALUES (:nev, :csop_id)";
				$result = $pdo->prepare($insertcommand);
				$result->execute(array(':nev'=>$_POST['parmeter_hozzaadas'], ':csop_id'=>$_GET['id']));
				$parameter_id = $pdo->lastInsertId();							
			}			
		}
		
		// Érték
		$insertcommand = "INSERT INTO ".$webjel."term_csoportok_parameter_ertek (param_id, ertek, ertek2) VALUES (".$parameter_id.", :ertek, :ertek2)";
		$result = $pdo->prepare($insertcommand);
		$result->execute(array(':ertek'=>$_POST['uj_parameter_ertek'],':ertek2'=>htmlentities($_POST['uj_parameter_ertek'])));
	}
	else if(isset($_POST['command']) && $_POST['command']=='parameter_ertek_torlese')
	{

		$query = "SELECT param_id FROM ".$webjel."term_csoportok_parameter_ertek WHERE id=".$_POST['param_id'];
		$res = $pdo->prepare($query);
		$res->execute();
		$row_param_check = $res -> fetch();

		$pdo->exec("DELETE FROM ".$webjel."term_csoportok_parameter_ertek WHERE id =".$_POST['param_id']);		

		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok_parameter_ertek WHERE param_id=".$row_param_check['param_id']);
		$res->execute();
		$rownum = $res->fetchColumn();		

		if ($rownum == 0)
		{
			$pdo->exec("DELETE FROM ".$webjel."term_csoportok_parameter WHERE id =".$row_param_check['param_id']);					
		}
	}
	
	// Új paraméter hozzáadása select
	print '<div class="row">';
		// Új paraméter létrehozása
		print '<div class="col-md-12"><div class="form-group">
			<label>Új paraméter</label>
			<div class="input-group">
				<span class="input-group-addon input_jelolo_kek"><i class="fa fa-plus"></i></span>
				<input class="form-control" type="text" id="uj_parameter" name="uj_parameter" placeholder="Új paraméter">
			</div>
		</div></div>';
		print '<div class="col-md-5"><div class="form-group">
			<label>Hozzáadás</label>
			<div class="input-group">
				<span class="input-group-addon input_jelolo_nsarga"><i class="fa fa-folder-open"></i></span>
				<select onChange="parameterOpcioSelect()" class="form-control" id="parmeter_hozzaadas" attr_termek_id="'.$_GET['id'].'">
					<option value="0"> - Válassz - </option>';
					$query_param_valaszto = "SELECT * FROM ".$webjel."term_csoportok_parameter GROUP BY nev ORDER BY nev ASC";
					foreach ($pdo->query($query_param_valaszto) as $row_param_valaszto)
					{
						print '<option value="'.$row_param_valaszto['nev'].'" >'.$row_param_valaszto['nev'].'</option>';
					}
				print '</select>
			</div>
		</div></div>
		<div class="col-md-7"><div class="form-group">
			<label>Érték</label>
			<div class="input-group">
				<input class="form-control" type="text" id="uj_parameter_ertek" name="uj_parameter_ertek" placeholder="Paraméter értéke">
			</div>
		</div></div>		
	</div>';

	print '<div class="form-group">
		<div class="input-group">
			<a onClick="uj_parameter_hozzaadasa_mentese('.$_GET['id'].')" class="btn btn-primary" style="width: 100%;">Új paraméter hozzáadása/mentése</a>
		</div>
	</div>';
?>
	<div class="table-responsive">
		<table class="table table-hover table-bordered table-striped margbot0">
			<tbody>
				<tr>
					<th>Rögzített paraméterek</th>
					<th class="text-right">Érték</th>
					<th></th>
				</tr>
				<?php
					$query_param = "SELECT 
							pe.ertek,
							pe.id,
							p.nev
						FROM ".$webjel."term_csoportok_parameter_ertek pe
						INNER JOIN ".$webjel."term_csoportok_parameter p
						ON p.id = pe.param_id 
						WHERE p.csop_id=".$_GET['id']." 
						ORDER BY p.nev ASC, pe.ertek ASC";
					$pm = '';
					foreach ($pdo->query($query_param) as $row_param)
					{
						echo '<tr>
							<td>';
								if($row_param['nev'] != $pm) { echo $row_param['nev']; $pm = $row_param['nev']; }
							echo '</td>
							<td class="text-right">'.$row_param['ertek'].'</td>
							';

							echo '<td style="width: 38px;"><a onClick="param_ertek_torles2('.$_GET['id'].', '.$row_param['id'].')" class="btn fa fa-times" style="padding: 2px 4px;" ></a></td>
						</tr>';
					}
				?>
			</tbody>
		</table>
	</div>


