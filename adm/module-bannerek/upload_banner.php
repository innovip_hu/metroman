<?php
	header("Access-Control-Allow-Origin: *"); 
	header('Content-type: application/json');
	
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsol�dni az adatb�zishoz!");
	}
	
	$ds = DIRECTORY_SEPARATOR;
	$dir = $gyoker.'/images/termekek/';
	
	// K�p m�sol�sa mapp�ba
	if (!empty($_FILES)) {
		$tempFile = $_FILES['file']['tmp_name'];                    
		$targetPath = $dir . $ds;
		$file = $_FILES['file']['name'];
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		$fn = 'banner-'.time().rand(0,999999999999).'.'.$ext; // ez a j�
		$targetFile =  $targetPath. $fn; 
		move_uploaded_file($tempFile,$targetFile);
		$array = array("img"=>$targetPath.$fn);
		echo json_encode($array);
		
		// Adatb�zidba t�lt�s
		$insertcommand = "INSERT INTO ".$webjel."banner (kep) VALUES (:kep)";
		$result = $pdo->prepare($insertcommand);
		$result->execute(array(':kep'=>$fn));
	}
?>  