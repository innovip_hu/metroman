<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	else
	{
		// "Csak adminisztrátorok" session törlése
		unset($_SESSION['felhasznalok_csak_admin']);
	}
	
	if(isset($_GET['command']) && $_GET['command'] == 'csakadmin')
	{
		if($_GET['admin_szuro'] == 0)
		{
			$_SESSION['felhasznalok_csak_admin'] = 1;
		}
		else
		{
			unset($_SESSION['felhasznalok_csak_admin']);
		}
	}
	
	if(isset($_POST['command']) && $_POST['command'] == 'uj_felhasznalo')
	{
		if($_POST['admin'] == 'true')
		{
			$admin = 'admin';
		}
		else
		{
			$admin = '';
		}
		
		$insertcommand = "INSERT INTO ".$webjel."users (vezeteknev, felhnev, email, telefon, tipus, cim_varos, cim_utca, cim_hszam, cim_irszam, cim_szall_nev, cim_szall_varos, cim_szall_utca, cim_szall_hszam, cim_szall_irszam, reg_datum) VALUES (:vezeteknev, :felhnev, :email, :telefon, :tipus, :cim_varos, :cim_utca, :cim_hszam, :cim_irszam, :cim_szall_nev, :cim_szall_varos, :cim_szall_utca, :cim_szall_hszam, :cim_szall_irszam, NOW())";
		$result = $pdo->prepare($insertcommand);
		$result->execute(array(':vezeteknev'=>$_POST['nev'],
						':felhnev'=>$_POST['felhnev'],
						':email'=>$_POST['email'],
						':telefon'=>$_POST['telefon'],
						':tipus'=>$admin,
						':cim_varos'=>$_POST['cim_varos'],
						':cim_utca'=>$_POST['cim_utca'],
						':cim_hszam'=>$_POST['cim_hszam'],
						':cim_irszam'=>$_POST['cim_irszam'],
						':cim_szall_nev'=>$_POST['cim_szall_nev'],
						':cim_szall_varos'=>$_POST['cim_szall_varos'],
						':cim_szall_utca'=>$_POST['cim_szall_utca'],
						':cim_szall_hszam'=>$_POST['cim_szall_hszam'],
						':cim_szall_irszam'=>$_POST['cim_szall_irszam']));
	}
	
	$csakadmin_where = '';
	$csakadmin_where_count = '';
	if(isset($_SESSION['felhasznalok_csak_admin']) && $_SESSION['felhasznalok_csak_admin'] == 1)
	{
		$csakadmin_where = " WHERE ".$webjel."users.tipus='admin' ";
		$csakadmin_where_count = " WHERE tipus='admin' ";
	}
	
// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 50; //ALAPÁLLAPOT
	}
// Rekordok száma
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."users ".$csakadmin_where_count);
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'vezeteknev'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'ASC'; // Alap rendezési feltétel
	}
?>
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1>Felhasználók</h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Felhasználók</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body no-padding with-border">
						<div class="mailbox-controls">
							<div class="btn-group">
								<div class="has-feedback">
									<input id="tabla_kereso" type="text" class="form-control input-sm" placeholder="Kereső" onkeyup="tablaAutoComplete()" autocomplete="off" >
									<span class="glyphicon glyphicon-search form-control-feedback"></span>
								</div>
							</div>
							<span class="menu_gomb"><a href=""><i class="fa fa-close"></i> Keresés törlése</span></a>
						</div>
						<div class="table-responsive">
							<table class="table table-hover table-striped margbot0">
								<tbody>
									<tr>
										<th>Név</th>
										<th>Email</th>
										<th>Telefon</th>
										<th>Cím</th>
										<th style="text-align:right;">Vásárlások</th>
									</tr>
									<?php
									$query = "SELECT ".$webjel."users.vezeteknev, ".$webjel."users.email, ".$webjel."users.telefon, ".$webjel."users.cim_irszam, ".$webjel."users.cim_utca, ".$webjel."users.cim_varos, ".$webjel."users.cim_hszam,".$webjel."users.id as id, SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar > 0, ".$webjel."rendeles_tetelek.term_akcios_ar * ".$webjel."rendeles_tetelek.term_db, ".$webjel."rendeles_tetelek.term_ar * ".$webjel."rendeles_tetelek.term_db)) as rendelt_osszeg 
									FROM ".$webjel."users 
									LEFT JOIN ".$webjel."rendeles 
									ON ".$webjel."users.id = ".$webjel."rendeles.user_id AND ".$webjel."rendeles.noreg = 0 
									LEFT JOIN ".$webjel."rendeles_tetelek 
									ON ".$webjel."rendeles.id = ".$webjel."rendeles_tetelek.rendeles_id 
									WHERE ".$webjel."users.vezeteknev LIKE '%".$_GET['tabla_kereso']."%' OR ".$webjel."users.email LIKE '%".$_GET['tabla_kereso']."%' OR ".$webjel."users.telefon LIKE '%".$_GET['tabla_kereso']."%' OR CONCAT(cim_irszam, ' ', cim_varos, ', ', cim_utca, ' ', cim_hszam) LIKE '%".$_GET['tabla_kereso']."%' 
									GROUP BY ".$webjel."users.id
									ORDER BY ".$webjel."users.".$sorr_tip." ".$sorrend." LIMIT ".$kezd.",".$oldalszam;
									foreach ($pdo->query($query) as $row)
									{
										?>
										<tr class="kattintos_sor" onClick="Belepes('module-felhasznalok/felhasznalo.php', 'module-felhasznalok/lista.php', <?php print $row['id']; ?>)">
										<?php
										print '<td>'.$row['vezeteknev'].'</td>
											<td>'.$row['email'].'</td>
											<td>'.$row['telefon'].'</td>
											<td>'.$row['cim_irszam'].' '.$row['cim_varos'].', '.$row['cim_utca'].' '.$row['cim_hszam'].'</td>
											<td style="text-align:right;">';
												if($row['rendelt_osszeg'] == '')
												{
													print '0';
												}
												else
												{
													print number_format($row['rendelt_osszeg'], 0, ',', ' ');
												}
											print ' Ft</td>
										</tr>';
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<input type="hidden" name="oldalszam" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<?php print $kezd;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<?php print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<?php print $sorrend;?>"/>
