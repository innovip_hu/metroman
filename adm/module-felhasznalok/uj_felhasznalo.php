<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
?>
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1>Felhasználók<small>Új felhasználó</small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li><a onClick="ujVissza('<?php print $_GET['fajl']; ?>')"><i class="fa fa-user"></i> Felhasználók</a></li>
		<li class="active">Új felhasználó</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- Személyes adatok -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Személyes adatok</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div id="riaszt_nev"></div>
								<div class="form-group">
									<label>Név</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-user"></i></span>
										<input type="text" class="form-control" id="nev" placeholder="Felhasználó neve" value="">
									</div>
								</div>
								<div id="riaszt_email"></div>
								<div class="form-group">
									<label for="exampleInputEmail1">Email cím</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-envelope"></i></span>
										<input type="email" class="form-control" id="email" placeholder="Felhasználó email címe" value="">
									</div>
								</div>
								<div id="riaszt_jelszo"></div>
								<div class="form-group">
									<label for="exampleInputPassword1">Jelszó</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-lock"></i></span>
										<input type="password" class="form-control" id="jelszo1" placeholder="Felhasználó jelszava" value="">
									</div>
								</div>
								<div id="riaszt_jelszo2"></div>
								<div class="form-group">
									<label for="exampleInputPassword1">Jelszó ismét</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-lock"></i></span>
										<input type="password" class="form-control" id="jelszo2" placeholder="Felhasználó jelszava" value="">
									</div>
								</div>
								<div class="form-group">
									<label>Telefonszám</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-phone"></i></span>
										<input type="text" class="form-control" id="telefon" placeholder="Felhasználó telefonszáma" value="">
									</div>
								</div>
								<div class="checkbox margtop40">
									<label>
										<input type="checkbox" class="minimal" id="admin" > Adminisztrátor
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Számlázási név</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<input type="text" class="form-control" id="szla_nev" placeholder="Számlázási név" value="">
									</div>
								</div>								
								<div class="form-group">
									<label>Város</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<input type="text" class="form-control" id="cim_varos" placeholder="Város" value="">
									</div>
								</div>
								<div class="form-group">
									<label>Utca</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<input type="text" class="form-control" id="cim_utca" placeholder="Utca" value="" >
									</div>
								</div>
								<div class="form-group">
									<label>Házszám</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<input type="text" class="form-control" id="cim_hszam" placeholder="Házszám" value="">
									</div>
								</div>
								<div class="form-group">
									<label>Irányítószám</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<input type="text" class="form-control" id="cim_irszam" placeholder="Irányítószám" value="" >
									</div>
								</div>
								<div class="form-group">
									<label>Adószám</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
										<input type="text" class="form-control" id="adoszam" placeholder="Adószám" value="" >
									</div>
								</div>								
								<div class="form-group">
									<label>Szállítási név</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-truck"></i></span>
										<input type="text" class="form-control" id="cim_szall_nev" placeholder="Szállítási név" value="">
									</div>
								</div>
								<div class="form-group">
									<label>Város (szállítási cím)</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-truck"></i></span>
										<input type="text" class="form-control" id="cim_szall_varos" placeholder="Város (szállítási cím)" value="">
									</div>
								</div>
								<div class="form-group">
									<label>Utca (szállítási cím)</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-truck"></i></span>
										<input type="text" class="form-control" id="cim_szall_utca" placeholder="Utca (szállítási cím)" value="" >
									</div>
								</div>
								<div class="form-group">
									<label>Házszám (szállítási cím)</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-truck"></i></span>
										<input type="text" class="form-control" id="cim_szall_hszam" placeholder="Házszám (szállítási cím)" value="">
									</div>
								</div>
								<div class="form-group">
									<label>Irányítószám (szállítási cím)</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-truck"></i></span>
										<input type="text" class="form-control" id="cim_szall_irszam" placeholder="Irányítószám (szállítási cím)" value="" >
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="box-footer">
						<button type="submit" onClick="ujMentes('<?php print $_GET['fajl']; ?>')" class="btn btn-primary">Mentés</button>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
