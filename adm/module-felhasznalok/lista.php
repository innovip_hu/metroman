<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	if(isset($_GET['command']) && $_GET['command'] == 'csakadmin')
	{
		if($_GET['admin_szuro'] == '')
		{
			unset($_SESSION['szuro']);
		}
		else
		{
			$_SESSION['szuro'] = $_GET['admin_szuro'];
		}
	}
	
	if(isset($_POST['command']) && $_POST['command'] == 'uj_felhasznalo')
	{
		if($_POST['admin'] == 'true')
		{
			$admin = 'admin';
		}
		else
		{
			$admin = 'user';
		}

		$insertcommand = "INSERT INTO ".$webjel."users (vezeteknev, email, telefon, tipus, cim_varos, cim_utca, cim_hszam, cim_irszam, cim_szall_nev, cim_szall_varos, cim_szall_utca, cim_szall_hszam, cim_szall_irszam, reg_datum, jelszo, adoszam, szla_nev) VALUES (:vezeteknev, :email, :telefon, :tipus, :cim_varos, :cim_utca, :cim_hszam, :cim_irszam, :cim_szall_nev, :cim_szall_varos, :cim_szall_utca, :cim_szall_hszam, :cim_szall_irszam, NOW(), :jelszo, :adoszam, :szla_nev)";
		$result = $pdo->prepare($insertcommand);
		$result->execute(array(':vezeteknev'=>$_POST['nev'],
						':email'=>$_POST['email'],
						':telefon'=>$_POST['telefon'],
						':tipus'=>$admin,
						':adoszam'=>$_POST['adoszam'],
						':szla_nev'=>$_POST['szla_nev'],
						':cim_varos'=>$_POST['cim_varos'],
						':cim_utca'=>$_POST['cim_utca'],
						':cim_hszam'=>$_POST['cim_hszam'],
						':cim_irszam'=>$_POST['cim_irszam'],
						':cim_szall_nev'=>$_POST['cim_szall_nev'],
						':cim_szall_varos'=>$_POST['cim_szall_varos'],
						':cim_szall_utca'=>$_POST['cim_szall_utca'],
						':cim_szall_hszam'=>$_POST['cim_szall_hszam'],
						':cim_szall_irszam'=>$_POST['cim_szall_irszam'],
						':jelszo'=>password_hash($_POST['jelszo1'], PASSWORD_DEFAULT)));
	}
	
	$csakadmin_where = '';
	$csakadmin_where_count = '';
	if(isset($_SESSION['szuro']))
	{
		if ($_SESSION['szuro'] == 'admin')
		{
			$csakadmin_where = " WHERE ".$webjel."users.tipus='admin' ";
			$csakadmin_where_count = " WHERE tipus='admin' ";
		}
		else
		{
			$csakadmin_where = " WHERE ".$webjel."users.arlista_id='".$_SESSION['szuro']."'";
			$csakadmin_where_count = " WHERE arlista_id='".$_SESSION['szuro']."' ";			
		}
	}
	
// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 50; //ALAPÁLLAPOT
	}
// Rekordok száma
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."users ".$csakadmin_where_count);
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'vezeteknev'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'ASC'; // Alap rendezési feltétel
	}
?>
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1>Felhasználók</h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Felhasználók</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body no-padding with-border">
						<div class="mailbox-controls">
							<div class="btn-group">
								<div class="has-feedback">
									<input id="tabla_kereso" type="text" class="form-control input-sm" placeholder="Kereső" onkeyup="tablaAutoComplete()" autocomplete="off" >
									<span class="glyphicon glyphicon-search form-control-feedback"></span>
								</div>
							</div>
							<div class="btn-group">
								<select class="form-control input-sm" id="user_szuro" name="user_szuro" onchange="csakAdmin(this.value)">
									<option value=""> - Mind - </option>
									<option value="admin" <?php if (isset($_SESSION['szuro']) && $_SESSION['szuro'] == 'admin') {echo 'selected';} ?>>Adminisztrátor</option>
									<?php 
										$query = "SELECT a.id,a.nev FROM ".$webjel."users u INNER JOIN ".$webjel."arlista a ON a.id = u.arlista_id GROUP BY a.id ORDER BY a.nev";
										foreach ($pdo->query($query) as $key => $value)
										{
											$selected = '';
											if (isset($_SESSION['szuro']) && $_SESSION['szuro'] == $value['id'])
											{
												$selected = 'selected';
											}
											echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['nev'].'</option>';		
										}
									?>									
								</select>	
							</div>								
							<span class="menu_gomb"><a onClick="uj('module-felhasznalok/uj_felhasznalo.php', 'module-felhasznalok/lista.php')"><i class="fa fa-plus"></i> Új felhasználó</span></a>
							<a href="gdpr_export.php"><span class="menu_gomb"><i class="fa fa-cloud-download"></i>GDPR Export</span></a>
							<div class="pull-right">
								<?php
									if($oldalszam > $rownum) $oldalszam_mod = $rownum;
									else $oldalszam_mod = $oldalszam;
									$vege = $kezd+$oldalszam_mod;
									if($vege > $rownum) $vege = $rownum;
									print ($kezd+1).'-'.$vege.'/'.$rownum;
								?>
								<div class="btn-group">
								<?php
									if ($kezd != 0) // Lapozás vissza (balra)
									{
										?>
										<button onclick="lapozasVissza('module-felhasznalok/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
										<?php
									}
									if (($utolso_oldal / $aktualis_oldal) != 1) // Lapozás előre (jobbra)
									{
										?>
										<button onclick="lapozasTovabb('module-felhasznalok/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
										<?php
									}
								?>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table table-hover table-striped margbot0">
								<tbody>
									<tr>
										<th>Név</th>
										<th>Email</th>
										<th>Telefon</th>
										<th>Cím</th>
										<th style="text-align:right;">Vásárlások</th>
									</tr>
									<?php
									$query = "SELECT ".$webjel."users.vezeteknev, ".$webjel."users.email, ".$webjel."users.telefon, ".$webjel."users.cim_irszam, ".$webjel."users.cim_utca, ".$webjel."users.cim_varos, ".$webjel."users.cim_hszam,".$webjel."users.id as id, SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar > 0, ".$webjel."rendeles_tetelek.term_akcios_ar * ".$webjel."rendeles_tetelek.term_db, ".$webjel."rendeles_tetelek.term_ar * ".$webjel."rendeles_tetelek.term_db)) as rendelt_osszeg 
									FROM ".$webjel."users 
									LEFT JOIN ".$webjel."rendeles 
									ON ".$webjel."users.id = ".$webjel."rendeles.user_id AND ".$webjel."rendeles.noreg = 0 
									LEFT JOIN ".$webjel."rendeles_tetelek 
									ON ".$webjel."rendeles.id = ".$webjel."rendeles_tetelek.rendeles_id 
									".$csakadmin_where."
									GROUP BY ".$webjel."users.id
									ORDER BY ".$webjel."users.".$sorr_tip." ".$sorrend." LIMIT ".$kezd.",".$oldalszam;
									foreach ($pdo->query($query) as $row)
									{
										?>
										<tr class="kattintos_sor" onClick="Belepes('module-felhasznalok/felhasznalo.php', 'module-felhasznalok/lista.php', <?php print $row['id']; ?>)">
										<?php
										print '<td>'.$row['vezeteknev'].'</td>
											<td>'.$row['email'].'</td>
											<td>'.$row['telefon'].'</td>
											<td>'.$row['cim_irszam'].' '.$row['cim_varos'].', '.$row['cim_utca'].' '.$row['cim_hszam'].'</td>
											<td style="text-align:right;">';
												if($row['rendelt_osszeg'] == '')
												{
													print '0';
												}
												else
												{
													print number_format($row['rendelt_osszeg'], 0, ',', ' ');
												}
											print ' Ft</td>
										</tr>';
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer no-padding">
						<div class="mailbox-controls">
							<div class="pull-right">
								<?php
									print ($kezd+1).'-'.$vege.'/'.$rownum;
								?>
								<div class="btn-group">
								<?php
									if ($kezd != 0) // Lapozás vissza (balra)
									{
										?>
										<button onclick="lapozasVissza('module-felhasznalok/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
										<?php
									}
									if (($utolso_oldal / $aktualis_oldal) != 1) // Lapozás előre (jobbra)
									{
										?>
										<button onclick="lapozasTovabb('module-felhasznalok/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
										<?php
									}
								?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<input type="hidden" name="oldalszam" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<?php print $kezd;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<?php print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<?php print $sorrend;?>"/>
