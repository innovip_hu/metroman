<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		include '../config_adm.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}

	if(isset($_POST['command']) && $_POST['command'] =='uj_kapcsolat')
	{
		$pdo->exec("UPDATE ".$webjel."beallitasok SET kapcsolat='".$_POST['kapcsolat']."' WHERE id=1");
	}

	$query = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();	
?>
	<div class="form-group">
		<label>Tartalom</label>
		<textarea class="form-control" id="kapcsolat" rows="6" placeholder="Tartalom"><?php print $row['kapcsolat']; ?></textarea>
	</div>
