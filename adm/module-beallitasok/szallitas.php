<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		include '../config_adm.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	if(isset($_POST['command']) && $_POST['command'] == 'szallitasi_arak')
	{
		$pdo->exec("UPDATE ".$webjel."beallitasok SET szallitas_afa=".$_POST['szallitas_afa']." WHERE id=1");
		$query = "SELECT * FROM ".$webjel."kassza_szall_mod";
		foreach ($pdo->query($query) as $row)
		{
			$updatecommand = "UPDATE ".$webjel."kassza_szall_mod SET ingyenes_szallitas=?, leiras=?, leiras_hosszu=? WHERE id=?";
			$result = $pdo->prepare($updatecommand);
			$result->execute(array($_POST['ingyenes_szallitas_'.$row['id']],$_POST['leiras_'.$row['id']],$_POST['leiras_hosszu_'.$row['id']],$row['id']));
		}
	}

	if (isset($_POST['command']) && $_POST['command'] == 'uj_sav_rogzites')
	{
		$insertcommand = "INSERT INTO ".$webjel."kassza_szall_mod_savok (szall_mod_id,sav,ar,ar_utanvet) VALUES (:szall_mod_id,:sav,:ar,:ar_utanvet)";
		$result = $pdo->prepare($insertcommand);
		$result->execute(array(
							':szall_mod_id'=>$_POST['szall_id'],
							':sav'=>$_POST['sav_'.$_POST['szall_id']],
						  	':ar'=>$_POST['ar_'.$_POST['szall_id']],
						  	':ar_utanvet'=>$_POST['ar_utanvet_'.$_POST['szall_id']]));
	}
	
	if (isset($_POST['command']) && $_POST['command'] == 'sav_mentese')
	{
		$updatecommand = "UPDATE ".$webjel."kassza_szall_mod_savok SET sav=?, ar=?,ar_utanvet=? WHERE id=?";
		$result = $pdo->prepare($updatecommand);
		$result->execute(array($_POST['sav_mentes_'.$_POST['sav_id']],$_POST['ar_mentes_'.$_POST['sav_id']],$_POST['ar_utanvet_mentes_'.$_POST['sav_id']],$_POST['sav_id']));
	}

	if (isset($_POST['command']) && $_POST['command'] == 'sav_torles')
	{
		$deletecommand = "DELETE FROM ".$webjel."kassza_szall_mod_savok WHERE id =".$_POST['sav_id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();
	}	

	$query = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	$szallitas_afa = $row['szallitas_afa'];
?>
	<form id="szallitas_form">
	<?php
		$query = "SELECT * FROM ".$webjel."kassza_szall_mod ORDER BY sorrend ASC";
		foreach ($pdo->query($query) as $row)
		{
			if ($row['lathato'] == 0)
			{
				$display = 'style="display:none;"';
			}
			else
			{
				$display = '';	
			}
			echo '<div class="row" '.$display.'>
				<div class="col-md-12 text-center"><h5><b>'.$row['nev'].'</b></h5></div>';	

				echo '<div class="col-md-4"><div class="form-group">
					<label>Új sáv</label>
					<div class="input-group">
						<span class="input-group-addon input_jelolo_kek"><i class="fa fa-plus"></i></span>
						<input type="text" class="form-control" name="sav_'.$row['id'].'" placeholder="Sáv kezdő értéke" value="0">
						<span class="input-group-addon">Ft</span>
					</div>
				</div></div>			
				<div class="col-md-4"><div class="form-group">
					<label>Ár</label>
					<div class="input-group">
						<span class="input-group-addon input_jelolo_kek"><i class="fa fa-truck"></i></span>
						<input type="text" class="form-control" name="ar_'.$row['id'].'" value="0">
						<span class="input-group-addon">Ft</span>
					</div>
				</div></div>
				<div class="col-md-4"><div class="form-group">
					<label>utánvét esetén</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-truck"></i></span>
						<input type="text" class="form-control" name="ar_utanvet_'.$row['id'].'" value="0">
						<span class="input-group-addon">Ft</span>
					</div>
				</div></div><div class="col-md-12 text-center"><a class="btn btn-primary btn-xs" onclick="ujSav('.$row['id'].')">Hozzáadás</a></div>';


				$query = "SELECT * FROM ".$webjel."kassza_szall_mod_savok WHERE szall_mod_id=".$row['id']." ORDER BY sav ASC";
				foreach ($pdo->query($query) as $value)
				{
					echo '<div class="col-md-4"><div class="form-group">
						<label>Ártól</label>
						<div class="input-group">
							<div class="input-group-btn">
                      			<button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Művelet <span class="fa fa-caret-down"></span></button>
                 				<ul class="dropdown-menu">
                    				<li><a onclick="mentesSav('.$value['id'].')">Mentés</a></li>
                        			<li class="divider"></li>
                        			<li><a onclick="torlesSav('.$value['id'].')">Törlés</a></li>
                      			</ul>
                    		</div>						
							<input type="text" class="form-control" name="sav_mentes_'.$value['id'].'" placeholder="Sáv kezdő értéke" value="'.$value['sav'].'">
							<span class="input-group-addon">Ft</span>
						</div>
					</div></div>			
					<div class="col-md-4"><div class="form-group">
						<label>Ár</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-truck"></i></span>
							<input type="text" class="form-control" name="ar_mentes_'.$value['id'].'" value="'.$value['ar'].'">
							<span class="input-group-addon">Ft</span>
						</div>
					</div></div>
					<div class="col-md-4"><div class="form-group">
						<label>utánvét esetén</label>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-truck"></i></span>
							<input type="text" class="form-control" name="ar_utanvet_mentes_'.$value['id'].'" value="'.$value['ar_utanvet'].'">
							<span class="input-group-addon">Ft</span>
						</div>
					</div></div>';					
				}

			echo '
				<div class="col-md-12"><div class="form-group">
					<label>Ingyenes szállítás <span class="small">(ha az érték 0, akkor nincs ingyenes szállítás)</span></label>
					<div class="input-group">
						<span class="input-group-addon input_jelolo_nsarga"><i class="fa fa-star"></i></span>
						<input type="text" class="form-control" name="ingyenes_szallitas_'.$row['id'].'" placeholder="Ingyenes szállítás" value="'.$row['ingyenes_szallitas'].'">
						<span class="input-group-addon">Ft</span>
					</div>
				</div></div>			
				<div class="col-md-12"><div class="form-group">
					<label>Leírás kassza</label>
					<textarea name="leiras_'.$row['id'].'" class="form-control" rows="3" placeholder="Leírás">'.$row['leiras'].'</textarea>
				</div></div>
				<div class="col-md-12"><div class="form-group">
					<label>Leírás hosszú</label>
					<textarea name="leiras_hosszu_'.$row['id'].'" class="form-control" rows="3" placeholder="Leírás">'.$row['leiras_hosszu'].'</textarea>
				</div></div>				
				<div class="col-md-12">
					<button type="button" onClick="mentesSzallitas()" class="btn btn-primary">Mentés</button>
				<hr>
				</div>							
			</div>';
		}
	?>
	<div class="form-group">
		<label>Szállítási költség ÁFA</label>
		<div class="input-group">
			<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
			<input type="text" class="form-control" name="szallitas_afa" placeholder="Szállítási költség ÁFA" value="<?= $szallitas_afa ?>">
			<span class="input-group-addon">%</span>
		</div>
	</div>

	</form>
