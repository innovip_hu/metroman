<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		include '../config_adm.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}

	if(isset($_POST['command']) && $_POST['command'] =='beszallito_mentes')
	{
		$query = "SELECT * FROM ".$webjel."beszallito";
		foreach ($pdo->query($query) as $row)
		{
			$updatecommand = "UPDATE ".$webjel."beszallito SET hatarido=? WHERE id=?";
			$result = $pdo->prepare($updatecommand);
			$result->execute(array($_POST['beszallito_'.$row['id']],$row['id']));
		}
	}
?>
	
	<form id="beszallito_form">

<?php	 

	$volt_tetel = 0;
	$query = "SELECT * FROM ".$webjel."beszallito ORDER BY nev ASC";
	foreach ($pdo->query($query) as $row)
	{
		$volt_tetel = 1;
		?>
		<div class="form-group">
			<label><?=$row['nev']?></label>
			<div class="input-group">
				<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
				<input type="text" class="form-control" name="beszallito_<?php print $row['id']; ?>" placeholder="Szállítási határidő (<?=$row['nev']?>)" value="<?php print $row['hatarido']; ?>">
			</div>
		</div>
		<?php
	}

	if ($volt_tetel == 0)
	{
		echo '<p>Még nincs szinkronból átvett beszállító!</p>';
	}
?>

	</form>