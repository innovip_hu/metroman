<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		include '../config_adm.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}

	if(isset($_POST['command']) && $_POST['command'] =='uj_cim')
	{
		$pdo->exec("UPDATE ".$webjel."beallitasok SET kiemelt_szekcio='".$_POST['kiemelt_szekcio']."' WHERE id=1");
	}

	$query = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();	
?>
	<div class="form-group">
		<label>Cím módosítása</label>
		<div class="input-group">
			<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
			<input type="text" class="form-control" id="kiemelt_adat" placeholder="Új cím" value="<?=$row['kiemelt_szekcio']?>">
		</div>
	</div>
