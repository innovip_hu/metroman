<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		include '../config_adm.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}

	$dir = $gyoker.'/hirek/css/';

	if (isset($_POST['command']) && $_POST['command'] == 'css_feltoltes') {
		if (isset($_FILES['css_fajl'])) {

			$tempFile = $_FILES['css_fajl']['tmp_name'];                    
			$targetPath = $dir;
			$fn = 'hirek.css';
			$targetFile =  $targetPath. $fn; 
			move_uploaded_file($tempFile,$targetFile);			
		}
	}

?>
	<form action="" enctype="multipart/form-data" id="css_form">

		<div class="form-group">
			<label>Fájl</label>
			<div class="input-group">
				<span class="input-group-addon input_jelolo_kek"><i class="fa fa-paperclip"></i></span>
				<input type="file" class="form-control" id="css_fajl" name="css_fajl" accept=".css">
			</div>
		</div>

		<input type="hidden" name="command" value="css_feltoltes">
	</form>

	<button type="button" class="btn btn-primary" onclick="cssFeltoltes()">Feltöltés</button>

	<?php

	if (file_exists($gyoker.'/hirek/css/hirek.css')) {
		echo '<p class="margtop20"><b>hirek.css</b> utolsó frisítés: '.date('Y-m-d H:i:s',filemtime($gyoker.'/hirek/css/hirek.css')).'</p>';
	}