<?php
	$datum = date("Y-m-d");
	
	$query_beall = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
	$res = $pdo->prepare($query_beall);
	$res->execute();
	$row_beall = $res -> fetch();

	$oldalleiras = $description;

// Facebook_Catalog

	$xml = new DOMDocument('1.0', 'UTF-8');
	$xml->preservWhiteSpace = false;
	$xml->formatOutput = true; //nem egy sorba formáz
	$rss = $xml->createElement("rss"); 
	$rss_node = $xml->appendChild($rss);
	
	$rss_node->setAttribute("xmlns:g","http://base.google.com/ns/1.0"); //kötelező mező
	$rss_node->setAttribute("version","2.0");
	
	$channel = $xml->createElement("channel");  
	$channel_node = $rss_node->appendChild($channel);

	$titl = $xml->createElement("title"); 
	$titl_node = $channel_node->appendChild($titl);
	$szoveg = $xml->createTextNode($title);
	$titl->appendChild($szoveg);

	$lin = $xml->createElement("link");
	$lin_node = $channel_node->appendChild($lin);
	$szoveg = $xml->createTextNode($domain);
	$lin ->appendChild($szoveg);

	$desc = $xml->createElement("description");
	$desc_node = $channel_node->appendChild($desc);
	$szoveg = $xml->createTextNode($oldalleiras);
	$desc->appendChild($szoveg);	

	//ITEMEK - termékek

	$query = "SELECT *,".$webjel."termekek.nev AS nev,".$webjel."term_csoportok.nev AS katnev,".$webjel."termekek.id AS id,".$webjel."termekek.nev_url AS nev_url,".$webjel."term_csoportok.nev_url AS kat_nev_url
		FROM ".$webjel."termekek 
		INNER JOIN ".$webjel."term_csoportok 
		ON ".$webjel."termekek.csop_id = ".$webjel."term_csoportok.id 
 		WHERE ".$webjel."termekek.lathato = 1 AND ".$webjel."termekek.rendelheto = 1";
	foreach ($pdo->query($query) as $row)
	{

			$item = $xml->createElement("item");
			$item_node = $channel_node->appendChild($item);

			$g_id = $xml->createElement("g:id");
			$g_id_node = $item_node->appendChild($g_id);
			$szoveg = $xml->createTextNode($row['id']);
			$g_id->appendChild($szoveg);

			$g_title = $xml->createElement("g:title");
			$g_title_node = $item_node->appendChild($g_title);
			$szoveg = $xml->createTextNode($row['nev']);
			$g_title->appendChild($szoveg);

			$g_description = $xml->createElement("g:description");
			$g_description_node = $item_node->appendChild($g_description);
			$szoveg = $xml->createTextNode(strip_tags($row['rovid_leiras']));
			$g_description->appendChild($szoveg);

			$g_link = $xml->createElement("g:link");
			$g_link_node = $item_node->appendChild($g_link);
			$szoveg = $xml->createTextNode($domain."/termekek/".$row['kat_nev_url']."/".$row['nev_url']);
			$g_link->appendChild($szoveg);


		//KÉP

			$g_image_link = $xml->createElement("g:image_link");
			$g_image_link_node = $item_node->appendChild($g_image_link);

				$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." AND spec = 0 ORDER BY alap DESC LIMIT 1";
				$res = $pdo->prepare($query_kep);
				$res->execute();
				$row_kep = $res -> fetch();
				$alap_kep = $row_kep['kep'];
				if ($alap_kep != '')
				{
					$szoveg = $xml->createTextNode($domain.'/images/termekek/'.$row_kep['kep']);
					$g_image_link->appendChild($szoveg);
				}

			$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." AND alap = 0 AND spec = 0 AND kep != '".$alap_kep."'";
			foreach ($pdo->query($query_kep) as $key => $value)
			{
				$additional_image_link = $xml->createElement("additional_image_link");
				$additional_image_link_node = $item_node->appendChild($additional_image_link);	
				$szoveg = $xml->createTextNode($domain.'/images/termekek/'.$value['kep']);
				$additional_image_link->appendChild($szoveg);
			}						

			$g_brand = $xml->createElement("g:brand");
			$g_brand_node = $item_node->appendChild($g_brand);
			$szoveg = $xml->createTextNode($webnev);
			$g_brand->appendChild($szoveg);

			//ÁLLAPOT

			$g_condition = $xml->createElement("g:condition");
			$g_condition_node = $item_node->appendChild($g_condition);
			$szoveg = $xml->createTextNode("new");
			$g_condition->appendChild($szoveg);

			$g_availability = $xml->createElement("g:availability");
			$g_availability_node = $item_node->appendChild($g_availability);
			$szoveg = $xml->createTextNode("in stock");
			$g_availability->appendChild($szoveg);

			//ÁR

			$g_price = $xml->createElement("g:price");
			$g_price_node = $item_node->appendChild($g_price);


			 	$datum = date('Y-m-d');
				if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) {
					$szoveg = $xml->createTextNode($row['akciosar']." HUF");
				} else
				{
					$szoveg = $xml->createTextNode($row['ar']." HUF");
				}

			$g_price->appendChild($szoveg);

			$g_google_product_category = $xml->createElement("g:google_product_category");
			$g_google_product_category_node = $item_node->appendChild($g_google_product_category);
			$szoveg = $xml->createTextNode($row['katnev']);
			$g_google_product_category->appendChild($szoveg);
	}



	$xml->save($gyoker."/adm/xml_facebook.xml");

// Google merchant

	$xml = new DOMDocument('1.0', 'UTF-8');
	$xml->preservWhiteSpace = false;
	$xml->formatOutput = true; //nem egy sorba formáz
	$rss = $xml->createElement("rss"); 
	$rss_node = $xml->appendChild($rss);
	
	$rss_node->setAttribute("xmlns:g","http://base.google.com/ns/1.0"); //kötelező mező
	$rss_node->setAttribute("version","2.0");
	
	$channel = $xml->createElement("channel");  
	$channel_node = $rss_node->appendChild($channel);

	//ITEMEK - termékek

	$query = "SELECT *,".$webjel."termekek.nev AS nev,".$webjel."term_csoportok.nev AS katnev,".$webjel."termekek.id AS id,".$webjel."termekek.nev_url AS nev_url,".$webjel."term_csoportok.nev_url AS kat_nev_url, ".$webjel."termekek.csop_id as csop_azonosito
		FROM ".$webjel."termekek 
		INNER JOIN ".$webjel."term_csoportok 
		ON ".$webjel."termekek.csop_id = ".$webjel."term_csoportok.id 
 		WHERE ".$webjel."termekek.lathato = 1 AND ".$webjel."termekek.rendelheto = 1";
	foreach ($pdo->query($query) as $row)
	{

			$item = $xml->createElement("item");
			$item_node = $channel_node->appendChild($item);

			$g_id = $xml->createElement("g:id");
			$g_id_node = $item_node->appendChild($g_id);
			$szoveg = $xml->createTextNode($row['id']);
			$g_id->appendChild($szoveg);

			$g_title = $xml->createElement("title");
			$g_title_node = $item_node->appendChild($g_title);
			$szoveg = $xml->createTextNode($row['nev']);
			$g_title->appendChild($szoveg);

			$g_description = $xml->createElement("g:description");
			$g_description_node = $item_node->appendChild($g_description);
			$szoveg = $xml->createTextNode($row['rovid_leiras']);
			$g_description->appendChild($szoveg);

			$g_link = $xml->createElement("link");
			$g_link_node = $item_node->appendChild($g_link);
			$szoveg = $xml->createTextNode($domain."/termekek/".$row['kat_nev_url']."/".$row['nev_url']);
			$g_link->appendChild($szoveg);


		//KÉP

			$g_image_link = $xml->createElement("g:image_link");
			$g_image_link_node = $item_node->appendChild($g_image_link);

				$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." AND spec = 0 ORDER BY alap DESC LIMIT 1";
				$res = $pdo->prepare($query_kep);
				$res->execute();
				$row_kep = $res -> fetch();
				$alap_kep = $row_kep['kep'];
				if ($alap_kep != '')
				{
					$szoveg = $xml->createTextNode($domain.'/images/termekek/'.$row_kep['kep']);
					$g_image_link->appendChild($szoveg);
				}

			$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." AND alap = 0 AND spec = 0 AND kep != '".$alap_kep."'";
			foreach ($pdo->query($query_kep) as $key => $value)
			{
				$additional_image_link = $xml->createElement("additional_image_link");
				$additional_image_link_node = $item_node->appendChild($additional_image_link);	
				$szoveg = $xml->createTextNode($domain.'/images/termekek/'.$value['kep']);
				$additional_image_link->appendChild($szoveg);
			}		

			$g_availability = $xml->createElement("g:availability");
			$g_availability_node = $item_node->appendChild($g_availability);
			$szoveg = $xml->createTextNode("in stock");
			$g_availability->appendChild($szoveg);

			$g_price = $xml->createElement("g:price");
			$g_price_node = $item_node->appendChild($g_price);
			$szoveg = $xml->createTextNode($row['ar']." HUF");
			$g_price->appendChild($szoveg);

		 	$datum = date('Y-m-d');
			if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum)
			{
				$g_sale_price = $xml->createElement("g:sale_price");
				$g_sale_price_node = $item_node->appendChild($g_sale_price);
				$szoveg = $xml->createTextNode($row['akciosar']." HUF");
				$g_sale_price->appendChild($szoveg);
			}

			$query_csop = "SELECT t1.nev AS lev1, t2.nev as lev2, t3.nev as lev3, t4.nev as lev4
			FROM ".$webjel."term_csoportok AS t1
			LEFT JOIN ".$webjel."term_csoportok AS t2 ON t2.id = t1.csop_id
			LEFT JOIN ".$webjel."term_csoportok AS t3 ON t3.id = t2.csop_id
			LEFT JOIN ".$webjel."term_csoportok AS t4 ON t4.id = t3.csop_id
			WHERE t1.id = ".$row['csop_azonosito'];	
			$res = $pdo->prepare($query_csop);
			$res->execute();
			$row_csop = $res -> fetch();

			if (!is_null($row_csop['lev4']))
			{
				$csop_szoveg = $row_csop['lev4'];
			}
			elseif (!is_null($row_csop['lev3']))
			{
				$csop_szoveg = $row_csop['lev3'];
			}
			else
			{
				$csop_szoveg = $row_csop['lev2'];
			}			

			$g_product_type = $xml->createElement("g:product_type");
			$g_product_type_node = $item_node->appendChild($g_product_type);
			$szoveg = $xml->createTextNode($csop_szoveg);
			$g_product_type->appendChild($szoveg);

			$g_brand = $xml->createElement("g:brand");
			$g_brand_node = $item_node->appendChild($g_brand);
			$szoveg = $xml->createTextNode($row['katnev']);
			$g_brand->appendChild($szoveg);

			$g_ident = $xml->createElement("g:identifier_exists"); 
			$ident_node = $item_node->appendChild($g_ident);
			$szoveg = $xml->createTextNode("no");
			$g_ident->appendChild($szoveg);

	}

	$xml->save($gyoker."/adm/xml_google_merchant.xml");


// ÁRUKERESŐ
	$xml = new DOMDocument('1.0', 'UTF-8');
	
	$root = $xml->createElement("products");
	$xml->appendChild($root);

	$query = "SELECT *, ".$webjel."afa.afa as afa_szaz 
					, ".$webjel."termekek.id as id 
					, ".$webjel."termekek.leiras as leiras 
					, ".$webjel."termekek.nev as nev 
					, ".$webjel."termekek.kep as kep 
					, ".$webjel."termekek.nev_url as nev_url 
					, ".$webjel."term_csoportok.nev as csop_nev 
					, ".$webjel."term_csoportok.csop_id as csop_id 
					, ".$webjel."term_csoportok.nev_url as kat_nev_url 
	FROM ".$webjel."termekek
	INNER JOIN ".$webjel."afa 
	ON ".$webjel."termekek.afa=".$webjel."afa.id 
	INNER JOIN ".$webjel."term_csoportok 
	ON ".$webjel."termekek.csop_id=".$webjel."term_csoportok.id 
	WHERE ".$webjel."termekek.lathato=1 AND ".$webjel."termekek.rendelheto = 1";
	foreach ($pdo->query($query) as $row)
	{
		if($row['csop_id'] > 0)
		{
			$query_szulkat = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row['csop_id'];
			$res = $pdo->prepare($query_szulkat);
			$res->execute();
			$row_szulkat  = $res -> fetch();
			$kategoria = $row_szulkat['nev'].' > '.$row['csop_nev'];
		}
		else
		{
			$kategoria = $row['csop_nev'];
		}
		
			$id = $xml->createElement("sku");
			$idText = $xml->createTextNode($row['id']);
			$id->appendChild($idText);
		
			// $delivery_time = $xml->createElement("delivery_time");
			// $delivery_timeText = $xml->createTextNode($row['aruk_szall_ido']);
			// $delivery_time->appendChild($delivery_timeText);

			// $delivery_cost = $xml->createElement("delivery_cost");
			// $delivery_costText = $xml->createTextNode($row_beall['szall_kolts_utanvet']);
			// $delivery_cost->appendChild($delivery_costText);

			// $manufacturer = $xml->createElement("manufacturer");
			// $manufacturerText = $xml->createTextNode($row['gyarto']);
			// $manufacturer->appendChild($manufacturerText);

			$name = $xml->createElement("name");
			$nameText = $xml->createTextNode($row['nev']);
			$name->appendChild($nameText);

			$category = $xml->createElement("category");
			$categoryText = $xml->createTextNode($kategoria);
			$category->appendChild($categoryText);

			$datum = date("Y-m-d");
			if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) // Ha akciós
			{
				$ws_ar = $row['akciosar'];
			}
			else
			{
				$ws_ar = $row['ar'];
			}
			$price = $xml->createElement("price");
			$priceText = $xml->createTextNode($ws_ar);
			$price->appendChild($priceText);

			$net_price = $xml->createElement("net_price");
			$net_priceText = $xml->createTextNode(round($ws_ar/(1+($row['afa_szaz']/100))));
			$net_price->appendChild($net_priceText);

			$identifier = $xml->createElement("identifier");
			$identifierText = $xml->createTextNode($row['id']);
			$identifier->appendChild($identifierText);

			// $warranty = $xml->createElement("warranty");
			// $warrantyText = $xml->createTextNode($row['aruk_garancia']);
			// $warranty->appendChild($warrantyText);

			$product_url = $xml->createElement("product_url");
			$product_urlText = $xml->createTextNode($domain.'/termekek/'.$row['kat_nev_url'].'/'.$row['nev_url']);
			$product_url->appendChild($product_urlText);
			
			// Kép
			$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
			$res = $pdo->prepare($query_kep);
			$res->execute();
			$row_kep = $res -> fetch();
			$alap_kep = $row_kep['kep'];
			if ($alap_kep != '')
			{
				$image_url = $xml->createElement("image_url");
				$image_urlText = $xml->createTextNode($domain.'/images/termekek/'.$row_kep['kep']);
				$image_url->appendChild($image_urlText);
			}

			$product = $xml->createElement("product");
			$product->appendChild($id);
			// $product->appendChild($manufacturer);
			$product->appendChild($category);
			$product->appendChild($name);
			// $product->appendChild($delivery_time);
			// $product->appendChild($delivery_cost);
			$product->appendChild($price);
			$product->appendChild($net_price);
			$product->appendChild($identifier);
			// $product->appendChild($warranty);
			$product->appendChild($product_url);
			if ($alap_kep != '')
			{
				$product->appendChild($image_url);
			}

		$root->appendChild($product);
	}


	$xml->save($gyoker."/adm/xml_arukereso.xml");

// Olcsóbbat.hu
	$xml = new DOMDocument('1.0', 'UTF-8');
	
	$root = $xml->createElement("products");
	$xml->appendChild($root);

	foreach ($pdo->query($query) as $row)
	{
		if($row['csop_id'] > 0)
		{
			$query_szulkat = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row['csop_id'];
			$res = $pdo->prepare($query_szulkat);
			$res->execute();
			$row_szulkat  = $res -> fetch();
			$kategoria = $row_szulkat['nev'].' > '.$row['csop_nev'];
		}
		else
		{
			$kategoria = $row['csop_nev'];
		}
		
		$itemid = $xml->createElement("itemid");
		$itemidText = $xml->createTextNode($row['id']);
		$itemid->appendChild($itemidText);
	
		$id = $xml->createElement("id");
		$idText = $xml->createTextNode($row['id']);
		$id->appendChild($idText);

		$name = $xml->createElement("name");
		$nameText = $xml->createTextNode($row['nev']);
		$name->appendChild($nameText);

		$category = $xml->createElement("category");
		$categoryText = $xml->createTextNode($kategoria);
		$category->appendChild($categoryText);

		$datum = date("Y-m-d");
		if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) // Ha akciós
		{
			$ws_ar = $row['akciosar'];
		}
		else
		{
			$ws_ar = $row['ar'];
		}
		$price = $xml->createElement("grossprice");
		$priceText = $xml->createTextNode($ws_ar);
		$price->appendChild($priceText);

		if ($row['raktaron'] >0) // Ha raktáron van
		{
			$raktaron = 'true';
		}
		else
		{
			$raktaron = 'false';
		}
	
		// $delivery_time = $xml->createElement("deliverytime");
		// $delivery_timeText = $xml->createTextNode($row['aruk_szall_ido']);
		// $delivery_time->appendChild($delivery_timeText);

		// $manufacturer = $xml->createElement("manufacturer");
		// $manufacturerText = $xml->createTextNode($row['gyarto']);
		// $manufacturer->appendChild($manufacturerText);
		
		// $stock = $xml->createElement("stock");
		// $stockText = $xml->createTextNode($raktaron);
		// $stock->appendChild($stockText);

		$product_url = $xml->createElement("urlsite");
		$product_urlText = $xml->createTextNode($domain.'/termekek/'.$row['nev_url'].'/'.$row['nev_url']);
		$product_url->appendChild($product_urlText);
		
		// Kép
		$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
		$res = $pdo->prepare($query_kep);
		$res->execute();
		$row_kep = $res -> fetch();
		$alap_kep = $row_kep['kep'];
		if ($alap_kep != '')
		{
			$image_url = $xml->createElement("urlpicture");
			$image_urlText = $xml->createTextNode($domain.'/images/termekek/'.$row_kep['kep']);
			$image_url->appendChild($image_urlText);
		}

		$product = $xml->createElement("product");
		$product->appendChild($id);
		$product->appendChild($itemid);
		$product->appendChild($category);
		$product->appendChild($name);
		// $product->appendChild($manufacturer);
		// $product->appendChild($delivery_time);
		// $product->appendChild($stock);
		
		$product->appendChild($price);
		$product->appendChild($product_url);
		if ($alap_kep != '')
		{
			$product->appendChild($image_url);
		}

		$root->appendChild($product);
	}


	$xml->save($gyoker."/adm/xml_olcsobbat.xml");
	
// ÁRGÉP

	$xml = new DOMDocument('1.0', 'UTF-8');
	
	$root = $xml->createElement("termeklista");
	$xml->appendChild($root);

	foreach ($pdo->query($query) as $row)
	{
			$nev = $xml->createElement("nev");
			$nevText = $xml->createCDATASection($row['nev']);
			$nev->appendChild($nevText);

			$leiras = $xml->createElement("leiras");
			$leirasText = $xml->createCDATASection($row['leiras']);
			$leiras->appendChild($leirasText);

			// $ido = $xml->createElement("ido");
			// $idoText = $xml->createCDATASection($row['aruk_szall_ido']);
			// $ido->appendChild($idoText);

			$datum = date("Y-m-d");
			if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) // Ha akciós
			{
				$ws_ar = $row['akciosar'];
			}
			else
			{
				$ws_ar = $row['ar'];
			}
			$ar = $xml->createElement("ar");
			$arText = $xml->createCDATASection($ws_ar);
			$ar->appendChild($arText);

			$termeklink = $xml->createElement("termeklink");
			$termeklinkText = $xml->createCDATASection($domain.'/termekek/'.$row['nev_url'].'/'.$row['nev_url']);
			$termeklink->appendChild($termeklinkText);
			
			// Kép
			$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
			$res = $pdo->prepare($query_kep);
			$res->execute();
			$row_kep = $res -> fetch();
			$alap_kep = $row_kep['kep'];
			if ($alap_kep != '')
			{
				$fotolink = $xml->createElement("fotolink");
				$fotolinkText = $xml->createCDATASection($domain.'/images/termekek/'.$row_kep['kep']);
				$fotolink->appendChild($fotolinkText);
			}

			$product = $xml->createElement("termek");
			$product->appendChild($nev);
			$product->appendChild($ar);
			$product->appendChild($termeklink);
			$product->appendChild($leiras);
			// $product->appendChild($ido);
			if ($alap_kep != '')
			{
				$product->appendChild($fotolink);
			}

		$root->appendChild($product);
	}

	$xml->save($gyoker."/adm/xml_argep.xml");

?>