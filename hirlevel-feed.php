<?php
	include 'config.php';

	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	$query = $pdo->prepare("SELECT t.cikkszam, t.nev, CONCAT('".$domain."/termekek/',tcs.nev_url,'/',t.nev_url) as termek_url, IF( t.akciosar > 0 AND t.akcio_ig >= CURDATE(), t.akciosar, 0) AS akcios_ar, t.ar, ( SELECT CONCAT('".$domain."/images/termekek/',thumb) FROM `metro_termek_kepek` WHERE spec = 0 AND termek_id = t.id ORDER BY alap DESC LIMIT 1 ) AS elsodleges_kep FROM `metro_termekek` t INNER JOIN `metro_term_csoportok` tcs ON tcs.id = t.csop_id WHERE t.lathato = 1");
	$query->execute();
	$row_feed = $query -> fetchAll(PDO::FETCH_ASSOC);

	header('Content-Type: application/json; charset=utf-8');
	echo json_encode($row_feed, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES );