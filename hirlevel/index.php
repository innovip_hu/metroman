<?php 
  session_start();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
      <?php 
        $oldal = 'hirlevel';
        include '../config.php';
        include $gyoker.'/module/mod_head.php';
      ?>

      <title>Hírlevél feliratkozás | <?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">      
  </head>
  <body>
    <?php include $gyoker.'/module/mod_body_start.php'; ?>
    <div class="page">
      <?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(../images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                <h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5">Hírlevél feliratkozás</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">
            <ul class="breadcrumbs-custom-path">
              <li><a href="<?=$domain?>">Főoldal</a></li>
              <li class="active">Hírlevél feliratkozás</li>
            </ul>  
          </div>
        </div>
      </section>

      <section class="section section-lg bg-default">
        <div class="container">      	
          <div class="row row-30 justify-content-center">
            <div class="col-md-8">
								
				<?php
					if (isset($_GET['kod']))
					{
						$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirlevel WHERE kod=".$_GET['kod']);
						$res->execute();
						$rownum = $res->fetchColumn();
						if ($rownum > 0)
						{
							$query = 'SELECT * FROM '.$webjel.'hirlevel WHERE kod='.$_GET['kod'];
							$res = $pdo->prepare($query);
							$res->execute();
							$row = $res -> fetch();
							if ($row['datum'] == '0000-00-00')
							{
								$datum = date("Y-m-d");
								$updatecommand = 'UPDATE '.$webjel.'hirlevel SET datum = "'.$datum.'" WHERE kod='.$_GET['kod'];
								$result = $pdo->prepare($updatecommand);
								$result->execute();

								function sendPostToWebgalamb($url, $data) {
									$ch = curl_init();
									
									$data['sub'] = 'Feliratkozás';

									curl_setopt($ch, CURLOPT_URL, $url);
									curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
									curl_setopt($ch, CURLOPT_POST, 1);
									curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
									curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // Nem ellenorzi az SSL tanúsítvány érvényességét

									$server_output = curl_exec($ch);
									curl_close($ch);
									
									return $server_output;
								}

								$url = 'http://wg.metroman.hu/subscriber.php?g=1&f=kf892b'; // a linket módosítani kell az adott csoport saját linkjére
								$data = array(
									'subscr'=>$row['email'], // fix adat, az e-mail cím helye
									'f_5'=>$row['nev']
								);

								sendPostToWebgalamb($url, $data);
								
								
								print '<p>Regisztrációját aktiváltuk.</p>';
								print '<p>Köszönjük érdeklődését!</p>';
							}
							else
							{
								print '<p>A regisztráció már korábban megtörtént!</p>';
							}
						}
						else 
						{
							print '<p>A hitelesítő kód nen érvényes!</p>';
						}
					}
					
					elseif (isset($_POST['command']) && $_POST['command'] == 'hirl_felir')
					{
					  	if (isset($_POST['g-recaptcha-response']))
					  	{
						    $secret = '6LfB6skZAAAAAAoFle-wih5rYC4l7AR9PcYtloCx';
						    $url = 'https://www.google.com/recaptcha/api/siteverify';

						    $post = [
						        'secret' => $secret,
						        'response' => $_POST['g-recaptcha-response']
						    ];

						    $ch = curl_init($url);
						    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

						    // execute!
						    $response = curl_exec($ch);

						    // close the connection, release resources used
						    curl_close($ch);

						    // do anything you want with your response
						    $response = json_decode($response, TRUE);
					  	}		
						  				
						if (isset($response['success']) && $response['success'] == 'true')
						{
							$kod = time().rand(1, 99999);
							
						// Levélküldés
							$uzenet = '<p>Köszönjük, hogy feliratkozott hírlevelünkre!</p>
										<p>Legyen kedves visszaigazolni ezen a <a href="'.$domain.'/hirlevel/?kod='.$kod.'" target="_blank">LINKEN</a> regisztrációját, hogy értesítést kaphasson legfrissebb híreinkről. </p>';
							include $gyoker.'/module/mod_email.php';
							//phpMailer
							require_once($gyoker.'/webshop/PHPMailer-master/PHPMailerAutoload.php');
							$mail = new PHPMailer();
							$mail->isHTML(true);
							$mail->CharSet = 'UTF-8';
							// $mail->addCustomHeader('MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n");
							if($conf_smtp == 1)
							{
								// SMTP
								$mail->IsSMTP(); // telling the class to use SMTP
								$mail->Host       = $smtp_host; // SMTP server
								$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
																		   // 1 = errors and messages
																		   // 2 = messages only
								$mail->SMTPAuth   = true;                  // enable SMTP authentication
								$mail->SMTPSecure = $smtp_protokol;
								$mail->Host       = $smtp_host; // sets the SMTP server
								$mail->Port       = $smtp_port;                    // set the SMTP port for the GMAIL server
								$mail->Username   = $smtp_user; // SMTP account username
								$mail->Password   = $smtp_pass;        // SMTP account password
								$mail->SetFrom($smtp_email, $smtp_name);
								$mail->AddReplyTo($smtp_email, $smtp_name);
							}
							else
							{
								$mail->SetFrom($email, $webnev);
							}
							$mail->AddAddress($_POST['email'], $_POST['nev']);
							$mail->Subject = "Hírlevél feliratkozás";
							$htmlmsg = $mess;
							$mail->Body = $htmlmsg;
							if(!$mail->Send()) {
							  echo "Mailer Error: " . $mail->ErrorInfo;
							}
							
							if(isset($_POST['webshop']) && $_POST['webshop'] == 'webshop')
							{
								$webshop = 1;
							}
							else
							{
								$webshop = 0;
							}
							if(isset($_POST['kozbeszerzes']) && $_POST['kozbeszerzes'] == 'kozbeszerzes')
							{
								$kozbeszerzes = 1;
							}
							else
							{
								$kozbeszerzes = 0;
							}

							
							$insertcommand = "INSERT INTO ".$webjel."hirlevel (nev,email,kod,webshop,kozbeszerzes) VALUES ('".$_POST['nev']."','".$_POST['email']."','".$kod."','".$webshop."','".$kozbeszerzes."')";
							$result = $pdo->prepare($insertcommand);
							$result->execute();
							
							print '<h2>Érdeklődését köszönjük!</h2><p>Hamarosan e-mail-ben küldünk egy megerősítő levelet, melyet legyen kedves visszaigazolni, hogy regisztrációja megtörténjen. <br>Ha véletlen nem kap levelet fél órán belül, akkor kérjük ellenőrizze spam (levélszemét) fiókját.</p>';
						}
					}
					else
					{
						header('Location: '.$domain);
					}
				?>
            </div>
          </div>
        </div>
      </section>      

      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>
  </body>
</html>