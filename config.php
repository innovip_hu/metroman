<?php
	$dbhost = "localhost";
	$dbuser = "root";
	$dbpass = "";
	$dbname = "metroman";
	// $dbpass = "root";
	// $dbname = "demo_webaruhaz";
// WebShop
	//$gyoker = "c:/wamp64/www/metroman";
	$domain = "//localhost/metroman";
	$gyoker = "/wamp64/www/metroman";
	 // $domain = "http://localhost/dem-web-ruh-z";
	$webjel = "metro_";
	$email = "studio@innovip.hu";
	// $email = "exishop@exishop.hu";
	$webnev = 'Metroman.hu';
	$bankszamla = " 123456 123456 123456";
// E-mail a tulajnak a vásárlásról
	$config_email_vasar_tulaj = "N"; // I vagy N
// Készlet kezelés
	$config_keszlet_kezeles = "I"; // I vagy N
// Összehasonlító modul
	$config_osszehasonlito = "N"; // I vagy N
	
// E-commerce trackoing
	$config_ecommerce = 0; // 0 = nem, 1 = igen
	
// KUPON
	$config_kupon = 1; // 0 = nem, 1 = igen
	
// régi OVIP - Majd ki kellene szedni!!!!!
	$config_ovip = "N"; // I vagy N
	
// OVIP-pa összekpcsolva
	//$ovip_authCode = "136_bodeXokn7iKjljjP";  // üres vagy kód legyen
	$ovip_authCode = "";  // üres vagy kód legyen
	$ovip_ugyfel_id = 1;
	$ovip_webshop_id = 17;
	$ovip_soap_link = 'http://localhost/ovip-google/src/webshopAPI/';
	$ovip_ip_cim = '79.172.239.199';
	
	$title = "Metroman.hu - Autós műszaki és elektronikai webáruház";
	$description = "Válogass otthonról sok ezer termékünk közt, rendeld meg és akár holnapra megkaphatod! Autósfelszerelés elektronika biztonságtechnikai termékek kedvező áron!";
	
	$og_type = "website";
	
	// Termék paraméter beállítások
	if (!defined('TERMEK_PARAMETER_NEV_GYARTO')) define('TERMEK_PARAMETER_NEV_GYARTO', 'Gyártó');
	if (!defined('TERMEK_PARAMETER_TIPUS_EGYEDI')) define('TERMEK_PARAMETER_TIPUS_EGYEDI', 'egyedi');
	if (!defined('TERMEK_PARAMETER_TIPUS_IGEN_NEM')) define('TERMEK_PARAMETER_TIPUS_IGEN_NEM', 'igen_nem');
	if (!defined('TERMEK_PARAMETER_TIPUS_LENYILO_EGY_VALASZTHATO')) define('TERMEK_PARAMETER_TIPUS_LENYILO_EGY_VALASZTHATO', 'lenyilo-egy_valaszthato');
	if (!defined('TERMEK_PARAMETER_TIPUS_LENYILO_TOBB_VALASZTHATO')) define('TERMEK_PARAMETER_TIPUS_LENYILO_TOBB_VALASZTHATO', 'lenyilo-tobb_valaszthato'); // Csak itt lehet felár
	// Termék csoport - Termék paraméter beállítások
	if (!defined('TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_NAGYOBB')) define('TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_NAGYOBB', 'nagyobb');
	if (!defined('TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KISEBB')) define('TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KISEBB', 'kisebb');
	if (!defined('TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KOZOTT')) define('TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KOZOTT', 'kozott');
	
	// host-onkénti konfigurációs lehetőség
	if (is_dir($configDir = __DIR__.DIRECTORY_SEPARATOR.'hosts'.DIRECTORY_SEPARATOR.(strpos($_SERVER['HTTP_HOST'], 'www.') === 0 ? substr($_SERVER['HTTP_HOST'], 4) : $_SERVER['HTTP_HOST']))
		AND
		is_file($configFile = $configDir.DIRECTORY_SEPARATOR.'config.php')
	) {
		include $configFile;
	}

// SMTP	
	$conf_smtp = 1; // 0-nem, 1-igen
	$smtp_host = "sandbox.smtp.mailtrap.io";
	$smtp_port = 2525;
	$smtp_user = "883bb74fcefea8";
	$smtp_protokol = "tls";
	$smtp_pass = "5015fff0ea608d";
	$smtp_email = 'info@metroman.hu';
	$smtp_name = $webnev;

// Innomedio szinkron
	$conf_inno_szink = 0; // 0-nem, 1-igen
	$conf_inno_kapcs_felhasznalo = 'hrs';
	$conf_inno_kapcs_kulcs = 'QiwWMERn0tydu3Yk1cm1cF4ca7LEAzbJ';
	$conf_inno_store_id = array(17); // Raktár azonosító
	$conf_inno_priceCategoryId = 2; // Árlista azonosító

//Facebook belépés
	$conf_facebook_belepes = 1; // 0-nem. 1-igen
	$conf_facebook_apikey = "187890505668619";
	$conf_facebook_secret = "5631c655c6af6bc460c7bc11f3061b70";

// Sitemap-hez aloldalak
	$conf_aloldalak=array("akciok","hirek","galeria","video-galeria");