<?php

	ini_set('max_execution_time', 0);


	include 'config.php';

	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	$sikeres_darab = 0;
	$osszesen = 0;

	require_once($gyoker.'/adm/simpleImage_class3.php');

	$query = "SELECT * FROM ".$webjel."termekek";
	foreach ($pdo->query($query) as $key => $value) {

		$query = "SELECT * FROM ".$webjel."termek_kepek_import WHERE sku=? AND helyere_kerult = 0";
		$res = $pdo->prepare($query);
		$res->execute(array(trim($value['cikkszam'])));
		$row = $res -> fetchAll();		

		if (!empty($row))
		{
			foreach ($row as $value2)
			{
				$osszesen++;

				$eleres = explode('/', $value2['eredeti']);
				
				//var_dump($eleres);

				$fajlnev = trim($value2['fajl']);

				$link = $gyoker.'/static/images/product_large/'.$eleres[6].'/'.$eleres[7].'/'.$fajlnev;
				$link2 = $gyoker.'/static/images/product_list/'.$eleres[6].'/'.$eleres[7].'/'.$fajlnev;

			    if (file_exists($link2) || file_exists($link))
			    {
			    	$sikeres_darab++;

			    	//nagy kép keresés

			    	if (!file_exists($link))
			    	{
			    		//csak kiskép van - ez lesz a nagy kép is.
			    		$link = $link2;
			    	}

			    	$ujlink = $gyoker.'/images/termekek/'.$fajlnev;

			    	//kis kép keresés
			    	if (file_exists($link2))
			    	{
			    		//van kiskép, mehet thumnail-nek
			    		$ext = pathinfo($link2);
			    		$fajlnev2 = $ext['filename'].'-thumb.'.$ext['extension'];
			    		$ujlink2 = $gyoker.'/images/termekek/'.$fajlnev2;
			    		$file_regi2 = file_get_contents($link2);
			    		file_put_contents($ujlink2, $file_regi2);
			    	}
			    	else
			    	{
			    		//nincs kiskép, mehet a nagykép kisképnek

			    		$ext = pathinfo($link);
			    		$fajlnev2 = $ext['filename'].'-thumb.'.$ext['extension'];

			    		$image = new SimpleImage();		

			    		$image->load($link);
			    		$image->resizeToWidth(340);
			    		$image->save($gyoker.'/images/termekek/'.$fajlnev2);

			    	}


			    	$file_regi = file_get_contents($link);
			    	$ujlink = $gyoker.'/images/termekek/'.$fajlnev;
			    	file_put_contents($ujlink, $file_regi);

					$insertcommand = "INSERT INTO ".$webjel."termek_kepek (termek_id,kep,thumb,alap) VALUES (:termek_id,:kep,:thumb,:alap)";
					$result = $pdo->prepare($insertcommand);
					$result->execute(array(':termek_id'=>$value['id'],
									  ':kep'=>$fajlnev,
									  ':thumb'=>$fajlnev2,    	
									  ':alap'=>$value2['alap']));	

					$updatecommand = "UPDATE ".$webjel."termek_kepek_import SET helyere_kerult=1 WHERE id=?";
					$result = $pdo->prepare($updatecommand);
					$result->execute(array($value2['id']));			

			    }				

				/*

			    if (file_exists($link) && file_exists($link2))
			    {
					$ext = pathinfo($link2, PATHINFO_EXTENSION);
					$fajlnev2 = $ext['filename'].'-thumb.'.$ext;

					$ujlink = $gyoker.'/images/termekek/'.$fajlnev;
					$ujlink2 = $gyoker.'/images/termekek/'.$fajlnev2;

			    	$sikeres_darab++;

			    	$file_regi = file_get_contents($link);
			    	$file_regi2 = file_get_contents($link2);
			    	file_put_contents($ujlink, $file_regi);	
			    	file_put_contents($ujlink2, $file_regi2);	

					$insertcommand = "INSERT INTO ".$webjel."termek_kepek (termek_id,kep,thumb,alap) VALUES (:termek_id,:kep,:thumb,:alap)";
					$result = $pdo->prepare($insertcommand);
					$result->execute(array(':termek_id'=>$value['id'],
									  ':kep'=>$fajlnev,
									  ':thumb'=>$fajlnev2,    	
									  ':alap'=>$value2['alap']));	

					$updatecommand = "UPDATE ".$webjel."termek_kepek_import SET helyere_kerult=1 WHERE id=?";
					$result = $pdo->prepare($updatecommand);
					$result->execute(array($value2['id']));				    			    	

			    }	

			    */		

				/*

				$ujlink = $gyoker.'/images/termekek/'.$fajlnev;

			    if (file_exists($link))
			    {
				    $ext = pathinfo($link, PATHINFO_EXTENSION);

				    $ext = strtolower($ext);

			    	



					$insertcommand = "INSERT INTO ".$webjel."termek_kepek (kep,termek_id,spec,extension) VALUES (:kep,:termek_id,:spec,:extension)";
					$result = $pdo->prepare($insertcommand);
					$result->execute(array(':kep'=>$fajlnev,
									  ':termek_id'=>$value['id'],
									  ':spec'=>1,    	
									  ':extension'=>$ext));	

					$updatecommand = "UPDATE ".$webjel."pdf SET helyere_kerult=1 WHERE id=?";
					$result = $pdo->prepare($updatecommand);
					$result->execute(array($value2['id']));									  	    	

			    }

			    */				
			}

		}
	}


	echo 'Eredmény: '.$sikeres_darab.'/'.$osszesen;
/*

		$prefix = 'https://www.metroman.hu/static/images/product_list/';
		$str = $value['eredeti'];

		if (substr($str, 0, strlen($prefix)) == $prefix) {	

		    $str = substr($str, strlen($prefix));
		    $ujszoveg = "";
		    $str = $ujszoveg.$str;

		    $link = $metrogyoker.'/product_large/'.$str;

		    if (file_exists($link))
		    {
			    $ext = pathinfo($link, PATHINFO_EXTENSION);

			    $ext = strtolower($ext);

			    $file_regi = file_get_contents($link);

				if ($value['alap'] == 1)
				{
					$i = 0;
					$fn = $value['sku'].'.'.$ext;
					$encoded_filename = iconv("UTF-8","Windows-1252//IGNORE",$fn);
				}
				else
				{
					$i++;
					$fn = $value['sku'].'altpic'.$i.'.'.$ext;
					$encoded_filename = iconv("UTF-8","Windows-1252//IGNORE",$fn);
				}

				$ujlink = $metrogyoker_athelyez.$encoded_filename;

				file_put_contents($ujlink, $file_regi);
		    }


		    //echo $str.'<br>';
*/