<?php 
  session_start();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
      <?php 
        $oldal = 'belepes';
        include '../config.php';
        include $gyoker.'/module/mod_head.php';

        if (isset($_SESSION['kosar_id']) && isset($_SESSION['login_id']))
        {
            $pdo->exec("UPDATE ".$webjel."kosar SET user_id=".$_SESSION['login_id']." WHERE kosar_id=".$_SESSION['kosar_id']);
            header('Location: '.$domain.'/kassza/');
        }
        else if (isset($_SESSION['login_id']))
        {
            header('Location: '.$domain.'/');
        }


      ?>

      <title>Belépés | <?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">      
  </head>
  <body>
    <?php include $gyoker.'/module/mod_body_start.php'; ?>
    <div class="page">
      <?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(../images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                <h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5 fejresz-title">Belépés</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">
            <ul class="breadcrumbs-custom-path">
              <li><a href="<?=$domain?>">Főoldal</a></li>
              <li class="active">Belépés</li>
            </ul> 
          </div>
        </div>
      </section>

      <section class="section section-lg bg-default">
        <div class="container">         
          <div class="row row-30 justify-content-center">
            <div class="col-md-6 col-xl-5">
              <?php
                if(isset($uzenet))
                { ?>

                    <div class="alert alert-danger alert-dismissible fade show bg-secondary" role="alert">
                      <strong><?=$uzenet?></strong>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>

                  <?php
                }
              ?>       
              <!--RD Mailform-->
              <form class="rd-form" method="POST">
                <div class="form-wrap">
                  <input class="form-input" id="login-email" type="email" name="login-email">
                  <label class="form-label" for="login-email">E-mail</label>
                </div>
                <div class="form-wrap">
                  <input class="form-input" id="login-password" type="password" name="login-password">
                  <label class="form-label" for="login-password">Jelszó</label>
                </div>
                <p class="mb-3 text-center">Elfelejtetted jelszavad? <a href="<?php print $domain; ?>/jelszo-emlekezteto/">Kattints egy újért</a></p>
                <p class="mb-3 text-center">Amennyiben még nem regisztráltál a lenti gombbal megteheted.</p>
                <div class="form-button group-xs group-justify">
                  <button class="button button-gray-5" type="submit">Belépés</button>
                  <a class="button button-default-outline-2" href="<?=$domain?>/regisztracio">Regisztráció</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>      

      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>
  </body>
</html>