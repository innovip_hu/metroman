<?php 
  session_start();
  ob_start();
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="hu">
  <head>
      <?php 
        $oldal = 'szallitas';
        include '../config.php';
        include $gyoker.'/module/mod_head.php';

      ?>
      <link rel="stylesheet" href="<?php print $domain; ?>/css/szall-info.css?v=1">

      <title>Szállítási feltételek | <?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">      
  </head>
  <body>
    <?php include $gyoker.'/module/mod_body_start.php'; ?>
    <div class="page">
      <?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="breadcrumbs-custom overlay-creative">
        <div class="breadcrumbs-main" style="background-image: url(../images/breadcrumbs-bg.png);">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 text-center">
                <h2 class="breadcrumbs-custom-title scroll-title pb-0 pb-lg-5 fejresz-title">Szállítási feltételek</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="breadcrumbs-panel">
          <div class="container">
            <ul class="breadcrumbs-custom-path">
              <li><a href="<?=$domain?>">Főoldal</a></li>
              <li class="active">Szállítási feltételek</li>
            </ul>
          </div>
        </div>
      </section>

      <section class="section section-lg bg-default">
        <div class="container">
          <div class="row row-30 justify-content-center">
            <?php 
              $query = "SELECT * FROM ".$webjel."kassza_szall_mod WHERE lathato=1 ORDER BY sorrend ASC";
            ?>
            
            <?php foreach ($pdo->query($query) as $key => $value): ?>
              <div class="col-md-12">
                  <h5 class="text-center"><?=$value['nev']?></h5>
                  <?php if ($value['ingyenes_szallitas'] > 0): ?>
                    <p class="text-primary text-center text-bold">Ingyenes kiszállítás: <?=number_format($value['ingyenes_szallitas'],0,',',' ')?> Ft felett!</p>
                  <?php endif ?>
                  <p class="text-center"><?=$value['leiras_hosszu']?></p>
                  <div class="table-responsive pt-3">
                      <table class="table table-bordered table-striped table-condensed">
                        <thead>
                          <tr>
                            <th>Összegtől</th>
                            <th class="text-right">Szállítási díj</th>
                            <th class="text-right">Utánvét költség *</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $query2 = "SELECT * FROM ".$webjel."kassza_szall_mod_savok WHERE szall_mod_id=".$value['id']." ORDER BY sav ASC"; ?>
                          <?php foreach ($pdo->query($query2) as $key => $value2): ?>
                            <tr>
                              <td><?=number_format($value2['sav'],0,',',' ')?> Ft-tól</td>
                              <td class="text-right"><?=number_format($value2['ar'],0,',',' ')?> Ft</td>
                              <td class="text-right"><?=number_format(($value2['ar_utanvet'] - $value2['ar']),0,',',' ')?> Ft</td>
                            </tr>
                          <?php endforeach ?>
                        </tbody>
                      </table>
                  </div>
                  <p class="text-center">* Utánvétes fizetési mód esetén ez a kezelési költség kerül felszámításra!</p>
              </div>
            <?php endforeach ?>

          </div>
        </div>
      </section>      

      <?php include $gyoker.'/module/mod_footer.php'; ?>
    </div>
    <?php include $gyoker.'/module/mod_body_end.php'; ?>
  </body>
</html>